package com;

import messages.Message;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Project: dist2-chord
 * Package: PACKAGE_NAME
 * User: c08esn
 * Date: 12/9/14
 * Time: 1:24 PM
 * Interface for the RMI implementation
 */
public interface Communication extends Remote {
     void send(Message message) throws RemoteException;
     int getID() throws RemoteException;

}
