package com;

import messages.Message;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Observable;

/**
 * Project: dist2-chord
 * Package: PACKAGE_NAME
 * User: c08esn
 * Date: 12/9/14
 * Time: 1:26 PM
 * Implementation of RMI
 */
public class ComUnit extends Observable implements Communication, Serializable {
    private int id;

    public ComUnit(int id) {
        this.id = id;
    }

    @Override
    public void send(final Message message) throws RemoteException {
        setChanged();
        notifyObservers(message);
    }

    @Override
    public int getID() throws RemoteException {
        return id;
    }




}
