package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by Eric on 12/20/2014.
 */
public class Utils {
    public static final int RINGSIZE=100;
    public static final int SUCCESSOR=110;
    public static final int PREDECESSOR=200;
    public static final int FINALSUCC=300;
    public static final int FINALPRED=400;
    public static final int STORE=500;
    public static final int RETREIVE=600;
    public static final int FINGERSEARCH = 700;
    public static final int FINGERRETURN = 800;
    public static final int SECOND = 1000;
    public static final int DATASAVEFINDNODE = 2000;
    public static final int DATASAVENODE = 3000;


    public static int LEADERID = 0;
    public static volatile int ID;
    public static volatile int PORT = 13337;


    public static Properties readSettings() {
        Properties p = new Properties();
        try {
            File file = new File("config.properties");
            FileInputStream fileInput;
            fileInput = new FileInputStream(file);
            p = new Properties();
            p.load(fileInput);
            fileInput.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return p;
    }

}
