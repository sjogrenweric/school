package utils;

/**

 * User: c08esn c11ean
 * Date: 9/18/14
 * Time: 1:35 PM
 * Log class, uses log4j
 */




import org.apache.log4j.*;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.sql.Date;
import java.text.SimpleDateFormat;

public class Log {

    private Logger log;
    private SimpleDateFormat ft;
    private boolean debugEnabled = false;
    private boolean errorEnabled = false;
    private boolean infoEnabled = false;
    private boolean enabled = true;

    public Log(Class c) {
        log = Logger.getLogger(c);
        PropertyConfigurator.configure("log4j.properties");
        ft = new SimpleDateFormat("hh:mm:ss");
    }


    public void disable() {
        enabled = false;
    }
    public void info(String message) {
        Date d = new Date(System.currentTimeMillis());
        if(enabled) log.info(ft.format(d) + ": " + message);
    }

    public void debug(String message) {
        Date d = new Date(System.currentTimeMillis());
        log.debug(ft.format(d) + ": " + message);
    }

    public void error(String message) {
        Date d = new Date(System.currentTimeMillis());
        log.error(ft.format(d) + ": " + message);
    }

    public void error(String message, Throwable t) {
        Date d = new Date(System.currentTimeMillis());
        //log.error(ft.format(d) + ": " + message + " ## " + e.getClass().getSimpleName() + " ## " + e.getMessage());
        log.error(ft.format(d) + ": " + message, t);
    }
    public void error(Exception e, Throwable t) {
        Date d = new Date(System.currentTimeMillis());
        //log.error(ft.format(d) + ": " + message + " ## " + e.getClass().getSimpleName() + " ## " + e.getMessage());
        log.error(e, t);
    }

    public void warn(String message) {
        Date d = new Date(System.currentTimeMillis());
        log.warn(ft.format(d) + ": " + message);
    }

    public boolean isDebugEnabled() {
        return debugEnabled;
    }

    public boolean isErrorEnabled() {
        return errorEnabled;
    }

    public String paintItRed(String text) {
        return "\u001b["  // Prefix
                + "0"        // Brightness
                + ";"        // Separator
                + "31"       // Red foreground
                + "m"        // Suffix
                + text       // the text to output
                + "\u001b[m ";
    }

    /**
     * Writes to a file
     * @param message
     * @param file
     */
    public void file(String message , File file){
        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));

            out.println(message);
            out.close();
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }

    public boolean isInfoEnabled() {
        return infoEnabled;
    }
}