package messages;

import com.*;


/**
 * Created by c08esn
 * This message is used when joining a chord ring.
 */
public class JoinMessage implements Message{

    private Communication comUnit;
    private int id;

    public JoinMessage(int id, Communication comUnit){
        this.id = id;
        this.comUnit = comUnit;
    }

    @Override
    public Communication getComUnit() {
        return comUnit;
    }

    @Override
    public int getID() {
        return id;
    }
}
