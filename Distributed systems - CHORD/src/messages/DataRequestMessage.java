package messages;

import com.Communication;

/**
 * Project: assignment4
 * Package: messages
 * User: c08esn c11ean
 * Date: 1/12/15
 * Time: 2:56 PM
 */
public class DataRequestMessage implements Message {


    private Communication comUnit;
    private int messageID;
    private int id;
    private int type;

    public DataRequestMessage(int id, Communication comUnit, int messageID, int type) {
        this.id = id;
        this.comUnit = comUnit;
        this.messageID = messageID;
        this.type = type;
    }

    public int getMessageID() {
        return messageID;
    }

    @Override
    public Communication getComUnit() {
        return comUnit;
    }

    @Override
    public int getID() {
        return id;
    }

    public int getType() {
        return type;
    }

}


