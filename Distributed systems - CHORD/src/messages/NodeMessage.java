package messages;

import com.*;

/**
 * Created with IntelliJ IDEA.
 * User: c08esn
 * Date: 2014-12-25
 * Time: 13:43
 * This class holds information that are interpreted by the nodes to decide their successors and predecessors
 */
public class NodeMessage implements Message {

    private int id;
    private Communication comUnit;
    private int messageType;
    private int findID;


    public NodeMessage(int id, Communication comUnit,int messageType){
        this.id = id;
        this.comUnit = comUnit;
        this.messageType = messageType;
    }

    public NodeMessage(int id, Communication comUnit,int messageType, int findID){
        this.id = id;
        this.comUnit = comUnit;
        this.messageType = messageType;
        this.findID = findID;
    }


    public int getType(){
        return messageType;
    }

    @Override
    public Communication getComUnit() {
        return comUnit;
    }

    @Override
    public int getID() {
        return id;
    }

    public int getFindID() {
        return findID;
    }
}
