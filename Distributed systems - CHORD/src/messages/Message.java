package messages;

import com.Communication;

import java.io.Serializable;

/**
 * Created by Eric on 12/20/2014.
 *
 * Interface for all the messages that sends
 */
public interface Message extends Serializable {

    public Communication getComUnit();
    public int getID();



}


