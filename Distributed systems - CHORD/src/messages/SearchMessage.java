package messages;

import com.*;

/**
 * Created with IntelliJ IDEA.
 * User: c08esn
 * Date: 2014-12-26
 * Time: 16:38
 * This class holds information about a message someone is interested in.
 */
public class SearchMessage implements Message{


    private int id;
    private final String search;
    private final Communication communication;
    private int searchHash;

    public SearchMessage(int id, String search,Communication communication){
        this.id = id;
        this.search = search;
        this.communication = communication;
        searchHash = search.hashCode();

    }


    public int getSearchHash() {
        return searchHash;
    }

    @Override
    public Communication getComUnit() {
        return communication;
    }

    @Override
    public int getID() {
        return id;
    }
}
