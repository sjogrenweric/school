package messages;

import com.Communication;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: c08esn
 * Date: 2014-12-26
 * Time: 16:09
 * This class holds the information about a single message that is stored in the chord.
 */
public class DataMessage implements Message {


    private String sender;
    private String recipient;
    private String topic;
    private String content;
    private int messageID;
    private long timestamp;
    private ArrayList<Integer> keylist;
    private Communication com;
    private int id;



    private int type;

    public DataMessage() {
        sender = "Default";
        recipient = "Default";
        topic = "Default";
        content = "Default";
        timestamp = System.currentTimeMillis();
        messageID = 0;
    }


    public DataMessage(int messageID, String sender, String recipient, String topic, String content,Communication com, int id,int type) {
        //System.out.println("Message created with id; "+messageID);
        this.com = com;
        this.id = id;
        this.type = type;
        this.sender = (sender.length() > 1 && sender.length() < 255) ? sender : "Default";
        this.recipient = (recipient.length() > 1 && recipient.length() < 255) ? recipient : "Default";
        this.topic = (topic.length() > 1 && topic.length() < 255) ? topic : "Default";
        this.content = (content.length() > 1 && content.length() < 65536) ? content : "Default";
        this.messageID = messageID;
        this.timestamp = System.currentTimeMillis();

        addKeys();

    }
    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    private void addKeys() {
        keylist= new ArrayList<Integer>();
        keylist.add(sender.hashCode());
        keylist.add((recipient.hashCode()));
        keylist.add((topic.hashCode()));
        keylist.add((content.hashCode()));
        keylist.add(messageID);
        keylist.add((int) (timestamp+"".hashCode()));
       // System.out.println(keylist.toString());
    }

    public void setMessageID(int messageID) {
        this.messageID = messageID;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }


    public void setTopic(String topic) {
        this.topic = topic;
    }


    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSender() {
        return sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getTopic() {
        return topic;
    }

    public String getContent() {
        return content;
    }

    public int getMessageID() {
        return messageID;
    }

    public long getTimestamp() {
        return timestamp;
    }


    public boolean contains(String someIdentifier) {
        if (someIdentifier.equals(sender) || someIdentifier.equals(recipient)
                || someIdentifier.equals(topic) || content.contains(someIdentifier) || someIdentifier.equals(messageID)) {
            return true;
        }
        return false;
    }

    public boolean matchKeyHash(int hashkey){

        for(int entry : keylist){
           System.out.println(hashkey + "   mess: " + entry);
            if( hashkey == entry){
               return true;
           }
        }
        return false;
    }


    public String toString() {

        return "Sender: "+ sender + " Recipient: " + recipient + " Topic: " + topic + " Message ID: " + messageID + " Content: " + content;
    }


    @Override
    public Communication getComUnit() {
       return com;
    }

    @Override
    public int getID() {
        return id;
    }
}
