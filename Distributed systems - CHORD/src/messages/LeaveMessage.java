package messages;

import com.Communication;

/**
 * Created by c08esn
 * Message that is used when leaving the chord ring.
 */
public class LeaveMessage implements Message {


    private final Communication comUnit;
    private final int id;

    public LeaveMessage(int id, Communication comUnit){
        this.id = id;

        this.comUnit = comUnit;
    }

    @Override
    public Communication getComUnit() {
        return comUnit;
    }

    @Override
    public int getID() {
        return id;
    }
}
