package node;

import com.ComUnit;
import com.Communication;
import messages.*;
import utils.Log;
import utils.Utils;

import javax.rmi.CORBA.Util;
import javax.xml.crypto.Data;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Johan
 * Date: 2014-12-25
 * Time: 13:21
 * Class that intercepts all the messages and acts opon them
 */
public class NodeEngine implements Observer {

    private Communication successor;

    private Communication predecessor;

    private Log log;

    private Communication nodeComUnit;
    private ArrayList<DataMessage> data = new ArrayList<DataMessage>();
    private ArrayList<Communication> fingers = new ArrayList<Communication>();
    private HashMap<Integer, DataMessage> unsentMessages = new HashMap<>();


    public NodeEngine(Communication nodeComUnit, boolean schedule) {

        log = new Log(this.getClass());
        if (schedule) {
            int timer = 10 * Utils.SECOND;
            log.info("Starting node with automatic finger updates every " + timer / 1000 + " seconds");
            Timer t = new Timer();
            t.schedule(new FingerScheduler(this), timer, timer);
        }

        runClient();

        this.nodeComUnit = nodeComUnit;
        successor = nodeComUnit;
        predecessor = nodeComUnit;

    }

    private void runClient() {


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    input();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    /**
     * User interface for the program
     *
     * @throws IOException
     */
    public void input() throws IOException {
        prompt();

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input;
        String[] args;
        do {
            input = br.readLine();
            args = input.split("\\|");

            log.info("Content of input: " + Arrays.toString(args));

            if (args.length < 2 && args[0].equals("finger") && !input.equals("exit")) {
                updateFingerTable();

            } else if (args.length == 2 && !input.equals("exit") && args[0]
                    .equals("search")) {

                log.info("Searching for: " + args[1]);
                send(successor, new SearchMessage(Utils.ID, args[1], nodeComUnit));
                //TODO USE finger?
                log.info("Sent search message");
            } else if (args[0].equals("put") && args.length == 5) {


                DataMessage mess = new DataMessage(Math.abs(UUID.randomUUID().hashCode() % 100),
                        args[1], args[2],
                        args[3], args[4], nodeComUnit, Utils.ID, Utils.STORE);

                log.info("Message created with id: " + mess.getMessageID());
                unsentMessages.put(mess.getMessageID(), mess);
                sendToClosestIDFinger(new DataRequestMessage(Utils.ID, nodeComUnit, mess.getMessageID(), Utils.DATASAVEFINDNODE));

            } else if (args[0].equals("state") && args.length == 1) {
                promptState();
            }
            prompt();
        } while (!input.equals("exit"));

        exit(br);
    }


    /**
     * Shows input information
     */
    private void prompt() {
        log.info(log.paintItRed("-----------------------------"));
        log.info("Provide an input:");
        log.info("finger - Updates finger table");
        log.info("[put|from|to|topic|message] - Stores a message in the chord ring");
        log.info("[search|the-search] - searches the chord ring for a message");
        log.info("state - displays the state of successor and predecessor");
        log.info("exit - Closes the program");
        log.info(log.paintItRed("-----------------------------"));
    }

    /**
     * Shows the current successors and predecessors
     */
    private void promptState() {
        try {
            if (successor != null && predecessor != null)
                log.info("Current state: successor=" + successor.getID() + " predecessor=" + predecessor.getID());
        } catch (RemoteException e) {
            log.error("Probably did not shut down the program correctly last time");
            System.exit(-1);
        }
    }

    /**
     * Closes the program and notifies surrounding nodes
     *
     * @param br
     * @throws IOException
     */
    private void exit(BufferedReader br) throws IOException {
        send(successor, new NodeMessage(Utils.ID, predecessor, Utils.FINALPRED));
        send(successor, new LeaveMessage(Utils.ID, nodeComUnit));
        send(predecessor, new NodeMessage(Utils.ID, successor, Utils.FINALSUCC));
        log.info("Shutting down program...");
        br.close();
        System.exit(0);
    }

    /**
     * Sends a message to the node with the closest ID in the finger table
     *
     * @param mess
     * @throws RemoteException
     */
    private void sendToClosestIDFinger(DataRequestMessage mess) throws RemoteException {
        int messageID = mess.getMessageID();
        int distance = 100;
        Communication receiver = null;
        for (Communication finger : fingers) {
            if (Math.abs(messageID - finger.getID()) < distance) {
                distance = Math.abs(messageID - finger.getID());
                receiver = finger;
            }
        }
        if (receiver != null) {
            log.info("Sending message " + messageID + " to: " + receiver.getID());
            send(receiver, mess);

        } else {
            log.info("Sending to self");
            send(nodeComUnit, mess);
        }
    }
    /* TODO kolla
    private void sendToClosestFinger(SearchMessage searchMessage) throws RemoteException {
        int messageID = searchMessage.getSearchHash();
        int distance = 100;
        Communication receiver = nodeComUnit;
        for (Communication finger : fingers) {
            if (Math.abs(messageID - (finger.getID() == 0 ? 100 : finger.getID())) < distance) {
                distance = Math.abs(messageID - finger.getID());
                receiver = finger;
            }
        }
        send(receiver, searchMessage);
    }
    */

    /**
     * handle finger request
     *
     * @param m
     * @throws RemoteException
     */
    private void handleFinger(NodeMessage m) throws RemoteException {
        if (m.getFindID() <= Utils.ID || Utils.ID == 0) {
            m.getComUnit().send(new NodeMessage(Utils.ID, nodeComUnit, Utils.FINGERRETURN, m.getFindID()));
        } else {
            send(successor, m);
        }

    }

    /**
     * send out requests for new fingers
     */
    public void updateFingerTable() {
        log.info("Updating my finger table..");
        int successorid = 0;
        fingers = new ArrayList<Communication>();
        fingers.add(nodeComUnit);
        for (int i = 0; i < Utils.RINGSIZE; i++) {
            successorid = (int) ((successorid > 100 ? 0 : Utils.ID) + Math.pow(2, i));
            if (successorid <= 100) {
                send(successor, new NodeMessage(Utils.ID, nodeComUnit, Utils.FINGERSEARCH, successorid));
                log.info("Finding this successor: " + (int) ((successorid > 100 ? 0 : Utils.ID) + Math.pow(2, i)));
            }
        }
    }

    /**
     * Receive information from RMI calls
     *
     * @param o
     * @param o
     * @param arg
     */
    @Override
    public void update(Observable o, Object arg) {
        Message m;
        if (o instanceof ComUnit) {
            try {
                m = (Message) arg;
                Communication comNode = m.getComUnit();
                int messageNodeId = comNode.getID();
                if (m instanceof JoinMessage) {
                    joinMessageHandler(comNode, messageNodeId);
                } else if (m instanceof NodeMessage) {
                    nodeMessageHandler((NodeMessage) m, comNode, messageNodeId);
                } else if (m instanceof SearchMessage) {
                    searchMessageHandler((SearchMessage) m, comNode, messageNodeId);
                } else if (m instanceof DataMessage) {
                    dataMessageHandler((DataMessage) m, comNode, messageNodeId);
                } else if (m instanceof LeaveMessage) {
                    leaveMessageHandler((LeaveMessage) m, comNode, messageNodeId);
                } else if (m instanceof DataRequestMessage) {
                    DataNodeRequestHandler((DataRequestMessage) m, comNode, messageNodeId);
                }

            } catch (RemoteException e) {
                e.printStackTrace();
                log.error("Probably did not shut down the program correctly last time");
                System.exit(-1);
            }
        }


    }

    private void DataNodeRequestHandler(DataRequestMessage m, Communication comNode, int messageNodeId) throws RemoteException {
        if (messageNodeId == Utils.ID) {
            data.add(unsentMessages.get(messageNodeId));
            unsentMessages.remove(messageNodeId);
            log.info("I saved the data");
        } else if (m.getType() == Utils.DATASAVEFINDNODE) {
            log.info("THE type was FIND MODE " + m.getType());
            if ((m.getID() == Utils.ID) || m.getMessageID() < (Utils.ID == 0 ? 100 : Utils.ID)) {
                log.debug("Sending myself to: " + comNode.getID());
                send(comNode, new DataRequestMessage(Utils.ID, nodeComUnit, m.getMessageID(), Utils.DATASAVENODE));

            } else if (m.getMessageID() > Utils.ID) {
                log.info("Successor " + successor.getID() + " is better suited to store this message: " + m.getMessageID());
                send(comNode, new DataRequestMessage(Utils.ID, successor, m.getMessageID(), Utils.DATASAVENODE));
            }
        } else if (m.getType() == Utils.DATASAVENODE) {
            log.debug("I received a node on which to save the data: " + m.getComUnit().getID() + " mess: " + unsentMessages.get(m.getMessageID()) );

           send(m.getComUnit(), unsentMessages.get(m.getMessageID()));
            unsentMessages.remove(m.getMessageID());
        }
    }


    /**
     * Handles leave messages
     *
     * @param m
     * @param comNode
     * @param messageNodeId
     */
    private void leaveMessageHandler(LeaveMessage m, Communication comNode, int messageNodeId) {
        if (fingers.contains(comNode)) {
            fingers.remove(comNode);
        }
        Utils.LEADERID = m.getID();
        if (messageNodeId == Utils.LEADERID) {
            log.info("Root left, good game...");

        }
    }

    /**
     * Handles data messages
     *
     * @param m
     * @param comNode
     * @param messageNodeId
     * @throws RemoteException
     */
    private void dataMessageHandler(DataMessage m, Communication comNode, int
            messageNodeId) throws RemoteException {
        int messageID = m.getMessageID();
        if (m.getType() == Utils.STORE) {
            // if ((m.getID() == Utils.ID) || messageID < Utils.ID) {
            log.info("Saved data: " + m.toString());
            data.add(m);
           /* } else if (m.getMessageID() > Utils.ID) {
                log.info("Successor is better suited to store this message: " + m.getMessageID());
                send(successor, m);
            }*/

        } else {
            log.info("Received a message : " + m.toString());
        }
    }

    /**
     * Handles search queries
     *
     * @param m
     * @param comNode
     * @param messageNodeId
     * @throws RemoteException
     */
    private void searchMessageHandler(SearchMessage m, Communication comNode, int messageNodeId) throws RemoteException {
        boolean haveMessage = false;
        DataMessage foundMess = null;
        int hash = m.getSearchHash();
        for (DataMessage mess : data) {
            if (mess.matchKeyHash(hash)) {
                haveMessage = true;
                foundMess = mess;
            }
        }
        if (haveMessage) {
            log.info("Someome searched for a message and i currently store it: " + foundMess);
            foundMess.setType(Utils.RETREIVE);
            send(comNode, foundMess);

        } else {
            if (messageNodeId == Utils.ID) {
                log.info("The search looped");
                return;
            }
            log.info("I currently do not hold the message: ");
            send(successor, m);
        }
    }


    /**
     * Handles new node information and requests
     *
     * @param m
     * @param comNode
     * @param messageNodeId
     * @throws RemoteException
     */
    private void nodeMessageHandler(NodeMessage m, Communication comNode, int messageNodeId) throws RemoteException {
        switch (m.getType()) {
            case Utils.SUCCESSOR:
                send(comNode, new JoinMessage(Utils.ID, nodeComUnit));
                break;
            case Utils.PREDECESSOR:
                predecessor = comNode;
                break;
            case Utils.FINALSUCC:
                successor = comNode;
                if (predecessor.getID() == nodeComUnit.getID()) {
                    predecessor = comNode;
                }
                break;
            case Utils.FINALPRED:

                predecessor = comNode;
                break;
            case Utils.FINGERSEARCH:
                handleFinger(m);
                break;
            case Utils.FINGERRETURN:
                Communication c = m.getComUnit();
                fingers.add(c);
                log.info("Added new Finger for id : " + m.getFindID() + " Real id : " + m.getID());

        }

    }


    /**
     * Handles the join process
     *
     * @param comNode
     * @param messageNodeId
     * @throws RemoteException
     */
    private void joinMessageHandler(Communication comNode, int messageNodeId) throws RemoteException {
        int successorID = successor.getID();
        int predecessorID = predecessor.getID();


        if (messageNodeId > Utils.ID && successorID != Utils.ID) {
            if (successorID == Utils.LEADERID) {
                log.info("Next was root node, sending root node as successor");

                send(comNode, new NodeMessage(Utils.ID, nodeComUnit, Utils.FINALPRED));
                send(comNode, new NodeMessage(Utils.ID, successor, Utils.FINALSUCC));
                successor = comNode;

            } else {
                log.info("My id: " + Utils.ID + " theirs: " + messageNodeId + "  sending my successor");
                send(comNode, new NodeMessage(Utils.ID, successor, Utils.SUCCESSOR));
            }
        } else {
            log.info("My id: " + Utils.ID + " theirs: " + messageNodeId + "  sending myself as successor");
            if (successorID == Utils.ID) {
                successor = comNode;
            }
            send(comNode, new NodeMessage(Utils.ID, predecessor, Utils.FINALPRED));
            send(comNode, new NodeMessage(Utils.ID, nodeComUnit, Utils.FINALSUCC));
            if (predecessorID != Utils.ID && predecessorID != messageNodeId) {
                send(predecessor, new NodeMessage(Utils.ID, comNode, Utils.FINALSUCC)); //old pred knows about new succ
            }
            predecessor = comNode;
        }

    }

    /**
     * Sends a message to a node via RMI
     *
     * @param com
     * @param m
     */
    private void send(Communication com, Message m) {
        try {
            com.send(m);
        } catch (RemoteException e) {
            System.err.println("Could not contact a member");
        }
    }


}
