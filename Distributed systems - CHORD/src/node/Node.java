package node;

import com.ComUnit;
import com.Communication;
import utils.Utils;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Project: dist2-chord
 * Package: PACKAGE_NAME
 * User: c08esn c11ean
 * Date: 12/9/14
 * Time: 1:24 PM
 */
public class Node {

    private static NodeEngine nodeEngine;

    public static void main(String args[]) {
    boolean schedule = false;

        if(args.length==1)
            schedule = args[0].equals("true");

        setupReg(schedule);

    }

    /**
     * Sets up the registry service to allow users to join.
     *
     */
    private static void setupReg( boolean schedule) {

        Utils.ID= 0;
        ComUnit comUnit = new ComUnit(Utils.ID);

        Communication stub;
        try {
            stub = (Communication) UnicastRemoteObject.exportObject(comUnit,
                    Utils.LEADERID);

            Registry registry = LocateRegistry.createRegistry(Utils.PORT);
            registry.rebind("join", stub);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        nodeEngine = new NodeEngine(comUnit,schedule);
        comUnit.addObserver(nodeEngine);
    }


}
