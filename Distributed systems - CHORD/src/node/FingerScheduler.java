package node;

import java.util.TimerTask;

/**
 * Created by eric on 2015-01-08.
 */
public class FingerScheduler extends TimerTask {

    private NodeEngine nodeEngine ;

    public FingerScheduler(NodeEngine nodeEngine){
        this.nodeEngine = nodeEngine;
    }

    @Override
    public void run() {
        nodeEngine.updateFingerTable();
    }
}
