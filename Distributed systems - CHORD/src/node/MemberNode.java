package node;

import com.ComUnit;
import com.Communication;
import messages.JoinMessage;
import utils.Utils;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Properties;


public class MemberNode {
    private static NodeEngine nodeEngine;

    public static void main(String[] args) {
        boolean schedule = false;
        String leaderIp= "localhost";
        try {
            Utils.ID = generateID();
            System.out.println("My id is: " + Utils.ID);

            ComUnit comUnit = new ComUnit(Utils.ID);

            if(args.length>0 && args.length<3) {
            leaderIp = args[0];
                if (args.length == 2)
                    schedule = args[1].equals("true");
            }

            nodeEngine = new NodeEngine(comUnit,schedule);

            comUnit.addObserver(nodeEngine);

            Registry registry = LocateRegistry.getRegistry(leaderIp,
                   Utils.PORT);

            Communication com = (Communication) registry.lookup("join");
            Communication stub = (Communication) UnicastRemoteObject.exportObject(comUnit, 0);

            com.send(new JoinMessage(Utils.ID, stub));

            Utils.LEADERID = com.getID();

            Registry registryOwn = LocateRegistry.createRegistry(Utils.PORT);
            registryOwn.rebind("join", stub);

        } catch (RemoteException e) {
            System.err.println("Probably did not shut down the program correctly last time");
            System.exit(-1);
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }


    /**
     * Generates a random id based on the time with the interval 0 - 100
     * @return
     * @throws NoSuchAlgorithmException
     */
    private static int generateID() throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
        byte[] result = messageDigest.digest((System.currentTimeMillis() % 10000 + "").getBytes());
        return Math.abs(Arrays.hashCode(result) %
                100);
    }
}
