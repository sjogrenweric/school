


import java.util.Collections;
import java.util.LinkedList;

/**
 * This class is used to represent game positions. It uses a 2-dimensional char
 * array for the board and a Boolean to keep track of which player has the move.
 *
 * @author Henrik Bj&ouml;rklund
 */

public class OthelloPosition {



	public enum directions  {
		UP,
		DOWN,
		LEFT,
		RIGHT,
		DNW,
		DNE,
		DSW,
		DSE
	}



	/** For a normal Othello game, BOARD_SIZE is 8. */
	protected static final int BOARD_SIZE = 8;

	/** True if the first player (white) has the move. */
	protected boolean playerToMove;



	protected OthelloAction action;



	/**
	 * The representation of the board. For convenience, the array actually has
	 * two columns and two rows more that the actual game board. The 'middle' is
	 * used for the board. The first index is for rows, and the second for
	 * columns. This means that for a standard 8x8 game board,
	 * <code>board[1][1]</code> represents the upper left corner,
	 * <code>board[1][8]</code> the upper right corner, <code>board[8][1]</code>
	 * the lower left corner, and <code>board[8][8]</code> the lower left
	 * corner. In the array, the charachters 'E', 'W', and 'B' are used to
	 * represent empty, white, and black board squares, respectively.
	 */
	protected char[][] board;

	/** Creates a new position and sets all squares to empty. */
	public OthelloPosition() {
		board = new char[BOARD_SIZE + 2][BOARD_SIZE + 2];
		for (int i = 0; i < BOARD_SIZE + 2; i++)
			for (int j = 0; j < BOARD_SIZE + 2; j++)
				board[i][j] = 'E';

	}

	public OthelloPosition(String s) {
		if (s.length() != 65) {
			board = new char[BOARD_SIZE + 2][BOARD_SIZE + 2];
			for (int i = 0; i < BOARD_SIZE + 2; i++)
				for (int j = 0; j < BOARD_SIZE + 2; j++)
					board[i][j] = 'E';
		} else {
			board = new char[BOARD_SIZE + 2][BOARD_SIZE + 2];
			if (s.charAt(0) == 'W') {
				playerToMove = true;
			} else {
				playerToMove = false;
			}
			for (int i = 1; i <= 64; i++) {
				char c;
				if (s.charAt(i) == 'E') {
					c = 'E';
				} else if (s.charAt(i) == 'O') {
					c = 'W';
				} else {
					c = 'B';
				}
				int column = ((i - 1) % 8) + 1;
				int row = (i - 1) / 8 + 1;
				board[row][column] = c;
			}
		}

	}

	/**
	 * Initializes the position by placing four markers in the middle of the
	 * board.
	 */
	public void initialize() {
		board[BOARD_SIZE / 2][BOARD_SIZE / 2] = board[BOARD_SIZE / 2 + 1][BOARD_SIZE / 2 + 1] = 'W';
		board[BOARD_SIZE / 2][BOARD_SIZE / 2 + 1] = board[BOARD_SIZE / 2 + 1][BOARD_SIZE / 2] = 'B';
		playerToMove = true;

	}

	/* getMoves and helper functions */

	/**
	 * Returns a linked list of <code>OthelloAction</code> representing all
	 * possible moves in the position. If the list is empty, there are no legal
	 * moves for the player who has the move.
	 */

	public LinkedList<OthelloAction> getMoves() {
		LinkedList<OthelloAction> movablePositionsW = new LinkedList<OthelloAction>();
		LinkedList<OthelloAction> movablePositionsB =  new LinkedList<OthelloAction>() ;


		int moveBricks;

		for (int i = 1; i < BOARD_SIZE+1; i++) {
			for (int j = 1; j < BOARD_SIZE+1; j++) {
				if(board[i][j] != 'E'){
					if(toMove() && board[i][j] == 'B') {
						findFreeSpaces(i, j, movablePositionsW,'W','B');

					}else if(!toMove() && board[i][j] == 'W'){
						findFreeSpaces(i, j,movablePositionsB,'B', 'W');

					}

				}
			}

		}

		return (toMove() ?  movablePositionsW : movablePositionsB);
	}

	private void findFreeSpaces( int i, int j, LinkedList<OthelloAction> movablePositions,char player,char opponent) {
		int moveBricks;
		OthelloAction action ;
		for (int k= i-1 ; k <= i+1 ; k++) {
			for (int l = j - 1 ; l <= j + 1 ; l++) {
				if(k < BOARD_SIZE+1 && k > 0 && l < BOARD_SIZE+1 && l > 0 && (k!=i || l != j) &&board[k][l] == 'E') {
					action = new OthelloAction(k, l);
					moveBricks = isValidMove(action, board, player, opponent);
					if (moveBricks>0) {
						checkIfExistsElseAdd(movablePositions, action);
					}
				}
			}
		}

	}

	/**
	 * Just checks if the position is already in the list
	 * @param movablePositions
	 * @param action
     */
	private void checkIfExistsElseAdd(LinkedList<OthelloAction> movablePositions, OthelloAction action) {
		boolean exists=false;
		for(OthelloAction act : movablePositions){
			if(act.getColumn()==action.getColumn() && act.getRow()==action.getRow()){
				exists=true;
			}
		}
		if(!exists){
			Collections.sort(movablePositions);
			movablePositions.add(action);
		}
	}

	/**
	 * Checks if a possible action is a valid move
	 * @param action
	 * @param board
	 * @param player
	 * @param opponent
     * @return
     */
	private int isValidMove(OthelloAction action, char[][] board, char player, char opponent) {
		int right=0,left=0,down=0,up=0,dnw=0,dne=0,dsw=0,dse=0;

		down=checkVertical(action, board, player, opponent,1,false);
		if(down > 0 )
			action.setDirect(directions.DOWN);

		up=checkVertical(action, board, player, opponent,-1,false);
		if(up > 0 )
			action.setDirect(directions.UP);

		right=checkHorizontal(action, board, player, opponent,1,false);
		if(right > 0 )
			action.setDirect(directions.RIGHT);

		left=checkHorizontal(action, board, player, opponent,-1,false);
		if(left > 0 )
			action.setDirect(directions.LEFT);

		dnw=checkDiag(action, board, player, opponent,-1,-1,false);
		if(dnw > 0 )
			action.setDirect(directions.DNW);

		dne=checkDiag(action, board, player, opponent,-1,1,false);
		if(dne > 0 )
			action.setDirect(directions.DNE);

		dsw=checkDiag(action, board, player, opponent,1,-1,false);
		if(dsw > 0 )
			action.setDirect(directions.DSW);

		dse=checkDiag(action, board, player, opponent,1,1,false);
		if(dse > 0 )
			action.setDirect(directions.DSE);


		return (right + left + down + up + dnw + dne+ dsw + dse);
	}


	/**
	 * Find all the spaces around a peice that are empty
	 * @param i
	 * @param j
     * @return
     */




	/* toMove */

	/** Returns true if the first player (white) has the move, otherwise false. */
	public boolean toMove() {
		return playerToMove;
	}

	public void setPlayerToMove(boolean player){ playerToMove = (player?  true : false);}
	/* makeMove and helper functions */

	/**
	 * Returns the position resulting from making the move <code>action</code>
	 * in the current position. Observe that this also changes the player to
	 * move next.
	 */
	public OthelloPosition makeMove(OthelloAction action)
			throws IllegalMoveException {
		OthelloPosition positionCopy = this.clone();

		this.action=action;

		char player = toMove() ? 'W' : 'B' ;
		char opponent = toMove() ? 'B' : 'W' ;

		for (directions dir: action.getDirects()) {
			switch (dir){
				case UP:
					checkVertical(action,positionCopy.board,player,opponent,-1,true);
					break;
				case DOWN:
					checkVertical(action,positionCopy.board,player,opponent,1,true);
					break;
				case LEFT:
					checkHorizontal(action,positionCopy.board,player,opponent,-1,true);
					break;
				case RIGHT:
					checkHorizontal(action,positionCopy.board,player,opponent,1,true);
					break;
				case DNE:
					checkDiag(action, positionCopy.board, player, opponent,-1,1,true);
					break;
				case DNW:
					checkDiag(action, positionCopy.board, player, opponent,-1,-1,true);
					break;
				case DSE:
					checkDiag(action, positionCopy.board, player, opponent,1,1,true);
					break;
				case DSW:
					checkDiag(action, positionCopy.board, player, opponent,1,-1,true);
					break;

			}
		}

		positionCopy.playerToMove = !toMove();

		return positionCopy;

	}


	/**
	 * IF flip is false then checks if the action has a valid move in a vertical direction
	 * if flip i true, then flip the pieces
	 * @param action
	 * @param board
	 * @param player
	 * @param opponent
	 * @param addrow
     * @param flip
     * @return
     */
	private int checkVertical(OthelloAction action, char[][] board, char player, char opponent, int addrow, boolean flip) {
		boolean valid=false;
		int numOpp=0, i = action.getRow()+addrow;

		if(flip){
			i = action.getRow();
			while(i <= BOARD_SIZE && i >= 0 ){
				board[i][action.getColumn()] = player;
				i+=addrow;
				try {
					if(board[i][action.getColumn()]=='E' || board[i][action.getColumn()]==player) break;
				}catch (IndexOutOfBoundsException e){
					break;
				}

			}

		}else {

			while (i >= 0 && i <= BOARD_SIZE) {

				if (board[i][action.getColumn()] == 'E' || (board[i][action.getColumn()] == player  && numOpp==0) ) {
					break;
				} else if (board[i][action.getColumn()] == opponent) {
					//System.out.println("hitta"+action.getRow()+" "+ action.getColumn() );
					numOpp++;
				} else if (board[i][action.getColumn()] == player && numOpp > 0) {
					//System.out.println(action.getRow()+" "+ action.getColumn() + " "+ numOpp + " succ");
					valid = true;
					break;
				}
				i += addrow;

			}


		}
		return valid ? numOpp : 0;

	}

	/**
	 *  * IF flip is false then checks if the action has a valid move in a horizontal direction
	 * if flip i true, then flip the pieces
	 * @param action
	 * @param board
	 * @param player
	 * @param opponent
	 * @param addcol
     * @param flip
     * @return
     */
	private int checkHorizontal(OthelloAction action, char[][] board, char player, char opponent, int addcol,boolean flip) {
		boolean valid=false;
		int numOpp=0,i = action.getColumn()+addcol;

		if(flip){
			i = action.getColumn();
			while( i <= BOARD_SIZE && i >= 1 ){
				board[action.getRow()][i] = player;
				i+=addcol;
				try {
					if(board[action.getRow()][i]=='E' || board[action.getRow()][i]==player ) break;
				}catch (IndexOutOfBoundsException e){
					break;
				}

			}

		}else {

			while (i >= 1 && i <= BOARD_SIZE) {
				if (board[action.getRow()][i] == 'E' || (board[action.getRow()][i] == player  && numOpp==0)) {
					break;
				} else if (board[action.getRow()][i] == opponent) {
					numOpp++;
				} else if (board[action.getRow()][i] == player && numOpp > 0) {
					valid=true;
					break;
				}
				i += addcol;
			}
		}
		return (valid ? numOpp : 0);
	}


	/**
	 *  * IF flip is false then checks if the action has a valid move in a diagonal direction
	 * if flip i true, then flip the pieces
	 * @param action
	 * @param board
	 * @param player
	 * @param opponent
	 * @param addrow
	 * @param addcol
     * @param flip
     * @return
     */
	private int checkDiag(OthelloAction action, char[][] board, char player, char opponent,int addrow, int addcol, boolean flip) {
		boolean valid=false;
		int numOpp=0;
		int i = action.getRow()+addrow;
		int j = action.getColumn()+addcol;

		if(flip){
			i = action.getRow();
			j = action.getColumn();
			while((i <= BOARD_SIZE && i >= 1) && (j <= BOARD_SIZE && j >= 1) ){
				board[i][j] = player;
				i+=addrow;
				j+=addcol;
				try {
					if (board[i][j] == 'E' ||  board[i][j] == player) break;
				}catch (IndexOutOfBoundsException e){
					break;
				}

			}

		}else {

			while ((i <= BOARD_SIZE && i >= 1) && (j <= BOARD_SIZE && j >= 1)) {
				if (board[i][j] == 'E' || (board[i][j] == player && numOpp==0)) {
					break;
				} else if (board[i][j] == opponent) {
					numOpp++;
				} else if (board[i][j] == player && numOpp > 0) {
					valid = true;
					break;
				}
				i += addrow;
				j += addcol;
			}
		}

		return valid ? numOpp : 0;
	}





	/**
	 * Returns a new <code>OthelloPosition</code>, identical to the current one.
	 */
	protected OthelloPosition clone() {
		OthelloPosition newPosition = new OthelloPosition();
		newPosition.playerToMove = playerToMove;
		for (int i = 0; i < BOARD_SIZE + 2; i++)
			for (int j = 0; j < BOARD_SIZE + 2; j++)
				newPosition.board[i][j] = board[i][j];
		return newPosition;
	}

	/* illustrate and other output functions */

	/**
	 * Draws an ASCII representation of the position. White squares are marked
	 * by '0' while black squares are marked by 'X'.
	 */
	public void illustrate() {
		System.out.print("   ");
		for (int i = 1; i <= BOARD_SIZE; i++)
			System.out.print("| " + i + " ");
		System.out.println("|");
		printHorizontalBorder();
		for (int i = 1; i <= BOARD_SIZE; i++) {
			System.out.print(" " + i + " ");
			for (int j = 1; j <= BOARD_SIZE; j++) {
				if (board[i][j] == 'W') {
					System.out.print("| 0 ");
				} else if (board[i][j] == 'B') {
					System.out.print("| X ");
				} else {
					System.out.print("|   ");
				}
			}
			System.out.println("| " + i + " ");
			printHorizontalBorder();
		}
		System.out.print("   ");
		for (int i = 1; i <= BOARD_SIZE; i++)
			System.out.print("| " + i + " ");
		System.out.println("|\n");
	}

	private void printHorizontalBorder() {
		System.out.print("---");
		for (int i = 1; i <= BOARD_SIZE; i++) {
			System.out.print("|---");
		}
		System.out.println("|---");
	}

	public String toString() {
		String s = "";
		char c, d;
		if (playerToMove) {
			s += "W";
		} else {
			s += "B";
		}
		for (int i = 1; i <= BOARD_SIZE; i++) {
			for (int j = 1; j <= BOARD_SIZE; j++) {
				d = board[i][j];
				if (d == 'W') {
					c = 'O';
				} else if (d == 'B') {
					c = 'X';
				} else {
					c = 'E';
				}
				s += c;
			}
		}
		return s;
	}




}
