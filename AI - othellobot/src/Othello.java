import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by eric on 2015-11-24.
 */
public class Othello {

    private static OthelloAction action;
    private static int i = 1;

    public static void main(String[] args) throws IllegalMoveException {

        String state="WEEEEEEEEEEEEEEEEEEEEEEEEEEEOXEEEEEEXOEEEEEEEEEEEEEEEEEEEEEEEEEEE";
        double timelimit=2;

        int numberOfFreeSpaces;

        Timer timer = new Timer();

        action  = new OthelloAction("pass");


        if(args.length > 1) {
            state = args[0];
            timelimit=Double.parseDouble(args[1]);
            checkParams(state, timelimit);

        } else {

            System.out.println("Invalid parameters");
            System.exit(-1);
        }

        AlphaBeta alphaBeta = new AlphaBeta(new StateEvaluator(timelimit));

        OthelloPosition position = new OthelloPosition(state);

        timer.schedule(new TimerTasker(),(int)timelimit*1000);

        numberOfFreeSpaces = findFreespaces(state);

      //  test(state);

        while(i <= numberOfFreeSpaces){
            alphaBeta.setSearchDepth(i);
            action = alphaBeta.evaluate(position.clone());
            i++;
       }

      //  System.err.println("action points " + action.getValue());
        action.print();
        timer.cancel();
        System.exit(1);
    }

    private static void test(String state) {
        StateEvaluator s = new StateEvaluator(1);
        OthelloPosition position = new OthelloPosition(state);
        position.illustrate();
        for (OthelloAction act: position.getMoves()) {
            OthelloPosition clone = position.clone();
            act.print();
            for(OthelloPosition.directions d : act.getDirects()) {
                System.out.println(d);
            }

            try {
                clone.makeMove(act).illustrate();
                System.out.println(s.evaluate(clone));
            } catch (IllegalMoveException e) {
                e.printStackTrace();
            }
        }


    }


    private static void checkParams(String state, double timelimit) {
        Pattern pattern = Pattern.compile("[WBOXE]+");
        Matcher matcher = pattern.matcher(state);


        if(state.length() != 64 && !matcher.matches()){
            System.out.println("Invalid parameters");
            System.exit(-1);
        }


    }


    private static int findFreespaces(String state) {
      int spaces = 0;
        char[] chars = state.toCharArray();
        for (int i = 0; i < chars.length ; i++) {
            if(chars[i] == 'E') spaces++;
            }

        return spaces;
    }

    public static OthelloAction getAction() {
        return action;
    }




     static class TimerTasker extends TimerTask {

        @Override
        public void run() {
           // System.err.println("action points " + action.getValue());
            System.err.println("depth: " + i);
            Othello.getAction().print();
            System.exit(1);
        }
    }


}
