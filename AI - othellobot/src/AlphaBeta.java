import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Created by eric on 2015-11-24.
 */
public class AlphaBeta implements OthelloAlgorithm {
    protected int searchDepth;

    protected OthelloEvaluator othelloEvaluator;


    public AlphaBeta(OthelloEvaluator evaluator) {
        this.othelloEvaluator = evaluator;

    }

    @Override
    public void setEvaluator(OthelloEvaluator evaluator) {

    }

    @Override
    public OthelloAction evaluate(OthelloPosition position) {
        try {
            if (position.toMove()) {
                OthelloAction p = evaluateMax(position, this.searchDepth,Integer.MIN_VALUE, Integer.MAX_VALUE);
                return p;

            } else {
                OthelloAction p =  evaluateMin(position, this.searchDepth,Integer.MIN_VALUE, Integer.MAX_VALUE);
                return p;
            }
        } catch (IllegalMoveException e) {
            e.printStackTrace();
        }
        return null;
    }



    @Override
    public void setSearchDepth(int depth) {
        this.searchDepth = depth;

    }


    /**
     *
     * @param position
     * @param searchDepth
     * @param alpha
     * @param beta
     * @return
     * @throws IllegalMoveException
     */
    private OthelloAction evaluateMin(OthelloPosition position, int searchDepth,int alpha, int beta) throws IllegalMoveException {
        OthelloAction action;
        if (searchDepth == 0) {
            action = new OthelloAction(0, 0);
            action.value = othelloEvaluator.evaluate(position);
            return action;
        }
        int value = Integer.MAX_VALUE;
        LinkedList moves = position.getMoves();
        ListIterator movesiterator = moves.listIterator();
        if (!movesiterator.hasNext()) {
            action = new OthelloAction(0, 0,true);
            action.value = othelloEvaluator.evaluate(position);
            return action;
        } else {
            OthelloAction themove = new OthelloAction(0, 0);
            while (movesiterator.hasNext()) {
                action = (OthelloAction) movesiterator.next();
                OthelloPosition pos = position.makeMove(action);
                OthelloAction nextAction = evaluateMax(pos, searchDepth - 1,alpha,beta);
                if (value > nextAction.value) {

                    value = nextAction.value;
                    action.value = nextAction.value;
                    themove = action;
                }
                if(value <= alpha) {
                    action.value = value;
                    return action;
                }

                if(value < beta) {
                    beta = value;
                }
            }
            return themove;
        }

    }

    /**
     *
     * @param position
     * @param searchDepth
     * @param alpha
     * @param beta
     * @return
     * @throws IllegalMoveException
     */
    private OthelloAction evaluateMax(OthelloPosition position, int searchDepth,int alpha, int beta) throws IllegalMoveException {
        OthelloAction action;
        if (searchDepth == 0) {
            action = new OthelloAction(0, 0);
            action.value = othelloEvaluator.evaluate(position);
            return action;
        }
        int value = Integer.MIN_VALUE;
        LinkedList<OthelloAction> moves = position.getMoves();
        ListIterator movesiterator = moves.listIterator();
        if (!movesiterator.hasNext()) {
            action = new OthelloAction(0, 0,true);
            action.value = othelloEvaluator.evaluate(position);
            return action;
        } else {
            OthelloAction themove = new OthelloAction(0, 0);

            while (movesiterator.hasNext()) {
                action = (OthelloAction) movesiterator.next();
                OthelloPosition pos = position.makeMove(action);
                OthelloAction nextAction = evaluateMin(pos, searchDepth - 1,alpha, beta);
                if (value < nextAction.value) {
                    value = nextAction.value;
                    action.value = nextAction.value;
                    themove = action;

                }
                if(value >= beta) {
                    action.value = value;
                    return action;
                }

                if(value > alpha) {
                    alpha = value;
                }
            }
            return themove;
        }
    }


}


















