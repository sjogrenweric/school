/**
 * Created by eric on 2015-11-24.
 *  private static final int COINPARITYWEIGHT = 5;
 private static final int CAPTUREDCORNERSPARITYWEIGHT = 40;
 private static final int CAPTUREDSIDEPARITYWEIGHT = 12;
 private static final int MOBILITYPARITYWEIGHT = 4;
 private static final int BADCAPTUREDPARITYWEIGHT = 30;
 */
public class StateEvaluator implements OthelloEvaluator {

    private double timeLimit;
    private static final int COINWEIGHT = 5;
    private static final int BADPOS = 30;
    private static final int CORNER = 40;
    private static final int EDGE = 12;


    public StateEvaluator(double time){
        timeLimit = time;

    }

    /**
     * Evaluates the board and returns a heuristic
     * @param position2
     * @return
     */
    @Override
    public int evaluate(OthelloPosition position2) {
        return shortTime(position2);
/*
        if (timeLimit < 2) {
            return shortTime(position2);
        }else {
            return longTime(position2);
        }*/
    }

    private int test(OthelloPosition position2) {
        OthelloPosition position = position2.clone();
        int freespots = 0;
        int white = 0;
        int black = 0;

        for (int i = 1; i <= 8; i++) {
            for (int j = 1; j <= 8; j++) {
                if (position.board[i][j] == 'W') {
                    white++;
                }else if (position.board[i][j] == 'B') {
                    black++;
                }
            }
        }
        return white - black;
    }

    private int shortTime(OthelloPosition position2) {
        OthelloPosition position = position2.clone();
        int freespots = 0;
        int white = 0;
        int black = 0;

        for (int i = 1; i <= 8; i++) {
            for (int j = 1; j <= 8; j++) {
                if(position.board[i][j] == 'E') freespots++;
               // if(isCorner(i,j) && position.board[i][j] == 'E') freecorners++;

            }
        }


        for (int i = 1; i <= 8; i++) {
            for (int j = 1; j <= 8; j++) {
                if (position.board[i][j] == 'W') {
                    if(freespots > 0) {
                        if (isCorner(i, j)) {
                            white +=CORNER ;
                        } else if (isBadPos(position.board, i, j)) {
                            white -= BADPOS;
                        } else if(isEdge(i,j)){
                            white+=EDGE;
                        }else{
                            white+=COINWEIGHT;
                        }
                    } else {
                        white+=COINWEIGHT;
                    }
                }else if (position.board[i][j] == 'B') {
                    if(freespots > 0 ) {
                        if (isCorner(i, j)) {

                            black += CORNER;
                        } else if (isBadPos(position.board, i, j)) {
                            black -= BADPOS;
                        } else if(isEdge(i,j)){
                            black+=EDGE;
                        }else{
                            black+=COINWEIGHT;
                        }
                    }else{
                        black+=COINWEIGHT;
                    }
                }

            }
        }


        return (white) - (black);
    }

    private int longTime(OthelloPosition position2){
        int freespots  = 0 ;
        int freecorners = 0 ;
        int white = 0, black = 0;
        OthelloPosition position = position2.clone();

        for (int i = 1; i <= 8; i++) {
            for (int j = 1; j <= 8; j++) {
                if(position.board[i][j] == 'E') freespots++;
                if(isCorner(i,j) && position.board[i][j] == 'E') freecorners++;

            }
        }



            for (int i = 1; i <= 8; i++) {
                for (int j = 1; j <= 8; j++) {
                    if (position.board[i][j] == 'W') {
                        if(freespots > 0) {
                            if (isCorner(i, j)) {
                                white += (freecorners == 0) ? 1 : 5 * (freecorners);
                            } else if (isEdge(i, j)) {

                                if(isBadPos(position.board,i,j)){
                                    white-=5;
                                } else {
                                    white += 1;
                                }
                            } else {
                                ++white;
                            }
                        } else {
                            ++white;
                        }
                    }

                    if (position.board[i][j] == 'B') {
                        if (freespots > 0) {
                            if (isCorner(i, j)) {
                                black += (freecorners == 0) ? 1 : 5 * (freecorners);
                            } else if (isEdge(i, j)) {
                                if(isBadPos(position.board,i,j)){
                                    black-=5;
                                } else {
                                    black += 1;
                                }
                            } else {
                                ++black;
                            }
                        }else {
                            ++black;

                        }
                    }
                }
            }


     /*   position.setPlayerToMove(true);
        int whiteMoves =  position.getMoves().size();
        position.setPlayerToMove(false);
        int blackMoves = position.getMoves().size();
*/
        return (white) - (black );
    }

    private boolean borderBadPos(char board[][], int i, int j, char opponent){
        boolean bad = false;
        if(i == 1 || i == 8){
            if((board[i-1][j] == opponent) && (board[i+1][j] == 'E')){
                bad = true;
            } else if((board[i+1][j] == opponent) && (board[i-1][j] == 'E')){
                bad = true;
            }
        } else{
            if((board[i][j-1] == opponent) &&  (board[i][j+1] == 'E') ){
                bad = true;
            } else if ((board[i][j+1] == opponent) && (board[i][j-1] == 'E')){
                bad = true;
            }
        }
        return bad;
    }




    private boolean isBadPos(char board[][],int i , int j){
        if((i == 2 && (j == 2 || j == 1)) || (i == 1 && j == 2) ){
            if(board[1][1] == 'E') return true;
        }
        if((i == 2 && (j == 7 || j == 8)) || (i == 1 && j == 7) ){
            if(board[1][8] == 'E') return true;
        }
        if((i == 7 && (j == 2 || j == 1)) || (i == 8 && j == 2) ){
            if(board[8][1] == 'E') return true;
        }
        if((i == 7 && (j == 7 || j == 8)) || (i == 8 && j == 7) ){
            if(board[8][8] == 'E') return true;
        }

        return false;
    }

    private boolean isEdge(int i , int j){
        if(i == 1 || j == 1 || i == 8 || j == 8) return true;
        return false;
    }
    private boolean isCorner(int i, int j) {
       return (i == 1 && j == 1) || (i == 8 && j == 8) || (i == 1 && j == 8 ) || (i == 8 && j == 1);
    }
}


