package util;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Properties;

public class Constants {
    public static Charset DEFAULT_CHARSET;
    public static Charset BYTE_CHARSET;
    public static final String DEFAULT_ENCODING = "UTF8";
    public static final String BYTE_ENCODING = "ISO-8859-1";
    public static int OUR_LISTEN_PORT;

    public static final int DEFAULT_BLOCK_SIZE = 1024 * 16;

    public static byte[] OUR_PEER_ID;
    public static String OUR_IP_ADDRESS;
    public static String DOWNLOAD_PATH;

    public static final int TORRENT = 0;
    public static final int TRACKER = 1;
    public static final int PEER = 2;
    public static final int PROGRESS = 3;
    public static final int PIECES = 4;
    public static final int PIECESIZE = 5;
    public static final int COMPLETEDPIECES = 6;
    public static final int SAVELOCATION = 7;
    public static final int DOWNLOADED = 8;
    public static final int DOWNLOADSPEED = 9;



    static {
        loadConstants();
    }

    private static void loadConstants() {
        OUR_IP_ADDRESS = new String(HttpUtils.doGet("http://api.ipify.org"));
        OUR_PEER_ID = Utils.generatePeerId();
        BYTE_CHARSET = Charset.forName(Constants.BYTE_ENCODING);
        DEFAULT_CHARSET = Charset.forName(Constants.DEFAULT_ENCODING);
        DOWNLOAD_PATH = System.getProperty("user.home") + File.separator + "Downloads" + File.separator + "EE_TORRENTS" + File.separator;
        File logPath = new File("misc" + File.separator + "logs");
        if (!logPath.exists()) {
            if (!logPath.mkdirs()) {
                System.err.println("Could not create dirs: " + logPath);
            }
        }
        loadConfig();
    }


    private static void loadConfig() {
        Properties properties = Utils.loadProperties();
        OUR_LISTEN_PORT = Integer.valueOf(properties.getProperty("port"));
        System.out.println("Our IP: " + OUR_IP_ADDRESS + ":" + OUR_LISTEN_PORT);
    }

    public static byte[] getPeerId() {
        return OUR_PEER_ID;
    }

    public static int getListenPort() {
        return OUR_LISTEN_PORT;
    }

    public static String getIpAddress() {
        return OUR_IP_ADDRESS;
    }

    public static String getDownloadPath() {
        return DOWNLOAD_PATH;
    }

    public static Charset getDefaultCharset() {
        return DEFAULT_CHARSET;
    }

    public static Charset getByteCharset() {
        return BYTE_CHARSET;
    }

    public static int getBlockSize() {
        return DEFAULT_BLOCK_SIZE;
    }
}
