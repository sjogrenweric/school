package util;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class HaSha {


    private static MessageDigest digest;
    private static byte[] currentBytes = new byte[0];

    static {
        newSha();
    }

    private static void resetDigest() {
        try {
            digest = MessageDigest.getInstance("SHA-1");
            digest.reset();
            currentBytes = new byte[0];
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            System.err.println("SHA-1 Algorithm not found, quitting.");
            System.exit(-1);
        }
    }

    public static void newSha() {
        resetDigest();
    }

    public static byte[] hashIt() {
        byte[] hash = digest.digest();
        newSha();
        return hash;
    }

    private static void update(byte[] bytes) {
        byte[] buffer = new byte[bytes.length + currentBytes.length];
        System.arraycopy(currentBytes, 0, buffer, 0, currentBytes.length);
        System.arraycopy(bytes, 0, buffer, currentBytes.length, bytes.length);
        currentBytes = buffer;
        digest.update(currentBytes);
    }

    public static void append(long bigNumber) {
        update(ByteBuffer.allocate(8).putLong(bigNumber).array());
    }

    public static void append(String string) {
        update(string.getBytes());
    }

    public static void append(byte[] bytes) {
        update(bytes);
    }

    public static boolean verifyEquals(byte[] theirHash) {
        byte[] ourHash = hashIt();
        return Arrays.equals(ourHash, theirHash);
    }
}
