package util;

import messaging.BytePacket;
import torrent.Peer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class Utils {


    /**
     * Generate the client id, which is a fixed string of length 8 concatenated with 12 random bytes
     *
     * @return byte[]
     */
    public static byte[] generatePeerId() {
        byte[] id = new byte[12];
        Random r = new Random(System.currentTimeMillis());
        r.nextBytes(id);

        BytePacket peerIdBytes = new BytePacket();
        peerIdBytes.append("-EE0001-".getBytes());
        peerIdBytes.append(id);
        return peerIdBytes.getBytes();
    }

    /**
     * Reads a properties file
     *
     * @return Properties object
     */
    public static Properties loadProperties() {
        Properties properties = new Properties();
        Log log = new Log(Utils.class);
        try {
            InputStream is = new FileInputStream("misc/config.properties");
            //InputStream is = classlocation.getResourceAsStream(file);
            properties.load(is);
            Enumeration enuKeys = properties.keys();
            while (enuKeys.hasMoreElements()) {
                String key = (String) enuKeys.nextElement();
                String value = properties.getProperty(key);
                log.debug(key + " : " + value);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return properties;
    }


    public static ArrayList<String> trackerResponseToPeerIp(Map m) {
        ArrayList<String> ips = new ArrayList<String>();
        LinkedHashMap<String, Peer> l = null;
        long interval = 0;
        if (m != null) {
            if (m.containsKey("failure reason")) {
                System.err.println("Failure reason: " + m.get("failure reason"));
            } else {
                interval = ((Long) m.get("interval")).intValue();
                /*if (((Long) m.get("interval")).intValue() < interval)
                    interval = ((Long) m.get("interval")).intValue();
                else
                    interval *= 2;
*/
               // System.out.println("interval" + interval);

                Object peers = m.get("peers");
                ArrayList peerList = new ArrayList();
                l = new LinkedHashMap<String, Peer>();

                if (peers instanceof List) {
                    peerList.addAll((List) peers);
                    if (peerList != null && peerList.size() > 0) {
                        for (Object aPeerList : peerList) {
                            String peerID = new String((byte[]) ((Map) aPeerList).get("peer_id"));
                            String ipAddress = new String((byte[]) ((Map) aPeerList).get("ip"));
                            int port = ((Long) ((Map) aPeerList).get("port")).intValue();
                            ips.add(peerID + ":::" + ipAddress + ":::" + port);
                        }
                    }
                } else if (peers instanceof byte[]) {
                    byte[] p = ((byte[]) peers);
                    for (int i = 0; i < p.length; i += 6) {

                        String ipAddress = (Convert.byteToUnsignedInt(p[i]) + "." +
                                Convert.byteToUnsignedInt(p[i + 1]) + "." +
                                Convert.byteToUnsignedInt(p[i + 2]) + "." +
                                Convert.byteToUnsignedInt(p[i + 3]));
                        int port = (Convert.byteArrayToInt(Arrays.copyOfRange(p, i + 4, i + 6)));
                        //Utils.subArray(p, i + 4, 2)));
                        //System.out.println("per:" + ip + ":" + po);
                        ips.add(ipAddress + ":" + port);

                    }
                }
            }
            return ips;
        }

        return null;
    }

    public static byte[] concatBytePackets(BytePacket[] packets) {
        BytePacket root = new BytePacket(packets[0].getBytes());
        for(int i = 1; i < packets.length; i++) {
            root.append(packets[i].getBytes());
        }
        return root.getBytes();
    }
}
