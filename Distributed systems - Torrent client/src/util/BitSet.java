package util;

import java.util.ArrayList;
import java.util.List;

public class BitSet {

    private boolean[] bits;
    private int nrOfSetBits = 0;
    private final Object mutex = new Object();

    public BitSet(int size) {
        bits = new boolean[size];
    }

    public void set(int index) {
        synchronized (mutex) {
            if (index > bits.length || index < 0) return;
            boolean val = bits[index];
            bits[index] = true;
            if (val != true && nrOfSetBits < bits.length)
                nrOfSetBits++;
        }
    }

    public boolean get(int index) {
        boolean val = false;
        synchronized (mutex) {

            if (index > bits.length || index < 0) return false;
            val = bits[index];
        }
        return val;
    }


    public int nextSetBit(int offset) {
        synchronized(mutex) {

            if (offset > bits.length || offset < 0) return -1;
            for (int i = offset; i < bits.length; i++) {
                if (bits[i]) return i;
            }
        }
        return -1;
    }


    public int cardinality() {
        int cardinality = 0;
        synchronized (mutex) {

            cardinality = 0;
            for (int i = 0; i < bits.length; i++) {
                if (bits[i]) cardinality++;
            }
        }
        return cardinality;
    }

    public int size() {
        return bits.length;
    }

    public void unset(int pieceIndex) {
        synchronized (mutex) {

            if (pieceIndex > bits.length || pieceIndex < 0) return;
            boolean val = bits[pieceIndex];
            bits[pieceIndex] = false;
            if (val != false && nrOfSetBits > 0)
                nrOfSetBits--;
        }
    }

    public int nextClear() {
        synchronized (mutex) {
            for (int i = 0; i < bits.length; i++) {
                if (!bits[i]) return i;
            }
        }
        return -1;
    }

    public int nextClear(int index) {
        synchronized (mutex) {
            for (int i = index; i < bits.length; i++) {
                if (!bits[i]) return i;
            }
        }
        return -1;
    }

    public int[] clearedBits() {
        int[] clearedIndices = null;
        synchronized (mutex) {

            Integer[] cleared = (Integer[]) clearedBitsArray();
            clearedIndices = new int[cleared.length];
            int i = 0;
            for (Integer integer : cleared) {
                clearedIndices[i] = integer.intValue();
                i++;
            }
        }
        return clearedIndices;
    }

    private Object[] clearedBitsArray() {
        List cleared = null;
        synchronized (mutex) {

            cleared = new ArrayList<Integer>();
            for (int i = 0; i < bits.length; i++) {
                if (!bits[i]) {
                    cleared.add(i);
                }
            }
        }
        return cleared.toArray(new Integer[cleared.size()]);
    }

    public Integer[] setBits() {
        return (Integer[]) clearedBitsArray();
    }

    private Object[] setBitsArray() {
        List setBits = null;
        synchronized (mutex) {

            setBits = new ArrayList<Integer>();
            for (int i = 0; i < bits.length; i++) {
                if (bits[i]) {
                    setBits.add(i);
                }
            }
        }
        return setBits.toArray(new Integer[setBits.size()]);
    }

    public boolean isAllSet() {
        synchronized (mutex) {

            for (int i = 0; i < bits.length; i++) {
                if (!bits[i]) return false;
            }
        }
        return true;
    }
}
