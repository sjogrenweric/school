package util;

import java.nio.ByteBuffer;

public class Convert {

    /**
     * Convert a byte array to a URL encoded string
     *
     * @param in byte[]
     * @return String
     */
    public static String byteArrayToURLString(byte in[]) {
        byte ch = 0x00;
        int i = 0;
        if (in == null || in.length <= 0)
            return null;

        String pseudo[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
                "A", "B", "C", "D", "E", "F"};
        StringBuffer out = new StringBuffer(in.length * 2);

        while (i < in.length) {
            // First check to see if we need ASCII or HEX
            if ((in[i] >= '0' && in[i] <= '9')
                    || (in[i] >= 'a' && in[i] <= 'z')
                    || (in[i] >= 'A' && in[i] <= 'Z') || in[i] == '$'
                    || in[i] == '-' || in[i] == '_' || in[i] == '.'
                    || in[i] == '!') {
                out.append((char) in[i]);
                i++;
            } else {
                out.append('%');
                ch = (byte) (in[i] & 0xF0); // Strip off high nibble
                ch = (byte) (ch >>> 4); // shift the bits down
                ch = (byte) (ch & 0x0F); // must do this is high order bit is
                // on!
                out.append(pseudo[(int) ch]); // convert the nibble to a
                // String Character
                ch = (byte) (in[i] & 0x0F); // Strip off low nibble
                out.append(pseudo[(int) ch]); // convert the nibble to a
                // String Character
                i++;
            }
        }

        return new String(out);

    }


    /**
     * Convert a byte array integer (4 bytes) to its int value
     *
     * @param b byte[]
     * @return int
     */
    public static int byteArrayToInt(byte[] b) {
        if (b.length == 4)
            return ByteBuffer.wrap(b).getInt();
            //return b[0] << 24 | (b[1] & 0xff) << 16 | (b[2] & 0xff) << 8 |
                   // (b[3] & 0xff);
        else if (b.length == 2)
            return 0x00 << 24 | 0x00 << 16 | (b[0] & 0xff) << 8 | (b[1] & 0xff);

        return 0;
    }

    public static int byteToUnsignedInt(byte b) {
        return 0x00 << 24 | b & 0xff;
    }


    public static byte[] intToBytes(int integer) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        byteBuffer.putInt(integer);
        return byteBuffer.array();
        /*
        byte[] len = new byte[4];
        len[3] = (byte) (integer % 10);
        if ((integer - integer % 10) / 10 > 9) {
            len[1] = (byte) ((integer - integer % 10) / 100);
            len[2] = (byte) ((byte) ((integer - integer % 10) / 10) - 10);
        } else {
            len[2] = (byte) ((integer - integer % 10) / 10);
        }
        return len;
*/
    }

    public static byte[] intTo2Bytes(int integer) {
        byte[] data = new byte[2];
        data[1] = (byte) (integer & 0xFF);
        data[0] = (byte) ((integer >> 8) & 0xFF);
        return data;
    }
}
