package gui;


import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.Border;
import com.googlecode.lanterna.gui.Component;
import com.googlecode.lanterna.gui.Window;
import com.googlecode.lanterna.gui.component.Button;
import com.googlecode.lanterna.gui.component.InteractableComponent;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.gui.component.TextBox;
import com.googlecode.lanterna.gui.dialog.MessageBox;
import com.googlecode.lanterna.gui.listener.ComponentListener;

/**
 * Created by eric on 2014-12-13.
 */
public class GuiWindow  extends Window{
    public GuiWindow() {
        super("Save location");

        Panel horisontalPanel = new Panel(new Border.Invisible(), Panel.Orientation.HORISONTAL);
        String savelocation;
        TextBox textBox = new TextBox("Save location",horisontalPanel.getMinimumSize().getColumns());
        textBox.addComponentListener(new ComponentListener() {
            @Override
            public void onComponentInvalidated(Component component) {

            }

            @Override
            public void onComponentReceivedFocus(InteractableComponent interactableComponent) {

            }

            @Override
            public void onComponentLostFocus(InteractableComponent interactableComponent) {

            }

            void onComponentValueChanged(InteractableComponent component) {
                String savelocation = ((TextBox)component).getText();
                System.out.println(savelocation);
            }
        });

        horisontalPanel.addComponent(textBox);

        this.
        addComponent(horisontalPanel);
        addComponent(new Button("NOT WORKING!", new Action() {
            @Override
            public void doAction() {
                MessageBox.showMessageBox(getOwner(), "Saved", "Save location changed to");
                return;


            }
        }));
    }

}
