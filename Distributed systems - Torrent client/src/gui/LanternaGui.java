package gui;

import com.googlecode.lanterna.TerminalFacade;
import com.googlecode.lanterna.gui.GUIScreen;
import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.screen.ScreenWriter;
import com.googlecode.lanterna.terminal.Terminal;
import util.Constants;
import util.Log;

/**
 * Created by eric on 2014-12-13.
 * package gui;
 .
 */

public class LanternaGui {

    Log log;
    Terminal terminal;
    GUIScreen gui;
    ScreenWriter writer;
    private int noPieces = 0;
    private int pieceSize = 0;
    private int middle;

    public LanternaGui() {
        init();
        listener();


    }

    /**
     * Listener for exit (CTRL+q)
     */
    private void listener() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Key key = terminal.readInput();
                    if (key != null) {

                        if (key.isCtrlPressed() && key.getCharacter() == 'q') {
                            gui.getScreen().stopScreen();
                            System.exit(1);
                            //}
                        } else if (key.isCtrlPressed() && key.getCharacter() == 's') {
                            GuiWindow guiWindow = new GuiWindow();
                            gui.showWindow(guiWindow, GUIScreen.Position.CENTER);
                        }
                    }
                }

            }
        }).start();
    }

    /**
     * Initialize the gui
     */
    private void init() {
        log = new Log(this.getClass());
        terminal = TerminalFacade.createSwingTerminal(150,40);

        middle = terminal.getTerminalSize().getColumns() / 2;
        gui = TerminalFacade.createGUIScreen(terminal);
        writer = new ScreenWriter(gui.getScreen());
        terminal.setCursorVisible(false);
        if (gui == null) {
            log.error("Could not start gui window");
            return;
        }

        gui.getScreen().startScreen();
        writer.drawString(0, 0, "Torrent file: ");
        writer.drawString(0, 1, "Tracker URL: ");
        writer.drawString(0, 2, "Save location: " );
        writer.drawString(0, 4, "Total size : ");
        writer.drawString(0, 3, "Piece size : ");
        writer.drawString(0, 5, "Pieces : ");
        writer.drawString(0, 6, "Progess: " + percentToProgress(0));
        writer.drawString(middle, 2, "Completed pieces: ");
        writer.drawString(middle, 3, "Connected peers: ");
        writer.drawString(middle, 4, "Downloaded: " + 0 + " MB");
        writer.drawString(middle,5, "MB/s: " + 0);
        writer.setBackgroundColor(Terminal.Color.BLACK);
        writer.setForegroundColor(Terminal.Color.WHITE);
        writer.drawString(0, terminal.getTerminalSize().getRows() - 1, "CTRL+q to exit", ScreenCharacterStyle.Bold);
        gui.getScreen().refresh();
    }

    /**
     * Writes to certain section in the GUI
     *  TORRENT to change the torrent name
        * TRACKER to change the current tracker connected;
     PEER changes the number of currently connected peers
      PROGRESS sets the current progress in percentage
      PIECES number of total pieces
      PIECESIZE size of each piece
      COMPLETEDPIECES number of completed pieces
      SAVELOCATION changes the location of the file
      DOWNLOADED changes the amount of data downloaded
     * @param type
     * @param text
     */

    public void write(int type, String text) {

        switch (type) {
            case Constants.TORRENT:
                writer.drawString(0, 0, "Torrent file: " + text);
                break;
            case Constants.TRACKER:
                writer.drawString(0, 1, "Tracker URL: " + text);
                break;
            case Constants.PIECES:
                noPieces = Integer.parseInt(text);
                writer.drawString(0, 5, "Pieces: " + text);
                break;
            case Constants.PIECESIZE:
                pieceSize = Integer.parseInt(text);
                writer.drawString(0, 3, "Piece size(kB): " + Double.parseDouble(text) / 1024);
                break;
            case Constants.PROGRESS:
                writer.drawString(0, 6, "Progress: " + percentToProgress(Integer.parseInt(text)));
                break;
            case Constants.PEER:
                writer.drawString(middle, 3, "Connected peers: " + Integer.parseInt(text));
                break;
            case Constants.DOWNLOADED:
                writer.drawString(middle, 4, "Downloaded(MB): " + Double.parseDouble(text) / 1024 / 1000);
                break;
            case Constants.DOWNLOADSPEED:
                writer.drawString(middle, 5, "Speed(MB/s): " + Double.parseDouble(text) / 1024 / 1000 );
                break;
            case Constants.COMPLETEDPIECES:
                writer.drawString(middle, 2, "Completed pieces: " + Integer.parseInt(text));
                int currentPieces = Integer.parseInt(text);
                int percent;
                if (currentPieces > 0) {
                    percent = (int) Math.floor((float) currentPieces / (float) noPieces * 100);
                    write(Constants.PROGRESS, percent + "");
                    write(Constants.DOWNLOADED, currentPieces * pieceSize + "");
                }
                break;
            case Constants.SAVELOCATION:
                writer.drawString(0, 2, "Save location: " + text);
                break;
            default:
                break;

        }
        if (noPieces != 0 && pieceSize != 0) {
            writer.drawString(0, 4, "Total size(MB): " + noPieces * pieceSize / 1024 /1000);
        }
        gui.getScreen().refresh();
    }

    /**
     * Converts percentage to a progress bar
     * @param percent
     * @return
     */
    private String percentToProgress(int percent) {
        char[] progress = "[--------------------------------------------------]".toCharArray();
        int i;
        for (i = 2; i <= percent; i++) {
            progress[i / 2] = '#';
        }
        return new String(progress) + " " + percent + "%";
    }


}

