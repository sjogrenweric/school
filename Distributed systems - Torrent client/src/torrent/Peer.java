package torrent;

import messaging.BytePacket;
import messaging.PeerMessage;
import niocommunicator.channel.PacketChannel;
import storage.Piece;
import util.BitSet;
import util.Log;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;

public class Peer {

    private Log log = new Log(Peer.class);
    private byte[] byteInfoHash;
    private InetSocketAddress remoteAddress;
    private boolean handshakeReceived = false;
    private boolean handshakeSent = false;

    private boolean choked = true;
    private boolean interested = false;
    private boolean peer_choked = true;
    private boolean peer_interested = false;
    private boolean isConnected = false;

    private long downloaded = 0;
    private long uploaded = 0;

    private ArrayList<BytePacket> receiveBuffer = new ArrayList<BytePacket>();
    private PacketChannel<BytePacket> packetChannel;
    private BitSet availablePieces;
    private volatile long previousKeepAliveSent = 0;
    private int previousBufferSize = 0;
    private volatile long timeSinceLastRequest = 0;
    public volatile int requests = 0;
    public volatile int received = 0;

    public long getTimeSinceLastReceive() {
        return timeSinceLastReceive;
    }

    public void setTimeSinceLastReceive(long timeSinceLastReceive) {
        this.timeSinceLastReceive = timeSinceLastReceive;
    }

    private volatile long timeSinceLastReceive = 0;


    private Piece asignedPiece = null;
    private long previousSentInterested;

    public Peer(PacketChannel<BytePacket> packetChannel, byte[] byteInfoHash, int nPieces) {
        this.packetChannel = packetChannel;
        this.byteInfoHash = byteInfoHash;
        this.remoteAddress = packetChannel.getRemoteSocketAddress();
        availablePieces = new BitSet(nPieces);
    }

    public void sendPacket(PeerMessage peerMessage) throws IOException {
        if(peerMessage instanceof PeerMessage.KeepAliveMessage) {
            sentKeepAlive();
        } else if(peerMessage instanceof PeerMessage.InterestedMessage) {
            sentInterested();
        }
        packetChannel.sendPacket(peerMessage.getBytePacket());
    }

    private void sentInterested() {
        previousSentInterested = System.currentTimeMillis();
    }

    private void sentKeepAlive() {
        previousKeepAliveSent = System.currentTimeMillis();
    }

    public void keepConnectionAlive() throws IOException {
        if(!isConnected) return;

        long timeDiff = System.currentTimeMillis() - previousKeepAliveSent;
        int secondsDiff = (int) (timeDiff % 1000);
        if(secondsDiff > 90) {
            sendPacket(PeerMessage.KeepAliveMessage.create());
        }
    }

    public void close() {
        packetChannel.stop();
    }

    public void reConnect() {

    }

    public PacketChannel<BytePacket> getPacketChannel() {
        return packetChannel;
    }

    public boolean handshakeReceived() {
        return handshakeReceived;
    }

    public void setHandshakeReceived(boolean handshakeReceived) {
        this.handshakeReceived = handshakeReceived;
    }

    public byte[] getByteInfoHash() {
        return byteInfoHash;
    }

    public boolean handshakeSent() {
        return handshakeSent;
    }

    public void setHandshakeSent(boolean handshakeSent) {
        this.handshakeSent = handshakeSent;
    }

    public ArrayList<BytePacket> getReceiveBuffer() {
        return receiveBuffer;
    }

    public boolean isChoked() {
        return choked;
    }

    public void setChoked(boolean choked) {
        this.choked = choked;
    }

    public boolean isInterested() {
        return interested;
    }

    public void setInterested(boolean interested) {
        this.interested = interested;
    }

    public boolean isPeer_choked() {
        return peer_choked;
    }

    public void setPeerChoked(boolean peer_choked) {
        this.peer_choked = peer_choked;
    }

    public boolean isPeer_interested() {
        return peer_interested;
    }

    public void setPeerInterested(boolean peer_interested) {
        this.peer_interested = peer_interested;
    }



    public long getDownloaded() {
        return downloaded;
    }

    public long getUploaded() {
        return uploaded;
    }

    public BitSet getAvailablePieces() {
        return availablePieces;
    }

    public void setAvailablePieces(BitSet availablePieces) {
        this.availablePieces = availablePieces;
    }

    public void addAvailablePiece(int pieceIndex) {
        availablePieces.set(pieceIndex);
    }

    public boolean hasPiece(int pieceIndex) {
        return availablePieces.get(pieceIndex);
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean isConnected) {
        this.isConnected = isConnected;
    }

    public int getPreviousBufferSize() {
        return previousBufferSize;
    }

    public void setPreviousBufferSize(int previousBufferSize) {
        this.previousBufferSize = previousBufferSize;
    }


    public boolean hasAsignedPiece() {
        return asignedPiece != null;
    }

    public Piece getAsignedPiece() {
        return asignedPiece;
    }

    public void asignPiece(Piece asignedPiece) {
        this.asignedPiece = asignedPiece;
    }

    public long getTimeSinceLastRequest() {
        return timeSinceLastRequest;
    }

    public void setTimeSinceLastRequest(long timeSinceLastRequest) {
        this.timeSinceLastRequest = timeSinceLastRequest;
    }

    public long getPreviousSentInterested() {
        return previousSentInterested;
    }



}
