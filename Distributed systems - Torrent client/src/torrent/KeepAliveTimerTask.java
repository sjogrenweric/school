package torrent;

import messaging.BytePacket;
import niocommunicator.channel.PacketChannel;
import util.Log;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

public class KeepAliveTimerTask extends TimerTask {

    private final ConcurrentHashMap<PacketChannel<BytePacket>, Peer> peerMap;
    private Log log = new Log(KeepAliveTimerTask.class);
    private final Timer timer;

    public KeepAliveTimerTask(ConcurrentHashMap<PacketChannel<BytePacket>, Peer> peerMap, int intervalSeconds) {
        this.peerMap = peerMap;
        timer = new Timer(true);
        timer.scheduleAtFixedRate(this, 0, intervalSeconds * 1000);
        log.info("Keep Alive timer started.");

    }
    @Override
    public void run() {
        log.info("Running keep-alive task - " + peerMap.size() + " peers.");
        for(Peer peer : peerMap.values()) {
            try {
                peer.keepConnectionAlive();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
