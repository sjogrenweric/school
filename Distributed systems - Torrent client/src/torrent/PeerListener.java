package torrent;

import messaging.BytePacket;
import storage.Piece;

import util.BitSet;

public interface PeerListener {

    public void handleChoked(Peer peer);

    public void handleUnchoked(Peer peer);

    public void handleHave(Peer peer, int pieceIndex);

    public void handleBitfield(Peer peer, BitSet bitfield);

    public void handleRequest(Peer peer, int pieceIndex, int blockOffset, int blockLength);

    public void handleHandshake(Peer peer);

    public void handleBlockSent(Peer peer, Piece piece);

    public void handleBlockReceived(Peer peer, int pieceIndex, int blockOffset, BytePacket data);

    public void handlePieceCancel(Peer peer, int pieceIndex, int blockOffset, int blockLength);

    public void handleConnect(Peer peer);

    public void handleDisconnect(Peer peer);
}
