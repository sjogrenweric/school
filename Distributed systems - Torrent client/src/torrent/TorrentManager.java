package torrent;

import gui.LanternaGui;
import messaging.BytePacket;
import messaging.PeerMessage;
import niocommunicator.channel.PacketChannel;
import storage.FileStorage;
import storage.Piece;
import util.BitSet;
import util.Constants;
import util.Convert;
import util.Log;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class TorrentManager implements PeerListener, Runnable {

    Log log = new Log(TorrentManager.class);
    private ArrayList<Piece> sortedPieces;
    private long uploaded = 0;
    private long downloaded = 0;
    private long prevDownloaded = 0;
    private long downloadTime = 0;

    private final FileStorage fileStorage;
    private final TorrentFile torrentFile;
    private ConcurrentHashMap<PacketChannel<BytePacket>, Peer> connectedPeeersMap;
    private int blocksPerPiece;
    private LanternaGui gui;

    private Piece[] pieces;
    private volatile BitSet piecesCompleted;
    private volatile BitSet piecesRequested;

    public TorrentManager(Piece[] pieces, FileStorage fileStorage, TorrentFile torrentFile, ConcurrentHashMap<PacketChannel<BytePacket>, Peer> connectedPeeersMap, int blocksPerPiece) {
        gui = new LanternaGui();

        this.pieces = pieces;
        this.fileStorage = fileStorage;
        this.torrentFile = torrentFile;
        this.connectedPeeersMap = connectedPeeersMap;
        this.blocksPerPiece = blocksPerPiece;
        piecesCompleted = new BitSet(pieces.length);
        piecesRequested = new BitSet(pieces.length);
        downloadTime = System.currentTimeMillis();
        sortedPieces = new ArrayList<Piece>(Arrays.asList(pieces));
        updateGui();
        new Thread(this).start();
    }

    private void updateGui() {
        gui.write(Constants.TORRENT, torrentFile.getFilenames()[0]);
        gui.write(Constants.TRACKER, torrentFile.getTracker());
        gui.write(Constants.PIECES, pieces.length+"");
        gui.write(Constants.PIECESIZE, blocksPerPiece*Constants.getBlockSize()+"");
        gui.write(Constants.SAVELOCATION, fileStorage.getStoragePath());


    }
    @Override
    public void handleChoked(Peer peer) {
        peer.setPeerChoked(true);
    }

    @Override
    public void handleUnchoked(Peer peer) {
        peer.setPeerChoked(false);
    }

    @Override
    public void handleHave(Peer peer, int pieceIndex) {
        peer.getAvailablePieces().set(pieceIndex);
        pieces[pieceIndex].incrementAvailability();
    }

    @Override
    public void handleBitfield(Peer peer, BitSet bitfield) {
        for (int i = bitfield.nextSetBit(0); i >= 0; i = bitfield.nextSetBit(i + 1)) {
            if (i < pieces.length)
                pieces[i].incrementAvailability();
        }

    }

    @Override
    public void handleRequest(Peer peer, int pieceIndex, int blockOffset, int blockLength) {

    }

    @Override
    public void handleHandshake(Peer peer) {
        try {
            peer.sendPacket(PeerMessage.BitFieldMessage.create(piecesCompleted));
            if (!peer.handshakeSent()) {
                peer.sendPacket(PeerMessage.HandshakeMessage.create(peer));
                peer.setHandshakeSent(true);
            }
            peer.sendPacket(PeerMessage.KeepAliveMessage.create());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void handleBlockSent(Peer peer, Piece piece) {

    }

    @Override
    public void handleBlockReceived(Peer peer, int pieceIndex, int blockOffset, BytePacket data) {
       // log.info("Received piece: " + pieceIndex + " block: " + blockOffset);
        Piece piece = null;
        try {
            peer.setTimeSinceLastReceive(System.currentTimeMillis());
            piece = pieces[pieceIndex];
            if(blockOffset % Constants.getBlockSize() != 0) {
                log.error(log.paintItRed("BLOCK OFFSET ERROR on piece: " + pieceIndex + " block " + blockOffset));
            }
            if (piece.isComplete() || piecesCompleted.get(pieceIndex)) {
                log.info(log.paintItRed("Received duplicate! index " + pieceIndex));
                unasignPeer(peer);
                return;
            }

            piece.setCompletedBlock(blockOffset / Constants.getBlockSize());
            boolean completedPiece = fileStorage.write(pieceIndex, blockOffset, data, piece.getHash());

            if (completedPiece) {
                log.info(log.paintItRed("Piece completed: " + pieceIndex));
                unasignPeer(peer);
                piecesCompleted.set(pieceIndex);
                downloaded += Constants.getBlockSize() * blocksPerPiece;
                gui.write(Constants.COMPLETEDPIECES,piecesCompleted.cardinality()+"");

                return;
            }
            peer.received++;
            if (peer.requests == peer.received) {
                if(!piece.isComplete()) {
                    sendNewBlockRequests(peer);
                    long prevTime = peer.getTimeSinceLastRequest();
                    long diff = System.currentTimeMillis() - prevTime;
                }
            }
        } catch (FileStorage.InvalidHashException e) {
            log.error("Invalid hash at piece index: " + e.getPieceIndex());
            piece.clearCompletedBlocks();
            piecesCompleted.unset(e.getPieceIndex());
            unasignPeer(peer);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("Failed to write");
        }
    }


    @Override
    public void handlePieceCancel(Peer peer, int pieceIndex, int blockOffset, int blockLength) {

    }

    @Override
    public void handleConnect(Peer peer) {
        try {
            gui.write(Constants.PEER, connectedPeeersMap.size() + "");
            peer.sendPacket(PeerMessage.HandshakeMessage.create(peer));
            peer.setHandshakeSent(true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void handleDisconnect(Peer peer) {
        if (peer != null && peer.getAvailablePieces() != null) {
            for (int i = peer.getAvailablePieces().nextSetBit(0); i >= 0; i = peer.getAvailablePieces().nextSetBit(i + 1)) {
                pieces[i].decrementAvailability();
            }
        }
        gui.write(Constants.PEER, connectedPeeersMap.size() + "");
    }

    public String generateTrackerRequest() {
        String infoHash = "info_hash=" + Convert.byteArrayToURLString(torrentFile.getByteInfoHash());
        String peerId = "peer_id=" + Convert.byteArrayToURLString(Constants.getPeerId());
        String ip = "ip=" + Constants.getIpAddress();
        String port = "port=" + Constants.getListenPort();
        String uploaded = "uploaded=" + 0;
        String downloaded = "downloaded=" + 0;
        String left = "left=" + (torrentFile.getNumPieces() * torrentFile.getPieceLength());
        String event = "event=" + "started";
        String compact = "compact=" + "1";

        return "?" + infoHash + "&" + peerId + "&" + ip + "&" + port + "&" + uploaded + "&" + downloaded + "&"
                + left + "&" + event + "&" + compact;
    }


    private void sendNewBlockRequests(Peer peer) throws IOException {
        if (peer.hasAsignedPiece()) {
            if (!peer.getAsignedPiece().isComplete()) {
                Piece piece = peer.getAsignedPiece();
                int[] unCompletedBlockIndices = piece.getUncompletedRequests();
                int nrOfRequests = unCompletedBlockIndices.length < (blocksPerPiece / 2) + blocksPerPiece % 2 ? unCompletedBlockIndices.length : (blocksPerPiece / 2) + blocksPerPiece % 2;
                int i = 0;
                peer.received = 0;
                peer.setTimeSinceLastRequest(System.currentTimeMillis());
                for (int index : unCompletedBlockIndices) {
                    if (i == nrOfRequests) break;
                    peer.sendPacket(PeerMessage.RequestMessage.create(piece.getIndex(), index * Constants.getBlockSize(), Constants.DEFAULT_BLOCK_SIZE));
                    i++;
                }

                peer.requests = nrOfRequests;
            }
        }
    }

    private ArrayList<Piece> getDownloadablePiece() {
        ArrayList<Piece> downloadable = new ArrayList<Piece>();
        for (int i = 0, j = 0; i < sortedPieces.size() ; i++) {
            if(piecesCompleted.cardinality() < pieces.length * 0.95) {
                if (!piecesCompleted.get(i) && !piecesRequested.get(i)) {
                    downloadable.add(sortedPieces.get(i));
                    j++;
                }
            } else {
                int[] uncompleted = piecesCompleted.clearedBits();
                for(int q = 0; q < uncompleted.length; q++) {
                    downloadable.add(pieces[uncompleted[q]]);
                }
            }
        }
        if(piecesCompleted.cardinality() >= (pieces.length * 0.95)) {
            System.out.println("Uncompleted pieces:");
            int[] uncleared = piecesCompleted.clearedBits();
            for(int i =0 ; i < uncleared.length; i++) {
                System.out.print(" " + uncleared[i] + " ");
            }
            System.out.println();
        }

        return downloadable;
    }

    private void updatePeerRequest(Peer peer) throws IOException {
        if (peer.isPeer_choked()) {
            Piece unAsigned = peer.getAsignedPiece();
            log.info(log.paintItRed("CHOKED: unasigneing"));
            unasignPeer(peer);
            peer.sendPacket(PeerMessage.InterestedMessage.create());
        } else {
            Piece asignedPiece = peer.getAsignedPiece();
            if (asignedPiece.isComplete()) {
                log.info(log.paintItRed("COMPLETE: unasigneing"));
                // piecesCompleted.set(peer.getAsignedPiece().getIndex());
                unasignPeer(peer);
                return;
            }
            long peerTime = peer.getTimeSinceLastReceive() / 1000;
            long ourTime = System.currentTimeMillis() / 1000;
            if (!asignedPiece.isComplete() && ((ourTime - peerTime) >= 5 && peer.getTimeSinceLastReceive() != 0)) {
              //  log.info(log.paintItRed("TIME : unasigneing, time: " + (ourTime - peerTime) + " complete? " + asignedPiece.isComplete()));
                unasignPeer(peer);
            } else if (!asignedPiece.isComplete() && ((ourTime - peerTime) >= 1 && peer.getTimeSinceLastReceive() != 0)) {

                 sendNewBlockRequests(peer);
            }
        }

    }

    private void updateDownloadSpeed() {
        if(System.currentTimeMillis() - downloadTime > 1000) {
            long bytes = downloaded - prevDownloaded;
            prevDownloaded = downloaded;
            gui.write(Constants.DOWNLOADSPEED, bytes + "");
            downloadTime = System.currentTimeMillis();
        }
    }

    @Override
    public void run() {
        boolean stop = false;

        try {
            Thread.sleep(3 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        while (!stop) {
            try {
                if(piecesCompleted.cardinality() == pieces.length) {
                    stop = true;
                }
                Thread.sleep(1 * 1000);
                updateDownloadSpeed();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //Collections.sort(sortedPieces);
            // Collections.shuffle(sortedPieces);
            List<Peer> shuffledPeers = new ArrayList(connectedPeeersMap.values());
            Collections.shuffle(shuffledPeers);

            ArrayList<Piece> downloadablePieces = getDownloadablePiece();

            log.info( "Completed pieces:  " + piecesCompleted.cardinality());
            int count = 0;

            try {
                for (Peer peer : shuffledPeers) {

                    if (peer.isPeer_choked() && peer.getPreviousSentInterested() == 0) {
                        log.info("sendin interested");
                        peer.sendPacket(PeerMessage.InterestedMessage.create());
                        continue;
                    } else if (peer.isPeer_choked() && peer.getPreviousSentInterested() != 0) {
                        long peerTime = peer.getPreviousSentInterested() / 1000;
                        long ourTime = System.currentTimeMillis() / 1000;
                        if (ourTime - peerTime > 5) {
                            log.info(log.paintItRed("Removing peer"));
                            if(peer.hasAsignedPiece()) {
                                unasignPeer(peer);
                            }
                            handleDisconnect(peer);
                            connectedPeeersMap.remove(peer.getPacketChannel());
                        }
                        continue;
                    }


                    if (peer.hasAsignedPiece()) {
                        updatePeerRequest(peer);
                        continue;
                    }
                    for (Piece piece : downloadablePieces) {

                        if(piecesCompleted.cardinality() >= pieces.length * 0.95 && peer.hasPiece(piece.getIndex())) {
                            //log.info("Piece availability for: " + piece.getIndex() + " = " + piece.availability() + " peer has piece? " + peer.hasPiece(piece.getIndex()));
                            count++;
                        }

                        if (peer.hasPiece(piece.getIndex())) {
                            if (!peer.isPeer_choked()) {
                                if ((!peer.hasAsignedPiece() || peer.getAsignedPiece().isComplete()) && !piecesCompleted.get(piece.getIndex())) {
                                    if(piecesCompleted.cardinality() > pieces.length * 0.95 || !piecesRequested.get(piece.getIndex())) {
                                       // log.info("Asigning piece: " + piece.getIndex());
                                        peer.asignPiece(piece);
                                        piecesRequested.set(piece.getIndex());

                                        sendNewBlockRequests(peer);
                                        downloadablePieces.remove(piece);
                                        break;
                                    }
                                }
                            }
                        }
                        if(piecesCompleted.cardinality() >= pieces.length * 0.95 && count == 0) {
                            log.error("ERROR: NOBODY HAS PIECE: " + piece.getIndex());
                        }
                    }

                }

                int unchoked = 0;
                int asigned = 0;
                for (Peer peer : connectedPeeersMap.values()) {
                    if (peer.hasAsignedPiece()) {
                        unchoked++;
                    }
                    if(!peer.isPeer_choked()) asigned++;
                }
                log.info("Asigned peers: " + unchoked + " unchoked " + asigned + ", total peers: " + connectedPeeersMap.size());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void unasignPeer(Peer peer) {
        if(peer != null &&peer.hasAsignedPiece()) {
            piecesRequested.unset(peer.getAsignedPiece().getIndex());
            peer.asignPiece(null);
            peer.requests = 0;
        }
    }


}
