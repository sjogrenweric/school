package torrent;

import util.Constants;
import util.Log;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class TorrentClient {
    private final Log log;
    private HashMap<String, TorrentInstance> activeTorrentsMap = new HashMap<String, TorrentInstance>();

    public static void main(String[] args) throws IOException {

        if (args.length == 0) {
            new TorrentClient();
        } else if(args.length == 1) {
            new TorrentClient(args[0]);
        } else {
            //start torrents from previous run?
        }

    }

    public TorrentClient() {
        log = new Log(TorrentClient.class);
        log.info(Constants.getDownloadPath());
        //TODO Load emptpy GUI
        try {
            initiateTorrentInstance("misc/debian.torrent");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public TorrentClient(String fileName) {
        log = new Log(TorrentClient.class);
        log.info(Constants.getDownloadPath());

        //TODO Load emptpy GUI

        try {
            initiateTorrentInstance(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initiateTorrentInstance(String torrentFileName) throws IOException {
        File localTorrentFile = new File(torrentFileName);
        TorrentFile torrentFile = new TorrentFile(localTorrentFile);
        log.info("Parsing torrent file: " + torrentFileName);
        System.out.println(torrentFile);

        TorrentInstance torrentInstance = new TorrentInstance(torrentFile);
        activeTorrentsMap.put(torrentFile.getName(), torrentInstance);
        torrentInstance.startTorrent();
    }



}
