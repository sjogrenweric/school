package torrent;

import bencode.BDecoder;
import messaging.BytePacket;
import messaging.PeerProtocol;
import niocommunicator.SelectorThread;
import niocommunicator.channel.*;
import niocommunicator.communicator.DefaultPacketClient;
import niocommunicator.communicator.DefaultPacketServer;
import niocommunicator.serialization.ByteArraySerializer;
import storage.FileStorage;
import storage.Piece;
import util.Constants;
import util.HttpUtils;
import util.Log;
import util.Utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class TorrentInstance extends PacketChannelEventProcessor<BytePacket> implements PacketAssemblerFactory<BytePacket>, PacketChannelListener<BytePacket>, PacketAssembler<BytePacket> {

    private final Log log = new Log(TorrentInstance.class);
    private final TorrentFile torrentFile;
    private DefaultPacketClient<BytePacket> channelClient;
    private DefaultPacketServer<BytePacket> channelServer;

    private PeerProtocol peerProtocol;
    private TorrentManager torrentManager;
    private ConcurrentHashMap<PacketChannel<BytePacket>, Peer> connectedPeeersMap = new ConcurrentHashMap<PacketChannel<BytePacket>, Peer>();
    private KeepAliveTimerTask keepAliveTimerTask;
    private int numPieces;

    public TorrentInstance(TorrentFile torrentFile) {

        this.torrentFile = torrentFile;

        //TODO.. fix file offsets
        numPieces = torrentFile.getNumPieces();
        int pieceLength = torrentFile.getPieceLength();
        long totalLength = torrentFile.getTotalLength();
        long lastPieceOffset = Math.abs(totalLength - (numPieces * pieceLength));
        String[] pieceHashes = torrentFile.getPieces();
        Piece[] pieces = new Piece[numPieces];
        long[] fileLengths = torrentFile.getLengths();
        long[] fileOffsets = new long[fileLengths.length];
        int blocksPerPiece = pieceLength / Constants.getBlockSize();;

        for (int i = 0; i < fileLengths.length; i++) {
            fileOffsets[i] = fileOffsets[i - (i > 0 ? 1 : 0)] + fileLengths[i];
            log.info("Offset: " + fileOffsets[i] + ", " + (fileOffsets[i] / pieceLength));
        }

        for (int i = 0; i < numPieces; i++) {
            pieces[i] = new Piece(i, (pieceLength * i), pieceLength - (i == numPieces - 1 ? lastPieceOffset : 0), blocksPerPiece, pieceHashes[i]);
        }


        try {
            FileStorage fileStorage = new FileStorage(Constants.getDownloadPath(), torrentFile.getName(), torrentFile.getFilenames(), pieceLength, numPieces, fileLengths);
            torrentManager = new TorrentManager(pieces, fileStorage, torrentFile, connectedPeeersMap, blocksPerPiece);
            peerProtocol = new PeerProtocol(torrentManager,pieceLength/blocksPerPiece, false);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.err.println("Something wrong with filesystem - quitting.. yeh yeh? ?+??");
            System.exit(1);
        }

        try {
            SelectorThread selectorThread = new SelectorThread();
            channelServer = new DefaultPacketServer<BytePacket>(selectorThread, new InetSocketAddress("localhost", Constants.getListenPort()), this, this, this);
            channelServer.start();
            channelClient = new DefaultPacketClient<BytePacket>(selectorThread, this, this, this);
            channelClient.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Start the torrent download process:
     * Read current files (if any) and check validity.
     * Get Peers from Tracker -> handshake with Peers.
     * Download file(s).
     */

    public void startTorrent() {
        getPeerIpList();
        keepAliveTimerTask = new KeepAliveTimerTask(connectedPeeersMap, 10);
    }

    /**
     * Stop the download process.
     */
    public void stopTorrent() {

    }

    public void pauseTorrent() {

    }

    /**
     * Query tracker for ip and port of Peers.
     */
    private void getPeerIpList() {

        log.info("Query tracker for Peers at: " + (torrentFile.getTracker() + torrentManager.generateTrackerRequest()));
        byte[] trackerResponse = HttpUtils.doGet(torrentFile.getTracker() + torrentManager.generateTrackerRequest());

        try {
            HashMap trackerResponseMap = (HashMap) BDecoder.decode(trackerResponse);
            ArrayList<String> peerInfo = Utils.trackerResponseToPeerIp(trackerResponseMap);
            int i = 0;
            for (String ipAndPort : peerInfo) {
                log.info("Connecting to peer: " + ipAndPort);
                String[] address = ipAndPort.split(":");
                channelClient.connect(new InetSocketAddress(address[0], Integer.parseInt(address[1])), this);
                i++;
            }

        } catch (IOException e) {
            e.printStackTrace();
            //TODO handle this
        }
    }


    @Override
    public void socketConnected(PacketChannel<BytePacket> packetChannel, Object context) {
        if (packetChannel.getLocalSocketAddress().getPort() == Constants.getListenPort()) {
            log.info("Someone connected to server socket.");
        } else {
            Peer peer = new Peer(packetChannel, torrentFile.getByteInfoHash(), numPieces);
            peer.setConnected(true);
            connectedPeeersMap.put(packetChannel, peer);
            torrentManager.handleConnect(peer);
        }
    }

    @Override
    public void packetArrived(PacketChannel<BytePacket> packetChannel, BytePacket packet) {

        if (packetChannel.getLocalSocketAddress().getPort() == Constants.getListenPort()) {
            log.info("Packat arrived on server socket.");
        } else {
            try {
                peerProtocol.decode(connectedPeeersMap.get(packetChannel), packet);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void socketDisconnected(PacketChannel<BytePacket> packetChannel) {
        Peer peer = connectedPeeersMap.get(packetChannel);
        log.info("Peer disconnected: " + packetChannel);
        connectedPeeersMap.remove(packetChannel);
        torrentManager.handleDisconnect(peer);
    }

    @Override
    public void socketException(PacketChannel<BytePacket> packetChannel, Exception ex) {
        Peer peer = connectedPeeersMap.get(packetChannel);
        connectedPeeersMap.remove(packetChannel);
        torrentManager.handleDisconnect(peer);
    }

    @Override
    public List<BytePacket> appendReceivedData(PacketChannel<BytePacket> packetChannel, byte[] data) {
        List<BytePacket> l = new ArrayList<BytePacket>();
        l.add(new BytePacket(data));
        return l;
    }

    @Override
    public PacketAssembler<BytePacket> create() {
        return this;
    }

    // --- Not really interested --- //


    @Override
    public void packetSent(PacketChannel<BytePacket> packetChannel, BytePacket packet) {

    }

    @Override
    public void onSocketReadyForWrite() {

    }

    @Override
    public void onSocketNotReadyForWrite() {

    }

    @Override
    public ByteArraySerializer getByteArraySerializer() {
        return null;
    }

    @Override
    public void setByteArraySerializer(ByteArraySerializer byteArraySerializer) {

    }

}
