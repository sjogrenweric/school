package tests;

import messaging.BytePacket;

import java.util.Arrays;

public class PeerPacketTest {

    public static void main(String[] args) {
        BytePacket p = new BytePacket(4);
        System.out.println(p);
        System.out.println(p.append(new byte[]{1, 1}));
        System.out.println(p.append(new byte[]{2}));
        System.out.println(p.append(new byte[]{3, 3}));
        System.out.println(p.append(new byte[]{4, 4}));
        System.out.println(p.prepend(new byte[]{5, 5}));
        System.out.println(p.append((byte) 19));

        p = new BytePacket(new byte[] {0, 1,2,3,4,5,6,7,8,9});
        System.out.println(p);
        p.remove(0, p.size() - 2);
        System.out.println(p);
        p = new BytePacket(new byte[] {0, 0, 0, 5, -1, 1, 2, 3, 4, 5});
        System.out.println(p);
        byte[] asd = p.read(5, 5 + 5);
        System.out.println(Arrays.toString(asd));
        //System.out.println(PeerProtocol.createHandshake());
    }
}
