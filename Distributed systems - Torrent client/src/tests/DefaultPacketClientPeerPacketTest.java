package tests;

import messaging.BytePacket;
import niocommunicator.SelectorThread;
import niocommunicator.channel.*;
import niocommunicator.communicator.DefaultPacketClient;
import niocommunicator.communicator.DefaultPacketServer;
import niocommunicator.serialization.ByteArraySerializer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

public class DefaultPacketClientPeerPacketTest extends PacketChannelEventProcessor<BytePacket> implements PacketAssemblerFactory<BytePacket>, PacketChannelListener<BytePacket>, PacketAssembler<BytePacket> {

    private PacketChannel<BytePacket> packetChannel;
    private volatile boolean written = false;

    public static void main(String[] args) {

        try {
            DefaultPacketClientPeerPacketTest test = new DefaultPacketClientPeerPacketTest();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public DefaultPacketClientPeerPacketTest() throws IOException {
        DefaultPacketClient<BytePacket> defaultPacketClient = new DefaultPacketClient<BytePacket>(new SelectorThread(), this, this, this);
        defaultPacketClient.start();//128.69.142.141 on port: 5217

        DefaultPacketServer<BytePacket> defaultPacketServer = new DefaultPacketServer<BytePacket>(new SelectorThread(),  new InetSocketAddress("localhost", 13337), this, this, this);
        defaultPacketServer.start();

        defaultPacketClient.connect(new InetSocketAddress("localhost", 13337), this);
    }


    @Override
    public PacketAssembler<BytePacket> create() {
        System.out.println("create");
        return this;
    }

    @Override
    public ByteArraySerializer getByteArraySerializer() {
        System.out.println("serialize");

        return null;
    }

    @Override
    public void setByteArraySerializer(ByteArraySerializer byteArraySerializer) {
        System.out.println("setbyte");

    }

    @Override
    public void socketConnected(PacketChannel<BytePacket> packetChannel, Object context) {
        System.out.println("socketConnected");
        if(packetChannel.getLocalSocketAddress().getPort() != 13337) {
            try {
                packetChannel.sendPacket(new BytePacket(new byte[] {-1, 0, 1, 2, 3}));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
/*
        try {
            BytePacket handshake = PeerProtocol.createHandshake();
            packetChannel.sendPacket(handshake);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public void packetArrived(PacketChannel<BytePacket> packetChannel, BytePacket packet) {
        System.out.println("packetARrived " + packet);
        /*PeerProtocol.handshakeDecode(packet);
        System.out.println(new String(packet.getBytes()));*/

    }

    @Override
    public void packetSent(PacketChannel<BytePacket> packetChannel, BytePacket packet) {
        System.out.println("packetSent" + packet);
    }

    @Override
    public void socketException(PacketChannel<BytePacket> packetChannel, Exception ex) {
        System.out.println("packet exeption");
    }

    @Override
    public void socketDisconnected(PacketChannel<BytePacket> packetChannel) {
        System.out.println("socket disconnect");

    }

    @Override
    public void onSocketReadyForWrite() {
        System.out.println("ready for write");
    }

    @Override
    public void onSocketNotReadyForWrite() {
        System.out.println("not ready for write");

    }

    @Override
    public List<BytePacket> appendReceivedData(PacketChannel<BytePacket> packetChannel, byte[] data) {
        System.out.println("appending received data ");
        //System.err.println(new PeerPacket(data));
        List<BytePacket> l = new ArrayList<BytePacket>();
        l.add(new BytePacket(data));
        return l;
    }
}
