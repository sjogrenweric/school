package messaging;

import niocommunicator.channel.Packet;
import util.Convert;

import java.util.Arrays;

public class BytePacket implements Packet {

    private byte[] packet;
    private int position = 0;

    public BytePacket(int size) {
        packet = new byte[size];
    }

    public BytePacket() {
        packet = new byte[0];
    }

    public BytePacket(byte[] data) {
        packet = new byte[0];
        extend(data.length);
        System.arraycopy(data, 0, packet, position, data.length);
        position += data.length;
    }

    public BytePacket append(byte[] data) {
        int extension = position + data.length;
        if(extension >= packet.length) {
            extend((extension - packet.length));
        }
        System.arraycopy(data, 0, packet, position, data.length);
        position += data.length;
        return this;
    }

    public BytePacket append(byte b) {
        if(position == packet.length) {
            extend(1);
        }
        packet[position] = b;
        position++;
        return this;
    }

    public BytePacket prepend(byte[] data) {
        byte[] buffer = new byte[packet.length + data.length];
        System.arraycopy(data, 0, buffer, 0, data.length);
        System.arraycopy(packet, 0, buffer, data.length, packet.length);
        position = position + data.length;
        packet = buffer;
        return this;
    }

    public byte[] read(int from, int to) {
        return Arrays.copyOfRange(packet, from, to);
    }

    public int readInt(int from) {
        return Convert.byteArrayToInt(Arrays.copyOfRange(packet, from, from + 4));
    }

    public byte[] remove(int from, int to) {
        if(to < 0 || to > size() || from < 0 || from > to) {
            //System.err.println("Illegal arguments for remove, BytePacket (" + from + ", " + to + ")");
            return new byte[0];
        }
        int newSize = size() - (to - from);
        int shaveHigh = size() - to;
        int shaveLow = from;
        //System.out.println("size: " + newSize + " high: " + shaveHigh + " low: " + shaveLow + " (" + from + "," + to + ")");
        byte[] newBytes = new byte[newSize];
        System.arraycopy(packet, 0, newBytes, 0, shaveLow);
        System.arraycopy(packet, to, newBytes, shaveLow, shaveHigh);
        packet = newBytes;
        position = 0;
        return packet;
    }

    public byte[] remove(int length) {
        byte[] newBytes = new byte[length];
        System.arraycopy(packet, 0, newBytes, 0, length);
        packet = newBytes;
        position = 0;
        return packet;
    }


    private BytePacket extend(int extension) {
        byte[] buffer = new byte[packet.length + extension];
        System.arraycopy(packet, 0, buffer, 0, position);
        packet = buffer;
        return this;
    }

    @Override
    public byte[] getBytes() {
        return packet;
    }

    public int size() {
        return packet.length;
    }

    @Override
    public String toString() {
        return "size: " + size() + ", " + (packet.length > 20 ? Arrays.toString(this.read(0, 20)) + "...]" : Arrays.toString(packet));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BytePacket that = (BytePacket) o;

        if (!Arrays.equals(packet, that.packet)) return false;

        return true;
    }


    public byte getByte(int index) {
        return packet[index];
    }
}
