package messaging;

import torrent.Peer;
import torrent.PeerListener;
import util.BitSet;
import util.Constants;
import util.Convert;
import util.Log;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;

public class PeerProtocol {
    private static final byte KEEP_ALIVE = -1; // NOT APPLICABLE
    private static final byte CHOKE = 0;
    private static final byte UN_CHOKE = 1;
    private static final byte INTERESTED = 2;
    private static final byte NOT_INTERESTED = 3;
    private static final byte HAVE = 4;
    private static final byte BITFIELD = 5;
    private static final byte REQUEST = 6;
    private static final byte PIECE = 7;
    private static final byte CANCEL = 8;
    private static final int HANDSHAKE_SIZE = 68;

    private static final int LEN_BITS = 4;
    private static final int PAYLOAD_STRT = 5;
    private static Log log = new Log(PeerProtocol.class);
    private PeerListener peerListener;
    private int blockLength;


    public PeerProtocol(PeerListener peerListener, int blockLength, boolean enableLog) {
        this.peerListener = peerListener;
        this.blockLength = blockLength;

        log.logToFile("PeerProtocol");
        if(!enableLog) {
            log.disable();
        }
    }

    public synchronized void decode(Peer peer, BytePacket bytePacket) throws IOException {

        if (peer == null) return;

        if (peer.getReceiveBuffer().size() > 0) {
            bytePacket = mergeBuffer(bytePacket, peer);
            /*for (int i = 0; i <9 ; i++) {
                System.out.println(bytePacket.getBytes()[i]);

            } */
        }

        if (bytePacket.size() == 0) return;

        log.info("Decoding: from (" + peer.getPacketChannel() + ")");
       // log.info("Decoding: msg " + bytePacket);


        if (!peer.handshakeReceived() && bytePacket.getBytes()[0] == 19) {
            log.info("Received handshake: " + bytePacket);
            //TODO - if handshake contains bad data -> destroy connection to peer and ignore
            if (bytePacket.size() >= HANDSHAKE_SIZE) {
                String protocol = new String(bytePacket.read(1, 20));

                if (protocol.equals("BitTorrent protocol")) {
                    byte[] reserved = bytePacket.read(20, 28);
                    byte[] info_hash = bytePacket.read(28, 48);

                    if (Arrays.equals(info_hash, peer.getByteInfoHash())) {
                        byte[] peer_id = bytePacket.read(48, 68);

                        if (Arrays.equals(peer_id, Constants.getPeerId())) {
                            log.warn("Received handshake from ourselves, dropping connection.");
                            peer.close();

                        } else {
                            peer.setHandshakeReceived(true);
                            log.info("Received handshake from: " + peer.getPacketChannel().getRemoteSocketAddress().toString());
                            peerListener.handleHandshake(peer);

                        }

                    }

                    bytePacket.remove(0, HANDSHAKE_SIZE);
                    //log.info("Removing handshake bytes - rerunning decode: " + bytePacket);
                    if (bytePacket.size() > 0) {
                        decode(peer, bytePacket);
                    }
                }
            }
        } else {
            if (bytePacket.size() >= LEN_BITS) {
                byte[] lengthBytes = bytePacket.read(0, LEN_BITS);
                int msgLength = Convert.byteArrayToInt(lengthBytes);

                if (msgLength == 0 && bytePacket.size() == 4) {
                    log.info("Keep-alive");
                    return;
                    //todo keep-alive
                }

                /*if (bytePacket.size() < LEN_BITS + msgLength) {
                    log.info("Missing part of the message, at least (" + (msgLength - bytePacket.size()) + " bytes), saving for later.");
                    log.info("Header: " + printHeader(bytePacket));
                    peer.getReceiveBuffer().add(bytePacket);
                    return;
                } */

                if (bytePacket.size() >= PAYLOAD_STRT) {
                    byte msgType = bytePacket.getByte(LEN_BITS);
                    log.info("Packet size: " + bytePacket.size() + ", msg length: " + msgLength + ", msg type: " + msgType + " " + bytePacket);

                    if(msgType < 1 || msgType > 8  || msgLength > blockLength+9 || msgLength < 1 ) {
                        log.info("Unrecognized header: " + printHeader(bytePacket));
                        peer.getReceiveBuffer().clear();
                        return;
                    }
                    if (bytePacket.size() < LEN_BITS + msgLength) {
                        log.info("Missing part of the message, at least (" + (msgLength - bytePacket.size()) + " bytes), saving for later.");
                        log.info("Header: " + printHeader(bytePacket));
                        log.info("Message typ: " + msgType);
                        peer.getReceiveBuffer().add(bytePacket);
                        return;
                    }


                    switch (msgType) {
                        case CHOKE:
                            if (msgLength == 1) {
                                log.info(log.paintItRed("Choked Choked Choked Choked"));
                                peer.setPeerChoked(true);
                                peerListener.handleChoked(peer);
                                bytePacket.remove(0, LEN_BITS + msgLength);
                            }
                            break;
                        case UN_CHOKE:
                            if (msgLength == 1) {
                                log.info("Unchoked");
                                peer.setPeerChoked(false);
                                peerListener.handleUnchoked(peer);
                                bytePacket.remove(0, LEN_BITS + msgLength);
                            }
                            break;
                        case INTERESTED:
                            if (msgLength == 1) {
                                log.info("Interested");
                                peer.setPeerInterested(true);
                                bytePacket.remove(0, LEN_BITS + msgLength);
                            }
                            break;
                        case NOT_INTERESTED:
                            if (msgLength == 1) {
                                log.info("Not interested");
                                peer.setPeerInterested(false);
                                bytePacket.remove(0, LEN_BITS + msgLength);
                            }

                            break;
                        case HAVE:
                            if (msgLength == 5) {
                                log.info("Have");
                                if (bytePacket.size() >= PAYLOAD_STRT + LEN_BITS) {
                                    while (bytePacket.size() > 0) {
                                        if (bytePacket.size() >= PAYLOAD_STRT + LEN_BITS) {
                                            byte[] lenBytes = bytePacket.read(0, LEN_BITS);
                                            int len = Convert.byteArrayToInt(lenBytes);
                                            if (len == 5 && bytePacket.getByte(LEN_BITS) == HAVE) {
                                                int pieceIndex = bytePacket.readInt(PAYLOAD_STRT);
                                                peer.addAvailablePiece(pieceIndex);
                                                peerListener.handleHave(peer, pieceIndex);
                                                bytePacket.remove(0, LEN_BITS + len);
                                            } else {
                                                log.info(log.paintItRed("HAVE END: " + bytePacket));
                                                decode(peer, bytePacket);
                                            }
                                        }
                                    }

                                }
                            }
                            break;
                        case BITFIELD:
                            log.info("Bitfield");
                            byte[] bits = bytePacket.read(PAYLOAD_STRT, LEN_BITS + msgLength);
                            BitSet bitField = new BitSet(bits.length * 8);
                            for (int i = 0; i < bits.length * 8; i++) {
                                if ((bits[(i / 8)] & (1 << (7 - (i % 8)))) > 0) {
                                    bitField.set(i);
                                }
                            }
                            peer.setAvailablePieces(bitField);
                            peerListener.handleBitfield(peer, bitField);
                            bytePacket.remove(0, LEN_BITS + msgLength);
                            // }
                            break;
                        case REQUEST:
                            log.info("Request");
                            if (msgLength == 13) {
                                int pieceIndex = Convert.byteArrayToInt(bytePacket.read(PAYLOAD_STRT, LEN_BITS + msgLength - 2 * 4));
                                int blockOffset = Convert.byteArrayToInt(bytePacket.read(PAYLOAD_STRT, LEN_BITS + msgLength - 4));
                                int blockLength = Convert.byteArrayToInt(bytePacket.read(PAYLOAD_STRT, LEN_BITS + msgLength));
                                peerListener.handleRequest(peer, pieceIndex, blockOffset, blockLength);
                                bytePacket.remove(0, LEN_BITS + msgLength);
                            }
                            break;
                        case PIECE:
                            log.info("Piece");
                            if (msgLength > 9) {
                                int pieceIndex = bytePacket.readInt(PAYLOAD_STRT);
                                int blockOffset = bytePacket.readInt(PAYLOAD_STRT + 4);
                               // log.info("Piece header: " + Arrays.toString(bytePacket.read(0, PAYLOAD_STRT + 4 + 4)));
                                log.debug("Whole packet for "+ pieceIndex + ":"+blockOffset);
                                //System.out.println("DECODE PIECE: " + Arrays.toString(bytePacket.read(0, PAYLOAD_STRT + 4 + 5)));
                                BytePacket blockData = new BytePacket(bytePacket.read(PAYLOAD_STRT + 4 + 4,  LEN_BITS + msgLength ));

                                //    log.info("block: " + blockData);
                               //     log.info("left: " + Arrays.toString(bytePacket.read(LEN_BITS + msgLength, bytePacket.size())));
                                peerListener.handleBlockReceived(peer, pieceIndex, blockOffset, blockData);
                               // log.info("Bits left: " + Arrays.toString(bytePacket.read(LEN_BITS + msgLength, bytePacket.size())));
                               // log.info("Last bits: " + Arrays.toString(bytePacket.read(msgLength - LEN_BITS, LEN_BITS + msgLength)));
                                bytePacket.remove(0,  LEN_BITS +  msgLength);
                                //System.err.println("AFTER REMOVE " + bytePacket);
                              //  log.info("After remove: " + bytePacket);
                            }
                            break;
                        case CANCEL:
                            log.info("Cancel");
                            if (msgLength == 13) {
                                int pieceIndex = Convert.byteArrayToInt(bytePacket.read(PAYLOAD_STRT, LEN_BITS + msgLength - 2 * 4));
                                int blockOffset = Convert.byteArrayToInt(bytePacket.read(PAYLOAD_STRT, LEN_BITS + msgLength - 4));
                                int blockLength = Convert.byteArrayToInt(bytePacket.read(PAYLOAD_STRT, LEN_BITS + msgLength));
                                peerListener.handlePieceCancel(peer, pieceIndex, blockOffset, blockLength);
                                bytePacket.remove(0, LEN_BITS + msgLength);
                            }
                            break;
                        default:
                            log.info(log.paintItRed("WTF!!!! ") + " removing buffer.");
                            peer.getReceiveBuffer().clear();
                            //peer.getReceiveBuffer().add(bytePacket);
                           // System.exit(-1);
                            return;
                    }
                    if (bytePacket.size() >= LEN_BITS) {
                        if (peer.getPreviousBufferSize() != bytePacket.size()) {
                            log.info("Rerunning decode with remaining bytes: " + bytePacket);
                            peer.setPreviousBufferSize(bytePacket.size());
                             decode(peer, bytePacket);
                        } else {
                        //  log.info("Putting message into receive buffer");
                            //decode(peer,bytePacket);
                            peer.getReceiveBuffer().add(bytePacket);
                        }
                    }
                }

            }
        }
    }

    private String printHeader(BytePacket bytePacket) {
        if (bytePacket.size() >= PAYLOAD_STRT) {
            return Arrays.toString(bytePacket.read(0, PAYLOAD_STRT + 1));
        }
        return Arrays.toString(bytePacket.getBytes());
    }

    private BytePacket mergeBuffer(BytePacket bytePacket, Peer peer) {
        BytePacket buffer = new BytePacket();
        Iterator<BytePacket> iterator = peer.getReceiveBuffer().iterator();
        while (iterator.hasNext()) {
            BytePacket savedPacket = iterator.next();
            iterator.remove();
            buffer.append(savedPacket.getBytes());
        }
        buffer.append(bytePacket.getBytes());
        return buffer;
    }


}
