package messaging;

import torrent.Peer;
import util.BitSet;
import util.Constants;
import util.Convert;

/**
 * Inspired/based on
 * https://github.com/mpetazzoni/ttorrent/blob/master/src/main/java/com/turn/ttorrent/common/protocol/PeerMessage.java
 */
public abstract class PeerMessage {

    public static final int MESSAGE_LENGTH_FIELD_SIZE = 4;

    private final Type type;


    private final BytePacket bytePacket;

    public PeerMessage(Type type, BytePacket bytePacket) {
        this.type = type;
        this.bytePacket = bytePacket;
    }

    public Type getType() {
        return type;
    }

    public BytePacket getBytePacket() {
        return bytePacket;
    }


    public enum Type {
        KEEP_ALIVE(-1),
        CHOKE(0),
        UNCHOKE(1),
        INTERESTED(2),
        NOT_INTERESTED(3),
        HAVE(4),
        BITFIELD(5),
        REQUEST(6),
        PIECE(7),
        CANCEL(8),
        HANDSHAKE(100);

        private byte id;

        Type(int id) {
            this.id = (byte) id;
        }

        public boolean equals(byte c) {
            return this.id == c;
        }

        public byte getTypeByte() {
            return this.id;
        }

        public static Type get(byte c) {
            for (Type t : Type.values()) {
                if (t.equals(c)) {
                    return t;
                }
            }
            return null;
        }
    }




    public static class HandshakeMessage extends PeerMessage {

        public static BytePacket createHandshake(Peer peer) {
            BytePacket handshake = new BytePacket();
            handshake.append((byte) 19);
            handshake.append(Constants.BYTE_CHARSET.encode("BitTorrent protocol").array());
            //handshake.append("BitTorrent protocol".getBytes("US-ASCII"));
            handshake.append(new byte[8]);
            handshake.append(peer.getByteInfoHash());
            handshake.append(Constants.getPeerId());

            return handshake;
        }

        public HandshakeMessage(Type type, BytePacket bytePacket) {
            super(type, bytePacket);
        }

        public static HandshakeMessage create(Peer peer) {
            BytePacket bp = createHandshake(peer);
            return new HandshakeMessage(Type.HANDSHAKE, bp);
        }
    }

    /**
     * Keep alive : <len=0000>
     */
    public static class KeepAliveMessage extends PeerMessage {

        private static final int DEFAULT_LEN = 0;

        private KeepAliveMessage(Type type, BytePacket bytePacket) {
            super(type, bytePacket);
        }

        public static KeepAliveMessage create() {
            BytePacket bp = new BytePacket();
            bp.append(Convert.intToBytes(DEFAULT_LEN));
            return new KeepAliveMessage(Type.KEEP_ALIVE, bp);
        }
    }

    /**
     * choke: <len=0001><id=0>
     */
    public static class ChokeMessage extends PeerMessage {

        private static final int DEFAULT_LEN = 1;

        private ChokeMessage(Type type, BytePacket bytePacket) {
            super(type, bytePacket);
        }

        public static ChokeMessage create() {
            BytePacket bp = new BytePacket();
            bp.append(Convert.intToBytes(DEFAULT_LEN)).append((byte) 0);
            return new ChokeMessage(Type.CHOKE, bp);
        }
    }

    /**
     * unchoke: <len=0001><id=1>
     */
    public static class UnChokeMessage extends PeerMessage {

        private static final int DEFAULT_LEN = 1;

        private UnChokeMessage(Type type, BytePacket bytePacket) {
            super(type, bytePacket);
        }

        public static UnChokeMessage create() {
            BytePacket bp = new BytePacket();
            bp.append(Convert.intToBytes(DEFAULT_LEN)).append((byte) 1);
            return new UnChokeMessage(Type.UNCHOKE, bp);
        }

    }

    /**
     * interested: <len=0001><id=2>
     * The interested message is fixed-length and has no payload.
     *
     * @return
     */

    public static class InterestedMessage extends PeerMessage {

        private static final int DEFAULT_LEN = 1;

        private InterestedMessage(Type type, BytePacket bytePacket) {
            super(type, bytePacket);
        }

        public static InterestedMessage create() {
            BytePacket bp = new BytePacket();
            bp.append(Convert.intToBytes(DEFAULT_LEN)).append((byte) 2);
            return new InterestedMessage(Type.INTERESTED, bp);
        }
    }

    /**
     * not interested: <len=0001><id=3>
     * The not interested message is fixed-length and has no payload.
     *
     * @return
     */
    public static class NotInterestedMessage extends PeerMessage {

        private static final int DEFAULT_LEN = 1;

        private NotInterestedMessage(Type type, BytePacket bytePacket) {
            super(type, bytePacket);
        }

        public static NotInterestedMessage create() {
            BytePacket bp = new BytePacket();
            bp.append(Convert.intToBytes(DEFAULT_LEN)).append((byte) 3);
            return new NotInterestedMessage(Type.NOT_INTERESTED, bp);
        }
    }


    /**
     * have: <len=0005><id=4><piece index>
     * The payload is the zero-based index of a
     * piece that has just been successfully downloaded and verified via the
     * hash.
     */
    public static class HaveMessage extends PeerMessage {

        private static final int DEFAULT_LEN = 5;

        private HaveMessage(Type type, BytePacket bytePacket) {
            super(type, bytePacket);
        }


        public static HaveMessage create(int pieceIndex) {
            BytePacket bp = new BytePacket();
            bp.append(Convert.intToBytes(DEFAULT_LEN)).append((byte) 4).append
                    (Convert.intToBytes(pieceIndex));
            return new HaveMessage(Type.HAVE, bp);
        }
    }

    /**
     * bitfield: <len=0001+X><id=5><bitfield>
     * The bitfield message may only be sent immediately after the handshaking
     * sequence is completed, and before any other messages are sent. It is
     * optional, and need not be sent if a client has no pieces.
     * <p/>
     * The bitfield message is variable length, where X is the length of the
     * bitfield. The payload is a bitfield representing the pieces that have
     * been
     * successfully downloaded. The high bit in the first byte corresponds to
     * piece
     * index 0. Bits that are cleared indicated a missing piece, and set bits
     * indicate a valid and available piece. Spare bits at the end are set to
     * zero.
     * <p/>
     * A bitfield of the wrong length is considered an error. Clients should
     * drop
     * the connection if they receive bitfields that are not of the correct
     * size, or
     * if the bitfield has any of the spare bits set.
     *
     * @return
     */

    public static class BitFieldMessage extends PeerMessage {

        private static final int DEFAULT_LEN = 1;

        private BitFieldMessage(Type type, BytePacket bytePacket) {
            super(type, bytePacket);
        }

        public static BitFieldMessage create(BitSet availablePieces) {
            byte[] bitfield = new byte[availablePieces.size() / 8];
            for (int i = availablePieces.nextSetBit(0); i >= 0; i = availablePieces.nextSetBit(i + 1)) {
                bitfield[i / 8] |= 1 << (7 - (i % 8));
            }
            BytePacket bp = new BytePacket();
            bp.append(Convert.intToBytes(DEFAULT_LEN + bitfield.length));
            bp.append((byte) 5);
            bp.append(bitfield);
            return new BitFieldMessage(Type.BITFIELD, bp);
        }
    }


    /**
     * request: <len=0013><id=6><index><begin><length>
     * The request message is fixed length, and is used to request a block.
     * The payload contains the following information:
     * <p/>
     * index: integer specifying the zero-based piece index
     * begin: integer specifying the zero-based byte offset within the piece
     * length: integer specifying the requested length.
     */

    public static class RequestMessage extends PeerMessage {

        private static final int DEFAULT_LEN = 13;

        public static final int DEFAULT_BLOCK_SIZE = Constants.getBlockSize();


        private RequestMessage(Type type, BytePacket bytePacket) {
            super(type, bytePacket);
        }


        public static RequestMessage create(int pieceIndex, int blockOffset, int blockLength) {
            BytePacket bp = new BytePacket();
            bp.append(Convert.intToBytes(DEFAULT_LEN)).append((byte) 6);
            bp.append(Convert.intToBytes(pieceIndex));
            bp.append(Convert.intToBytes(blockOffset));
            bp.append(Convert.intToBytes(blockLength));
            return new RequestMessage(Type.REQUEST, bp);
        }
    }

    /**
     * piece: <len=0009+X><id=7><index><begin><block>
     * The piece message is variable length, where X is the length of the
     * block. The
     * payload contains the following information:
     * <p/>
     * index: integer specifying the zero-based piece index
     * begin: integer specifying the zero-based byte offset within the piece
     * block: block of data, which is a subset of the piece specified by index.
     */

    public static class PieceMessage extends PeerMessage {

        private static final int DEFAULT_LEN = 9;

        private PieceMessage(Type type, BytePacket bytePacket) {
            super(type, bytePacket);
        }

        public static PieceMessage create(int pieceIndex, int blockOffset, BytePacket pieceData) {
            BytePacket bp = new BytePacket();
            bp.append(Convert.intToBytes(9 + pieceData.size()));
            bp.append((byte) 7);
            bp.append(Convert.intTo2Bytes(pieceIndex));
            bp.append(Convert.intTo2Bytes(blockOffset));
            bp.append(pieceData.getBytes());
            return new PieceMessage(Type.PIECE, bp);
        }
    }


    /**
     * cancel: <len=0013><id=8><index><begin><length>
     * The cancel message is fixed length, and is used to cancel block requests.
     * The payload is identical to that of the "request" message. It is
     * typically used during "End Game"
     * (see the Algorithms section below).
     *
     * @return
     */
    public static class CancelMessage extends PeerMessage {

        private static final int DEFAULT_LEN = 9;

        private CancelMessage(Type type, BytePacket bytePacket) {
            super(type, bytePacket);
        }

        public static CancelMessage create(int pieceIndex, int blockOffset, int blockLength) {
            BytePacket bp = new BytePacket();
            bp.append(Convert.intToBytes(DEFAULT_LEN));
            bp.append((byte) 8);
            bp.append(Convert.intToBytes(pieceIndex));
            bp.append(Convert.intToBytes(blockOffset));
            bp.append(Convert.intToBytes(blockLength));
            return new CancelMessage(Type.CANCEL, bp);
        }
    }

}
