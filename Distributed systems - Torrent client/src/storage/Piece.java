package storage;

import messaging.BytePacket;
import util.BitSet;

public class Piece implements Comparable {

    private final int index;
    private final long offset;
    private final long length;
    private int nrOfBlocks;
    private final String hash;
    private BytePacket data;
    private volatile boolean valid;
    private int availabilityCount = 0;

    private BitSet completedBlocks;

    public Piece(int index, long offset, long length, int nrOfBlocks, String hash) {
        this.index = index;
        this.offset = offset;
        this.length = length;
        this.nrOfBlocks = nrOfBlocks;
        this.hash = hash;
        completedBlocks = new BitSet(nrOfBlocks);
    }

    public int getIndex() {
        return index;
    }

    public long getOffset() {
        return offset;
    }

    public long getLength() {
        return length;
    }

    public String getHash() {
        return hash;
    }

    public boolean available() {
        return availabilityCount > 0;
    }

    public void incrementAvailability() {
        availabilityCount++;
    }

    public int availability() {
        return availabilityCount;
    }

    public void decrementAvailability() {
        if(availabilityCount > 0) {
            availabilityCount--;
        }
    }

    @Override
    public String toString() {
        return "Piece{" +
                "index=" + index +
                ", offset=" + offset +
                ", length=" + length +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        Piece other = (Piece) o;
        if (this.availabilityCount != other.availabilityCount) {
            return this.availabilityCount < other.availabilityCount ? -1 : 1;
        }
        return this.index == other.index ? 0 :
                (this.index < other.index ? -1 : 1);
    }

    public boolean isComplete() {
        return completedBlocks.isAllSet();
    }


    public void setCompletedBlock(int completedBlock) {
        completedBlocks.set(completedBlock);
    }

    public boolean blockCompleted(int blockIndex) {
        return completedBlocks.get(blockIndex);
    }

    public int[] getUncompletedRequests() {
        return completedBlocks.clearedBits();
    }

    public void clearCompletedBlocks() {
        completedBlocks = new BitSet(nrOfBlocks);
    }
}
