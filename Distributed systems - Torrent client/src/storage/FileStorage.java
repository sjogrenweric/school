package storage;

import messaging.BytePacket;
import util.Constants;
import util.HaSha;
import util.Log;
import util.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;

import static java.io.File.separator;

public class FileStorage {

    private RandomAccessFile[] randomAccessFiles;
    private Log log = new Log(FileStorage.class);
    private BytePacket[][] blockBuffer;
    private final int blocksPerPiece;
    private final int pieceSize;
    private String fullStoragePath;

    public FileStorage(String path, String storageName, String[] fileNames, int pieceSize, int nrOfPieces, long[] fileLengths) throws FileNotFoundException {
        this.pieceSize = pieceSize;
        fullStoragePath = path + storageName;

        File storagePath = new File(fullStoragePath);
        //log.disable();
        log.info("Starting FileStorage...");
        if (!storagePath.exists()) {
            if (!storagePath.mkdirs()) {
                System.err.println("Could not create dirs: " + fullStoragePath);
            }
        }

        randomAccessFiles = new RandomAccessFile[fileNames.length];
        for (int i = 0; i < fileNames.length; i++) {
            String filePath = fullStoragePath + separator + fileNames[i];
            log.info("Attempting to open/create file(path): " + filePath);
            File file = new File(filePath);
            if(file.exists()) { //TODO - remove later when we can finish from where we left off....
                log.info("Deleting old file: " + filePath);
                file.delete();
            }
            randomAccessFiles[i] = new RandomAccessFile(filePath, "rwd");

        }
        //TODO dude, if piece size < 16kb ??
        blocksPerPiece = pieceSize / Constants.getBlockSize();
        log.info("Creating block buffer: " + nrOfPieces + " x " + blocksPerPiece);
        blockBuffer = new BytePacket[nrOfPieces][blocksPerPiece];
        for(int i = 0; i < nrOfPieces; i++) {
            for(int j = 0; j < blocksPerPiece; j++)
                blockBuffer[i][j] = null;
        }

    }

    public boolean write(int pieceIndex, int blockOffset, BytePacket blockData, String pieceHash) throws InvalidHashException, IOException {
        boolean pieceCompleted = true;
        blockBuffer[pieceIndex][blockOffset/Constants.getBlockSize()] = blockData;

        for (int i = 0; i < blocksPerPiece; i++) {
            if (blockBuffer[pieceIndex][i] == null) {
                pieceCompleted = false;
                break;
            }
        }
        if (pieceCompleted) {
            int fileIndex = calculateFileIndex(pieceIndex, blockOffset);
            boolean validHash = writePiece(pieceIndex, fileIndex, blockBuffer[pieceIndex], pieceHash);
            if(!validHash) {
                for(int j = 0; j < blocksPerPiece; j++) {
                    blockBuffer[pieceIndex][j] = null;
                }

                //pieceBlocks[pieceIndex]= 0;
                throw new InvalidHashException(pieceIndex);
            }
        }

            return pieceCompleted;
    }

    private void writeBlock(int pieceIndex, byte[] completePiece) throws IOException {
        randomAccessFiles[0].seek(pieceIndex * pieceSize);
        randomAccessFiles[0].write(completePiece);
    }

    private int calculateFileIndex(int pieceIndex, int blockOffset) {
        return 0;
    }

    private boolean writePiece(int pieceIndex, int fileIndex, BytePacket[] pieceBlocks, String correctPieceHash) throws IOException {
        byte[] completePiece = Utils.concatBytePackets(pieceBlocks);

        HaSha.newSha();
        HaSha.append(completePiece);

        byte[] pieceHash = HaSha.hashIt();
        boolean validHash = Arrays.equals(Constants.getByteCharset().encode(correctPieceHash).array(), pieceHash);


        if(validHash) {
         //   log.error("HASH VALID: " + pieceIndex);
        } else {
            log.error("HASH FAILED: " + pieceIndex);
        }

        writeBlock(pieceIndex, completePiece);

        return validHash;
    }

    public String getStoragePath() {
        return fullStoragePath;
    }


    public class InvalidHashException extends Throwable {
        public int getPieceIndex() {
            return pieceIndex;
        }

        private int pieceIndex;

        public InvalidHashException(int pieceIndex) {
            super();
            this.pieceIndex = pieceIndex;
        }
    }
}
