<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
<title>Upload File Request Page</title>
</head>
<body>
 
    <form method="POST" action="file" enctype="multipart/form-data">
        File to upload: <input type="file" name="file"><br />
        Filename: <input type="text" name="filename"><br /> <br />
        Token: <input type="text" name="token"><br /> <br />
        Filepath: <input type="text" name="filepath"><br /> <br />
        Username: <input type="text" name="username"><br /> <br />
        <input type="submit" value="Upload"> Press here to upload the file!
    </form>
     
</body>
</html>