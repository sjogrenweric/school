package authService;

/**
 * Project: drop
 * Package: fileService
 * User: c08esn c11ean
 * Date: 2/26/15
 * Time: 11:01 AM
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import shared.DropFile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import shared.BaseService;
import shared.RDSConnector;
import shared.Token;

import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.List;


@Controller
public class AuthService extends BaseService {

    @RequestMapping(value = "/login", method = RequestMethod.GET, produces = "text/html")
    @ResponseBody
    ResponseEntity<String> login(@RequestParam String username, @RequestParam String password, final HttpServletResponse response) {


        try {
            String token = RDSConnector.getInstance().login(username, password);
            if (token.equals("")) {
                return generateResponse(response, "Error no access.", HttpStatus.BAD_REQUEST);
            }

            return generateResponse(response, token, HttpStatus.OK);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return generateResponse(response, "Error", HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/createUser", method = RequestMethod.POST, produces = "text/html")
    @ResponseBody
    ResponseEntity<String> createUser(@RequestParam String username, @RequestParam String password, final HttpServletResponse response) {
        try {
            RDSConnector.getInstance().createUser(username, password);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return generateResponse(response, "Successfully created user: " + username, HttpStatus.OK);
    }


    @RequestMapping(value = "/getitems", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    ResponseEntity<String> getitems(@RequestParam String token, final HttpServletResponse response) {
        try {

            Token t = RDSConnector.getInstance().getToken(token);
            if (t != null && t.isValid()) {
                List<DropFile> files = RDSConnector.getInstance().getFiles(t.getUsername());
                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();
                return generateResponse(response, gson.toJson(files), HttpStatus.OK);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return generateResponse(response, "Failed.", HttpStatus.BAD_REQUEST);

    }

    @RequestMapping(value = "/verifyAction", method = RequestMethod.GET, produces = "text/html")
    @ResponseBody
    ResponseEntity<String> verifyAction(@RequestParam String token,
                                        @RequestParam String action,
                                        @RequestParam String filename,
                                        @RequestParam String username,
                                        @RequestParam String filepath,
                                        final HttpServletResponse response) {
        try {
            Token t = RDSConnector.getInstance().getToken(token);
            if (t != null && t.isValid()) {
                if (t.getUsername().equalsIgnoreCase(username)) {
                    DropFile d = RDSConnector.getInstance().getFile(username, filename, filepath);
                    switch (action) {
                        case "upload":
                            return generateResponse(response, "User allowed to upload.", HttpStatus.OK);
                        case "download":
                            //System.out.println(d.toString());
                            if (d != null && d.getPermission().read()) {
                                return generateResponse(response, d.getS3URL(), HttpStatus.OK);
                            } else {
                                return generateResponse(response, "Not allowed user permissions.", HttpStatus.BAD_REQUEST);
                            }

                        case "update":
                            if (d != null && d.getPermission().write()) {
                                return generateResponse(response, "User allowed to update", HttpStatus.OK);
                            } else {
                                return generateResponse(response, "Not allowed user permissions.", HttpStatus.BAD_REQUEST);
                            }

                        case "delete":
                            if (d != null && d.getPermission().readwrite()) {
                                return generateResponse(response, "User allowed to delete", HttpStatus.OK);
                            } else {
                                return generateResponse(response, "Not allowed user permissions.", HttpStatus.BAD_REQUEST);
                            }
                        default:
                            return generateResponse(response, "Unknown action", HttpStatus.BAD_REQUEST);
                    }
                } else {
                    return generateResponse(response, "Token username and username does not match.", HttpStatus.BAD_REQUEST);
                }
            } else {
                return generateResponse(response, "User token not vaild", HttpStatus.BAD_REQUEST);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return generateResponse(response, "Something went wrong...", HttpStatus.FORBIDDEN);
    }


    @RequestMapping(value = "/actionComplete", method = RequestMethod.POST, produces = "text/html")
    @ResponseBody
    ResponseEntity<String> actionComplete(@RequestParam String action,
                                          @RequestParam String meta,
                                          @RequestParam String filename,
                                          @RequestParam String username,
                                          @RequestParam String filepath,
                                          @RequestParam String version,
                                          @RequestParam String md5,
                                          final HttpServletResponse response) {

        switch (action) {
            case "upload":
                try {
                    RDSConnector.getInstance().addFile(new DropFile(username, filename, filepath, "rw", meta, Long.parseLong(version),md5));
                } catch (SQLException e) {
                    return generateResponse(response, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
                }
                return generateResponse(response, "File added successfully.", HttpStatus.OK);
            case "update":
                try {
                    RDSConnector.getInstance().deleteFile(RDSConnector.getInstance().getFile(username, filename, filepath));
                } catch (SQLException e) {
                    return generateResponse(response, "Could not delete old file.", HttpStatus.INTERNAL_SERVER_ERROR);
                }
                try {
                    RDSConnector.getInstance().addFile(new DropFile(username, filename, filepath, "rw", meta, Long.parseLong(version),md5));
                } catch (SQLException e) {
                    return generateResponse(response, "Could not add file.", HttpStatus.INTERNAL_SERVER_ERROR);
                }
                return generateResponse(response, "File updated.", HttpStatus.OK);
            case "delete":
                //TODO evential flag something useful stuff
                try {
                    RDSConnector.getInstance().deleteFile(RDSConnector.getInstance().getFile(username, filename, filepath));
                } catch (SQLException e) {
                    return generateResponse(response, "Could not delete file.", HttpStatus.INTERNAL_SERVER_ERROR);
                }
                return generateResponse(response, "File deleted successfully.", HttpStatus.OK);
            default:
                break;
        }
        return generateResponse(response, "Something went wrong...", HttpStatus.FORBIDDEN);
    }
}
