package fileService;

import java.io.File;

/**
 * Created by c11epm on 3/11/15.
 */
public class S3file {
    private File fileToUpload;
    private String bucket;
    private String key;
    private String filename;

    public S3file(File fileToUpload, String bucket, String key, String filename) {
        this.fileToUpload = fileToUpload;
        this.bucket = bucket;
        this.key = key;
        this.filename = filename;
    }

    public File getFileToUpload() {
        return fileToUpload;
    }

    public String getBucket() {
        return bucket;
    }

    public String getKey() {
        return key;
    }

    public String getFilename() {
        return filename;
    }

    @Override
    public String toString() {
        return "S3file{" +
                "fileToUpload=" + fileToUpload +
                ", bucket='" + bucket + '\'' +
                ", key='" + key + '\'' +
                '}';
    }
}
