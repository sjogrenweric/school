package fileService;

/**
 * Project: drop
 * Package: fileService
 * User: c08esn c11ean
 * Date: 2/26/15
 * Time: 11:01 AM
 */


import shared.RDSConnector;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import shared.BaseService;
import shared.PropertyReader;
import shared.RestTemplateFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

@Configuration
@Controller
@ComponentScan
@RequestMapping(value = "/file")
public class Application extends BaseService {

    @Value("${accessKey}")
    String accessKey;
    @Value("${secretKey}")
    String secretKey;
    //@Value("${bucketName}")
    String bucketName = PropertyReader.getProperty("AwsCredentials.properties", "bucketName");

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    @ResponseBody
    ResponseEntity<String> upload(@RequestParam String filepath,
                                  @RequestParam String username,
                                  @RequestParam String token,
                                  @RequestParam String filename,
                                  @RequestParam String md5,
                                  @RequestParam Long version,
                                  @RequestParam MultipartFile file, final HttpServletResponse response) {
        String url = PropertyReader.getProperty("properties/servers.properties", "authService") + "/verifyAction?token=" + token + "&action=upload&filename="
                + filename + "&username=" + username + "&filepath=" + filepath + "&version=" + version + "&md5="+md5;

        RestTemplate template = RestTemplateFactory.getRestTemplateNoErrorCheck();
        ResponseEntity<String> r1 = template.getForEntity(url, String.class);

        File uploadedFile = null;

        if (r1.getStatusCode().equals(HttpStatus.OK)) {
            try {
                uploadedFile = receiveFile(filename, file);
            } catch (FileNotFoundException e) {
                return generateResponse(response, "File not created", HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (IOException e) {
                return generateResponse(response, "Could not write data to file", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return generateResponse(response, r1.getBody(), HttpStatus.BAD_REQUEST);
        }

        //TODO not save directly to S3 compress and put in uploadQueue instead.
        String key = UUID.randomUUID().toString();

        if (!uploadToS3(uploadedFile, bucketName, key, filename)) {
            return generateResponse(response, "Upload queue overloaded, upload dropped.", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String postURL = PropertyReader.getProperty("properties/servers.properties", "authService") + "/actionComplete?action=upload&" +
                "meta=" + key + "&filename=" + filename + "&username=" + username + "&filepath=" + filepath + "&version=" + version+ "&md5="+md5;
        ResponseEntity<String> r2 = template.postForEntity(postURL, "", String.class, new HashMap<String, String>());

        if (r2.getStatusCode().equals(HttpStatus.OK)) {
            return generateResponse(response, "File uploaded", HttpStatus.OK);
        } else {
            return generateResponse(response, r2.getBody(), r2.getStatusCode());
        }

    }

    @RequestMapping(method = RequestMethod.GET, produces = "text/html")
    @ResponseBody
    ResponseEntity<String> download(@RequestParam String filepath,
                                    @RequestParam String username,
                                    @RequestParam String token,
                                    @RequestParam String filename,
                                    final HttpServletResponse response) {
        String url = PropertyReader.getProperty("properties/servers.properties", "authService") + "/verifyAction?token=" + token +
                "&username=" + username + "&filepath=" + filepath + "&filename=" + filename + "&action=download";
        RestTemplate template = RestTemplateFactory.getRestTemplateNoErrorCheck();
        ResponseEntity<String> r1 = template.getForEntity(url, String.class);

        return generateResponse(response, r1.getBody(), r1.getStatusCode());
    }

    @RequestMapping(method = RequestMethod.DELETE, produces = "text/html")
    @ResponseBody
    ResponseEntity<String> delete(@RequestParam String filepath,
                                  @RequestParam String username,
                                  @RequestParam String token,
                                  @RequestParam String filename,
                                  final HttpServletResponse response) {
        String url = PropertyReader.getProperty("properties/servers.properties", "authService") + "/verifyAction?token=" + token +
                "&username=" + username + "&filepath=" + filepath + "&filename=" + filename + "&action=delete";

        RestTemplate template = RestTemplateFactory.getRestTemplateNoErrorCheck();
        ResponseEntity<String> r1 = template.getForEntity(url, String.class);

        if (r1.getStatusCode().equals(HttpStatus.OK)) {
            try {
                deleteS3File(bucketName, RDSConnector.getInstance().getFile(username, filename, filepath).getS3URL());

                String postURL = PropertyReader.getProperty("properties/servers.properties", "authService") + "/actionComplete?action=delete&" +
                        "meta=link-toS3&filename=" + filename + "&username=" + username + "&filepath=" + filepath + "&version=0" + "&md5=0";

                ResponseEntity<String> r2 = template.postForEntity(postURL, "", String.class, new HashMap<String, String>());

                if (r2.getStatusCode().equals(HttpStatus.OK)) {
                    return generateResponse(response, r2.getBody(), HttpStatus.OK);
                } else {
                    return generateResponse(response, r2.getBody(), r2.getStatusCode());
                }
            } catch (SQLException e) {
                return generateResponse(response, "Could not delete file: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            }

        } else {
            return generateResponse(response, r1.getBody(), r1.getStatusCode());
        }


    }

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    @ResponseBody
    ResponseEntity<String> put(@RequestParam String filepath,
                               @RequestParam String username,
                               @RequestParam String token,
                               @RequestParam String filename,
                               @RequestParam MultipartFile file, final HttpServletResponse response) {

        String url = PropertyReader.getProperty("properties/servers.properties", "authService") + "/verifyAction?token=" + token + "&action=update&filename="
                + filename + "&username=" + username + "&filepath=" + filepath;

        RestTemplate template = RestTemplateFactory.getRestTemplateNoErrorCheck();
        ResponseEntity<String> r1 = template.getForEntity(url, String.class);

        File uploadedFile = null;

        if (r1.getStatusCode().equals(HttpStatus.OK)) {
            try {
                uploadedFile = receiveFile(filename, file);
            } catch (FileNotFoundException e) {
                return generateResponse(response, "File not updated", HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (IOException e) {
                return generateResponse(response, "Could not read sent data", HttpStatus.INTERNAL_SERVER_ERROR);
            }

        } else {
            return generateResponse(response, r1.getBody(), HttpStatus.BAD_REQUEST);
        }

        try {
            deleteS3File(bucketName, RDSConnector.getInstance().getFile(username, filename, filepath).getS3URL());
        } catch (SQLException e) {
            return generateResponse(response, "Could not find the old file S3 key, update aborted.", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String key = UUID.randomUUID().toString();
        if (!uploadToS3(uploadedFile, bucketName, key, filename)) {
            return generateResponse(response, "Upload queue overloaded, upload dropped.", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String postURL = PropertyReader.getProperty("properties/servers.properties", "authService") + "/actionComplete?action=update&" +
                "meta=" + key + "&filename=" + filename + "&username=" + username + "&filepath=" + filepath;
        ResponseEntity<String> r2 = template.postForEntity(postURL, "", String.class, new HashMap<String, String>());

        if (r2.getStatusCode().equals(HttpStatus.OK)) {
            return generateResponse(response, "File uploaded", HttpStatus.OK);
        } else {
            return generateResponse(response, r2.getBody(), r2.getStatusCode());
        }
    }

    private File receiveFile(String filename, MultipartFile file) throws IOException {
        File dir = new File("uploadedFiles");
        if (!dir.exists()) {
            dir.mkdir();
        }
        File serverFile = new File("uploadedFiles/" + filename);
        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
        stream.write(file.getBytes());
        stream.close();
        return serverFile;
    }

    private boolean uploadToS3(File toUpload, String bucket, String key, String filename) {
        S3file s3file = new S3file(toUpload, bucket, key, filename);
        return S3FileService.getInstance().uploadFile(s3file);
    }

    private void deleteS3File(String bucket, String key) {
        S3FileService.getInstance().deleteFileOnS3(bucket, key);
    }
}
