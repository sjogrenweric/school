package fileService;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by c11epm on 3/11/15.
 */
public class S3FileService {
    LinkedBlockingQueue<S3file> uploadQueue = new LinkedBlockingQueue<>(500);

    private static S3FileService instance = null;

    protected S3FileService() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        S3file file = uploadQueue.take();
                        File compressedFile = compressS3File(file);

                        consumeUpload(compressedFile, file.getBucket(), file.getKey());

                        removeTempFiles(file);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public static S3FileService getInstance() {
        if (instance == null) {
            instance = new S3FileService();
        }
        return instance;
    }

    public boolean uploadFile(S3file file) {
        return uploadQueue.offer(file);
    }

    public void deleteFileOnS3(String bucket, String key) {
        AmazonS3 s3 = new AmazonS3Client();
        Region region = Region.getRegion(Regions.US_EAST_1);

        s3.setRegion(region);
        s3.deleteObject(bucket, key);
    }

    private File compressS3File(S3file toCompress) {
        byte[] buffer = new byte[1024];

        File compressed = null;
        try {
            File dir = new File("compressedFiles");
            if (!dir.exists()) {
                dir.mkdir();
            }
            compressed = new File("compressedFiles/" + toCompress.getFilename() + ".zip");

            FileOutputStream fos = new FileOutputStream(compressed);
            ZipOutputStream zos = new ZipOutputStream(fos);
            ZipEntry entry = new ZipEntry(toCompress.getFilename());
            zos.putNextEntry(entry);

            FileInputStream fis = new FileInputStream(toCompress.getFileToUpload());

            int len;
            while ((len = fis.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }
            fis.close();
            zos.closeEntry();

            zos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return compressed;
    }

    private void consumeUpload(File toUpload, String bucket, String key) {

        AmazonS3 s3 = new AmazonS3Client();
        Region region = Region.getRegion(Regions.US_EAST_1);

        s3.setRegion(region);
        s3.putObject(new PutObjectRequest(bucket, key, toUpload).withCannedAcl(CannedAccessControlList.PublicRead));
    }

    private void removeTempFiles(S3file file) {
        if (!new File("uploadedFiles/" + file.getFilename()).delete()) {
            System.err.println("[ERROR] " + "uploadedFiles/" + file.getFilename() + " could not be deleted.");
        }
        if (!new File("compressedFiles/" + file.getFilename() + ".zip").delete()) {
            System.err.println("[ERROR] " + "compressedFiles/" + file.getFilename() + ".zip" + " could not be deleted.");
        }
    }

}
