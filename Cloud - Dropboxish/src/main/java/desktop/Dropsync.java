package desktop;


import shared.DropFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Project: drop
 * Package: desktop
 * User: c08esn c11epm
 * Date: 3/9/15
 * Time: 3:39 PM
 */
public class Dropsync {

    FileManager fileManager;
    Utils utils;
    public static String token = "NULL";
    private Object files;
    public static String username = "NULL";
    Properties properties;
    TimerTask fileLoader;
    Properties serverproperties;

    public Dropsync() {
        setup();

    }

    private void setup() {
        utils = new Utils();
        properties = utils.loadProperties("src/main/resources/properties/settings.properties");
        serverproperties = utils.loadProperties("src/main/resources/properties/servers.properties");
        try {
            login(false);
            fileManager = new FileManager((properties.getProperty("directory")));
            fileLoader = new FileLoader(token, fileManager);

            sync();
            prompt();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sync() {
        Timer timer = new Timer(true);
        timer.scheduleAtFixedRate(fileLoader, 0, 30 * 1000);
    }


    private void login(boolean manual) throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        String requestParams = "";
        if (properties.getProperty("username").equals("") || properties.getProperty("password").equals("") || manual) {
            System.out.print("Username: ");
            String username = bufferRead.readLine();
            this.username = username;
            System.out.print("Password: ");
            String password = bufferRead.readLine();
            requestParams = "username=" + URLEncoder.encode(username, "UTF-8") + "&password=" + URLEncoder.encode(password, "UTF-8");
        } else {
            this.username = properties.getProperty("username");
            requestParams = "username=" + URLEncoder.encode(properties.getProperty("username"), "UTF-8") + "&password=" + URLEncoder.encode(properties.getProperty("password"), "UTF-8");
        }
        String response = utils.sendGet("/login", requestParams);
        if (response.contains("400")) {
            System.err.println("Wrong password or username");
            login(true);
        }
        token = response;
        System.out.println(token);
        System.out.println("Login successful");

    }


    public void prompt() {
        try {
            do {
                BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
                printMenu();
                int command = Integer.parseInt(bufferRead.readLine());
                switch (command) {
                    case 1:
                        changeFile(bufferRead, true);
                        break;
                    case 2:
                        changeFile(bufferRead, false);
                        break;
                    case 3:
                        return;
                    default:
                        break;

                }
            } while (true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void changeFile(BufferedReader bufferRead, boolean delete) throws IOException {
        System.out.print("Enter path and filename (ex: drop/dir1/hello.txt): ");
        String filename = bufferRead.readLine();
        DropFile updateFile = fileManager.getLocalfiles().get(filename);
        if (updateFile != null) {
            System.out.println("Did you mean this file?: " + updateFile + " \ny/n");
            String res = bufferRead.readLine();
            executeChange(updateFile, res, delete);
        } else {
            System.out.println("No such file");
        }
    }

    private void executeChange(DropFile updateFile, String res, boolean delete) throws IOException {
        if (res.equals("y") && delete) {
            fileManager.deleteFile(updateFile, true);
        } else if (res.equals("y") && !delete) {
            fileManager.deleteFile(updateFile, false);
            Utils.sendFile(updateFile);
        }
    }


    private void printMenu() {
        System.out.println("-----MENU-----");
        System.out.println("1: Delete file");
        System.out.println("2: Update file");
        System.out.println("3: Exit");
    }
}
