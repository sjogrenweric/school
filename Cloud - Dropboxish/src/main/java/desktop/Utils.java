package desktop;

import shared.DropFile;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import shared.PropertyReader;
import shared.RestTemplateFactory;

import java.io.*;
import java.util.Properties;

/**
 * Project: drop
 * Package: desktop
 * User: c08esn c11epm
 * Date: 3/9/15
 * Time: 3:43 PM
 */
public class Utils {

    public static Properties loadProperties(String file) {
        Properties properties = new Properties();
        try {
            InputStream is = new FileInputStream(file);
            properties.load(is);

        } catch (FileNotFoundException e) {
            throw new RuntimeException("Error: propertyfile: " + file + " was not found.");
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        return properties;
    }

    public static String sendGet(String handle, String requestParams) throws IOException {

        String url = PropertyReader.getProperty("properties/servers.properties", "authService") + handle + "?" + requestParams;//TODO fix better
        RestTemplate template = RestTemplateFactory.getRestTemplateNoErrorCheck();
        ResponseEntity<String> r1 = template.getForEntity(url, String.class);
        if (r1.getStatusCode().equals(HttpStatus.OK)) {
            return r1.getBody();
        }
        System.out.println(r1.getBody());
        return r1.getStatusCode().toString();


    }

    public static String sendDelete(String handle, String requestParams) throws IOException {
        String url = PropertyReader.getProperty("properties/servers.properties", "dropService") + handle + "?" + requestParams;//TODO fix better
        RestTemplate template = RestTemplateFactory.getRestTemplateNoErrorCheck();
        System.out.println("Sending delete to: " + url);

        ResponseEntity<String> exchange = template.exchange(
                url,
                HttpMethod.DELETE,
                new HttpEntity<String>("some sample body sent along the DELETE request"),
                String.class);
        return exchange.getStatusCode().toString() + exchange.getBody();
    }

    public static String sendUpdate(String handle, String requestParams, DropFile df) {
        try {
            String url = PropertyReader.getProperty("properties/servers.properties", "dropService") + handle + "?" + requestParams;//TODO fix better
            System.out.println("Sending update to: " + url);
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(url);
            org.apache.http.HttpEntity entity = MultipartEntityBuilder.create().addBinaryBody("file", new File(df.getFilePath())).build();

            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            //org.apache.commons.io.IOUtils.copy(response.getEntity().getContent(), System.out);
            return response.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "ERROR";
    }


    public static String sendFile(DropFile df) throws IOException {
        System.out.println("Uploading : " + df);
        String url = PropertyReader.getProperty("properties/servers.properties", "dropService")
                + "/file?username=" + Dropsync.username
                + "&filepath=" + df.getFilePath()
                + "&filename=" + df.getFileName()
                + "&token=" + Dropsync.token
                + "&version=" + df.getVersion()
                + "&md5=" + df.getMd5();
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(url);
        org.apache.http.HttpEntity entity = MultipartEntityBuilder.create().addBinaryBody("file", new File(df.getFilePath())).build();

        post.setEntity(entity);
        HttpResponse response = client.execute(post);

        return response.toString();
    }
}
