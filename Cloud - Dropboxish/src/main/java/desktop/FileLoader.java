package desktop;


import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import shared.DropFile;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.Properties;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Project: drop
 * Package: desktop
 * User: c08esn c11epm
 * Date: 3/10/15
 * Time: 11:48 AM
 */
public class FileLoader extends TimerTask implements Runnable {

    LinkedBlockingDeque<DropFile> downloadQueue;
    LinkedBlockingDeque<DropFile> uploadQueue;
    private String token;
    String access_key_id;
    String secret_access_key;
    BasicAWSCredentials awsCreds;
    AmazonS3 s3Client;
    String bucketName;
    Properties properties;
    Properties serverProps;
    FileManager fileManager;


    public FileLoader(String token, FileManager fileManager) {
        Utils utils = new Utils();
        properties = utils.loadProperties("src/main/resources/AwsCredentials.properties");
        serverProps = utils.loadProperties("src/main/resources/properties/servers.properties");
        access_key_id = properties.getProperty("accessKey");
        secret_access_key = properties.getProperty("secretKey");
        bucketName = properties.getProperty("bucketName");
        this.token = token;
        this.fileManager = fileManager;

        awsCreds = new BasicAWSCredentials(access_key_id, secret_access_key);
        s3Client = new AmazonS3Client(awsCreds);
        Region useast = Region.getRegion(Regions.US_EAST_1);
        s3Client.setRegion(useast);


    }

    private void updateFileInformation() {
        try {
            fileManager.update();
            downloadQueue = fileManager.getDownloadQueue();
            uploadQueue = fileManager.getUploadQueue();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void run() {

        System.out.println("Automatic sync starting...");
        updateFileInformation();
        while (downloadQueue.peek() != null) {
            try {
                DropFile df = downloadQueue.pop();
                System.out.println("Downloading : " + df);

                S3Object object = s3Client.getObject(new GetObjectRequest(bucketName, df.getS3URL()));
                saveToFile(object.getObjectContent(), df.getFilePath());
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        while (uploadQueue.peek() != null) {
            try {
                DropFile df = uploadQueue.pop();
                sendFile(df);
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
        System.out.println("Automatic sync completed.");
    }

    private void sendFile(DropFile df) throws IOException {
        Utils.sendFile(df);
    }


    private static void saveToFile(InputStream input, String filepath) throws IOException {
        String[] split = filepath.split("/");
        String dirs = "";

        for (int i = 0; i < split.length - 1; i++) {
            dirs += split[i] + "/";
        }

        new File(dirs).mkdirs();

        File zip = new File(filepath + ".zip");
        OutputStream outputStream = new FileOutputStream(zip);
        IOUtils.copy(input, outputStream);
        input.close();
        outputStream.close();

        uncompressFile(zip, filepath);


        System.out.println("Download complete!");

    }

    private static void uncompressFile(File zip, String filepath) throws IOException {
        ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(zip));
        ZipEntry entry = zipInputStream.getNextEntry();

        byte[] buffer = new byte[1024];

        if (entry != null) {
            //String filename = entry.getName();
            File uncompressed = new File(filepath);

            FileOutputStream fos = new FileOutputStream(uncompressed);

            int len;
            while ((len = zipInputStream.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }

            fos.close();
            zipInputStream.closeEntry();
            zipInputStream.close();

            if (!zip.delete()) {
                System.err.println("[ERROR] Zipfile not deleted!");
            }
        } else {
            System.err.println("[INFO] File already downloaded.");
        }
    }


}
