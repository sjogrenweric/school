package desktop;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by c11epm on 3/12/15.
 */
public class FileHasher {

    private static FileHasher hasher = null;

    protected FileHasher() {
    }

    public static FileHasher getInstance() {
        if (hasher == null) {
            hasher = new FileHasher();
        }
        return hasher;
    }

    public String getMd5HashForFile(File file) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        String md5 = DigestUtils.md5Hex(fis);
        fis.close();
        return md5;
    }
}


