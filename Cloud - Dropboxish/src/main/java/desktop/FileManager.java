package desktop;

import com.google.gson.*;
import shared.DropFile;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Project: drop
 * Package: desktop
 * User: c08esn c11epm
 * Date: 3/9/15
 * Time: 3:48 PM
 */
public class FileManager {

    String rootdir = "drop";

    HashMap<String, DropFile> localfiles;
    HashMap<String, DropFile> remotefiles;
    LinkedBlockingDeque<DropFile> downloadQueue;
    LinkedBlockingDeque<DropFile> uploadQueue;
    FileHasher fileHasher;


    public FileManager(String rootdir) {
        this.rootdir = rootdir;
        fileHasher = FileHasher.getInstance();

    }

    public void update() throws IOException {
        localfiles = new HashMap<String, DropFile>();
        remotefiles = new HashMap<String, DropFile>();
        filesInDirectory(rootdir);
        parseDbFiles();
        createJobs();
    }


    public void filesInDirectory(String directory) throws IOException {
        File dir = new File(directory);
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                if (child.isFile() && !child.getName().contains("~")) {
                    DropFile df = new DropFile(Dropsync.username,
                            child.getName(), child.getPath(),
                            "rw",
                            "NULL",
                            child.lastModified(),
                            fileHasher.getMd5HashForFile(new File(child.getPath())));


                    localfiles.put(child.getPath(), df);
                }
                if (child.isDirectory()) {
                    filesInDirectory(directory + "/" + child.getName());
                }

            }
        }

    }

    public void parseDbFiles() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonElement jelem = gson.fromJson(getDbFiles(), JsonElement.class);
        JsonArray jarr = jelem.getAsJsonArray();
        for (int i = 0; i < jarr.size(); i++) {
            JsonObject jo = jarr.get(i).getAsJsonObject();
            DropFile df = new DropFile(Dropsync.username,
                    jo.get("fileName").getAsString(),
                    jo.get("filePath").getAsString(),
                    jo.get("permission").getAsString(),
                    "NULL",
                    jo.get("version").getAsLong(),
                    jo.get("md5").getAsString());

            df = getS3Link(df);
            remotefiles.put(jo.get("filePath").getAsString(), df);
        }
        //  printfiles();
    }

    public String getDbFiles() {
        String response = "ERROR";
        try {
            String requestParams = null;
            requestParams = "token=" + URLEncoder.encode(Dropsync.token, "UTF-8");
            response = Utils.sendGet("/getitems", requestParams);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    private DropFile getS3Link(DropFile df) {
        String requestParams = null;

        try {
            requestParams = "token=" + Dropsync.token
                    + "&username=" + Dropsync.username
                    + "&filename=" + df.getFileName()
                    + "&filepath=" + df.getFilePath();
            String response = Utils.sendGet("/file", requestParams);
            df.setS3URL(response);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return df;
    }

    public void createJobs() {
        downloadQueue = new LinkedBlockingDeque();
        uploadQueue = new LinkedBlockingDeque();
        for (Map.Entry<String, DropFile> file : localfiles.entrySet()) {
            if (!remotefiles.containsKey(file.getKey())) {
                uploadQueue.add(file.getValue());
            } else if (!file.getValue().getMd5().equals(remotefiles.get(file.getKey()).getMd5())) {
                if (file.getValue().getVersion() < remotefiles.get(file.getKey()).getVersion()) {
                    downloadQueue.add(remotefiles.get(file.getKey()));
                }
            }
        }

        for (Map.Entry<String, DropFile> file : remotefiles.entrySet()) {

            if (!localfiles.containsKey(file.getKey())) {
                downloadQueue.add(file.getValue());
            } else if (!file.getValue().getMd5().equals(localfiles.get(file.getKey()).getMd5())) {
                if (file.getValue().getVersion() < localfiles.get(file.getKey()).getVersion()) {
                    try {
                        deleteFile(localfiles.get(file.getKey()), false);
                        Utils.sendFile(localfiles.get(file.getKey()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

        }
    }

    public void deleteFile(DropFile delFile, boolean delete) {
        String request = "username=" + Dropsync.username
                + "&filepath=" + delFile.getFilePath()
                + "&filename=" + delFile.getFileName()
                + "&token=" + Dropsync.token;
        String handle = "/file";
        try {
            String response = Utils.sendDelete(handle, request);
            System.out.println(response);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (delete) {
            File file = new File(delFile.getFilePath());
            if (file.delete()) {
                System.out.println(delFile.getFilePath() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }
        }

    }


    public LinkedBlockingDeque<DropFile> getDownloadQueue() {
        return downloadQueue;
    }

    public LinkedBlockingDeque<DropFile> getUploadQueue() {
        return uploadQueue;
    }

    public HashMap<String, DropFile> getLocalfiles() {
        return localfiles;
    }

    private void printfiles() {
        System.out.println("local files:");
        for (Map.Entry<String, DropFile> file : localfiles.entrySet()) {
            System.out.println(file.getKey() + " : " + file.getValue());
        }
        System.out.println("Remote files:");
        for (Map.Entry<String, DropFile> file : remotefiles.entrySet()) {
            System.out.println(file.getKey() + " : " + file.getValue());
        }
    }


}
