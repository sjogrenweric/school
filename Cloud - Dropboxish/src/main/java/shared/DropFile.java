package shared;

import com.google.gson.annotations.Expose;

/**
 * Project: drop
 * Package: fileService
 * User: c08esn c11epm
 * Date: 3/6/15
 * Time: 1:32 PM
 */
public class DropFile {

    String userName;
    @Expose
    String fileName;
    @Expose
    String filePath;
    @Expose
    String permission;
    Permission permissionobj;

    String S3URL;
    @Expose
    long version;
    @Expose
    String md5;



    public DropFile(String userName, String fileName, String filePath, String permission, String s3url, long version, String md5) {
        this.userName = userName;
        this.fileName = fileName;
        this.filePath = filePath;
        this.S3URL = s3url;
        this.version = version;
        this.md5 = md5;
        this.permissionobj = new Permission(permission);
        this.permission = permission;
    }

    public String getUserName() {
        return userName;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public Permission getPermission() {
        return permissionobj;
    }

    public String getS3URL() {
        return S3URL;
    }

    public void setS3URL(String s3URL) {
        S3URL = s3URL;
    }

    public long getVersion() {
        return version;
    }

    public String getMd5() {
        return md5;
    }

    @Override
    public String toString() {
        return "DropFile{" +
                "userName='" + userName + '\'' +
                ", fileName='" + fileName + '\'' +
                ", filePath='" + filePath + '\'' +
                ", permission='" + permission + '\'' +
                ", permissionobj=" + permissionobj +
                ", S3URL='" + S3URL + '\'' +
                ", version=" + version +
                ", md5='" + md5 + '\'' +
                '}';
    }
}
