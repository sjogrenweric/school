package shared;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Properties;

/**
 * Project: PriceRunner
 * Package: pricerunner.amazonDB
 * User: c08esn c11ean
 * Date: 2/3/15
 * Time: 3:04 PM
 */
public class RDSConnector {
    Connection conn;

    private static RDSConnector dbhandler = null;

    protected RDSConnector() {
    }

    public static RDSConnector getInstance() {
        if (dbhandler == null) {
            dbhandler = new RDSConnector();
            dbhandler.connect();
        }
        return dbhandler;
    }


    /**
     * Reads a properties file
     *
     * @return Properties object
     */
    private Properties loadProperties(String file) {
        Properties properties = new Properties();
        try {
            InputStream is = new FileInputStream(file);
            properties.load(is);
            Enumeration enuKeys = properties.keys();
            while (enuKeys.hasMoreElements()) {
                String key = (String) enuKeys.nextElement();
                String value = properties.getProperty(key);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Error: propertyfile: " + file + " was not found.");
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }


        return properties;
    }

    public void connect() {
        try {
            Properties props = loadProperties("src/main/resources/properties/db.properties");
            conn = DriverManager.getConnection(props.getProperty("url"), props.getProperty("user"), props.getProperty("password"));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void createUser(String username, String password) throws SQLException {
        PreparedStatement statement = conn.prepareStatement("INSERT INTO users (user_name, password) VALUES (?,?)");
        statement.setString(1, username);
        statement.setString(2, password);
        statement.executeUpdate();
    }

    public String login(String username, String password) throws SQLException {
        PreparedStatement statement = conn.prepareStatement("SELECT * FROM users WHERE user_name=? AND password=?");
        statement.setString(1, username);
        statement.setString(2, password);
        ResultSet res = statement.executeQuery();
        while (res.next()) {
            if (res.getString("user_name").equals(username) && res.getString("password").equals(password)) {
                PreparedStatement tokenstate = conn.prepareStatement("SELECT * FROM tokens WHERE user_name=?");
                tokenstate.setString(1, username);
                ResultSet res2 = tokenstate.executeQuery();
                if (res2.next()) {
                    if(res2.getLong("expiration_time")> System.currentTimeMillis()) {
                        return res2.getString("token");
                    } else{
                        Token t = new Token(username);
                        newToken(t);
                        return t.getToken().toString();
                }
            }
        }

    }

    return "";
}

    public void newToken(Token token) throws SQLException {
        PreparedStatement removestate = conn.prepareStatement("DELETE FROM tokens WHERE user_name=?");
        removestate.setString(1, token.getUsername());
        removestate.executeUpdate();
        PreparedStatement statement = conn.prepareStatement("INSERT INTO tokens (token, expiration_time, user_name) VALUES (?,?,?)");
        statement.setString(1, token.getToken().toString());
        statement.setLong(2, token.getExpirationTime());
        statement.setString(3, token.getUsername());
        statement.executeUpdate();

    }

    public Token getToken(String token) throws SQLException {
        Token retreiveToken = null;
        PreparedStatement statement = conn.prepareStatement("SELECT * FROM tokens WHERE token=?");
        statement.setString(1, token);
        ResultSet res = statement.executeQuery();
        while (res.next()) {
            retreiveToken = new Token(res.getString("token"), res.getString("user_name"), res.getLong("expiration_time"));
        }
        return retreiveToken;
    }


    public ArrayList<DropFile> getFiles(String username) throws SQLException {
        ArrayList<DropFile> userFiles = new ArrayList<DropFile>();
        PreparedStatement statement = conn.prepareStatement("SELECT * FROM files WHERE user_name=?");
        statement.setString(1, username);
        ResultSet res = statement.executeQuery();
        while (res.next()) {
            userFiles.add(new DropFile(res.getString("user_name"),
                    res.getString("file_name"),
                    res.getString("file_path"),
                    res.getString("permission"),
                    res.getString("s3_url"),
                    res.getLong("version"),
                    res.getString("md5")));
        }
        return userFiles;
    }

    public void addFile(DropFile file) throws SQLException {
        PreparedStatement statement = conn.prepareStatement("INSERT INTO files (user_name,file_name,file_path,permission,s3_url,version,md5) VALUES (?,?,?,?,?,?,?)");
        statement.setString(1, file.getUserName());
        statement.setString(2, file.getFileName());
        statement.setString(3, file.getFilePath());
        statement.setString(4, file.getPermission().toString());
        statement.setString(5, file.getS3URL());
        statement.setLong(6, file.getVersion());
        statement.setString(7, file.getMd5());

        statement.executeUpdate();

    }

    //TODO kanske inte ska ha path som key
    public DropFile getFile(String username, String fileName, String filePath) throws SQLException {
        DropFile file = null;
        PreparedStatement statement = conn.prepareStatement("SELECT * FROM files WHERE user_name=? AND file_name=? AND file_path=?");
        statement.setString(1, username);
        statement.setString(2, fileName);
        statement.setString(3, filePath);
        ResultSet res = statement.executeQuery();
        while (res.next()) {
            file = new DropFile(res.getString("user_name"),
                    res.getString("file_name"),
                    res.getString("file_path"),
                    res.getString("permission"),
                    res.getString("s3_url"),
                    res.getLong("version"),
                    res.getString("md5"));
        }
        return file;
    }

    public boolean deleteFile(DropFile file) throws SQLException {
        if (file.getPermission().readwrite()) {
            PreparedStatement statement = conn.prepareStatement("DELETE FROM files WHERE user_name=? AND file_name=? AND file_path=?");
            statement.setString(1, file.getUserName());
            statement.setString(2, file.getFileName());
            statement.setString(3, file.getFilePath());
            statement.executeUpdate();
            return true;
        }
        return false;
    }


}