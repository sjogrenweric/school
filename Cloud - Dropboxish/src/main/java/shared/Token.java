package shared;

import java.util.UUID;

/**
 * Project: drop
 * Package: authService
 * User: c08esn c11epm
 * Date: 3/6/15
 * Time: 11:29 AM
 */
public class Token {


    int HOURMILLIS = 3600000;
    int HOURS = 3;
    UUID token;
    long expirationTime;
    String username;

    public Token(String username) {
        token = UUID.randomUUID();
        expirationTime = System.currentTimeMillis() + (HOURS * HOURMILLIS);
        this.username = username;
    }

    public Token(String uuid, String username, long expirationTime) {
        token = UUID.fromString(uuid);
        this.username = username;
        this.expirationTime = expirationTime;
    }


    public UUID getToken() {
        return token;
    }

    public boolean isValid() {
        if (System.currentTimeMillis() < expirationTime)
            return true;
        return false;
    }

    public String getUsername() {
        return username;
    }

    public long getExpirationTime() {
        return expirationTime;
    }


    @Override
    public String toString() {
        return "Token{" +
                "HOURMILLIS=" + HOURMILLIS +
                ", HOURS=" + HOURS +
                ", token=" + token +
                ", expirationTime=" + expirationTime +
                ", username='" + username + '\'' +
                '}';
    }
}
