package shared;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by c11epm on 3/9/15.
 */
public class PropertyReader {

    public static String getProperty(String filename, String propertyName) {
        Properties properties = new Properties();
        String propertyFile = "src/main/resources/" + filename;
        try {
            properties.load(new FileInputStream(propertyFile));
        } catch (IOException e) {
            throw new RuntimeException("Property file:" + propertyFile + " " + e.getMessage());
        }
        return properties.getProperty(propertyName);
    }
}
