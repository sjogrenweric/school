package shared;

import com.google.gson.annotations.Expose;

/**
 * Project: drop
 * Package: fileService
 * User: c08esn c11epm
 * Date: 3/6/15
 * Time: 1:35 PM
 */
public class Permission {

    @Expose
    private String permission;

    public Permission(String permission) {
        this.permission = permission;
    }

    public boolean read() {
        if (permission.contains("r"))
            return true;
        return false;
    }

    public boolean write() {
        if (permission.contains("w"))
            return true;
        return false;
    }

    public boolean readwrite() {
        if (permission.contains("rw"))
            return true;
        return false;
    }


    @Override
    public String toString() {
        return permission;
    }
}
