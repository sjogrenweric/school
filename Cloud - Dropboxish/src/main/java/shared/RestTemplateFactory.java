package shared;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

/**
 * Created by c11epm on 3/9/15.
 */
public class RestTemplateFactory {

    public static RestTemplate getRestTemplateNoErrorCheck() {
        RestTemplate template = new RestTemplate();
        template.setErrorHandler(new DefaultResponseErrorHandler() {
            //TODO not very good for error handling, but preventing the throw of an exception.
            @Override
            public boolean hasError(HttpStatus code) {
                return false;
            }
        });
        return template;
    }
}
