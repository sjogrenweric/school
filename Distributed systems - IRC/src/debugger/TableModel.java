package debugger;

import messaging.Message;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Project: GCom
 * Package: debugger
 * User: c08esn c11ean
 * Date: 9/24/14
 * Time: 11:16 AM
 * The tablemodel is a class extending the abstracttablemodel and contains
 * all information used for each JTable used within the GraphicalGUI
 */
public class TableModel extends AbstractTableModel {

    Vector<TableData> dataVector;
    String[] tableHeaders;
    private Vector<Message> messages;


    public TableModel(String[] tableHeaders) {
        this.tableHeaders = tableHeaders;
        dataVector = new Vector<TableData>(tableHeaders.length);
        messages = new Vector<Message>();

    }

    public void addRowData(String[] data) {
        if (data.length != tableHeaders.length) {
            System.err.println("Wrong number of data objects");
            return;
        }
        TableData tableData = new TableData();

        for (int i = 0; i < data.length; i++) {
            tableData.addData(data[i]);
        }
        dataVector.add(tableData);

        fireTableRowsInserted(dataVector.size() - 1, dataVector.size() - 1);

    }

    public void addRowData(String[] data, Message message) {
        if (data.length != tableHeaders.length) {
            System.err.println("Wrong number of data objects");
            return;
        }
        TableData tableData = new TableData();
        messages.add(message);
        for (int i = 0; i < data.length; i++) {
            tableData.addData(data[i]);
        }
        dataVector.add(tableData);

        fireTableRowsInserted(dataVector.size() - 1, dataVector.size() - 1);

    }

    public void flushAllData() {
        dataVector = new Vector<TableData>(tableHeaders.length);
        messages = new Vector<Message>();
    }

    public void removeRowData(int row) {
        dataVector.remove(row);
        messages.remove(row);
    }

    public Message getMessageAt(int rowIndex) {
        return messages.get(rowIndex);
    }


    @Override
    public int getRowCount() {
        return dataVector.size();
    }

    @Override
    public int getColumnCount() {
        return tableHeaders.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String s = dataVector.get(rowIndex).getData(columnIndex);
        return s;

    }

    @Override
    public String getColumnName(int col) {
        return tableHeaders[col];
    }

    public void remove(Message message) {
        for (int i = 0; i < messages.size(); i++) {
            if (messages.get(i).getMessageUID().equals(message.getMessageUID())) {
                removeRowData(i);
            }
        }
    }

    public void updateMessageCount(Message message, int count) {
        for (int i = 0; i < messages.size(); i++) {
            if (messages.get(i).getMessageUID().equals(message.getMessageUID())) {
                dataVector.get(i).putData(3, "" + count);
            }
        }
    }


    class TableData {
        private ArrayList<String> dataList;

        public TableData() {
            dataList = new ArrayList<String>();
        }

        public void addData(String data) {
            dataList.add(data);
        }

        public String getData(int index) {
            return dataList.get(index);
        }

        public void putData(int column, String data) {
            dataList.set(column, data);
        }
    }
}
