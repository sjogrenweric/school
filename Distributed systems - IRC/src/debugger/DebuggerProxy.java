package debugger;

import communication.Node;
import groupmanagement.GroupManagement;
import messageordering.MessageOrdering;
import messaging.Message;
import messaging.MetaMessage;
import utility.VectorClock;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Project: GCom
 * Package: debugger
 * User: c08esn c11ean
 * Date: 9/24/14
 * Time: 11:16 AM
 * The DebuggerProxy creates the debugger gui and provides the means to handle all events.
 * It is also responsible to act as a proxy between callee and the graphical interface, updating
 * the interface if such interface has been created.
 */
public class DebuggerProxy implements ActionListener {
    static DebuggerGUI gui;
    private JButton sendButton;
    private JButton shuffleButton;
    private JButton holdButton;
    private static DebuggerProxy selfRef;
    private JButton orderingButton;
    private JButton multicastButton;
    private JButton delayButton;
    private JButton dropButton;
    private JButton releaseButton;

    private DebuggerProxy() {
        createGUI();
    }

    /**
     * Create a new DebuggerGUI. If one already exists, destroy it and create a new one.
     */
    public static void startDebugger() {
        if (selfRef == null) {
            selfRef = new DebuggerProxy();
        } else {
            gui.destroyGUI();
            selfRef = new DebuggerProxy();
        }
    }

    /**
     * Creat the graphical debugger interface and bind all buttons to the
     * action listener contained within this DebuggerProxy
     */
    private void createGUI() {
        final ActionListener selfRef = this;

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                gui = new DebuggerGUI();
                holdButton = gui.getHoldButton();
                shuffleButton = gui.getShuffleButton();
                sendButton = gui.getSendButton();
                orderingButton = gui.getOrderingButton();
                multicastButton = gui.getMulticastButton();
                dropButton = gui.getDropButton();
                delayButton = gui.getDelayButton();
                releaseButton = gui.getReleaseButton();

                holdButton.addActionListener(selfRef);
                shuffleButton.addActionListener(selfRef);
                sendButton.addActionListener(selfRef);
                multicastButton.addActionListener(selfRef);
                orderingButton.addActionListener(selfRef);
                dropButton.addActionListener(selfRef);
                delayButton.addActionListener(selfRef);
                releaseButton.addActionListener(selfRef);
            }
        });


    }

    /**
     * Call the gui to add a new message to the message history
     * @param receivedMessage
     */
    public static void addReceivedMessage(Message receivedMessage) {
        if (selfRef == null) return;
        gui.addReceivedMessageToHistory(receivedMessage);
    }

    /**
     * Shuffle the messages contained within the send queue and update the
     * graphical interface
     */
    private static void shuffleSendQueue() {
        if (selfRef == null) return;
        gui.flushSendQueue(MessageOrdering.getDelayedMessage());
        MetaMessage[] mmarr = MessageOrdering.shuffleSendQueue();
        for (MetaMessage mm : mmarr) {
            gui.addMessageToSendQueue(mm.getMessage());
        }
    }

    /**
     * Call MessageOrdering to lock the send queue
     */
    private void lockSendQueue() {
        MessageOrdering.lockSendQueue();
    }


    /**
     * Call MessageOrdering to unlock the send queue
     */
    private void unlockSendQueue() {
        MessageOrdering.unlockSendQueue();
    }

    /**
     * Call MessageOrdering to check whether the send queue is locked
     */
    public boolean sendQueueIsLocked() {
        return MessageOrdering.sendQueueIsLocked();
    }

    /**
     * Call MessageOrdering to lock the receive queue
     */
    private void lockReceiveQueue() {
        MessageOrdering.lockReceiveQueue();
    }

    /**
     * Call MessageOrdering to unlock the receive queue
     */
    private void unlockReceiveQueue() {
        MessageOrdering.unlockReceiveQueue();
    }

    /**
     * Call MessageOrdering to check whether the receive queue is locked
     */
    private boolean receiveQueueIsLocked() {
        return MessageOrdering.receiveQueueIsLocked();
    }

    /**
     * Add a message to the send queue
     * @param message
     */
    public static void addToSendQueue(MetaMessage message) {
        if (selfRef == null) return;
        gui.addMessageToSendQueue(message.getMessage());
    }

    /**
     * Add a message to the receive queue
     * @param receivedMessage
     */
    public static void addToReceiveQueue(Message receivedMessage) {
        if (selfRef == null) return;
        gui.addMessageToHoldQueue(receivedMessage);
    }

    /**
     * Updat the vector clocks for the group members
     * @param vectorMap
     */
    public static void updateVectorClocks(ConcurrentHashMap<String, VectorClock> vectorMap) {
        if (selfRef == null) return;
        Iterator it = vectorMap.entrySet().iterator();
        gui.flushVectors();
        while (it.hasNext()) {
            Map.Entry<String, VectorClock> pair = (Map.Entry<String, VectorClock>) it.next();
            String[] values = pair.getValue().toString().split(",");
            for (int i = 0; i < values.length; i++) {
                String[] id = values[i].split(" : ");
                values[i] = "<"+id[0].substring(0,5)+" , "+id[1]+">";
                gui.addVectorClocks(pair.getKey(), values[i]);
            }

        }
    }

    /**
     * Update the number of times a message has been received.
     * @param mesage
     * @param count
     */
    public static void updateReceivedMessageCount(Message mesage, int count) {
        if (selfRef == null) return;
        gui.updateReceivedMessageHistoryCount(mesage, count);
    }


    /**
     * Update the "system performance" tab - called when the number of members
     * in the group has changed. THe system performance is calculated:
     * if reliable multicast: number of members n -> performance = n^2
     * if unreliable multicast: number of members n -> performance = n
     * @param groupMap
     */
    public static void updateSystemPerformance(ConcurrentHashMap<String, ConcurrentHashMap<String, Node>> groupMap) {
        if (selfRef == null) return;
        ArrayList<String[]> groupNames = new ArrayList<String[]>();
        boolean reliable = MessageOrdering.isReliableMulticast();
        for (Map.Entry<String, ConcurrentHashMap<String, Node>> group : groupMap.entrySet()) {
            String groupName = group.getKey();
            int size = group.getValue().size();

            groupNames.add(new String[]{groupName, "" + (reliable ? size * size : size)});
        }
        gui.flushPerformance();
        if(groupNames.size() > 0)
        gui.updatePerformance(groupNames);

    }

    /**
     * Remove a message from the hold-back queue
     * @param message
     */
    public static void removeFromHoldBackQueue(Message message) {
        if (selfRef == null) return;
        gui.removeFromHoldBackQueue(message);
    }


    /**
     * The action listener for all the buttons within the debugger ui
     * @param e
     */
    @Override
    public void actionPerformed(final ActionEvent e) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                if (e.getSource() == sendButton) {
                    if (sendQueueIsLocked()) {
                        sendButton.setText("Freeze");
                        unlockSendQueue();
                        gui.flushSendQueue(MessageOrdering.getDelayedMessage());
                    } else {
                        sendButton.setText("Unfreeze");
                        lockSendQueue();
                    }
                } else if (e.getSource() == holdButton) {
                    if (receiveQueueIsLocked()) {
                        holdButton.setText("Freeze");
                        unlockReceiveQueue();
                    } else {
                        holdButton.setText("Unfreeze");
                        lockReceiveQueue();
                    }
                } else if (e.getSource() == shuffleButton) {
                    if (MessageOrdering.sendQueueIsLocked()) {
                        shuffleSendQueue();
                    }
                } else if (e.getSource() == multicastButton) {
                    if (MessageOrdering.isReliableMulticast()) {
                        MessageOrdering.setBasicMutlicast();
                        GroupManagement.updateSystemPerformance();
                        multicastButton.setText("Set reliable");
                    } else {
                        MessageOrdering.setReliableMutlicast();
                        GroupManagement.updateSystemPerformance();
                        multicastButton.setText("Set unreliable");
                    }
                } else if (e.getSource() == orderingButton) {
                    if (MessageOrdering.isCausalOrdering()) {
                        MessageOrdering.setUnorderedOrdering();
                        orderingButton.setText("Set ordered");
                    } else {
                        MessageOrdering.setCausalOrdering();
                        orderingButton.setText("Set unordered");
                    }
                } else if (e.getSource() == dropButton) {
                    Message dropMessage = gui.getSelectedSendMessage();
                    if (dropMessage != null) {
                        gui.removeSelectedSendMessage();
                        MessageOrdering.dropSendMessage(dropMessage);
                        gui.deselectSendTable();
                    } else {
                        JOptionPane.showMessageDialog(gui.getFrame(), "No message selected.");
                    }

                } else if (e.getSource() == delayButton) {

                    Message delayMessage = gui.getSelectedSendMessage();
                    if (delayMessage != null) {
                        MessageOrdering.delaySendMessage(delayMessage);
                        gui.deselectSendTable();
                    } else {
                        JOptionPane.showMessageDialog(gui.getFrame(), "No message selected.");
                    }

                } else if (e.getSource() == releaseButton) {
                    if (!MessageOrdering.sendQueueIsLocked()) {
                        gui.flushSendQueue(null);
                        MessageOrdering.releaseDelayedMessages();
                        gui.deselectSendTable();
                    } else {
                        JOptionPane.showMessageDialog(gui.getFrame(), "Please unfreeze before release.");
                    }

                }
            }
        });

    }


}
