package debugger;


import groupmanagement.GroupManagement;
import messaging.ChatMessage;
import messaging.GroupViewMessage;
import messaging.Message;
import messaging.MetaMessage;
import sun.security.ssl.Debug;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * Project: GCom
 * Package: debugger
 * User: c08esn c11ean
 * Date: 9/24/14
 * Time: 11:16 AM
 * The DebuggerGUI handles the creation of the graphical user interface for the debugger
 * as well as handeling the datamodel
 */

public class DebuggerGUI {

    private JFrame frame;
    private TableModel holdModel;
    private TableModel sendModel;
    private TableModel vectorModel;
    private JButton sendButton;
    private JButton shuffleButton;
    private JButton holdButton;
    private TableModel tableModel;
    private JTable sentHistoryTable;
    private JTable sendTable;
    private JTable vectorTable;
    private JTable holdTable;
    private JButton orderingButton;
    private JButton multicastButton;
    private TableModel systemModel;
    private JTable systemTable;
    private JButton delayButton;
    private JButton dropButton;
    private JButton releaseButton;


    public DebuggerGUI() {
        initWindow();
        JPanel historyPane = createHistoryPane();
        frame.add(historyPane);
        JPanel queuesPane = createQueues();
        frame.add(queuesPane);
        frame.setVisible(true);
    }

    /**
     * Create the JFrame containing all the different views.
     */
    private void initWindow() {
        frame = new JFrame();
        frame.setLayout(new GridLayout(2, 1));
        frame.setSize(1200, 800);
        frame.setTitle("Debugger");

    }

    /**
     * Close the GUI window
     */
    public void destroyGUI() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
            }
        });

    }

    /**
     * Create the window containg received messages
     * @return
     */
    private JPanel createHistoryPane() {
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBorder(BorderFactory.createTitledBorder("Msg history"));
        String[] tableHistoryHeaders = new String[]{"Message", "Sender", "Path", "Receive count"};
        tableModel = new TableModel(tableHistoryHeaders);
        sentHistoryTable = new JTable(tableModel);

        JScrollPane scrollPane = new JScrollPane(sentHistoryTable);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        sentHistoryTable.setFillsViewportHeight(true);
        panel.add(scrollPane, BorderLayout.CENTER);
        return panel;
    }

    /**
     * Create all the different queues and panels:
     * Hold-back queue
     * Send queue
     * Vector view
     * System panel
     * @return
     */
    private JPanel createQueues() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(1, 4));

        JPanel holdPanel = createHoldPanel();
        JPanel sendPanel = createSendQueue();
        JPanel vectorPanel = createVectorPanel();
        JPanel systemPanel = createSystemPanel();

        panel.add(holdPanel);
        panel.add(sendPanel);
        panel.add(vectorPanel);
        panel.add(systemPanel);

        return panel;
    }

    /**
     * Creates the system panel, showing the current "performance" of the system
     * @return
     */
    private JPanel createSystemPanel() {
        JPanel systemPanel = new JPanel(new BorderLayout());
        orderingButton = new JButton("Set unordered");
        multicastButton = new JButton("Set unreliable");
        String[] systemHeaders = new String[]{"Group", "Performance"};
        systemModel = new TableModel(systemHeaders);
        systemTable = new JTable(systemModel);
        JScrollPane systemScroll = new JScrollPane(systemTable);
        systemScroll.setBorder(BorderFactory.createTitledBorder("System"));
        systemTable.setFillsViewportHeight(true);
        systemPanel.add(systemScroll, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel(new GridLayout(1, 2));
        buttonPanel.add(multicastButton);
        buttonPanel.add(orderingButton);
        systemPanel.add(buttonPanel, BorderLayout.PAGE_END);
        systemScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        return systemPanel;
    }

    /**
     * Create the view containing the hold-back queue along with associated buttons
     * @return
     */
    private JPanel createHoldPanel() {
        JPanel holdPanel = new JPanel(new BorderLayout());
        String[] holdHeaders = new String[]{"Msg", "Sender"};
        holdModel = new TableModel(holdHeaders);
        holdTable = new JTable(holdModel);
        JScrollPane holdScroll = new JScrollPane(holdTable);
        holdScroll.setBorder(BorderFactory.createTitledBorder("HoldBack Queue"));
        holdTable.setFillsViewportHeight(true);
        holdPanel.add(holdScroll, BorderLayout.CENTER);
        holdButton = new JButton("Freeze");
        holdPanel.add(holdButton, BorderLayout.PAGE_END);
        holdScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        return holdPanel;
    }

    /**
     * Create the view containing the send queue along with associated buttons
     * @return
     */
    private JPanel createSendQueue() {
        JPanel sendPanel = new JPanel(new BorderLayout());
        String[] sendHeaders = new String[]{"Msg", "Receiver"};
        sendModel = new TableModel(sendHeaders);
        sendTable = new JTable(sendModel);

        JScrollPane sendScroll = new JScrollPane(sendTable);
        sendScroll.setBorder(BorderFactory.createTitledBorder("Send Queue"));
        sendTable.setFillsViewportHeight(true);
        sendPanel.add(sendScroll, BorderLayout.CENTER);
        sendButton = new JButton("Freeze");
        shuffleButton = new JButton("Shuffle");

        JPanel sendButtonPanel = new JPanel(new GridLayout(2, 1));

        JPanel topButtons = new JPanel(new GridLayout(1, 2));
        topButtons.add(sendButton);
        topButtons.add(shuffleButton);

        JPanel bottomButtons = new JPanel(new GridLayout(1, 3));
        dropButton = new JButton("Drop");
        delayButton = new JButton("Delay");
        releaseButton = new JButton("Release");
        bottomButtons.add(dropButton);
        bottomButtons.add(delayButton);
        bottomButtons.add(releaseButton);

        sendButtonPanel.add(topButtons);
        sendButtonPanel.add(bottomButtons);
        sendPanel.add(sendButtonPanel, BorderLayout.PAGE_END);
        sendScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        return sendPanel;
    }

    /**
     * Create the view that shows the vector clocks of all members in the group(s)
     * @return
     */
    private JPanel createVectorPanel() {
        JPanel vectorPanel = new JPanel(new BorderLayout());
        String[] vectorHeaders = new String[]{"Group", "<Key, Value>"};
        vectorModel = new TableModel(vectorHeaders);
        vectorTable = new JTable(vectorModel);
        JScrollPane vectorScroll = new JScrollPane(vectorTable);
        vectorScroll.setBorder(BorderFactory.createTitledBorder("Vectors"));
        vectorTable.setFillsViewportHeight(true);
        vectorPanel.add(vectorScroll, BorderLayout.CENTER);
        vectorScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        return vectorPanel;
    }


    /**
     * Update the number of times a certain message has been received.
     * @param message
     * @param count
     */
    public void updateReceivedMessageHistoryCount(final Message message, final int count) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                tableModel.updateMessageCount(message, count);
                sentHistoryTable.repaint();
            }
        });
    }

    /**
     * Add a received message to the list of received messages. Information that is displayed:
     * What kind of message was received
     * The sender ID of the message
     * The path the message took in the network
     * A counter displaying how many times a message has been received
     * @param receivedMessage
     */
    public void addReceivedMessageToHistory(final Message receivedMessage) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                String[] data = new String[4];
                data[0] = receivedMessage.getClass().getSimpleName();
                data[1] = receivedMessage.getSenderID().substring(0,5);
                if (receivedMessage instanceof ChatMessage) {
                    data[0] += ": " + ((ChatMessage) receivedMessage).getMessage();
                    data[1] += ": " + ((ChatMessage) receivedMessage).getAlias();
                } else if (receivedMessage instanceof GroupViewMessage) {
                    data[0] += ": ";
                    Enumeration<String> nodes = ((GroupViewMessage) receivedMessage).getGroupView().keys();
                    while (nodes.hasMoreElements()) {
                        data[0] += nodes.nextElement().substring(0,5);
                        if (nodes.hasMoreElements()) {
                            data[0] += ", ";
                        }
                    }
                }

                ArrayList<String> path = receivedMessage.getRecipients();
                String sPath = path.get(0).substring(0,5);
                for (int i = 1; i < path.size(); i++) {
                    sPath += " -> " + path.get(i).substring(0,5);
                }
                sPath += " -> " + GroupManagement.getID().substring(0,5);
                data[2] = sPath;
                data[3] = "" + 1;
                tableModel.addRowData(data, receivedMessage);
                sentHistoryTable.scrollRectToVisible(sentHistoryTable.getCellRect(tableModel.getRowCount() - 1, 0, true));
            }
        });
    }

    /**
     * Add a message to the receive queue
     * @param message
     */
    public void addMessageToSendQueue(final Message message) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                String[] data = new String[2];
                data[0] = "";
                data[1] = message.getGroupName();
                if (message instanceof ChatMessage) {
                    data[0] += ((ChatMessage) message).getMessage();
                }

                sendModel.addRowData(data, message);
                sendTable.scrollRectToVisible(sendTable.getCellRect(sendModel.getRowCount() - 1, 0, true));
            }
        });
    }

    /**
     * Add a message to the hold-back queue
     * @param message
     */
    public void addMessageToHoldQueue(final Message message) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                String[] data = new String[2];
                data[0] = "";
                data[1] = message.getSenderID().substring(0, 5);
                if (message instanceof ChatMessage) {
                    data[0] += ((ChatMessage) message).getMessage();
                }

                holdModel.addRowData(data, message);
                holdTable.scrollRectToVisible(holdTable.getCellRect(holdModel.getRowCount() - 1, 0, true));


            }
        });
    }

    /**
     * Remove all entries from the send queue
     * @param delayedMessage
     */
    public void flushSendQueue(final ArrayList<MetaMessage> delayedMessage) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                sendModel.flushAllData();
                sendTable.repaint();
                sendTable.scrollRectToVisible(sendTable.getCellRect(sendModel.getRowCount() - 1, 0, true));
                if (delayedMessage != null) {
                    for (MetaMessage delayed : delayedMessage) {
                        addMessageToSendQueue(delayed.getMessage());
                    }
                }

            }
        });
    }


    /**
     * Remove all entries from the performance list
     */
    public void flushPerformance() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                systemModel.flushAllData();
                systemTable.repaint();
                systemTable.scrollRectToVisible(systemTable.getCellRect(systemModel.getRowCount() - 1, 0, true));
            }
        });
    }

    /**
     * Remove all vector clock entries
     */
    public void flushVectors() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                vectorModel.flushAllData();
                vectorTable.repaint();
                vectorTable.scrollRectToVisible(vectorTable.getCellRect(vectorModel.getRowCount() - 1, 0, true));
            }
        });
    }

    /**
     * Add a vector clock value to the vector clock list
     * @param key
     * @param value
     */
    public void addVectorClocks(final String key, final String value) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                String[] data = new String[2];
                data[0] = key;
                data[1] = value;

                vectorModel.addRowData(data);
                vectorTable.scrollRectToVisible(vectorTable.getCellRect(vectorModel.getRowCount() - 1, 0, true));
            }
        });
    }

    /**
     * Update the current "system performance"
     * @param groupNames
     */
    public void updatePerformance(final ArrayList<String[]> groupNames) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                for (String[] s : groupNames) {
                    String[] data = new String[2];
                    data[0] = s[0];
                    data[1] = s[1];

                    systemModel.addRowData(data);
                    systemTable.scrollRectToVisible(systemTable.getCellRect(systemModel.getRowCount() - 1, 0, true));
                }
            }
        });
    }

    /**
     * Used when a message in the send queue has been selected and one of the buttons
     * associated with the send queue has been pushed.
     * @return
     */
    public Message getSelectedSendMessage() {
        int row = sendTable.getSelectedRow();

        if (row == -1) return null;
        else return sendModel.getMessageAt(row);
    }

    /**
     * Deselect any selection within the send queue
     */
    public void deselectSendTable() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                sendTable.getSelectionModel().clearSelection();
            }
        });

    }

    /**
     * Remove a message from the send queue
     */
    public void removeSelectedSendMessage() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                int row = sendTable.getSelectedRow();
                if (row != -1) {
                    sendModel.removeRowData(row);
                    sendTable.repaint();
                }
            }
        });

    }

    /**
     * Get the reference to the JFrame
     * @return
     */
    public JFrame getFrame() {
        return frame;
    }

    /**
     * Remove a message from the hold-back queue
     * @param message
     */
    public void removeFromHoldBackQueue(final Message message) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                holdModel.remove(message);
                holdTable.repaint();
            }
        });

    }

    /*  GETTERS FOR ALL BUTTONS - THESE ARE USED BY THE DebuggerProxy */

    public JButton getSendButton() {
        return sendButton;
    }

    public JButton getShuffleButton() {
        return shuffleButton;
    }

    public JButton getHoldButton() {
        return holdButton;
    }

    public JButton getOrderingButton() {
        return orderingButton;
    }

    public JButton getMulticastButton() {
        return multicastButton;
    }

    public JButton getDropButton() {
        return dropButton;
    }

    public JButton getDelayButton() {
        return delayButton;
    }

    public JButton getReleaseButton() {
        return releaseButton;
    }


}
