package utility;

/**
 * Project: GCom
 * Package: namingservice
 * User: c08esn c11ean
 * Date: 9/18/14
 * Time: 1:35 PM
 * Log class, uses log4j
 */

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class Log {
    Logger log;
    SimpleDateFormat ft;

    public Log(Class c) {
        log = Logger.getLogger(c);
        PropertyConfigurator.configure("log4j.properties");
        ft = new SimpleDateFormat("hh:mm:ss");
    }

    public void info(String message) {
        Date d = new Date(System.currentTimeMillis());
        log.info(ft.format(d) + ": " + message);
    }

    public void debug(String message) {
        Date d = new Date(System.currentTimeMillis());
        log.debug(ft.format(d) + ": " + message);
    }

    public void error(String message) {
        Date d = new Date(System.currentTimeMillis());
        log.error(ft.format(d) + ": " + message);
    }

}
