package utility;


import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Project: GCom
 * Package: gcomclient
 * User: c08esn c11ean
 * Date: 9/24/14
 * Time: 10:32 AM
 * Data holder fro the vector clock.
 */
public class VectorClock implements Serializable {

    private String uid;
    private ConcurrentHashMap<String, Integer> vectorClock;

    public VectorClock(String uid) {
        this.uid = uid;
        vectorClock = new ConcurrentHashMap<String, Integer>();
        vectorClock.put(uid, 0);
    }

    public VectorClock copy() {
        VectorClock v = new VectorClock(uid);
        v.setVectorValues(vectorClock);
        return v;
    }

    public void setVectorValues(ConcurrentHashMap<String, Integer> vector) {
        this.vectorClock = new ConcurrentHashMap<String, Integer>(vector);
    }

    public ConcurrentHashMap<String, Integer> getClock() {
        return vectorClock;
    }

    public void incrementClock() {
        vectorClock.put(uid, vectorClock.get(uid) + 1);
    }

    public void removeMember(String uid){
        vectorClock.remove(uid);
    }

    /**
     * Compares if each element in another vector is
     * less or equal to this Vector
     * @param vector compare vector
     * @return boolean
     */
    public boolean lessEqualThan(ConcurrentHashMap<String, Integer> vector,String senderId) {
        boolean returnBoolean = true;
        Iterator it = vectorClock.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Integer> pair = (Map.Entry<String, Integer>) it.next();
            if (!pair.getKey().equals(senderId) && vector.get(pair.getKey()) != null) {
                /*if (!(pair.getValue() >= vector.get(pair.getKey()))) {
                    returnBoolean = false;
                }*/
                if(pair.getValue() < vector.get(pair.getKey())) {
                    System.err.println("our was less, returning false");
                    return false;
                }
            }
        }
                    System.err.println("our was higher, returing true");
        return returnBoolean;
    }
    @Override
    public String toString() {
        Iterator it = vectorClock.entrySet().iterator();
        String returnString = "";
        while (it.hasNext()) {
            Map.Entry<String, Integer> pair = (Map.Entry<String, Integer>) it.next();
            returnString += " " + pair.getKey() + " : " + pair.getValue();
            if(it.hasNext()) returnString += ",";
        }
        return returnString;
    }

}
