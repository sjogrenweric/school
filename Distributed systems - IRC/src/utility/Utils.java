package utility;

import communication.Node;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
/**
 * Project: GCom
 * Package: gcomclient
 * User: c08esn c11ean
 * Date: 9/24/14
 * Time: 10:32 AM
 *
 */

public class Utils {

    /**
     * Prints a hashmap
     * @param map map to be written
     * @return String the hashmap
     */
    public static String printHashMap(ConcurrentHashMap<String, ?> map) {
        StringBuilder builder = new StringBuilder();
        Iterator iterator = map.keySet().iterator();
        while (iterator.hasNext()) {
            String key = (String) iterator.next();

            builder.append("(" + key);
            if(iterator.hasNext()) {
                builder.append(", ");
            } else {
                builder.append(")");
            }
        }
        return builder.toString();
    }

}
