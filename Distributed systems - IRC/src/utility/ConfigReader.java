package utility;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;

/**
 * Project: GCom
 * Package: namingservice
 * User: c08esn c11ean
 * Date: 9/18/14
 * Time: 1:29 PM
 * Reads config files (.properties)
 */
public class ConfigReader {


    /**
     * Reads a properties file
     * @param classlocation the location of the class that reads the file
     * @param file name of file
     * @return Properties object
     */
    public Properties getProperties(Class classlocation, String file) {
        Properties properties = new Properties();
        Log log = new Log(ConfigReader.class);
        try {
            InputStream is = classlocation.getResourceAsStream(file);
            properties.load(is);
            Date time = new Date(System.currentTimeMillis());
            properties.setProperty("date", time.toString());
            Enumeration enuKeys = properties.keys();
            while (enuKeys.hasMoreElements()) {
                String key = (String) enuKeys.nextElement();
                String value = properties.getProperty(key);

                log.debug(key + " : " + value);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return properties;
    }
}
