package messaging;

import utility.VectorClock;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Project: GCom
 * Package: messaging
 * User: c08esn c11ean
 * Date: 9/24/14
 * Time: 10:45 AM
 * The ChatMessage contains a message created by a group member
 * and sent to the other members of the group.
 */
public class ChatMessage implements Message {

    private  String uid;
    private String message;
    private String senderID;
    private String alias;
    private String groupName;
    private VectorClock vectorClock;
    private ArrayList<String> recipientIds;

    public ChatMessage(String message, String senderID, String alias, String groupName) {
        this.message = message;
        this.senderID = senderID;
        this.alias = alias;
        this.groupName = groupName;
        recipientIds = new ArrayList<String>();
        uid = UUID.randomUUID().toString();
    }

    public String getMessage() {
        return message;
    }

    @Override
    public VectorClock getVectorClock()  {
        return vectorClock;
    }

    @Override
    public void setVectorClock(VectorClock vectorClock) {
        this.vectorClock = vectorClock;
    }

    @Override
    public String getSenderID() {
        return senderID;
    }

    public String getAlias() {
        return alias;
    }

    public String getGroupName() {
        return groupName;
    }

    @Override
    public ArrayList<String> getRecipients() {
        return recipientIds;
    }

    @Override
    public void addRecipient(String id) {
        recipientIds.add(id);
    }

    @Override
    public String getMessageUID() {
        return uid;
    }
}
