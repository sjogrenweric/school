package messaging;

/**
 * Project: GCom
 * Package: messaging
 * User: c08esn c11ean
 * Date: 10/2/14
 * Time: 10:42 AM
 * A System message is a flag specifiying that the message
 * is to be regarded in different manner than regular messages.
 * For instance, System Messages are not ordered once received
 * by a member, but instead directly propagated to the GroupManagement
 * module for instant processing.
 */
public interface SystemMessage {
}
