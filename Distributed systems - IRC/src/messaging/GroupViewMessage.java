package messaging;

import communication.Node;
import utility.VectorClock;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Project: GCom
 * Package: messaging
 * User: c08esn c11ean
 * Date: 9/24/14
 * Time: 11:24 AM
 * A groupViewMessage contains a view of all nodes currently participating in the group.
 */
public class GroupViewMessage implements SystemMessage, Message {

    private VectorClock vectorClock;
    private ConcurrentHashMap<String, Node> groupView;
    private String senderID;
    private String groupName;
    private ArrayList<String> recipientIds;
    private String uid;

    public GroupViewMessage(ConcurrentHashMap<String, Node> groupView, String senderID, String groupName) {
        this.groupView = groupView;
        this.senderID = senderID;
        this.groupName = groupName;
        recipientIds = new ArrayList<String>();
        uid = UUID.randomUUID().toString();
    }

    public ConcurrentHashMap<String, Node> getGroupView() {
        return groupView;
    }

    @Override
    public VectorClock getVectorClock() {
        return vectorClock;
    }

    @Override
    public void setVectorClock(VectorClock vectorClock) {
        this.vectorClock = vectorClock;
    }

    @Override
    public String getSenderID() {
        return senderID;
    }


    @Override
    public String getGroupName() {
        return groupName;
    }

    @Override
    public ArrayList<String> getRecipients() {
        return recipientIds;
    }

    @Override
    public void addRecipient(String id) {
        recipientIds.add(id);
    }

    @Override
    public String getMessageUID() {
        return uid;
    }
}
