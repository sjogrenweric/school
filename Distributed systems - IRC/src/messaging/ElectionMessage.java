package messaging;

import utility.VectorClock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

/**
 * Project: GCom
 * Package: messaging
 * User: c08esn c11ean
 * Date: 10/2/14
 * Time: 1:53 PM
 * An election message is sent when a node request to perform the Bully algorithm
 * in order to decide a new group leader.
 */
public class ElectionMessage implements Message,SystemMessage {
    private String messageuid;
    private String senderId;
    private String groupName;
    private boolean election;
    private VectorClock vectorClock;
    private ArrayList<String> recipientIds;


    public ElectionMessage(String uid, String groupName,boolean election){
        senderId = uid;
        this.groupName = groupName;
        this.election = election;
        recipientIds = new ArrayList<String>();
        messageuid = UUID.randomUUID().toString();
    }

    @Override
    public VectorClock getVectorClock() {
        return vectorClock;
    }

    @Override
    public void setVectorClock(VectorClock vectorClock) {
        this.vectorClock = vectorClock;
    }

    @Override
    public String getSenderID() {
        return senderId;
    }

    @Override
    public String getGroupName() {
        return groupName;
    }

    public boolean isElection() {
        return election;
    }

    @Override
    public ArrayList<String> getRecipients() {
        return recipientIds;
    }

    @Override
    public void addRecipient(String id) {
        recipientIds.add(id);
    }

    @Override
    public String getMessageUID() {
        return messageuid;
    }
}
