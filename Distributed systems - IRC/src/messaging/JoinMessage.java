package messaging;

import communication.Node;
import utility.VectorClock;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Project: GCom
 * Package: messaging
 * User: c08esn c11ean
 * Date: 9/25/14
 * Time: 1:33 PM
 * A join message is sent from a node requesting membership of a group.
 */
public class JoinMessage implements SystemMessage, Message {
    private String uid;
    private String alias;
    private Node sender;
    private String groupName;
    private VectorClock vectorClock;
    private String senderID;
    private ArrayList<String> recipientIds;


    public JoinMessage(String senderID, String alias, Node sender, String groupName) {
        super();
        this.senderID = senderID;
        this.alias = alias;
        this.sender = sender;
        this.groupName = groupName;
        recipientIds = new ArrayList<String>();
        uid = UUID.randomUUID().toString();
    }

    @Override
    public void setVectorClock(VectorClock vectorClock) {
        this.vectorClock = vectorClock;
    }

    @Override
    public String getSenderID() {
        return senderID;
    }

    @Override
    public String getGroupName() {
        return groupName;
    }

    public String getAlias() {
        return alias;
    }


    @Override
    public VectorClock getVectorClock() {
        return vectorClock;
    }

    public Node getMember() {
        return sender;
    }
    @Override
    public ArrayList<String> getRecipients() {
        return recipientIds;
    }

    @Override
    public void addRecipient(String id) {
        recipientIds.add(id);
    }

    @Override
    public String getMessageUID() {
        return uid;
    }
}
