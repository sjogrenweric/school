package messaging;

import utility.VectorClock;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Project: GCom
 * Package: messaging
 * User: c08esn c11ean
 * Date: 9/24/14
 * Time: 10:41 AM
 * The Message interface specifies which methods must be
 * declared within a message sent in the network.
 *
 */
public interface Message extends Serializable {

    public VectorClock getVectorClock();
    public void setVectorClock(VectorClock vectorClock);
    public String getSenderID();
    public String getGroupName();
    public ArrayList<String> getRecipients();
    public void addRecipient(String id);
    public String getMessageUID();

}
