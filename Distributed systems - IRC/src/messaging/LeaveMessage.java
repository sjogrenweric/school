package messaging;

import utility.VectorClock;

import java.util.ArrayList;
import java.util.UUID;
/**
 * Project: GCom
 * Package: messaging
 * User: c08esn c11ean
 * Date: 9/24/14
 * Time: 11:24 AM
 * A leave message is sent from a node if it decides to leave a group.
 */

public class LeaveMessage implements Message, SystemMessage {

    private String uid;
    private VectorClock vectorClock;
    private String senderId;
    private String groupName;
    private ArrayList<String> recipientIds;

    public LeaveMessage(String senderId, String groupName) {
        this.senderId = senderId;
        this.groupName = groupName;
        recipientIds = new ArrayList<String>();
        uid = UUID.randomUUID().toString();
    }

    @Override
    public VectorClock getVectorClock() {
        return vectorClock;
    }

    @Override
    public void setVectorClock(VectorClock vectorClock) {
        this.vectorClock = vectorClock;
    }

    @Override
    public String getSenderID() {
        return senderId;
    }

    @Override
    public String getGroupName() {
        return groupName;
    }
    @Override
    public ArrayList<String> getRecipients() {
        return recipientIds;
    }

    @Override
    public void addRecipient(String id) {
        recipientIds.add(id);
    }

    @Override
    public String getMessageUID() {
        return uid;
    }
}
