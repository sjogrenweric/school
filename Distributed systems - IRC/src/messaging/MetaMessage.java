package messaging;

import communication.Node;
import utility.VectorClock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Project: GCom
 * Package: messaging
 * User: c08esn c11ean
 * Date: 9/29/14
 * Time: 1:52 PM
 * A MetaMessage is used to propagate meta data regarding a message
 * through the system layers.
 * In this case, the MetaMessage contains the Message to be sent to
 * the group members as well as a HashMap with all recipients of that
 * message.
 */
public class MetaMessage {

    private final Message message;
    private final ConcurrentHashMap<String, Node> recipients;

    public MetaMessage(Message message, ConcurrentHashMap<String, Node> recipients) {
        this.message = message;
        this.recipients = recipients;
    }


    public Message getMessage() {
        return message;
    }

    public void setVectorClock(VectorClock vectorClock) {
        message.setVectorClock(vectorClock);
    }

    public ConcurrentHashMap<String, Node> getRecipients() {
        return recipients;
    }


}
