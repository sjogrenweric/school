package namingservice;

import communication.Node;
import utility.ConfigReader;
import utility.Log;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Properties;


/**
 * Project: GCom
 * Package: namingservice
 * User: c08esn c11ean
 * Date: 9/18/14
 * Time: 1:17 PM
 * NameService class, handles groups and leaders of those groups.
 */
public class NameService extends UnicastRemoteObject implements NameServiceInterface {
    private static HashMap<String, Node> groups;
    private static Log log;

    protected NameService() throws RemoteException {
    }

    /**
     * Initiate the name service.
     * Get the port to be used for the RMI registry by reading the
     * config.properties file and then start the registry on that port.
     *
     * @param args
     */
    public static void main(String args[]) {
        groups = new HashMap<String, Node>();
        log = new Log(NameService.class);
        log.info("Loading config ...");
        ConfigReader configreader = new ConfigReader();
        Properties properties = configreader.getProperties(NameService.class, "config.properties");
        int port = Integer.parseInt(properties.getProperty("port"));
        log.info("Config sucessfully read, Name Service starting using port: " + port);


        try {
            NameService ns = new NameService();
            Registry registry = LocateRegistry.createRegistry(port);
            registry.rebind("nameService", ns);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the group leader node of the group with group name groupName.
     * If no such leader is specified for the provided group name, then the
     * calling node will be set as the leader of the group.
     *
     * @param groupName name of the group
     * @param client    Node of client requesting
     * @return MemberNode of the leader in the group
     * @throws RemoteException
     * @see communication.MemberNode
     */
    @Override
    public Node getGroupLeader(String groupName, Node client) throws RemoteException {
        log.debug("A client is trying to join group " + groupName);
        Node leader = groups.get(groupName);
        if (leader != null) {
            log.debug(groupName + " was found, sending ping to leader node..");
            try {
                leader.ping();
            } catch (RemoteException re) {
                log.debug("Ping to leader of group " + groupName + " failed. Setting caller to leader.");
                groups.put(groupName, client);
                return client;
            }

            return leader;
        }
        log.debug(groupName + " was not found, creating and returning self " + client.getID());
        groups.put(groupName, client);
        return client;
    }

    /**
     * Sets the group leader of a group
     *
     * @param groupName name of the group
     * @param client    MemberNode
     * @throws RemoteException
     */
    @Override
    public void setGroupLeader(String groupName, Node client) throws RemoteException {

            if (client != null) {
                groups.put(groupName, client);
                log.debug("the new leader is:" + client.getID());
            } else {
                groups.remove(groupName);
            }

    }



    public boolean isLeader(String key, String groupName) throws RemoteException {
        return groups.get(groupName).getID().equals(key);
    }


}
