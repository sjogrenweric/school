package namingservice;

import communication.Node;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Project: GCom
 * Package: namingservice
 * User: c08esn c11ean
 * Date: 9/18/14
 * Time: 1:19 PM
 * Interface for the NameService, declaring which methods can be
 * invoked remotely through the RMI api.
 */
public interface NameServiceInterface extends Remote {
    Node getGroupLeader(String groupName, Node client) throws RemoteException;

    public void setGroupLeader(String groupName, Node client) throws RemoteException;

    public boolean isLeader(String key, String groupName) throws
            RemoteException;

}
