package gcomclient;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * Project: GCom
 * Package: gcomclient
 * User: c08esn c11ean
 * Date: 9/24/14
 * Time: 11:16 AM
 * The ChatUI is responsible for showing the chat messages sent and received within the group.
 * It shows a tab for each group the client is member of, where each tab is named by the name of the group.
 * If the client is leader of a certain group, then the tab will be renamed with "groupname (L)",
 * indicating the that the client is the current leader.
 */
public class ChatUI extends Observable implements ActionListener, Observer {

    private Container panel;
    private final JFrame frame;

    private JMenuItem itemClose;
    private JMenuItem itemDebug;
    private JMenuItem itemAlias;
    private JMenuItem itemGroup;
    private JMenuItem itemLeave;
    private JMenu mainMenu;
    private JTabbedPane tabbedPane;
    private HashMap<String, GroupChatTab> tabs;
    private String alias;
    private String selfID;

    /**
     * Create a JFrame that will contain all the different views associated with the GUI
     * @param alias
     */
    public ChatUI(String alias) {
        this.alias = alias;
        frame = new JFrame();

        tabs = new HashMap<String, GroupChatTab>();

        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                public void run() {
                    frame.setSize(800, 800);
                    panel = frame.getContentPane();
                    panel.setLayout(new BorderLayout());

                    createMenuBar();
                    tabbedPane = new JTabbedPane();
                    tabbedPane.addChangeListener(new ChangeListener() {
                        @Override
                        public void stateChanged(ChangeEvent changeEvent) {
                            JPanel selected = (JPanel) tabbedPane.getSelectedComponent();
                            if (selected != null) {
                                String selectedGroupName = selected.getName();
                                tabs.get(selectedGroupName).requestFocus();
                            }
                        }
                    });
                    panel.add(tabbedPane, BorderLayout.CENTER);
                    frame.setTitle("CHAT");
                    frame.setLocationRelativeTo(null);
                    frame.setVisible(true);
                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                }
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create a drop down menu for the chat GUI containing
     * all the different options available to the user.
     */
    private void createMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        mainMenu = new JMenu("File");

        itemClose = new JMenuItem("Close");
        itemClose.addActionListener(this);
        itemDebug = new JMenuItem("Debugger");
        itemDebug.addActionListener(this);
        itemAlias = new JMenuItem("Change alias");
        itemAlias.addActionListener(this);
        itemGroup = new JMenuItem("Join group");
        itemGroup.addActionListener(this);
        mainMenu.add(itemAlias);
        mainMenu.add(itemGroup);
        mainMenu.add(itemDebug);
        mainMenu.add(itemClose);

        menuBar.add(mainMenu);
        frame.setJMenuBar(menuBar);
    }

    /**
     * Create a new tab for a certain groupname, using a GroupChatTab for
     * each group the client is member of.
     * @param groupName
     * @param selfID
     */
    public void createGroupChat(final String groupName, String selfID) {
        this.selfID = selfID;
        final GroupChatTab groupTab = new GroupChatTab(groupName, alias, selfID);

        groupTab.addObserver(this);
        tabs.put(groupName, groupTab);
        final ChatUI selfRef = this;
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                tabbedPane.addTab(groupName, groupTab.getPanel());
                if(itemLeave == null) {
                    itemLeave = new JMenuItem("Leave current group");
                    mainMenu.add(itemLeave);
                    itemLeave.addActionListener(selfRef);
                }
                tabbedPane.setSelectedComponent(groupTab.getPanel());
                groupTab.requestFocus();
            }
        });

    }

    /**
     * Update the name of the chat pane which the client is leader of to
     * reflect leadership.
     * @param groupName
     */
    public void showTabAsLeader(final String groupName) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                for(int i = 0; i < tabbedPane.getTabCount(); i++) {
                    JPanel panel = (JPanel) tabbedPane.getComponentAt(i);
                    if(panel.getName().equals(groupName)) {
                        tabbedPane.setTitleAt(i, groupName + "(L)");
                    }
                }
            }
        });
    }

    /**
     * Remove the flag from the name of a group chat to reflect that
     * the client is no longer leader of this group.
     * @param groupName
     */
    public void removeShowTabAsLeader(final String groupName) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                for(int i = 0; i < tabbedPane.getTabCount(); i++) {
                    JPanel panel = (JPanel) tabbedPane.getComponentAt(i);
                    if(panel.getName().equals(groupName)) {
                        tabbedPane.setTitleAt(i, groupName);
                    }
                }
            }
        });
    }


    /**
     * Remove a group chat from the graphical user interface.
     * @param groupName
     */
    public void removeGroupChat(final String groupName) {
        final GroupChatTab removedTab = tabs.get(groupName);

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                tabbedPane.remove(removedTab.getPanel());
                tabs.remove(groupName);
                if(tabs.size() == 0) {
                    mainMenu.remove(itemLeave);
                    itemLeave = null;
                }
            }
        });

    }

    /**
     * Fetch the name of the group chat that is currently active.
     * @return
     */
    private String getSelectedGroupName() {
        JPanel selected = (JPanel) tabbedPane.getSelectedComponent();
        if(selected == null) {
            System.err.println("SELECTED TAB = NULL");
            return null;
        }
        return selected.getName();
    }

    /**
     * Propagate graphical user actions to parent observers
     * @param observable
     * @param o
     */
    @Override
    public void update(Observable observable, Object o) {
        setChanged();
        notifyObservers(o);
    }

    /**
     * Add a chat message to the chat view, by groupname.
     * @param message
     * @param alias
     * @param senderID
     * @param groupName
     */
    public void addChatMessage(String message, String alias, String senderID, String groupName) {
        GroupChatTab groupChatTab = tabs.get(groupName);
        groupChatTab.addChatMessage(message, alias, senderID);
    }

    /**
     * Update the list of user names currently active in the group chat
     * @param memberID
     * @param groupName
     * @param alias
     */
    public void addMemberToGroup(String memberID, String groupName, String alias) {
        GroupChatTab groupChatTab = tabs.get(groupName);
        groupChatTab.addGroupMember(memberID, alias);
    }

    /**
     * Check whether a user name is currently active within a group chat
     * @param groupName
     * @return
     */
    public boolean isMemberOfGroup(String groupName) {
        return tabs.get(groupName) != null;
    }

    /**
     * Remove a list of members from a group chat
     * @param groupName
     * @param nodesToRemove
     */
    public void removeMembersFromGroup(String groupName, ArrayList<String> nodesToRemove) {
        GroupChatTab groupChatTab = tabs.get(groupName);
        groupChatTab.removeGroupMembers(nodesToRemove);

    }

    /**
     * Remove a single member from a group chat
     * @param groupName
     * @param memberID
     */
    public void removeMemberFromGroup(String groupName, String memberID) {
        GroupChatTab groupChatTab = tabs.get(groupName);
        groupChatTab.removeGroupMember(memberID);
    }

    /**
     * Fetch the JFrame related to this chat interface.
     * @return
     */
    public JFrame getFrame() {
        return frame;
    }

    /**
     * Fetch the currently used alias within a certain group chat.
     * @param groupName
     * @return
     */
    public String getAliasByGroup(String groupName) {
        return tabs.get(groupName).getAlias();
    }


    /**
     * Declares the action listener used for the different menu bar items.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == itemClose) {
            System.exit(0);
        } else if (e.getSource() == itemAlias) {
            String currentAlias = alias;
            if(getSelectedGroupName() != null && tabs.get(getSelectedGroupName()) != null) {
                currentAlias = getAliasByGroup(getSelectedGroupName());
                System.err.println("SELECTED TAB FOUND cur alias " + currentAlias);
            }

            String newAlias = JOptionPane.showInputDialog(null, "Change alias - currently: " + currentAlias);
            if(newAlias != null) {
                alias = newAlias;
                System.err.println("NEW ALIAS " + alias);
            }
            if(getSelectedGroupName() != null && tabs.get(getSelectedGroupName()) != null) {
                tabs.get(getSelectedGroupName()).addGroupMember(selfID, alias);
            }
            Notification notification = new Notification(NotificationType.ALIAS, newAlias);
            setChanged();
            notifyObservers(notification);
        } else if (e.getSource() == itemGroup) {
            String groupName = JOptionPane.showInputDialog(null, "Input group name:");
            Notification notification = new Notification(NotificationType.JOIN_GROUP, groupName);
            setChanged();
            notifyObservers(notification);
        } else if(e.getSource() == itemLeave) {
            String leaveGroupName = getSelectedGroupName();
            removeGroupChat(leaveGroupName);
            Notification notification = new Notification(NotificationType.LEAVE_GROUP, leaveGroupName);
            setChanged();
            notifyObservers(notification);
        } else if(e.getSource() == itemDebug) {
            Notification notification = new Notification(NotificationType.DEBUG, null);
            setChanged();
            notifyObservers(notification);
        }
    }


}