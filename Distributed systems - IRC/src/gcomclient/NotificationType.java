package gcomclient;

/**
 * Project: GCom
 * Package: gcomclient
 * User: c08esn c11ean
 * Date: 9/24/14
 * Time: 11:16 AM
 * The NotificationType enum provides the means to mark events sent between objects/observers
 *
 */
public enum NotificationType {
    ALIAS, JOIN_GROUP, LEAVE_GROUP, DEBUG, NODE_UNAVAILABLE, MESSAGE
}
