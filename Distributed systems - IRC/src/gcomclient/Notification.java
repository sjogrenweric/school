package gcomclient;

public class Notification {
    private NotificationType notificationType;
    private String message;
    private String groupName;
    private String alias;

    public Notification(NotificationType notificationType, String message) {
        this.notificationType = notificationType;
        this.message = message;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupName() {
        return groupName;
    }

    public String getMessage() {
        return message;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
    public String getAlias() {
        return alias;
    }

    public NotificationType getNotificationType() {
        return notificationType;
    }
}

