package gcomclient;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Observable;

/**
 * Project: GCom
 * Package: gcomclient
 * User: c08esn c11ean
 * Date: 9/24/14
 * Time: 11:16 AM
 * The GroupChatTab is a graphical container, each instance being a
 * representation of a chat with a group. Every group the client joins
 * will be contained within a separate instance of this object.
 *
 */
public class GroupChatTab extends Observable {

    private JTextArea inputField;
    private JTextArea chatHistory;
    private DefaultListModel listModel;
    private final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    private JList list;
    private Calendar cal;
    private JPanel panel;
    private final String groupName;

    public String getAlias() {
        return alias;
    }

    private String alias;
    private String selfID;
    private HashMap<String, String> aliasMap;
    private Color borderColorBlue = new Color(200,221, 242);

    /**
     * Create the group chat once the object has been instanciated.
     * @param groupName
     * @param alias
     * @param selfID
     */
    public GroupChatTab(final String groupName, final String alias, final String selfID) {
        this.groupName = groupName;
        this.alias = alias;
        this.selfID = selfID;
        aliasMap = new HashMap<String, String>();
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                panel = new JPanel(new BorderLayout());
                inputField = new JTextArea();
                chatHistory = new JTextArea();
                panel.setPreferredSize(new Dimension(800, 800));
                panel.setName(groupName);
                createChatView();
                createInputField();
                createUserList();
                updateGroupMember(alias, selfID);
                requestFocus();

            }
        });
    }

    public JPanel getPanel() {
        return panel;
    }

    /**
     * Create the window that will contain all messages sent
     * and received within the group chat
     */
    private void createChatView() {
        JScrollPane chatScroll = new JScrollPane(chatHistory);
        chatHistory.setEditable(false);
        chatScroll.setPreferredSize(new Dimension(200, 200));

        chatScroll.setBorder(BorderFactory.createMatteBorder(1, 1, 0, 0, borderColorBlue));//.createEmptyBorder(5, 5, 0, 0));
        panel.add(chatScroll, BorderLayout.CENTER);
    }

    /**
     * Add a member to the group chat by adding them to the list of members within the group
     * @param senderID
     * @param alias
     */
    public void addGroupMember(final String senderID, final String alias) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                updateGroupMember(alias, senderID);
            }
        });
    }

    /**
     * Update the current alias of a group member and remove previous entries.
     * The alias of a group member is stored in a hash map bound to the unique
     * identifier of that group member.
     * @param member
     * @param senderID
     */
    private void updateGroupMember(final String member, final String senderID) {
        String prevAlias = aliasMap.get(senderID);
        if (prevAlias == null) {
            listModel.addElement(member);
        } else if (!prevAlias.equals(member)) {
                listModel.removeElement(prevAlias);
                listModel.addElement(member);
        }
        aliasMap.put(senderID, member);
        if(senderID.equals(selfID)) {
            alias = member;
        }
    }

    /**
     * Creat the list view containg the current members of the group chat
     */
    private void createUserList() {
        listModel = new DefaultListModel();
        list = new JList(listModel);
        list.setSelectionModel(new DisabledItemSelectionModel());
        list.setLayoutOrientation(JList.VERTICAL);
        list.setVisibleRowCount(-1);

        JScrollPane listScroller = new JScrollPane(list);
        listScroller.setPreferredSize(new Dimension(150, 80));
        listScroller.setBorder(BorderFactory.createMatteBorder(1, 3, 0, 1, borderColorBlue));//.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));
        panel.add(listScroller, BorderLayout.LINE_END);
    }

    /**
     * Create the input window where the user can input messages to be
     * sent to the other members of the group.
     * This method also creates the action listeners for the input window
     * so that the user may press "ENTER" to send a message
     */
    private void createInputField() {
        inputField.setPreferredSize(new Dimension(760, 100));
        inputField.setBorder(BorderFactory.createEmptyBorder());
        inputField.setLineWrap(true);
        inputField.setWrapStyleWord(true);

        inputField.getInputMap(JComponent.WHEN_FOCUSED).put(
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "Enter released");
        inputField.getActionMap().put("Enter released", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                final String message = inputField.getText().trim();
                if (message.length() == 0) return;
                Thread dispatchThread = new Thread() {
                    public void run() {
                        Notification notification = new Notification(NotificationType.MESSAGE, message);
                        notification.setGroupName(groupName);
                        notification.setAlias(aliasMap.get(selfID));
                        addChatMessage(message, alias, selfID);
                        setChanged();
                        notifyObservers(notification);
                    }
                };
                dispatchThread.start();
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        inputField.setText("");
                    }
                });

            }
        });

        inputField.getInputMap(JComponent.WHEN_FOCUSED).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, KeyEvent.SHIFT_DOWN_MASK, true), "Shift+Enter released");
        inputField.getActionMap().put("Shift+Enter released", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        inputField.append("\n");
                    }
                });
            }
        });

        JScrollPane inputPanel = new JScrollPane(inputField);
        inputPanel.setBorder(BorderFactory.createMatteBorder(3, 1, 1, 1, borderColorBlue));

        panel.add(inputPanel, BorderLayout.PAGE_END);
    }

    /**
     * Add a chat message to the view containing the history of sent and received chat messages
     * @param text
     * @param alias
     * @param senderID
     */
    public void addChatMessage(final String text, final String alias, final String senderID) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                if(!senderID.equals(selfID)) {
                    updateGroupMember(alias, senderID);
                }

                cal = Calendar.getInstance();
                cal.getTime();
                chatHistory.append("[" + sdf.format(cal.getTime()) + "] " + aliasMap.get(senderID) + ": " + text + "\n");
            }
        });

    }

    /**
     * We want to request focus to the input field once the currently selected group chat has changed.
     */
    public void requestFocus() {
        inputField.requestFocus();
    }

    /**
     * Remove a list of group members from the view containing all members of the group
     * @param nodesToRemove
     */
    public void removeGroupMembers(final ArrayList<String> nodesToRemove) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                for (String memberID : nodesToRemove) {
                    String alias = aliasMap.get(memberID);
                    if (alias != null) {
                        aliasMap.remove(memberID);
                        listModel.removeElement(alias);
                    }
                }
            }
        });
    }

    /**
     * Remove a member from the list containg all group members
     * @param memberID
     */
    public void removeGroupMember(final String memberID) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                String alias = aliasMap.get(memberID);
                if (alias != null) {
                    aliasMap.remove(memberID);
                    listModel.removeElement(alias);
                }
            }
        });
    }


    /**
     * Disable selections in the list of group members
     */
    class DisabledItemSelectionModel extends DefaultListSelectionModel {
    @Override
    public void setSelectionInterval(int index0, int index1) {
        super.setSelectionInterval(-1, -1);
    }
}
}
