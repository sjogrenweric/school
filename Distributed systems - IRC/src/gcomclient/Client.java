package gcomclient;

import debugger.DebuggerProxy;
import groupmanagement.GroupManagement;
import messaging.ChatMessage;

import javax.swing.*;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.*;

/**
 * Project: GCom
 * Package: gcomclient
 * User: c08esn c11ean
 * Date: 9/24/14
 * Time: 11:16 AM
 * The client is a representation of a single user participating in a group chat.
 * It initiates the graphical chat interface as well as setting up the group management module.
 * It also propagates action events from the graphical user interface to the group management -
 * such as sending messages and leaveing/joining groups.
 *
 */
public class Client implements Observer {
    private static GroupManagement groupManagement;
    private String alias;
    private ChatUI chatUI;

    /**
     * Start the client, randomize a nickname & initiate the Groupmanagement module.
     *
     */
    public Client() {
        alias = "Trooper " + (int) (100 * Math.random());
        chatUI = new ChatUI(alias);
        chatUI.addObserver(this);

        try {
            groupManagement = new GroupManagement(this);
        } catch (RemoteException e) {
            System.err.println("Could not contact Name Service - is it running? Shutting down.");
            JOptionPane.showMessageDialog(chatUI.getFrame(), "Could not contact Name Service - is it running?" +
                    " Shutting down.");
            System.exit(1);
        } catch (NotBoundException e) {
            System.err.println("Contacting Name Service failed - it seems that " +
                    "the requested method isn't bound. Shutting down.");
            JOptionPane.showMessageDialog(chatUI.getFrame(), "Contacting Name Service failed - it seems that " +
                    "the requested method isn't bound. Shutting down.");
            System.exit(1);
        }

    }


    public static void main(String[] args) {
        new Client();

    }

    /**
     * Receive notifications from the graphical interface and execute actions based on the type of notification.
     * @param o
     * @param arg
     */
    @Override
    public void update(Observable o, Object arg) {

        if (o instanceof ChatUI) {
            final Notification notification = (Notification) arg;
            if (notification.getNotificationType() == NotificationType.ALIAS) {
                alias = notification.getMessage();
            } else if (notification.getNotificationType() == NotificationType.MESSAGE) {
                final String message = notification.getMessage();
                groupManagement.sendChatMessage(message, notification.getAlias(), notification.getGroupName());
            } else if (notification.getNotificationType() == NotificationType.JOIN_GROUP) {
                try {
                    String groupName = notification.getMessage();
                    if(!isMemberOfGroup(groupName)) {
                        groupManagement.joinGroup(groupName, alias);
                    } else {
                        JOptionPane.showMessageDialog(chatUI.getFrame(), "Already member of group " + groupName);
                    }

                } catch (RemoteException e) {
                    JOptionPane.showMessageDialog(chatUI.getFrame(), "Something went wrong when trying to" +
                            "join the group " + notification.getMessage() + ".");
                    e.printStackTrace();
                }
            } else if (notification.getNotificationType() == NotificationType.LEAVE_GROUP) {
                groupManagement.leaveGroup(notification.getMessage());
            } else if (notification.getNotificationType() == NotificationType.DEBUG) {
                DebuggerProxy.startDebugger();
            }

        }

    }

/*
    HERE FOLLOWS INTERFACE METHODS TO BE USED BY THE GROUP MANAGEMENT MODULE
 */

    /**
     * Check if gui has started group chat for specified group name
     * @param groupName
     * @return
     */
    public boolean isMemberOfGroup(String groupName) {
        return chatUI.isMemberOfGroup(groupName);
    }

    /**
     * Propagate a received message to the graphical interface
     * @param chatMessage
     */
    public void receiveChatMessage(ChatMessage chatMessage) {
        chatUI.addChatMessage(chatMessage.getMessage(), chatMessage.getAlias(), chatMessage.getSenderID(), chatMessage.getGroupName());
    }

    /**
     * Update the chat to illustrate the client joining a group
     * @param groupName
     */
    public void joinGroup(String groupName) {
        chatUI.createGroupChat(groupName, groupManagement.getID());
    }

    /**
     * Tell the graphical user interface to add a new member to the list of group members
     * @param memberID
     * @param groupName
     * @param alias
     */
    public void addMemberToGroup(String memberID, String groupName, String alias) {
        chatUI.addMemberToGroup(memberID, groupName, alias);

    }

    /**
     * Tell the graphical user interface to remove a list of members from a group chat
     * @param groupName
     * @param nodesToRemove
     */
    public void removeMembersFromGroup(String groupName, ArrayList<String> nodesToRemove) {
        chatUI.removeMembersFromGroup(groupName, nodesToRemove);
    }

    /**
     * Tell the graphical user inteface to remove a member from a group chat
     * @param groupName
     * @param memberID
     */
    public void removeMemberFromGroup(String groupName, String memberID) {
        chatUI.removeMemberFromGroup(groupName, memberID);
    }

    /**
     * Tell the user inteface to illustrate that the client is leader of a particular group
     * @param groupName
     */
    public void setGroupLeader(String groupName) {
        chatUI.showTabAsLeader(groupName);
    }


    /**
     * Tell the user interface to illustrate that the client is no longer the leader of a group
     * @param groupName
     */
    public void unsetGroupLeader(String groupName) {
        chatUI.removeShowTabAsLeader(groupName);
    }
}
