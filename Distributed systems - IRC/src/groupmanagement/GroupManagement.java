package groupmanagement;

import communication.MemberNode;
import communication.Node;
import debugger.DebuggerProxy;
import gcomclient.Client;
import gcomclient.Notification;
import messageordering.MessageOrdering;
import messaging.*;
import namingservice.NameServiceInterface;
import utility.ConfigReader;
import utility.Log;
import utility.Utils;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Project: GCom
 * Package: communication
 * User: c08esn c11ean
 * Date: 9/24/14
 * Time: 10:21 AM
 * Class that handles everyting connected to group management,
 * different types of system messages aswell as chatmessages and elections.
 * Also intiates the contact with the nameservice
 */
public class GroupManagement implements Observer {

    public static TimerTasker timerTask;
    private static NameServiceInterface nameService;
    private static Client client;
    private static String uid;
    private static MemberNode clientNode;
    private static ConcurrentHashMap<String, ConcurrentHashMap<String,
            Node>> groupMap = new ConcurrentHashMap<String,
            ConcurrentHashMap<String, Node>>();
    private static ConcurrentHashMap<String, String> leaderMap = new
            ConcurrentHashMap<String, String>();
    private static Log log;
    private static long electionTimeout;
    private MessageOrdering messageOrdering;

    public GroupManagement(Client client) throws RemoteException, NotBoundException {
        this.client = client;
        uid = UUID.randomUUID().toString();
        // uid = uid.substring(0, 5);
        log = new Log(this.getClass());
        System.err.println("MY ID: " + uid);
        Properties properties = readConfig();
        electionTimeout = Long.parseLong(properties.getProperty("electionTimeout"));
        messageOrdering = MessageOrdering.getReference();
        messageOrdering.addObserver(this);

        clientNode = new MemberNode(uid);
        nameService = locateNameService(Integer.parseInt(properties
                .getProperty("port")), properties.getProperty("host"));

    }

    public static String getID() {
        return uid;
    }

    /**
     * updates the Gui with the current system perfomance
     */
    public static void updateSystemPerformance() {
        DebuggerProxy.updateSystemPerformance(groupMap);
    }

    /**
     * Checks if this node is the leader of the given group
     *
     * @param id        id of node
     * @param groupName name of the group
     * @return boolean
     */
    public static boolean isLeader(String id, String groupName) {
        return id.equals(leaderMap.get(groupName));
    }

    /**
     * Reads the config.properties file
     *
     * @return Properties
     */
    private Properties readConfig() {
        ConfigReader configReader = new ConfigReader();
        Properties properties = configReader.getProperties(GroupManagement
                .class, "config.properties");
        return properties;
    }

    /**
     * Finds and return the Remote object connected to the
     * Name service
     *
     * @param port of the name service
     * @param host of the name service
     * @return NameServiceInterface
     * @throws RemoteException
     * @throws NotBoundException
     */
    private NameServiceInterface locateNameService(int port, String host) throws
            RemoteException, NotBoundException {
        Registry reg = LocateRegistry.getRegistry(host, port);
        return (NameServiceInterface) reg.lookup("nameService");
    }

    /**
     * Joins a group given a groupName, has to know the name service
     * before doing this
     *
     * @param groupName of the group
     * @throws RemoteException
     */
    public void joinGroup(String groupName, String nickname) throws RemoteException {
        MessageOrdering.addVectorClock(groupName, uid);
        ConcurrentHashMap<String, Node> members = new ConcurrentHashMap<String, Node>();
        Node leader = nameService.getGroupLeader(groupName, clientNode);
        members.put(leader.getID(), leader);
        leaderMap.put(groupName, leader.getID());
        groupMap.put(groupName, members);
        sendJoinMessage(groupName, nickname);
    }

    /**
     * Leaves a group given a group name
     *
     * @param groupName
     */
    public void leaveGroup(String groupName) {
        LeaveMessage leaveMessage = new LeaveMessage(uid, groupName);

        if(groupMap.get(groupName).size() == 1) {
            try {
                nameService.setGroupLeader(groupName, null);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        groupMap.get(groupName).remove(uid);
        sendMetaMessage(leaveMessage);
        MessageOrdering.removeVectorClock(groupName);
        groupMap.remove(groupName);
        MessageOrdering.updateGUIVectorClocks();
        DebuggerProxy.updateSystemPerformance(groupMap);
    }

    /**
     * Extracts all the nodes with higher IDs, used for bully algorithm
     *
     * @param groupName name of the group
     * @return Hashmap of the nodes
     */
    private static ConcurrentHashMap<String, Node> findHigherIdMembers(String groupName) {
        ConcurrentHashMap<String, Node> sendList = new ConcurrentHashMap<String, Node>();
        Iterator iterator = groupMap.get(groupName).entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Node> pair = (Map.Entry<String,
                    Node>) iterator.next();
            if (pair.getKey().compareTo(uid) > 0) {
                sendList.put(pair.getKey(), pair.getValue());
            }
        }
        return sendList;
    }

    /**
     * Starts an elections, either declares leader if only node in group,
     * otherwise starts bully algorithm
     *
     * @param groupName name of the group
     */
    private static void startElection(String groupName) {
        ConcurrentHashMap<String, Node> groupview = groupMap.get(groupName);
        log.info("election started " + groupview.size());
        if (groupview.size() == 1 && groupview.containsKey(uid)) {
            log.info("Thinks its alone");
            declareLeader(groupName);
        } else {
            log.info("Starting bully");
            timerTask = new TimerTasker(groupName);
            Timer timer = new Timer(true);
            timer.schedule(timerTask, electionTimeout);
            ElectionMessage electionMessage = new ElectionMessage(uid,
                    groupName, true);
            if (!sendElectionMessage(electionMessage)) {
                log.info("Found no higher id nodes, declaring leader");
                declareLeader(groupName);
            }
        }

    }

    /**
     * If the node is the highest id node in the group
     * decided by bully algorithm, calls name services to set it self
     * as leader of the group.
     *
     * @param groupName
     */
    public static void declareLeader(String groupName) {
        try {
            leaderMap.put(groupName, uid);
            nameService.setGroupLeader(groupName, clientNode);
            log.debug("New leader:" + uid);
            timerTask.cancel();
        } catch (NullPointerException e) {
            log.info("timertask null");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        client.setGroupLeader(groupName);
    }

    /**
     * Sends a bully election message to all the nodes with higher ids
     *
     * @param message
     * @return
     */
    private static boolean sendElectionMessage(ElectionMessage message) {
        if (findHigherIdMembers(message.getGroupName()).size() == 0) {
            return false;
        }
        ConcurrentHashMap<String, Node> recipients = new
                ConcurrentHashMap<String, Node>(findHigherIdMembers(message
                .getGroupName()));
        MetaMessage metaMessage = new MetaMessage(message, recipients);
        MessageOrdering.addMessageToSendQueue(metaMessage);
        return true;
    }

    /**
     * Send a message to all the nodes in the group view
     *
     * @param message
     */
    public static void sendMetaMessage(Message message) {
        ConcurrentHashMap<String, Node> recipients = new
                ConcurrentHashMap<String, Node>(groupMap.get(message
                .getGroupName()));
        MetaMessage metaMessage = new MetaMessage(message, recipients);
        MessageOrdering.addMessageToSendQueue(metaMessage);
    }


    public static void resendMetaMessage(Message message) {
        ConcurrentHashMap<String, Node> recipients = new
                ConcurrentHashMap<String, Node>(groupMap.get(message
                .getGroupName()));
        MetaMessage metaMessage = new MetaMessage(message, recipients);
        MessageOrdering.addReceivedMessageToSendQueue(metaMessage);
    }

    /**
     * Creates and sends a Chatmessage
     *
     * @param message
     * @param alias
     * @param groupName
     */
    public void sendChatMessage(String message, String alias,
                                String groupName) {
        ChatMessage chatMessage = new ChatMessage(message, uid, alias,
                groupName);
        sendMetaMessage(chatMessage);
    }

    /**
     * Creates and sends a Join message to a group
     *
     * @param groupName
     * @param nickname
     * @throws RemoteException
     */
    private void sendJoinMessage(String groupName, String nickname) throws RemoteException {
        JoinMessage joinMessage = new JoinMessage(uid, nickname,
                clientNode, groupName);
        sendMetaMessage(joinMessage);
    }


    /**
     * Send a message to one single node, used in bully algorithm
     *
     * @param message
     * @param groupName
     * @param receiverID if of receiveing node
     */
    private static void sendMetaMessageToNode(Message message,
                                              String groupName,
                                              String receiverID) {
        ConcurrentHashMap<String, Node> recipients = new
                ConcurrentHashMap<String, Node>();
        recipients.put(receiverID, groupMap.get(groupName).get(receiverID));
        MetaMessage metaMessage = new MetaMessage(message, recipients);
        MessageOrdering.addMessageToSendQueue(metaMessage);

    }

    /**
     * Creates and sends a Group view message
     *
     * @param groupName
     */
    private static void sendGroupView(String groupName) {
        GroupViewMessage groupViewMessage = new GroupViewMessage(groupMap.get
                (groupName), uid, groupName);
        sendMetaMessage(groupViewMessage);
    }


    /**
     * Receives a message from the queue and processes it.
     *
     * @param receivedMessage
     */
    public static void receiveMessage(Message receivedMessage) {

        if (receivedMessage instanceof JoinMessage) {
            processJoinMessage(receivedMessage);
        } else if (receivedMessage instanceof ChatMessage) {
            processChatMessage(receivedMessage);
        } else if (receivedMessage instanceof GroupViewMessage) {
            processGroupView(receivedMessage);
        } else if (receivedMessage instanceof ElectionMessage) {
            processElectionMessage(receivedMessage);
        } else if (receivedMessage instanceof LeaveMessage) {
            processLeaveMessage(receivedMessage);
        }
        DebuggerProxy.addReceivedMessage(receivedMessage);
    }

    /**
     * Removes the leaving node from the group view, if
     *
     * @param receivedMessage
     */
    //TODO should it start election?
    private static void processLeaveMessage(Message receivedMessage) {
        log.debug("LEAVE MESSAGE FROM: " + receivedMessage.getSenderID());
        removeMemberFromGroup(receivedMessage.getSenderID(),
                receivedMessage.getGroupName());
        MessageOrdering.removeMemberFromVector(receivedMessage.getSenderID(), receivedMessage.getGroupName());
        MessageOrdering.updateGUIVectorClocks();
        //if (!receivedMessage.getSenderID().equals(uid)) {
        if (!receivedMessage.getSenderID().equals(uid)) {
            if (leaderMap.get(receivedMessage.getGroupName()).equals(receivedMessage.getSenderID())) {
                startElection(receivedMessage.getGroupName());
            } else {
                sendGroupView(receivedMessage.getGroupName());
            }
        }


    }

    /**
     * Processes a bully election message, if it was a response from a send election
     * message, stop the election, else send to higher ids
     *
     * @param receivedMessage
     */
    private static void processElectionMessage(Message receivedMessage) {
        log.info("ELECTION MESSAGE FROM: " + receivedMessage.getSenderID());
        if (((ElectionMessage) receivedMessage).isElection()) {
            ElectionMessage electionACKMessage = new ElectionMessage(uid,
                    receivedMessage.getGroupName(), false);
            sendMetaMessageToNode(electionACKMessage,
                    receivedMessage.getGroupName(),
                    receivedMessage.getSenderID());
            if (timerTask != null) {
                timerTask.cancel();
            }
            if (!receivedMessage.getSenderID().equals(uid)) {
                startElection(receivedMessage.getGroupName());
            }
        } else {
            timerTask.cancel();
            log.debug("Canceled election since i was not highest id");
            client.unsetGroupLeader(receivedMessage.getGroupName());
        }
    }

    /**
     * Updates the group view to the group view received, and update
     * gui
     *
     * @param receivedMessage
     */
    private static void processGroupView(Message receivedMessage) {
        log.info("GROUP VIEW FROM: " + receivedMessage.getSenderID());
        try {
            if (!client.isMemberOfGroup(receivedMessage.getGroupName())) {
                client.joinGroup(receivedMessage.getGroupName());
            }
            updateGroupView((GroupViewMessage) receivedMessage);
            DebuggerProxy.updateSystemPerformance(groupMap);
        } catch (RemoteException e) {
            log.error("could not update groupview");
            e.printStackTrace();
        }
    }

    /**
     * prints the chat message to the gui
     *
     * @param receivedMessage
     */
    private static void processChatMessage(Message receivedMessage) {
        log.info("CHAT MSG FROM: " + receivedMessage.getSenderID() + " " +
                "MSG: " + ((ChatMessage) receivedMessage).getMessage());
        client.receiveChatMessage((ChatMessage) receivedMessage);
    }

    /**
     * adds a memeber to the group view and sends the new group view
     * to all the members, this is only performed by the leader of the group
     *
     * @param receivedMessage
     */
    private static void processJoinMessage(Message receivedMessage) {
        log.info("JOIN MESSAGE FROM: " + receivedMessage.getSenderID());
        addMemberToGroup(receivedMessage.getSenderID(),
                ((JoinMessage) receivedMessage).getMember(),
                receivedMessage.getGroupName());
        if (groupMap.get(receivedMessage.getGroupName()) != null &&
                !client.isMemberOfGroup(receivedMessage.getGroupName())) {
            client.joinGroup(receivedMessage.getGroupName());
            if (leaderMap.get(receivedMessage.getGroupName()).equals(uid)) {
                client.setGroupLeader(receivedMessage.getGroupName());
            } else {
                client.unsetGroupLeader(receivedMessage.getGroupName());
            }
        }
        client.addMemberToGroup(receivedMessage.getSenderID(),
                receivedMessage.getGroupName(),
                ((JoinMessage) receivedMessage).getAlias());
    }

    /**
     * Updates the group view to the one received
     *
     * @param receivedMessage
     * @throws RemoteException
     */
    private static void updateGroupView(GroupViewMessage receivedMessage)
            throws RemoteException {
        log.debug("Printing received group view: \n" + Utils.printHashMap
                (receivedMessage.getGroupView()));
        log.debug("Printing our group view: \n" + Utils.printHashMap
                (groupMap.get(receivedMessage.getGroupName())));

        String groupName = receivedMessage.getGroupName();
        ConcurrentHashMap<String, Node> currentNodes = groupMap.get(groupName);
        ArrayList<String> nodesToRemove = new ArrayList<String>();

        ConcurrentHashMap<String, Node> receivedNodes = receivedMessage
                .getGroupView();
        for (Map.Entry<String, Node> currentNode : currentNodes.entrySet()) {
            if (receivedNodes.get(currentNode.getKey()) == null) {
                nodesToRemove.add(currentNode.getKey());
            }
        }

        for (Map.Entry<String, Node> nodeInSenderView : receivedNodes
                .entrySet()) {
            if (nameService.isLeader(nodeInSenderView.getKey(),
                    receivedMessage.getGroupName())) {
                if (!leaderMap.get(receivedMessage.getGroupName()).equals
                        (nodeInSenderView.getKey())) {
                    log.info("New leader = " + nodeInSenderView.getKey());
                    leaderMap.put(receivedMessage.getGroupName(),
                            nodeInSenderView.getKey());
                    client.unsetGroupLeader(groupName);
                }

            }
        }

        MessageOrdering.updateVectorClockByView(groupName, receivedNodes);
        MessageOrdering.removeTimeStampEntries(groupName, nodesToRemove);
        groupMap.put(receivedMessage.getGroupName(), receivedNodes);
        client.removeMembersFromGroup(groupName, nodesToRemove);
        MessageOrdering.updateGUIVectorClocks();
    }

    /**
     * Removes a memeber from the group view
     *
     * @param id
     * @param groupName
     */
    private static void removeMemberFromGroup(String id, String groupName) {
        ConcurrentHashMap<String, Node> groupMembers = groupMap.get(groupName);
        if (groupMembers.containsKey(id)) {
            groupMembers.remove(id);
            MessageOrdering.removeTimeStampOfNode(groupName, id);
        }
        groupMap.put(groupName, groupMembers);
        if (!id.equals(uid)) {
            client.removeMemberFromGroup(groupName, id);
        }
    }

    /**
     * Adds a member to the group view
     *
     * @param memberID
     * @param member
     * @param groupName
     */
    private static void addMemberToGroup(String memberID, Node member,
                                         String groupName) {
        ConcurrentHashMap<String, Node> groupMembers = groupMap.get(groupName);
        if (groupMembers != null) {
            if (!groupMembers.containsKey(memberID)) {
                groupMembers.put(memberID, member);
            }
            groupMap.put(groupName, groupMembers);
            sendGroupView(groupName);
        }

    }

    /**
     * If a node crashes , this function will start an election if it
     * was the leader, otherwise remove from groupview
     *
     * @param observable
     * @param o
     */
    @Override
    public void update(Observable observable, Object o) {
        Notification failedMessage = (Notification) o;
        String idOfLeaver = failedMessage.getMessage();
        log.debug("Member can't be contacted:" + idOfLeaver + ". Checking if " +
                "leader...");
        removeMemberFromGroup(idOfLeaver, failedMessage.getGroupName());

        if (isLeader(idOfLeaver, failedMessage.getGroupName())) {
            log.debug("Leader left. Starting election.");
            startElection(failedMessage.getGroupName());
        }
        sendGroupView(failedMessage.getGroupName());
    }

}
