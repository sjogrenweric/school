package groupmanagement;

import utility.Log;

import java.util.TimerTask;

/**
 * Project: GCom
 * Package: groupmanagement
 * User: c08esn c11ean
 * Date: 10/2/14
 * Time: 4:12 PM
 * Timed task for the bully algorithm.
 */
public class TimerTasker extends TimerTask {

    private String groupName;
    private long time = 0;
    Log log = new Log(this.getClass());

    public TimerTasker(String groupName) {
        this.groupName = groupName;
        time = System.currentTimeMillis();
    }

    /**
     * if the election to higher nodes does not answer within a certain time
     * delcare this node leader
     */
    @Override
    public void run() {
        log.debug("TIMER ENDED: DECLARING MYSELF LEADER. Waited: " + (System.currentTimeMillis() - time)
                % 1000);
        GroupManagement.declareLeader(groupName);
    }
}
