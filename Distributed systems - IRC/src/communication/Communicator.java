package communication;

import gcomclient.Notification;
import gcomclient.NotificationType;
import groupmanagement.GroupManagement;
import messaging.MetaMessage;
import utility.Log;

import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Project: GCom
 * Package: communication
 * User: c08esn c11ean
 * Date: 9/24/14
 * Time: 11:16 AM
 * Communicator handles the sending of the messages to all nodes within a network of nodes.
 * Only one instance of the Communicator is created, and therefore the Communicator
 * is responsible to sending messages to all groups and not just one.
 */
public class Communicator extends Observable implements Runnable {

    private static Communicator staticSelfReference = null;
    private static LinkedBlockingQueue<MetaMessage> sendQueue;
    private Log log = new Log(this.getClass());
    private static volatile boolean sendQueueLock = false;

    private Communicator() {
    }


    /**
     * Singleton pattern
     *
     * @param sendQueue
     * @return Communicator
     */
    public static Communicator getReference(LinkedBlockingQueue<MetaMessage> sendQueue) {
        if (staticSelfReference == null) {
            staticSelfReference = new Communicator();
            Communicator.sendQueue = sendQueue;
        }
        return staticSelfReference;
    }

    /**
     * Locks the sending queue
     */
    public static void lockSendQueue() {
        sendQueueLock = true;
    }

    /**
     * Unlocks the sending queue
     */
    public static void unlockSendQueue() {
        sendQueueLock = false;
    }

    public static boolean isLocked() {
        return sendQueueLock;
    }

    /**
     * Takes the first element in the send queue if it is not locked by
     * the debugger. The elements in the send queue consists of
     * a MetaMessage, then the receive functions are called of each
     * of the nodes that the message should be send to.
     * <p/>
     * If a message fails to send, the information about the error
     * is send up to GroupManagement
     *
     * @see groupmanagement.GroupManagement
     * @see messaging.MetaMessage
     */
    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(50);
                if (!sendQueueLock && sendQueue.size() != 0) {
                    final MetaMessage metaMessage = sendQueue.take();

                    ConcurrentHashMap<String, Node> recipients = metaMessage.getRecipients();
                    Iterator iterator = recipients.entrySet().iterator();

                    if (!metaMessage.getMessage().getRecipients().contains(GroupManagement.getID())){
                        metaMessage.getMessage().addRecipient(GroupManagement.getID());
                    }
                    while (iterator.hasNext()) {
                        final Map.Entry<String, Node> pair = (Map.Entry<String,
                                Node>) iterator.next();
                        try {
                            log.info("Sending message from " + GroupManagement.getID() + " to " +
                                    pair.getKey() + ": " + metaMessage.getMessage().getClass()
                                    .getSimpleName());

                            pair.getValue().receive(metaMessage.getMessage());
                        } catch (RemoteException e) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Notification failedNotification = new Notification
                                            (NotificationType.NODE_UNAVAILABLE, pair.getKey());
                                    failedNotification.setGroupName(metaMessage.getMessage()
                                            .getGroupName());
                                    setChanged();
                                    notifyObservers(failedNotification);
                                }
                            }).start();
                        }
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
