package communication;

import messaging.Message;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Project: GCom
 * Package: communication
 * User: c08esn c11ean
 * Date: 9/24/14
 * Time: 10:25 AM
 * This interface is the means of communication between
 * members of a network, by providing remote invocation through
 * the RMI api.
 */
public interface Node extends Remote {
    public void receive(Message message) throws RemoteException;
    public String getID() throws RemoteException;
    public void ping() throws RemoteException;

}
