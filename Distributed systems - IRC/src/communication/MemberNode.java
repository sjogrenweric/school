package communication;

import messageordering.MessageOrdering;
import messaging.Message;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Project: GCom
 * Package: communication
 * User: c08esn c11ean
 * Date: 9/25/14
 * Time: 2:50 PM
 * This is an implimentation of the Node interface
 * and specifies what happens in the event of a remote
 * invoation from a different MemberNode within the network.
 */
public class MemberNode extends UnicastRemoteObject implements Node {

    private String uid;

    public MemberNode(String uid) throws RemoteException {
        this.uid = uid;
    }

    /**
     * @return ID of the node
     */
    public String getID() {
        return uid;
    }

    /**
     * Receives a message and places it upwards in the system
     * to the Message ordering layer
     *
     * @param message
     * @throws RemoteException
     * @see messageordering.MessageOrdering
     */
    @Override
    public void receive(Message message) throws RemoteException {
        MessageOrdering.addMessageToReceiveQueue(message);
    }

    /**
     * Compares the unique ids of the nodes for comparison
     *
     * @param node
     * @return
     */
    @Override
    public boolean equals(Object node) {
        if (node instanceof MemberNode) {
            return ((MemberNode) node).getID().equals(uid);
        }
        return false;
    }

    /**
     * ping method used to determine whether the node has crashed.
     *
     * @throws RemoteException
     */
    @Override
    public void ping() throws RemoteException {

    }


}
