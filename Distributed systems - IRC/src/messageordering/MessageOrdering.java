package messageordering;

import communication.Node;
import debugger.DebuggerProxy;
import groupmanagement.GroupManagement;
import messaging.LeaveMessage;
import messaging.Message;
import messaging.MetaMessage;
import messaging.SystemMessage;
import utility.Log;
import utility.VectorClock;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Project: GCom
 * Package: communication
 * User: c08esn c11ean
 * Date: 9/24/14
 * Time: 10:51 AM
 * The MessageOrdering class is a means to propagate messages to the Receive Queue as well
 * as the the Send Queue. It can be thought of as a middle hand propagating message to
 * be handled by the lower and upper layers of the client.
 * It will also to some extent call the DebuggerProxy once data such as vector clocks
 * has been modified.
 */
public class MessageOrdering extends Observable implements Observer {

    private static MessageOrdering staticReference = null;
    private static ReceiveQueue receiveQueue;
    private static SendQueue sendQueue;
    private static Log log;
    private static ConcurrentHashMap<String, VectorClock> vectorMap = new ConcurrentHashMap<String, VectorClock>();

    private MessageOrdering() {
        log = new Log(this.getClass());
        sendQueue = SendQueue.getReference();
        receiveQueue = ReceiveQueue.getReference();
        receiveQueue.start();
        sendQueue.addObserver(this);
    }

    public static MessageOrdering getReference() {
        if (staticReference == null) {
            staticReference = new MessageOrdering();
        }
        return staticReference;
    }


    /**
     * Adds a message to the send queue.
     * The vectorclock is first incremented and then
     * added to the message to be sent. The debugger interface is updated to reflect
     * this change.
     * <p/>
     * If the message is a leave message then we will remove our time stamp entry from the vector
     * clock so that the recipients don't save it to their vector clocks.
     *
     * @param metaMessage
     */
    public static void addMessageToSendQueue(MetaMessage metaMessage) {
        VectorClock vectorClock = vectorMap.get(metaMessage.getMessage().getGroupName());

        if (metaMessage.getMessage() instanceof LeaveMessage) {
            VectorClock vectorClockCopy = new VectorClock(metaMessage.getMessage().getSenderID());
            ConcurrentHashMap<String, Integer> vectorMapCopy = vectorClockCopy.getClock();
            for (Map.Entry<String, Integer> currentTimeStamp : vectorClock.getClock().entrySet()) {
                if (!currentTimeStamp.getKey().equals(metaMessage.getMessage().getSenderID())) {
                    vectorMapCopy.put(currentTimeStamp.getKey(), currentTimeStamp.getValue());
                }
            }
            vectorMapCopy.remove(metaMessage.getMessage().getSenderID());
            metaMessage.setVectorClock(vectorClockCopy);

        } else {
            if (metaMessage.getMessage() instanceof SystemMessage == false) {
                vectorClock.incrementClock();
                updateGUIVectorClocks();
            }

            metaMessage.setVectorClock(vectorClock.copy());
        }

        if (sendQueue.isLocked()) {
            DebuggerProxy.addToSendQueue(metaMessage);
        }
        sendQueue.add(metaMessage);
    }

    public static void addReceivedMessageToSendQueue(MetaMessage metaMessage) {
        VectorClock vectorClock = vectorMap.get(metaMessage.getMessage().getGroupName());

        if (sendQueue.isLocked()) {
            DebuggerProxy.addToSendQueue(metaMessage);
        }
        sendQueue.add(metaMessage);
    }

    /**
     * Get the current vectorclock of a certain group
     *
     * @param groupName
     * @return
     */
    public static VectorClock getVectorClock(String groupName) {
        return vectorMap.get(groupName);
    }

    /**
     * Add a message to the receive queue - called from the MemberNode, which in turn
     * is the interface called from other clients within the group
     *
     * @param receivedMessage
     */
    public static void addMessageToReceiveQueue(Message receivedMessage) {
        receiveQueue.add(receivedMessage);
    }

    /**
     * Update the time stamp of nodes that have a larger time stamp than
     * the one we currently have saved in our vector clock for that group
     *
     * @param message
     */
    public static void syncVectorClock(Message message) {
        VectorClock vectorClock = vectorMap.get(message.getGroupName());
        ConcurrentHashMap<String, Integer> vector = vectorClock.getClock();

        log.debug("Our clock: " + vectorClock.toString());
        log.debug("Their clock: " + message.getVectorClock().toString());
        Iterator it = message.getVectorClock().getClock().entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Integer> pair = (Map.Entry<String, Integer>) it.next();
            if (!pair.getKey().equals(GroupManagement.getID())) {
                if (vector.get(pair.getKey()) != null && pair.getValue() > vector.get(pair.getKey())) {
                    vector.put(pair.getKey(), pair.getValue());
                }
            }
        }
        DebuggerProxy.updateVectorClocks(vectorMap);
        log.debug("Our clock updated: " + vectorClock.toString());
    }

    /**
     * Add an entry to the vector clock of a certain group if that node
     * is not currently represented in that vector clock
     *
     * @param message
     */
    public static void softSyncVectorClock(Message message) {
        VectorClock vectorClock = vectorMap.get(message.getGroupName());
        ConcurrentHashMap<String, Integer> vector = vectorClock.getClock();

        log.debug("Our clock: " + vectorClock.toString());
        log.debug("Their clock: " + message.getVectorClock().toString());
        Iterator it = message.getVectorClock().getClock().entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Integer> pair = (Map.Entry<String, Integer>) it.next();
            if (!pair.getKey().equals(GroupManagement.getID())) {
                if (vector.get(pair.getKey()) == null) {
                    vector.put(pair.getKey(), pair.getValue());
                }
            }
        }
        DebuggerProxy.updateVectorClocks(vectorMap);
        log.debug("Our clock updated: " + vectorClock.toString());
    }

    /**
     * Removing a single time stamp for a node within the vector clock of a specified group.
     *
     * @param groupName
     * @param id
     */
    public static void removeTimeStampOfNode(String groupName, String id) {
        VectorClock vectorClock = vectorMap.get(groupName);
        log.debug("Removing timestamp from vector, ID: " + id);
        vectorClock.getClock().remove(id);
        log.debug("Current vector clock:" + vectorClock.toString());
    }


    @Override
    public void update(Observable observable, Object o) {
        setChanged();
        notifyObservers(o);
    }

    /**
     * Update vector clocks based upon a received group view
     *
     * @param groupName
     * @param groupView
     */
    public static void updateVectorClockByView(String groupName, ConcurrentHashMap<String, Node> groupView) {
        VectorClock vectorClock = vectorMap.get(groupName);
        ConcurrentHashMap<String, Integer> vector = vectorClock.getClock();
        log.debug("Vector before update:" + vectorClock.getClock().toString());

        Iterator receivedGroupView = groupView.entrySet().iterator();
        while (receivedGroupView.hasNext()) {

            Map.Entry<String, Node> pairVector = (Map.Entry<String, Node>) receivedGroupView.next();
            if (vector.get(pairVector.getKey()) == null) {
                vector.put(pairVector.getKey(), 0);
            }
        }

    }

    /**
     * Remove entries in the vector clock of a group based upon a list of nodes provided in
     * the method call.
     *
     * @param groupName
     * @param nodesToRemove
     */
    public static void removeTimeStampEntries(String groupName, ArrayList<String> nodesToRemove) {
        ConcurrentHashMap<String, Integer> vector = vectorMap.get(groupName).getClock();
        for (String nodeToRemove : nodesToRemove) {
            vector.remove(nodeToRemove);
        }
        log.debug("Vector after update:" + vector.toString());
    }

    /* HERE FOLLOWS METHODS USED TO COMMUNICATE WITH THE DEBUGGER */

    public static void updateVectorClocks() {
        DebuggerProxy.updateVectorClocks(vectorMap);
    }

    public static void dropSendMessage(Message dropMessage) {
        sendQueue.dropMessage(dropMessage);
    }

    public static void delaySendMessage(Message delayMessage) {
        sendQueue.delayMessage(delayMessage);
    }

    public static void releaseDelayedMessages() {
        sendQueue.releaseDelayedMessages();
    }

    public static ArrayList<MetaMessage> getDelayedMessage() {
        return sendQueue.getDelayedMessages();
    }

    public static void lockSendQueue() {
        sendQueue.lockSendQueue();
    }

    public static void unlockSendQueue() {
        sendQueue.unlockSendQueue();
    }

    public static boolean sendQueueIsLocked() {
        return sendQueue.isLocked();
    }

    public static void lockReceiveQueue() {
        receiveQueue.lockReceiveQueue();
    }

    public static void unlockReceiveQueue() {
        receiveQueue.unlockReceiveQueue();
    }

    public static boolean receiveQueueIsLocked() {
        return receiveQueue.isLocked();
    }

    public static void setUnorderedOrdering() {
        receiveQueue.setUnorderedOrdering();
    }

    public static void setCausalOrdering() {
        receiveQueue.setCausalOrdering();
    }

    public static boolean isCausalOrdering() {
        return receiveQueue.isCausalOrdering();
    }

    public static void setBasicMutlicast() {
        GroupManagement.updateSystemPerformance();
        receiveQueue.setBasicMulticast();
    }

    public static void setReliableMutlicast() {
        GroupManagement.updateSystemPerformance();
        receiveQueue.setReliableMulticast();
    }


    public static MetaMessage[] shuffleSendQueue() {
        return sendQueue.shuffleQueue();
    }

    public static void updateGUIVectorClocks() {
        DebuggerProxy.updateVectorClocks(vectorMap);
    }

    public static boolean isReliableMulticast() {
        return receiveQueue.isReliableMulticast();
    }

    public static void addVectorClock(String groupName, String id) {
        VectorClock vectorClock = new VectorClock(id);
        vectorMap.put(groupName, vectorClock);
    }

    public static void removeVectorClock(String groupName){
        System.err.println("removing vector clock for "+ groupName);
        vectorMap.remove(groupName);
    }

    public static void removeMemberFromVector(String senderID, String groupName) {
        vectorMap.get(groupName).removeMember(senderID);
    }
}

