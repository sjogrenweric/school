package messageordering;


import communication.Communicator;
import messaging.ChatMessage;
import messaging.Message;
import messaging.MetaMessage;
import messaging.SystemMessage;
import utility.Log;
import utility.VectorClock;

import java.rmi.RemoteException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;


/**
 * Project: GCom
 * Package: communication
 * User: c08esn c11ean
 * Date: 9/24/14
 * Time: 10:51 AM
 * The SendQueue is responsible for managing the queue in which
 * messages to be sent to other nodes are stored.
 * It provides the means to shuffle the queue as well
 * as other functionality requested by the Debugger.
 *
 * The messages within the queue are thereafter sent
 * by the Communicator class.
 *
 */
public class SendQueue extends Observable implements Observer {
    private static LinkedBlockingQueue<MetaMessage> sendQueue;
    private static SendQueue staticSelfReference;
    private static ArrayList<MetaMessage> delayMessages;
    private static Log log;
    private static Communicator communicator;


    private SendQueue() {
        log = new Log(this.getClass());
        communicator = Communicator.getReference(sendQueue);
        delayMessages = new ArrayList<MetaMessage>();
        communicator.addObserver(this);
        new Thread(communicator).start();
    }

    public static void lockSendQueue() {
        communicator.lockSendQueue();
    }

    public static void unlockSendQueue() {
        communicator.unlockSendQueue();
    }

    /**
     * Shuffle the Message entries within the send queue.
     * This is only used once the send queue has been locked through
     * the debugging interface.
     * @return
     */
    public static MetaMessage[] shuffleQueue() {
        MetaMessage[] mmarr = new MetaMessage[sendQueue.size()];
        int i = 0;
        Random rnd = new Random();
        while (sendQueue.peek() != null) {
            try {
                mmarr[i] = sendQueue.take();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            i++;
        }

        for (int j = mmarr.length - 1; j > 0; j--) {
            int index = rnd.nextInt(j + 1);
            MetaMessage a = mmarr[index];
            mmarr[index] = mmarr[j];
            mmarr[j] = a;
        }

        for (MetaMessage mm : mmarr) {
            sendQueue.add(mm);
        }
        return mmarr;
    }

    public static SendQueue getReference() {
        if (staticSelfReference == null) {
            sendQueue = new LinkedBlockingQueue<MetaMessage>();
            staticSelfReference = new SendQueue();
        }
        return staticSelfReference;
    }


    /**
     * Add a message to the send queue. These are then
     * sent by the Communicator to the other nodes wihtin the group.
     * @param metaMessage
     */
    public void add(MetaMessage metaMessage) {
        sendQueue.add(metaMessage);
    }


    @Override
    public void update(Observable observable, Object o) {
        setChanged();
        notifyObservers(o);
    }

    public boolean isLocked() {
        return communicator.isLocked();
    }

    /**
     * Delay a message - called by the debugging interface, only when the send
     * queue has been locked.
     * A delayed message will be put in a separate list and
     * are later re-added to the send queue once the
     * send queue lock has been lifted.
     * @param delayMessage
     */
    public void delayMessage(Message delayMessage) {
        Iterator<MetaMessage> iterator = sendQueue.iterator();
        while(iterator.hasNext()) {
            MetaMessage current = iterator.next();
            if(delayMessage.getMessageUID().equals(current.getMessage().getMessageUID())) {
                delayMessages.add(current);
                iterator.remove();
                break;
            }
        }
    }

    /**
     * Re-add messages put in the delay list to the send queue
     * once the send queue lock has been removed.
     */
    public void releaseDelayedMessages() {
        for(MetaMessage delayed : delayMessages) {
            sendQueue.add(delayed);
        }
        delayMessages = new ArrayList<MetaMessage>();
    }

    /**
     * Simply removes a message from the send queue.
     * WARNING - this will break causual ordering
     * and causual ordering must be removed once a message
     * has been dropped in order to restore the functionality of
     * the network.
     * @param dropMessage
     */
    public void dropMessage(Message dropMessage) {
        Iterator<MetaMessage> iterator = sendQueue.iterator();
        while(iterator.hasNext()) {
            MetaMessage current = iterator.next();
            if(dropMessage.getMessageUID().equals(current.getMessage().getMessageUID())) {
                iterator.remove();
                break;
            }
        }
    }

    /**
     * Get the list of all delayed messages.
     * @return
     */
    public ArrayList<MetaMessage> getDelayedMessages() {
        return delayMessages;
    }
}
