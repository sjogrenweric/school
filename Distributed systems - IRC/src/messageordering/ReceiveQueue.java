package messageordering;

import debugger.DebuggerProxy;
import groupmanagement.GroupManagement;
import messaging.ChatMessage;
import messaging.GroupViewMessage;
import messaging.Message;
import messaging.SystemMessage;
import utility.Log;
import utility.VectorClock;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Project: GCom
 * Package: communication
 * User: c08esn c11ean
 * Date: 9/24/14
 * Time: 10:51 AM
 * The ReceiveQueue class is responsible for the queue in which we store
 * the messages we receive. To that end, it is also responsible for
 * ordering the received messages in a Causal ordering (if enabled by the user),
 * otherwise the received messages will be received in an unordered fashion.
 *
 * The ReceiveQueue is executing in a thread and constantly monitors the ReceiveQueue
 * for added messages. If one has been found, then the message will be pushed down to the
 * Hold-Back queue for further processing. However, if the message is a system message, then
 * the System Message is instantly propagated to the higher layers of the system.
 */
public class ReceiveQueue extends Thread {

    private static LinkedBlockingQueue<Message> receiveQueue;
    private static ReceiveQueue staticSelfReference;
    private static Log log;
    private static LinkedBlockingQueue<Message> holdBackQueue;
    private static volatile boolean causalOrdering = true;
    private static HashMap<String, Integer> receivedMessages;
    private static volatile boolean receiveQueueLock;
    private static volatile boolean basicReliable = true;


    private ReceiveQueue() {
        log = new Log(this.getClass());
    }

    public static ReceiveQueue getReference() {
        if (staticSelfReference == null) {
            staticSelfReference = new ReceiveQueue();
            receiveQueue = new LinkedBlockingQueue<Message>();
            holdBackQueue = new LinkedBlockingQueue<Message>();
            receivedMessages = new HashMap<String, Integer>();
        }
        return staticSelfReference;
    }


    /**
     * Poll the receiveQueue for new messages.
     * If a message is of type System Message then the message will
     * be propagated instantly to the higher layers of the system.
     * If not, then the message is pushed to the Hold-Back queue to await further
     * processing: such as Causual orderMessages, if enabled by the user.
     */
    @Override
    public void run() {
        try {
            while (true) {
                Thread.sleep(50);
                if (receiveQueue.size() != 0) {
                    Message receivedMessage = receiveQueue.take();
                    if (receivedMessage instanceof SystemMessage) {
                        if (receivedMessage instanceof GroupViewMessage) {
                            MessageOrdering.softSyncVectorClock(receivedMessage);
                        }
                            GroupManagement.receiveMessage(receivedMessage);
                    } else {
                        if (!receivedMessages.containsKey(receivedMessage.getMessageUID())) {
                            Iterator<Message> iterator = holdBackQueue.iterator();
                            boolean contains = false;
                            while (iterator.hasNext()) {
                                if (iterator.next().getMessageUID().equals(receivedMessage.getMessageUID())) {
                                    contains = true;
                                }
                            }
                            if (!contains) {
                                DebuggerProxy.addToReceiveQueue(receivedMessage);
                            }
                        }
                        holdBackQueue.add(receivedMessage);
                    }
                } else {
                    orderMessages();
                }

            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Check whether the receive queue has been locked by the user, in that case
     * do nothing. Else, poll the hold back queue entries and check whether they
     * fulfill the Causual ordering properties (if enabled by the user). If the
     * message is accepted it will then be propagated to the GroupManagement module
     * for for further processing.
     */
    private void orderMessages() throws InterruptedException {
        if (!receiveQueueLock && holdBackQueue.size() > 0) {
            Iterator<Message> iterator = holdBackQueue.iterator();
            while (iterator.hasNext()) {
                Message message = iterator.next();
                VectorClock vec = message.getVectorClock();
                String senderId = message.getSenderID();
                log.debug("\n---------------------------------------------------" +
                        "\nHold back queue entry: \nSender ID: " + senderId +
                        "\nOur Vector clock: " + MessageOrdering.getVectorClock(message.getGroupName()).getClock().toString() +
                        "\nSender vector clock: \n" + vec.getClock().toString() +
                        "\nMessage type: " + message.getClass().getSimpleName
                        () + "\nMesasge: " + ((ChatMessage) message).getMessage()
                        + "\n---------------------------------------------------");

                if (causalOrdering && vec.getClock().get(senderId) == MessageOrdering.getVectorClock(message.getGroupName()).getClock().get(senderId) + 1
                        && vec.lessEqualThan(message.getVectorClock().getClock(),message.getSenderID())) {
                    log.info("Found causal ordering for message from: " + message.getSenderID());
                    DebuggerProxy.removeFromHoldBackQueue(message);
                    iterator.remove();
                    if (basicReliable) {
                        if (!receivedMessages.containsKey(message.getMessageUID())) {
                            receivedMessages.put(message.getMessageUID(), 1);
                            MessageOrdering.syncVectorClock(message);
                            GroupManagement.receiveMessage(message);
                            GroupManagement.resendMetaMessage(message);
                        }
                    } else {
                        receivedMessages.put(message.getMessageUID(), 1);
                        MessageOrdering.syncVectorClock(message);
                        GroupManagement.receiveMessage(message);
                    }
                } else if (!causalOrdering && !message.getSenderID().equals(GroupManagement.getID
                        ()) && !receivedMessages.containsKey(message.getMessageUID())) {
                    DebuggerProxy.removeFromHoldBackQueue(message);
                    iterator.remove();
                    if (basicReliable) {
                        if (!receivedMessages.containsKey(message.getMessageUID())) {
                            receivedMessages.put(message.getMessageUID(), 1);
                            MessageOrdering.syncVectorClock(message);
                            GroupManagement.receiveMessage(message);
                            GroupManagement.resendMetaMessage(message);
                        }
                    } else {
                        receivedMessages.put(message.getMessageUID(), 1);
                        MessageOrdering.syncVectorClock(message);
                        GroupManagement.receiveMessage(message);
                    }
                } else if (message.getSenderID().equals(GroupManagement.getID
                        ()) && !receivedMessages.containsKey(message.getMessageUID())) {
                    log.info("Received message from myself");
                    receivedMessages.put(message.getMessageUID(), 1);
                    iterator.remove();
                    DebuggerProxy.removeFromHoldBackQueue(message);
                    DebuggerProxy.addReceivedMessage(message);
                    MessageOrdering.updateVectorClocks();
                } else if (receivedMessages.containsKey(message.getMessageUID())) {
                    MessageOrdering.syncVectorClock(message);
                    receivedMessages.put(message.getMessageUID(), receivedMessages.get(message.getMessageUID()) + 1);
                    DebuggerProxy.updateReceivedMessageCount(message, receivedMessages.get(message.getMessageUID()));
                    log.info("Already received this message");
                    DebuggerProxy.removeFromHoldBackQueue(message);
                    iterator.remove();
                }

            }
        }
    }

    /* INTERFACE TO THE DEBUGGER - IN ORDER TO LOCK AND CHANGE MULTICAST METHODOLOGY*/
    public static void setUnorderedOrdering() {
        causalOrdering = false;
    }

    public static void setCausalOrdering() {
        causalOrdering = true;
    }

    public static boolean isCausalOrdering() {
        return causalOrdering;
    }

    public void add(Message receivedMessage) {
        receiveQueue.add(receivedMessage);
    }

    public static void lockReceiveQueue() {
        receiveQueueLock = true;
    }

    public static void unlockReceiveQueue() {
        receiveQueueLock = false;
    }

    public static boolean isLocked() {
        return receiveQueueLock;
    }

    public static void setReliableMulticast() {
        basicReliable = true;
    }

    public static void setBasicMulticast() {
        basicReliable = false;
    }

    public static boolean isReliableMulticast() {
        return basicReliable;
    }
}


