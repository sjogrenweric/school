#! /bin/bash
echo "Compiling src/ folder into bin/ and copy config files into bin."
mkdir -p bin
cd src/
javac -cp "..:../log4j-1.2.17.jar" -d ../bin  ./*/*.java
cp groupmanagement/config.properties ../bin/groupmanagement/
cp namingservice/config.properties ../bin/namingservice/

