package controller;

import aws.SQSHandler;
import com.google.gson.JsonObject;
import utils.Log;

import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by mv on 2015-05-13.
 */
public class SQSScheduler extends TimerTask {

    private LinkedBlockingQueue<JsonObject> queue;
    private static Log log = new Log(SQSScheduler.class);

    public SQSScheduler(LinkedBlockingQueue queue) {
        this.queue = queue;
    }


    /**
     * Polls the SQS.
     */
    @Override
    public void run() {
        JsonObject jo=null;
        try {
             jo = SQSHandler.getMessage();

        } catch (NullPointerException e){
//            log.info("SQS empty");
        } catch (IndexOutOfBoundsException e2){
//            log.info("SQS empty");
        }
        if(jo!=null) {
            queue.add(jo);
        }
    }
}
