package controller;

import aws.DynamoDBHandler;
import aws.S3Handler;
import aws.SQSHandler;
import com.amazonaws.util.json.JSONException;
import com.google.gson.JsonObject;


import com.google.gson.stream.MalformedJsonException;
import twod.*;
import utils.Log;
import utils.Utils;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Map;
import java.util.Timer;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * The main controller
 *
 */
public class Controller {

    private static final int SECOND = 1000;
    private static final int TIMEBETWEENSQSPOLL = 100;
    private static final String DONE = "100";
    private static final int FPS = 30;


    private static Log log;


    private JsonParser jsonParser;
    private FileManager fileManager;

    private static LinkedBlockingQueue<JsonObject> jobQueue;
    private SQSScheduler sqsScheduler;


    /**
     * Initializes the system by loading in all the essential
     * 2d components, aswell as the classes for the amazon
     * aws cloud.
     */
    public Controller() {
        log = new Log(Controller.class);
        long startTime = System.currentTimeMillis();
        log.info("Initializing the system...");

        TwodTools.init();
        fileManager = new FileManager();
        jsonParser = new JsonParser();
        jobQueue = new LinkedBlockingQueue();
        jobQueue = new LinkedBlockingQueue<JsonObject>();
        SQSHandler.init();
        DynamoDBHandler.init();
        S3Handler.init();
        startScheduler();

        log.info("Initialization completed in : " +
                (System.currentTimeMillis() - startTime) / 1000 + "s.");
        log.info("Listening for jobs...");
        run();
    }

    /**
     * Starts the schedueler that polls jobs from the
     * amazon aws SQS.
     */
    private void startScheduler() {
        Timer t = new Timer();
        sqsScheduler = new SQSScheduler(jobQueue);
        t.schedule(sqsScheduler, SECOND, TIMEBETWEENSQSPOLL);
    }


    /**
     * Looks for jobs in the jobQueue and sends it on to processing
     */
    private void run() {
        while (true) {
            if (jobQueue.peek() != null) {
                JsonObject jo = jobQueue.poll();
                log.info("Found job, starting processing of: " + jo.toString());
                String sessiondir = fileManager.downloadFilesIfNotExist(jo);
                process(jo, sessiondir);
                log.info("Listening for jobs...");
            }
        }
    }


    /**
     * Starts processing the job.
     * @param jo JsonObject
     * @param sessionDir String
     */
    public void process(JsonObject jo, String sessionDir) {
        long startTime = System.currentTimeMillis();
        String videoDir = sessionDir + File.separator + "videos";
        ArrayList<String> files = fileManager.filesInDir(videoDir);


        if (files.size() == 1) {
            switch (jo.get("Tool").getAsString()) {
                case "Rect":
                case "Pencil":
                    segment2d(jo, sessionDir, startTime, videoDir, files);
                    break;
                case "Eraser":
                    feedbackSegmentation2d(jo);
                    break;
                case "Track":
                    trackObject(videoDir, jo);
                default:
                    break;
            }
        } else if(files.size()>1){
            //TODO 3d
            log.error(log.paintItRed("TODO 3d"));
        }
    }

    /**
     * Starts and saves the tracking information to the database
     * aswell as uploads the tracking datafile to S3 storage.
     * @param videoDir String
     * @param jo JsonObject
     */
    private void trackObject(String videoDir , JsonObject jo) {
        try {

            String sessionID = jo.get("sessionID").getAsString();
            String itemID = jo.get("itemID").getAsString();
            JsonParser jsonParser = new JsonParser();
            ArrayList<String> coordinates = new ArrayList<>();

            SelectionData selectionData = extractSelectionInformation(videoDir,sessionID, jo.get("Time").getAsDouble());

            TrackingResult trackingResult = TrackingCombiner.getInstance().track(selectionData);

            log.info("asd " + trackingResult);

            String instanceID = UUID.randomUUID().toString();
            coordinates.add(jsonParser.trackingResultToJson(trackingResult, itemID,instanceID).toString());

            ArrayList<String> fileLocations = S3Handler.uploadTrackingFiles(sessionID, itemID, coordinates, instanceID);

            DynamoDBHandler.addTrackingToItem(itemID, sessionID, fileLocations);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /**
     * Used in function trackObject to extract selection data from the job.
     *
     * @param videoDir String
     * @param sessionID String
     * @param time double
     * @return
     * @throws JSONException
     */
    private SelectionData extractSelectionInformation(String videoDir, String sessionID, double time)
            throws JSONException {

        ArrayList<Point[]> points = new ArrayList<Point[]>();
        SelectionData selectionData = new SelectionData();
        FileManager fm = new FileManager();
        com.google.gson.JsonParser gsonParser = new com.google.gson.JsonParser();

        JsonObject jsonCoords = gsonParser.parse(DynamoDBHandler.getSegmentation(sessionID)).getAsJsonObject();

        String feCoords = jsonCoords.get("coords").getAsString();
        RegionOfInterest roi = new RegionOfInterest(feCoords);

        points.add(roi.getPoints());

        selectionData.setPoints(points);
        selectionData.setFrameNr((int) Math.floor(time * FPS));
        String videoFileLocation  = videoDir+ File.separator+fm.filesInDir(videoDir).get(0);

        selectionData.setVideo(videoFileLocation);

        return selectionData;
    }

    /**
     *  Used to change the segmentation using negative / positive feedback.
     * @param jo JsonObject
     */
    private void feedbackSegmentation2d(JsonObject jo) {
        JsonParser jsonParser = new JsonParser();
        String sessionID = jo.get("sessionID").getAsString();

        System.out.println("JO: " + jo);

        try {
            Map<String, Point[]> points = jsonParser.parseFeedbackCoordinates(jo);
            System.err.println("P: " + points.get("positive").length + " N: " + points.get("negative").length);
            Mask mask = SegmentationCombiner.getInstance().setFeedback(points.get("positive"), points.get("negative"));

            String segmentation = mask.getBorders().getStringRepresentation();
            String segmentationID = jo.get("segmentationID").getAsString();
            //   Utils.writeToFile(sessionID, sessionDir + "/segmentations", segmentation);
            DynamoDBHandler.addSegmentation(sessionID, segmentation, DONE, segmentationID);

        } catch (MalformedJsonException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates an saves a segmentation from a selection made by the user.
     * @param jo JsonObject
     * @param sessionDir String
     * @param startTime long
     * @param videoDir String
     * @param files ArrayList<String>
     */
    private void segment2d(JsonObject jo, String sessionDir, long startTime, String videoDir, ArrayList<String> files) {
        for (String fileInFolder : files) {
            if (fileInFolder.contains(".avi")) {
                log.debug("Only one file found in the folder, filename: " + fileInFolder);

                String sessionID = jo.get("sessionID").getAsString();

                try {
                    //DEPRICATED & not needed
                    //  imagedir = videoParser.extractImage(jo, videolocation);
                    SelectionData selectionData = jsonParser.extractJsonData(videoDir, jo);
                    log.info("Selection data completed: " + selectionData.toString());

                    for (Point[] points : selectionData.getPoints()) {

                        Mask mask = SegmentationCombiner.getInstance().segment(new RegionOfInterest(points),
                                selectionData.getVideoDir() + File.separator + fileInFolder,
                                (int) Math.floor(selectionData.getTime() * FPS));

                        String segmentation = mask.getBorders().getStringRepresentation();
                        String segmentationID = jo.get("segmentationID").getAsString();
                        Utils.writeToFile(sessionID, sessionDir + "/segmentations", segmentation);
                        DynamoDBHandler.addSegmentation(sessionID, segmentation, DONE, segmentationID);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                log.info("Segmentation completed in: " + (System.currentTimeMillis() - startTime) / 1000 + "s");
            }
        }
    }

}
