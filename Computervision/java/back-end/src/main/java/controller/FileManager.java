package controller;

import aws.DynamoDBHandler;
import aws.S3Handler;
import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.io.IOUtils;
import utils.Log;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by mv on 2015-05-21.
 */
public class FileManager {

    private static Log log;

    private static final String convertScript = "sh src/main/resources/convertmp4avi.sh ";

    public FileManager() {
        log = new Log(FileManager.class);
    }


    /**
     * Downloads files all video files connected to a specific SessionID that is defined in the JsonObject.
     * @param jo JsonObject
     * @return SessionID String
     */
    public String downloadFilesIfNotExist(JsonObject jo) {

        String sessionID = jo.get("sessionID").getAsString();
        String videoFileLocalLocation = sessionID + "/videos";
        createDirs(sessionID);

        try {
            JSONArray ja = DynamoDBHandler.getVideoItems(sessionID);
            com.google.gson.JsonParser parser = new com.google.gson.JsonParser();
            JsonArray o = (JsonArray) parser.parse(ja.toString());
            for (JsonElement video : o.getAsJsonArray()) {
                downloadFile(videoFileLocalLocation, video);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sessionID;
    }

    /**
     * Create file structure for a session.
     * @param sessionId String
     */
    private void createDirs(String sessionId) {
        new File(sessionId + File.separator + "videos").mkdirs();
        new File(sessionId + File.separator + "segmentations").mkdirs();
        new File(sessionId + File.separator + "tracking").mkdirs();
    }

    /**
     * Downloads a file and converts it to avi format to a specific location
     * @param videoFileLocalLocation String
     * @param video JsonElement
     * @throws IOException
     */
    private void downloadFile(String videoFileLocalLocation, JsonElement video) throws IOException {

        String location = video.getAsJsonObject().get("videoLocation").getAsString();
        String key = location;
        String filename = location.split("/")[location.split("/").length - 1].split("-")[0];
        log.info("dir: " + videoFileLocalLocation + "  filename: " + filename + "   key: " + key);

        InputStream input = S3Handler.downloadVideo(key);

        if (input != null) {
            String aviFileName = filename.split("-")[0].split("\\.")[0] + ".avi";
            if (!new File(videoFileLocalLocation + "/" + aviFileName).exists()) {
                saveFile(videoFileLocalLocation, filename, input);
                Runtime rt = Runtime.getRuntime();
                Process pr = rt.exec(convertScript + videoFileLocalLocation + "/" + (filename.split("\\.")[0]));
                try {
                    pr.waitFor();
                    log.info("Conversion from " + filename + " to " + aviFileName + " Completed.");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (filesInDir(videoFileLocalLocation).size() > 1) {
                    new File(videoFileLocalLocation + "/" + filename).delete();
                    log.info(filename + " deleted.");
                } else {
                    log.error("could not convert");
                }
            }
        }
    }

    /**
     * Save a file to a specifi location
     * @param videoFileLocalLocation String
     * @param filename String
     * @param input InputStream
     * @throws IOException
     */
    private void saveFile(String videoFileLocalLocation, String filename, InputStream input) throws IOException {
        File file = new File(videoFileLocalLocation + "/" + filename.split("-")[0]);
        OutputStream outputStream = new FileOutputStream(file);
        IOUtils.copy(input, outputStream);
        input.close();
        outputStream.close();
        log.info("Download complete!" + videoFileLocalLocation + "/" + filename);
        input.close();
    }

    /**
     * Return a list of all files in a specific directory.
     * @param videoDir String
     * @return ArrayList<String>
     */
    public ArrayList<String> filesInDir(String videoDir) {
        ArrayList<String> files = new ArrayList<String>();
        File dir = new File(videoDir);
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                files.add(child.getName());

            }
        }
        return files;
    }
}
