package controller;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.MalformedJsonException;
import utils.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by mv on 2015-04-29.
 */
public class FileParser {

    private static Log log = new Log(FileParser.class);


    public static JsonObject parse(File jsonFile) throws MalformedJsonException {
        String jsonLine = null;
        JsonObject jsonCoordinates= null;
        try {
            jsonLine = readFile(jsonFile);
            log.info("Parsing coordinates");
            JsonElement jelement = new JsonParser().parse(jsonLine);
            jsonCoordinates = jelement.getAsJsonObject();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonCoordinates;

    }

    private static String readFile(File file) throws IOException {
        BufferedReader reader = new BufferedReader( new FileReader(file));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");

        while( ( line = reader.readLine() ) != null ) {
            stringBuilder.append( line );
            stringBuilder.append( ls );
        }

        return stringBuilder.toString();
    }
}
