package controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.MalformedJsonException;
import com.thoughtworks.xstream.mapper.Mapper;
import twod.SelectionData;
import twod.TrackingResult;
import utils.Log;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mv on 2015-05-06.
 */
public class JsonParser {

    Log log;

    public JsonParser() {
        log = new Log(JsonParser.class);
    }

    /**
     * TODO make nicer, factory?
     *
     * @param videolocation
     * @param coordFileJson
     * @return
     */
    public SelectionData extractJsonData(String videolocation, JsonObject coordFileJson) throws Exception {
        try {
            SelectionData selection = parseCoordinates(coordFileJson);
            selection.setId(coordFileJson.get("sessionID").getAsString());
            selection.setTime(coordFileJson.get("Time").getAsDouble());
            selection.setResolution(coordFileJson.get("Resolution").getAsString());
            selection.setTool(coordFileJson.get("Tool").getAsString());
            selection.setVideo(videolocation);
         return selection;
        } catch (Exception e) {
            throw e;
        }
    }


    /**
     * Parse coordinates from a JsonObject
     *
     * @param jsonObject
     * @return SelectionData
     */
    public SelectionData parseCoordinates(JsonObject jsonObject) throws MalformedJsonException {
        ArrayList<Point[]> points = new ArrayList<Point[]>();
        JsonArray jarr = null;
        try {
             jarr = jsonObject.get("Coordinates").getAsJsonArray();
        } catch (NullPointerException e){
            log.info("not a coord json");
            return new SelectionData();
        }
        if (jarr != null) {
            int i = 0;
            Point[] p = null;
            int nrOfPoints = 0;
            for (JsonElement a : jarr) {
                nrOfPoints += a.getAsJsonArray().getAsJsonArray().size();
            }
            Point[] pointArray = new Point[nrOfPoints];

            for (JsonElement a : jarr) {
                JsonArray arr = a.getAsJsonArray();
                for(JsonElement coords : arr) {
                    pointArray[i] = new Point(coords.getAsJsonArray().get(0).getAsInt(), coords.getAsJsonArray().get(1).getAsInt());
                    i++;
                }
            }
            points.add(pointArray);

            SelectionData coordpoints = new SelectionData(points);
            return coordpoints;
        }
        throw new MalformedJsonException("Could not find Coordinates");
    }

    /**
     * Parses feedback coordinates, returns a map with positive feedback with corresponding
     * Point[], same for negative.
     * @param jsonObject JsonObject
     * @return Map<String, Point[]>
     * @throws MalformedJsonException
     */
    public Map<String, Point[]> parseFeedbackCoordinates(JsonObject jsonObject) throws MalformedJsonException {
        JsonArray jarr = null;
        try {
            jarr = jsonObject.get("Coordinates").getAsJsonArray();
        } catch (NullPointerException e){
            log.info("not a coord json");
            return null;
        }

        if (jarr != null) {
            int nrOfPositive = 0;
            int nrOfNegative = 0;
            for (JsonElement a : jarr) {
                for(JsonElement coord : a.getAsJsonArray()) {
                    if (coord.getAsJsonArray().get(2).getAsString().equals("positive")) {
                        nrOfPositive++;
                    } else {
                        nrOfNegative++;
                    }
                }
            }
            Point[] positivePoints = new Point[nrOfPositive];
            Point[] negativePoints = new Point[nrOfNegative];

            System.err.println("pos: " + nrOfPositive + " neg: " + nrOfNegative);

            int p = 0;
            int n = 0;
            for (JsonElement a : jarr) {
                JsonArray arr = a.getAsJsonArray();
                for(JsonElement coords : arr) {
                    if (coords.getAsJsonArray().get(2).getAsString().equals("positive")) {
                        positivePoints[p] = new Point(coords.getAsJsonArray().get(0).getAsInt(), coords.getAsJsonArray().get(1).getAsInt());
                        p++;
                    } else {
                        negativePoints[n] = new Point(coords.getAsJsonArray().get(0).getAsInt(), coords.getAsJsonArray().get(1).getAsInt());
                        n++;
                    }
                }
            }

            Map<String, Point[]> map = new HashMap<>();
            map.put("positive", positivePoints);
            map.put("negative", negativePoints);

            return map;
        }
        throw new MalformedJsonException("Could not find Coordinates");
    }

    /**
     * Converts a tracking result to a Json that the front end can read.
     * @param result TrackingResult
     * @param objectID String
     * @param instanceID String
     * @return JsonObject
     */
    public JsonObject trackingResultToJson(TrackingResult result, String objectID,String instanceID) {
        JsonObject json = new JsonObject();
        json.addProperty("id", objectID);

        json.addProperty("instance", instanceID);

        JsonArray coords = new JsonArray();
        for (TrackingResult.TrackingPoint trackingPoint : result.getTrackingPoints()) {
            JsonObject obj = new JsonObject();
            obj.addProperty("ms", trackingPoint.timeMs);
            obj.addProperty("coord", trackingPoint.roi.getStringRepresentation());
            coords.add(obj);
        }
        json.add("coords", coords);

        return json;
    }
}
