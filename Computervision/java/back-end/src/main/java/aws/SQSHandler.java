package aws;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.kms.model.NotFoundException;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.*;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.thoughtworks.xstream.mapper.Mapper;

import java.util.List;

/**
 * Created by mv on 2015-05-13.
 */
public class SQSHandler {

    private static AmazonSQS sqs;
    private static String queueURL = "https://sqs.us-east-1.amazonaws.com/218732864622/mv-queue";

    public static void init() {
        sqs = new AmazonSQSClient(new ProfileCredentialsProvider().getCredentials());
    }

    public static JsonObject getMessage()  throws NullPointerException, IndexOutOfBoundsException{

        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueURL);
        List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();
        Message message = messages.get(0);
        if(message==null){
            throw new  NullPointerException();
        }
        sqs.deleteMessage(new DeleteMessageRequest()
                .withQueueUrl(queueURL)
                .withReceiptHandle(message.getReceiptHandle()));
        JsonParser parser = new JsonParser();
        JsonObject o = (JsonObject)parser.parse(message.getBody());
        return o;
    }

    public static void sendMessage(String json) {
        sqs.sendMessage(new SendMessageRequest(queueURL, json));
    }
}