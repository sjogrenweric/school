package aws;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import utils.Log;

public class S3Handler {

    private static AmazonS3 s3;
    private static String bucketName = "mv-bucket";
    private static Log log;

    public static void init() {
        log = new Log(S3Handler.class);
        s3 = new AmazonS3Client(
                new ProfileCredentialsProvider().getCredentials());
    }

    public static String uploadImage(InputStream imageFile, String sessionID,
                                     String filename, long size) {

        try {
            filename = sessionID + "/images/"
                    + filename;
            ObjectMetadata obj = new ObjectMetadata();
            obj.setContentLength(size);
            s3.putObject((
                    new PutObjectRequest(bucketName, filename, imageFile, obj)
                            .withCannedAcl(CannedAccessControlList.PublicRead)));
        } catch (AmazonServiceException ase) {
            printASE(ase);
        } catch (AmazonClientException ace) {
            printACE(ace);
        }

        return "http://s3.amazonaws.com/" + bucketName + "/" + filename;
    }

    public static String uploadVideo(InputStream videoFile, String sessionID, String filename, long size) {
        //TODO Upload video to storage.

        try {
            filename = sessionID + "/videos/"
                    + filename;
            ObjectMetadata obj = new ObjectMetadata();
            obj.setContentLength(size);
            s3.putObject((
                    new PutObjectRequest(bucketName, filename, videoFile, obj)
                            .withCannedAcl(CannedAccessControlList.PublicRead)));
        } catch (AmazonServiceException ase) {
            printASE(ase);
        } catch (AmazonClientException ace) {
            printACE(ace);
        }
        return "http://s3.amazonaws.com/" + bucketName + "/" + filename;
    }

    public static InputStream downloadVideo(String key) {
        InputStream input = null;
        try {
            GetObjectRequest rangeObjectRequest = new GetObjectRequest(
                    bucketName, key);
            S3Object objectPortion = s3.getObject(rangeObjectRequest);
            input = objectPortion.getObjectContent();
        } catch (Exception e) {
            log.error(log.paintItRed("Could not find a file in the database"));
        }
        return input;
    }


    public static ArrayList<String> uploadTrackingFiles(String sessionID, String itemID, List<String> coordinates, String instanceID) {

        ArrayList<String> fileLocations = new ArrayList<>();
        try {
            for(int i = 0; i < coordinates.size(); i++) {
                String filename = sessionID + "/items/"
                        + itemID + "/" +instanceID;
                String coordinate = coordinates.get(i);
                ObjectMetadata obj = new ObjectMetadata();
                InputStream is = new ByteArrayInputStream(coordinate.getBytes());
                obj.setContentLength(coordinate.getBytes().length);
                s3.putObject((
                        new PutObjectRequest(bucketName, filename, is, obj)
                                .withCannedAcl(CannedAccessControlList.PublicRead)));
                fileLocations.add("http://s3.amazonaws.com/" + bucketName + "/" + filename);
            }


        } catch (AmazonServiceException ase) {
            printASE(ase);
        } catch (AmazonClientException ace) {
            printACE(ace);
        }

        return fileLocations;
    }

    public static String getCoordinates(List<String> fileLocations) throws IOException {

        ArrayList<String> coordList = new ArrayList();

        for(int i = 0; i < fileLocations.size(); i++) {
            URL url = new URL(fileLocations.get(i));
            HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            int responseCode = httpConn.getResponseCode();
            // always check HTTP response code first
            if (responseCode == HttpURLConnection.HTTP_OK) {
                String fileName = "";
                String disposition = httpConn.getHeaderField("Content-Disposition");
                String contentType = httpConn.getContentType();
                int contentLength = httpConn.getContentLength();

                if (disposition != null) {
                    // extracts file name from header field
                    int index = disposition.indexOf("filename=");
                    if (index > 0) {
                        fileName = disposition.substring(index + 10,
                                disposition.length() - 1);
                    }
                } else {
                    // extracts file name from URL
                    fileName = fileLocations.get(i).substring(fileLocations.get(i).lastIndexOf("/") + 1,
                            fileLocations.get(i).length());
                }

                System.out.println("Content-Type = " + contentType);
                System.out.println("Content-Disposition = " + disposition);
                System.out.println("Content-Length = " + contentLength);
                System.out.println("fileName = " + fileName);

                // opens input stream from the HTTP connection
                InputStream inputStream = httpConn.getInputStream();
                StringWriter writer = new StringWriter();
                IOUtils.copy(inputStream, writer, "UTF-8");
                String coordinates = writer.toString();

                coordList.add(coordinates);

                inputStream.close();

                System.out.println("File downloaded");
            } else {
                System.out.println("No file to download. Server replied HTTP code: " + responseCode);
            }
            httpConn.disconnect();
        }

        String json = new Gson().toJson(coordList);

        return json;

    }

    private static void printACE(AmazonClientException ace) {
        log.error("Caught an AmazonClientException, which means the client encountered "
                + "a serious internal problem while trying to communicate with SimpleDB, "
                + "such as not being able to access the network.");
        log.error("Error Message: " + ace.getMessage());
    }

    private static void printASE(AmazonServiceException ase) {
        log.info("Caught an AmazonServiceException, which means your request made it "
                + "to Amazon SimpleDB, but was rejected with an error response for some reason.");
        log.info("Error Message:    " + ase.getMessage());
        log.info("HTTP Status Code: " + ase.getStatusCode());
        log.info("AWS Error Code:   " + ase.getErrorCode());
        log.info("Error Type:       " + ase.getErrorType());
        log.info("Request ID:       " + ase.getRequestId());
    }

}