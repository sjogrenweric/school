package aws;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.google.gson.JsonObject;
import temp.PolygonContainer;
import utils.TrackingContainer;

public class DynamoDBHandler {
    private static DynamoDB dynamoDB;

    static String itemsTable = "itemsTable";
    static String polygonsTable = "polygonsTable";
    static String videoTable = "videoTable";
    static String segmentationTable = "segmentationTable";
    static String userTable = "userTable";
    static String trackingTable = "trackingTable";

    public static void init() {
        dynamoDB = new DynamoDB(new AmazonDynamoDBClient(new ProfileCredentialsProvider().getCredentials()));
    }

    /**
     * Returns a list with JSON objects based on the session ID provided.
     *
     * @param sessionID The session ID for the items.
     * @return An array list with JSON objects.
     * @throws JSONException
     */
    public static String getItems(String sessionID) throws JSONException {
        if(sessionID == null) {
            return null;
        }
        Table table = dynamoDB.getTable(itemsTable);
        Index index = table.getIndex("sessionID-index");
        ItemCollection<QueryOutcome> items = index.query("sessionID", sessionID);
        Iterator<Item> itemsIter = items.iterator();
        JSONArray jsonArray = new JSONArray();

        while (itemsIter.hasNext()) {
            Item item = itemsIter.next();
            jsonArray.put(new JSONObject(item.toJSON()));
        }

        return jsonArray.toString();
    }

    public static void deleteItem(String itemID) {
        Table table = dynamoDB.getTable(itemsTable);
        table.deleteItem("itemID", itemID);
    }

    public static void deleteSession(String sessionID) {
        Table table = dynamoDB.getTable(itemsTable);
        Index index = table.getIndex("sessionID-index");
        ItemCollection<QueryOutcome> items = index.query("sessionID", sessionID);
        Iterator<Item> itemsIter = items.iterator();

        while (itemsIter.hasNext()) {
            Item item = itemsIter.next();
            String itemID = item.getString("itemID");
            table.deleteItem("itemID", itemID);
        }
    }

    public static String addSession(String userID) {

        Table table = dynamoDB.getTable(userTable);
        String sessionID = UUID.randomUUID().toString();
        Item item = table.getItem("userID", userID,
                "sessions",
                null);
        List<String> sessions = item.getList("sessions");
        sessions.add(sessionID);

        UpdateItemSpec updateItemSpec = new UpdateItemSpec()
                .withPrimaryKey("userID", userID)
                .addAttributeUpdate(new AttributeUpdate("sessions")
                        .put(sessions));

        table.updateItem(updateItemSpec);

        return sessionID;
    }

    public static String getPolygon(String sessionID) throws JSONException {
        if(sessionID == null) {
            return null;
        }
        Table table = dynamoDB.getTable(polygonsTable);
        Index index = table.getIndex("sessionID-index");
        ItemCollection<QueryOutcome> items = index.query("sessionID", sessionID);
        Iterator<Item> itemsIter = items.iterator();
        JSONArray jsonArray = new JSONArray();

        while (itemsIter.hasNext()) {
            Item item = itemsIter.next();
            jsonArray.put(new JSONObject(item.toJSON()));
        }

        return jsonArray.toString();
    }

    public static void insertPolygon(PolygonContainer polygonContainer) throws JSONException {
        Table table = dynamoDB.getTable(polygonsTable);
        //String itemID = UUID.randomUUID().toString();
        Item item = new Item()
                .withPrimaryKey("itemID", polygonContainer.getItemID())
                .withString("sessionID", polygonContainer.getSessionID())
                .withString("instanceID", polygonContainer.getInstanceID())
                .withList("coords", polygonContainer.getCoordinates());

        table.putItem(item);
    }

    public static JSONArray getVideoItems(String sessionId) throws JSONException {
        Table table = dynamoDB.getTable(videoTable);
        Index index = table.getIndex("sessionID-index");
        ItemCollection<QueryOutcome> items = index.query("sessionID", sessionId);
        Iterator<Item> itemsIter = items.iterator();
        JSONArray jsonArray = new JSONArray();

        while (itemsIter.hasNext()) {
            Item item = itemsIter.next();
            jsonArray.put(new JSONObject(item.toJSON()));
        }

        return jsonArray;
    }

    public static String getSegmentation(String sessionID) throws JSONException {
        if (sessionID == null) {
            return null;
        }
        Table table = dynamoDB.getTable(segmentationTable);
        Item item = table.getItem("sessionID", sessionID,
                "coords, done",
                null);

        if(item == null) {
            return new JSONObject().append("done", "0").toString();
        }
        System.out.println("seg json: " + item.toJSON().toString());

        return item.toJSON();
    }

    public static void addSegmentation(String sessionID, String segmentation, String done,String segmentationID) {
        Table table = dynamoDB.getTable(segmentationTable);
        //String itemID = UUID.randomUUID().toString();
        Item item = new Item()
                .withPrimaryKey("sessionID", sessionID)
                .withString("done", done)
                .withString("coords", segmentation)
                .withString("segmentationID", segmentationID);

        table.putItem(item);
    }



    public static void addTrackingToItem(String itemID, String sessionID,  List<String> fileLocations) {

        Table table = dynamoDB.getTable(trackingTable);

        for(String fileLocation : fileLocations) {

            //TODO get instanceid in a safer way?
            String[] temp = fileLocation.split("/");
            String instanceID = temp[temp.length-1];

            Item item = new Item()
                    .withPrimaryKey("instanceID", instanceID)
                    .withString("itemID", itemID)
                    .withString("sessionID", sessionID)
                    .withString("fileLocation", fileLocation);

            table.putItem(item);
        }

        updateTrackingID(itemID);
    }

    private static void updateTrackingID(String itemID) {
        Table table = dynamoDB.getTable(itemsTable);
        UpdateItemSpec updateItemSpec = new UpdateItemSpec()
                .withPrimaryKey("itemID", itemID)
                .withAttributeUpdate(new AttributeUpdate("trackingID").put(UUID.randomUUID().toString()));
        table.updateItem(updateItemSpec);
    }

}