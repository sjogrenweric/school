package utils;

import java.util.List;

/**
 * Container for information about a tracking.
 */
public class TrackingContainer {

    List<String> fileLocations;
    String trackingID;

    /**
     * Creates a new TrackingContainer with the specified file lication and tracking id.
     * @param fileLocations
     * @param trackingID
     */
    public TrackingContainer(List<String> fileLocations, String trackingID) {
        this.fileLocations = fileLocations;
        this.trackingID = trackingID;
    }

    public List<String> getFileLocations() {
        return fileLocations;
    }

    public void setFileLocations(List<String> fileLocations) {
        this.fileLocations = fileLocations;
    }

    public String getTrackingID() {
        return trackingID;
    }

    public void setTrackingID(String trackingID) {
        this.trackingID = trackingID;
    }


}
