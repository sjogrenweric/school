package utils;

import java.io.*;

/**
 * This file contains static util methods. At the moment, it only contains a method for writing to a file.
 */
public class Utils {

    private static PrintWriter printWriter;

    /**
     * Writes the specified data to the given file in the specified directory.
     *
     * @param itemID - the name of the file
     * @param dir - the directory
     * @param data - the data
     */
    public static void  writeToFile(String itemID, String dir, String data) {

        try {
            printWriter = new PrintWriter(dir+File.separator+itemID);
            printWriter.println(data);
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
