package utils;

/**
 * Self explanatory file!
 *
 * User: c08esn c11ean
 * Date: 9/18/14
 * Time: 1:35 PM
 * Log class, uses log4j
 */


import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.sql.Date;
import java.text.SimpleDateFormat;

public class Log {

    private Logger log;
    private SimpleDateFormat ft;
    private boolean debugEnabled = false;
    private boolean errorEnabled = false;
    private boolean infoEnabled = false;
    private boolean enabled = true;
    private File logFile = null;
    private PrintWriter printWriter;

    public Log(Class c) {
        log = Logger.getLogger(c);
        PropertyConfigurator.configure("src" + File.separator+ "main"+ File.separator+
                "resources"+ File.separator+ "log4j.properties");
        ft = new SimpleDateFormat("hh_mm_ss");
    }

    public void logToFile(String name) {
        Date d = new Date(System.currentTimeMillis());

        logFile = new File(name + "_" + ft.format(d) + ".log");
        try {
            printWriter = new PrintWriter(new BufferedWriter(new FileWriter(logFile, true)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void disable() {
        enabled = false;
    }

    private void writeToFile(String data) {
        printWriter.println(data);
    }

    public void info(String message) {
        Date d = new Date(System.currentTimeMillis());
        if (enabled) {
            if (logFile == null) {
                log.info(ft.format(d) + ": " + message);
            } else {
                writeToFile(ft.format(d) + ": " + message);
            }
        }
    }

    public void debug(String message) {
        Date d = new Date(System.currentTimeMillis());
        if (logFile == null) {
            log.debug(ft.format(d) + ": " + message);
        } else {
            writeToFile("debug: " + ft.format(d) + ": " + message);
        }
    }

    public void error(String message) {
        Date d = new Date(System.currentTimeMillis());
        if (logFile == null) {
            log.error(ft.format(d) + ": " + message);
        } else {
            writeToFile("error: " + ft.format(d) + ": " + message);
        }
    }

    public void error(String message, Throwable t) {
        Date d = new Date(System.currentTimeMillis());
        //log.error(ft.format(d) + ": " + message + " ## " + e.getClass().getSimpleName() + " ## " + e.getMessage());
        log.error(ft.format(d) + ": " + message, t);
    }

    public void error(Exception e, Throwable t) {
        Date d = new Date(System.currentTimeMillis());
        //log.error(ft.format(d) + ": " + message + " ## " + e.getClass().getSimpleName() + " ## " + e.getMessage());
        log.error(e, t);
    }

    public void warn(String message) {
        Date d = new Date(System.currentTimeMillis());
        log.warn(ft.format(d) + ": " + message);
    }

    public boolean isDebugEnabled() {
        return debugEnabled;
    }

    public boolean isErrorEnabled() {
        return errorEnabled;
    }

    public String paintItRed(String text) {
        return "\u001b["  // Prefix
                + "0"        // Brightness
                + ";"        // Separator
                + "31"       // Red foreground
                + "m"        // Suffix
                + text       // the text to output
                + "\u001b[m ";
    }

    /**
     * Writes to a file
     *
     * @param message
     * @param file
     */
    public void file(String message, File file) {
        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));

            out.println(message);
            out.close();
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }

    public boolean isInfoEnabled() {
        return infoEnabled;
    }
}