package utils;

import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;
import twod.Feature;
import twod.Frame;
import twod.Mask;

import java.util.List;


/**
 * File which contains util methods used by the 2d back-end part of the project.
 */
public class TwodUtilities {

    /**
     * Converts a frame to a mat.
     *
     * @param frame the frame to be converted
     * @return the mat which corresponds to the frame
     */
    public static Mat frameToMat(Frame frame){
        Mat matFrame = new Mat(frame.rows(), frame.columns(), CvType.CV_8UC3);//CvType.CV_8UC1
        for(int i = 0; i < frame.rows(); i++)
            for(int j = 0; j < frame.columns(); j++)
                matFrame.put(i, j, new byte[]{(byte) frame.getFrame()[i][j][0], (byte) frame.getFrame()[i][j][1], (byte) frame.getFrame()[i][j][2]});
        return matFrame;
    }

    /**
     * Converts the content of a frame specified by the mask to a mat.
     *
     * @param mask the mask
     * @param frame the frame
     * @return the mat
     */
    public static Mat frameToMat(Mask mask, Frame frame){
        int[][] maskPoints = mask.getMask();
        Mat matFrame = new Mat(frame.rows(), frame.columns(), CvType.CV_8UC3);//CvType.CV_8UC1
        for(int i = 0; i < frame.rows(); i++){
            for(int j = 0; j < frame.columns(); j++){
                if(maskPoints[i][j] == 1)
                    matFrame.put(i, j, frame.getFrame()[i][j]);
                else
                    matFrame.put(i, j, new byte[]{0, 0, 0});
            }
        }
        return matFrame;
    }

    /**
     * Applies the mask to the specified mat frame and returns the result.
     *
     * @param mask the mask
     * @param matFrame the mat frame
     * @return the new mat
     */
    public static Mat getROI(Mask mask, Mat matFrame){
        Mat roiMat = matFrame.clone();
        int[][] maskPoints = mask.getMask();
        for(int i = 0; i < matFrame.rows(); i++){
            for(int j = 0; j < matFrame.cols(); j++)
                if(!(maskPoints[i][j] == 1))
                    roiMat.put(i, j, new byte[]{(byte)0, (byte)0, (byte)0});
        }
        return roiMat;
    }

    /**
     * Returns an array of points containing the corners of the specified rectangel.
     *
     * @param rect the ractangle
     * @return the points
     */
    public static java.awt.Point[] rectToPoints(Rect rect){
        java.awt.Point[] rectPoints = new java.awt.Point[5];

        rectPoints[0] = new java.awt.Point(rect.x, rect.y);
        rectPoints[1] = new java.awt.Point(rect.x + rect.width, rect.y);
        rectPoints[2] = new java.awt.Point(rect.x + rect.width, rect.y + rect.height);
        rectPoints[3] = new java.awt.Point(rect.x, rect.y + rect.height);
        rectPoints[4] = rectPoints[0];
        return rectPoints;

    }

    /**
     * Converts from a matrix of points to an array of java awt points.
     *
     * @param points the matrix of points
     * @return the array of java awt points.
     */
    public static java.awt.Point[] toPointArray(MatOfPoint points){
        Point[] pArray = points.toArray();
        java.awt.Point[] features = new java.awt.Point[pArray.length];
        for(int i = 0; i < features.length; i++){
            int x = (int) pArray[i].x;
            int y = (int) pArray[i].y;
            features[i] = new java.awt.Point(x, y);
        }
        return features;
    }

    /**
     * Converts from a matrix of key points to an array features.
     *
     * @param keyPoints the matrix of key points
     * @return the array of features
     */
    public static Feature[] toPointArray(MatOfKeyPoint keyPoints){
        KeyPoint[] kpArray = keyPoints.toArray();
        //java.awt.Point[] features = new java.awt.Point[kpArray.length];
        Feature[] features = new Feature[kpArray.length];
        for(int i = 0; i < features.length; i++){
            int x = (int) kpArray[i].pt.x;
            int y = (int) kpArray[i].pt.y;
            features[i] = new Feature(new java.awt.Point(x, y), kpArray[i].angle, kpArray[i].octave, kpArray[i].size);
        }
        return features;
    }

    /**
     * Returns a rectangle which contains all of the specified points.
     *
     * @param points the points
     * @return the rectangle
     */
    public static Rect pointsToRect(java.awt.Point[] points){
        Point[] roiPoints = new Point[points.length];
        double x = 0;
        double y = 0;
        for(int i = 0; i < points.length; i++){
            x = points[i].getX();
            y = points[i].getY();
            roiPoints[i] = new Point(x, y);
        }
        return Imgproc.boundingRect(new MatOfPoint(roiPoints));
    }

    /**
     * Converts a mat to a mask.
     *
     * @param mat the mat
     * @return the mask
     */
    public static Mask matToMask(Mat mat){
        int[][] mask = new int[mat.rows()][mat.cols()];
        for(int i = 0; i < mat.rows(); i++){
            for(int j = 0; j < mat.cols(); j++){
                mask[i][j] = (int) (mat.get(i, j)[0]) > 0? 1 : 0;
            }
        }
        return new Mask(mask);
    }

    /**
     * Converts a mask to a mat.
     *
     * @param mask the mask
     * @return the mat
     */
    public static Mat maskToMat(Mask mask){
        Mat mat = new Mat(mask.getHeight(), mask.getWidth(), CvType.CV_8UC1);//CvType.CV_8UC1

        for(int i = 0; i < mat.rows(); i++){
            for(int j = 0; j < mat.cols(); j++){
                mat.put(i, j, mask.getMask()[i][j]);
            }
        }
        return mat;
    }

    /**
     * Prints the given mat.
     *
     * @param mat the mat
     */
    public static void printMat(Mat mat){
        for(int i = 0; i < mat.rows(); i++){
            for(int j = 0; j < mat.cols(); j++){
                System.out.print((int) (mat.get(i, j)[0]));
                System.out.print(", ");
            }
            System.out.println();
        }
    }

    /**
     * Converts an array of OpenCV points to java awt points.
     *
     * @param points the array of OpenCV points
     * @return the array of java awt point
     */
    public static java.awt.Point[] pointToPoint(org.opencv.core.Point[] points){
        java.awt.Point[] points2 = new java.awt.Point[points.length];
        // Feature[] features = new Feature[pArray.length];
        for(int i = 0; i < points.length; i++){
            points2[i] = new java.awt.Point((int)points[i].x, (int)points[i].y);
        }
        return points2;
    }

    /**
     * Applies the mask to the specified frame and returns the result as a mat.
     *
     * @param mask the mask
     * @param frame the frame
     * @return the mat
     */
    public static Mat roiInMat(Mask mask, Frame frame){
        int[][] maskPoints = mask.getMask();
        Mat matFrame = new Mat(frame.rows(), frame.columns(), CvType.CV_8UC3);//CvType.CV_8UC1
        for(int i = 0; i < frame.rows(); i++){
            for(int j = 0; j < frame.columns(); j++){
                if(maskPoints[i][j] == 1)
                    matFrame.put(i, j, frame.getFrame()[i][j]);
                else
                    matFrame.put(i, j, new int[]{0, 0, 0});
            }
        }
        return matFrame;
    }

    /**
     * Finds the largest contour from a list of matrices of points.
     *
     * @param matOfPointList the list of matrices of points
     * @return the largest contour as a matrix of points.
     */
    public static MatOfPoint findLargestContour(List<MatOfPoint> matOfPointList) {
        double maxArea = 0;
        MatOfPoint largestContour = null;

        for (MatOfPoint mp : matOfPointList) {
            double area = Imgproc.contourArea(mp);
            if(area >= maxArea) {
                maxArea = area;
                largestContour = mp;
            }

        }

        return largestContour;
    }

    /**
     * Finds the largest rectangle from the given array of rectangles.
     *
     * @param rects the array of rectangles
     * @return the largest rectangle
     */
    public static Rect findLargestRect(Rect[] rects){
        double area = 0;
        int index = 0;
        for(int i = 0; i < rects.length; i++){
            if (rects[i].area() > area)
                index = i;
        }
        return rects[index];
    }
}
