package utils;

import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Removes bad points (false positives) from the tracking process.
 */
public class PointRemover {
    private List<Point[]> pointHistory;
    private boolean[] badPoints;
    private int numPoints;
    private int historySize;

    /**
     * Creates a new PointRemover with the specified settings.
     *
     * @param numPoints the number of points
     * @param historySize how far back in time (frames) the removal process should operate
     */
    public PointRemover(int numPoints, int historySize) {
        this.numPoints = numPoints;
        this.historySize = historySize;
        pointHistory = new ArrayList<>();
        badPoints = new boolean[numPoints];
        Arrays.fill(badPoints, false);
    }

    /**
     * Adds the specified points the the PointRemover.
     *
     * @param points the points
     */
    public void addPoints(Point[] points) {
        pointHistory.add(points);
        if(pointHistory.size() > historySize) {
            pointHistory.remove(0);
        }
    }

    /**
     * Removes the specified points from the PointRemover.
     *
     * @param points the points
     * @return the points that are still left after rhe removal
     */
    public Point[] removePoints(Point[] points) {
        List<Point> goodPoints = new LinkedList<>();
        for (int i = 0; i < points.length; i++) {
            if(isBadPoint(points, i)) {
                badPoints[i] = true;
            } else {
                goodPoints.add(points[i]);
            }
        }

        Point[] result = new Point[goodPoints.size()];
        goodPoints.toArray(result);
        return result;
    }

    /**
     * Returns the historical mean point of the point at the specified index.
     *
     * @param pointIndex the index of the point
     * @return the mean of the point
     */
    private Point getHistoricMeanPoint(int pointIndex) {
        Point pointSum = new Point();
        for(Point[] p : pointHistory) {
            pointSum.x += p[pointIndex].x;
            pointSum.y += p[pointIndex].y;
        }

        pointSum.x /= historySize;
        pointSum.y /= historySize;

        return pointSum;
    }

    /**
     * Returns the point which corresponds to the standard deviation of the point at the specified index.
     *
     * @param pointIndex the index of the point
     * @return the standard deviation of the point
     */
    private Point getHistoricStdDev(int pointIndex) {
        Point mean = getHistoricMeanPoint(pointIndex);
        Point temp = new Point();
        for(Point[] p : pointHistory) {
            temp.x += (mean.x-p[pointIndex].x)*(mean.x-p[pointIndex].x);
            temp.y += (mean.y-p[pointIndex].y)*(mean.y-p[pointIndex].y);
        }
        temp.x /= historySize;
        temp.y /= historySize;

        return new Point(Math.sqrt(temp.x), Math.sqrt(temp.y));
    }

    /**
     * Returns the point which corresponds to the mean standard deviation of all points.
     *
     * @return the mean standard deviation of all points
     */
    private Point getHistoricMeanStdDev() {
        Point pointSum = new Point();
        for(int i = 0; i < numPoints; i++) {
            Point stdDev = getHistoricStdDev(i);
            pointSum.x += stdDev.x;
            pointSum.y += stdDev.y;
        }

        pointSum.x /= numPoints;
        pointSum.y /= numPoints;

        return pointSum;
    }

    /**
     * Checks if the point at the specified index has moved to much compared to the points in the given array of points.
     *
     * @param points the array of points
     * @param pointIndex the index of the point
     * @return true if the point has moved to much, false otherwise
     */
    private boolean movedToMuch(Point[] points, int pointIndex) {
        if (pointHistory.size() == 0) {
            return false;
        }

        double totalMovement = 0;
        Point[] prevPoints = pointHistory.get(pointHistory.size() - 1);
        for (int i = 0; i < points.length; i++) {
            totalMovement += Math.sqrt(Math.pow(points[i].y -  prevPoints[i].y, 2) + Math.pow(prevPoints[i].x -  prevPoints[i].x, 2));
        }

        double meanMovement = totalMovement / points.length;
        double actualMovement = Math.sqrt(Math.pow(points[pointIndex].y -  prevPoints[pointIndex].y, 2) + Math.pow(prevPoints[pointIndex].x -  prevPoints[pointIndex].x, 2));
        //System.out.println(actualMovement + " " + meanMovement);

        return actualMovement > meanMovement * Math.PI * 2.2;
    }

    /**
     * Checks of the point at the specified index is a bad point.
     *
     * @param points all points
     * @param i the index
     * @return true if the point is a bad point, false otherwise
     */
    private boolean isBadPoint(Point[] points, int i) {
        return badPoints[i] || movedToMuch(points, i) || badStdDev(points, i);
    }

    /**
     * Checks if the point at tha specified index has a bad standard deviation.
     *
     * @param points all points
     * @param pointIndex the index of the point
     * @return true if the standard deviation of the point is bad, false otherwise
     */
    private boolean badStdDev(Point[] points, int pointIndex) {
        if(pointHistory.size() < historySize) {
            return false;
        }

        Point globalMeanDev = getHistoricMeanStdDev();
        Point stdDev = getHistoricStdDev(pointIndex);

        boolean bad = globalMeanDev.x > 5 && stdDev.x < globalMeanDev.x * 0.1 || globalMeanDev.y > 5 && stdDev.y < globalMeanDev.y * 0.1;

        return bad;
    }
}
