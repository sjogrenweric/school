package temp;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by mv on 2015-05-07.
 */
public class PolygonContainer {

    private String itemID;
    private String sessionID;
    private String instanceID;
    private ArrayList<Set<String>> coordinates;

    public PolygonContainer(String itemID, String sessionID, String instanceID, ArrayList<Set<String>> coordinates) {
        this.itemID = itemID;
        this.sessionID = sessionID;
        this.instanceID = instanceID;
        this.coordinates = coordinates;
    }

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getInstanceID() {
        return instanceID;
    }

    public void setInstanceID(String instanceID) {
        this.instanceID = instanceID;
    }

    public ArrayList<Set<String>> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(ArrayList<Set<String>> coordinates) {
        this.coordinates = coordinates;
    }
}