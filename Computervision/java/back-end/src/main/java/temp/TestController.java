package temp;

import aws.DynamoDBHandler;
import aws.S3Handler;
import aws.SQSHandler;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import controller.JsonParser;
import controller.SQSScheduler;
import controller.VideoParser;
import twod.SelectionData;
import utils.Log;

import java.io.File;
import java.util.Timer;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by mv on 2015-04-29.
 */
public class TestController {

    static Log log;
    private VideoParser videoParser;
    private JsonParser jsonParser;
    LinkedBlockingQueue<JsonObject> queue;
    SQSScheduler sqsScheduler;
    private String bucketName = "mv-bucket";
    private String access_key_id;
    private String secret_access_key;


    public TestController() {
        videoParser = new VideoParser();
        log = new Log(TestController.class);
        jsonParser = new JsonParser();

        queue = new LinkedBlockingQueue();
        // startScheduler();

        queue = new LinkedBlockingQueue<JsonObject>();
        SQSHandler.init();
        DynamoDBHandler.init();
        S3Handler.init();
        startScheduler();

       // init();
        run();


    }

    private void startScheduler() {
        //TODO TESTing this
        SQSHandler.sendMessage("{\"id\":\"1337\",\"Resolution\":\"300x150\",\"Tool\":\"Pencil\",\"Time\":0,\"Coordinates\":[[[188,88,false],[189,88,true],[190,88,true],[191,88,true],[192,88,true],[193,88,true],[194,88,true],[195,88,true],[196,88,true],[197,88,true],[198,88,true],[201,88,true],[204,88,true],[207,88,true],[209,88,true],[212,88,true],[213,88,true],[214,88,true],[216,88,true],[217,88,true],[218,88,true]],[[188,88,false],[189,88,true],[190,88,true],[191,88,true],[192,88,true],[193,88,true],[194,88,true],[195,88,true],[196,88,true],[197,88,true],[198,88,true],[201,88,true],[204,88,true],[207,88,true],[209,88,true],[212,88,true],[213,88,true],[214,88,true],[216,88,true],[217,88,true],[218,88,true]]]}");
        Timer t = new Timer();
        sqsScheduler = new SQSScheduler(queue);
        t.schedule(sqsScheduler, 3 * 1000, 10 * 1000);
    }


    private void run() {
        while(true){
            try {
                Thread.sleep(250);
                if(queue.peek()!=null){
                    JsonObject jo = queue.poll();
                    log.info("found message, starting processing" + jo.toString());
                    downloadFiles(jo);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

    private void downloadFiles(JsonObject jo) {
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(access_key_id, secret_access_key);
        log.info(log.paintItRed("Downloading files..."));
        String sessionID =jo.get("id").getAsString();
        String videoFileLocalLocation = "video-"+sessionID;
        String[] split = videoFileLocalLocation.split("mv-bucket");
        log.info(log.paintItRed(split[2]));
        new File("video-"+sessionID).mkdir();
        try {
            JSONArray ja = DynamoDBHandler.getVideoItems(sessionID);
            com.google.gson.JsonParser parser = new com.google.gson.JsonParser();
            JsonArray o = (JsonArray)parser.parse(ja.toString());
            for(JsonElement video : o.getAsJsonArray()){
              String location = video.getAsJsonObject().getAsString();
              log.info(location);


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void process(JsonObject jo) {

        //TODO find out work type in a switch or so.
        String videolocation = retrieveVideo();
        JsonObject coordFileJson = null;// jsonParser.getCoordinatesInformation();
        String dir = null;
        try {
            dir = videoParser.extractImage(coordFileJson, videolocation);
            SelectionData selectionData = jsonParser.extractJsonData(videolocation, coordFileJson);
            //TrackingCombiner.getInstance().track(selectionData);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * TODO should get from db and s3
     * @return
     */
    private String retrieveVideo() {
        return "/home/mv5/5dv115/testdata/catwalk_3.avi";
    }


}
