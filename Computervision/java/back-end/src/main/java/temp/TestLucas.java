package temp;

import controller.JsonParser;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.imgproc.Imgproc;
import twod.*;
import twod.Frame;
import utils.Log;
import utils.TwodUtilities;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by mv5 on 2015-05-13.
 */
public class TestLucas {

    private static Log log = new Log(TestLucas.class);

    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    public static void main(String[] argv) throws Exception {
        log.logToFile("track");


        /*RegionOfInterest roi = new RegionOfInterest(new Point[]{new Point(10,10), new Point(15, 15), new Point(10, 15), new Point(7, 7)});
        Mask mask = roi.toMask(20, 20);

        Frame frame = new Frame();
        frame.setMask(mask.getMask());
        Mat mat = TwodUtilities.frameToMat(frame);


        displayImage(Mat2BufferedImage(mat));*/
        System.out.println(Core.NATIVE_LIBRARY_NAME);
        System.out.print("Initializing segmentator... ");

        RegionOfInterest roi = new RegionOfInterest();
        //roi.setPoints(new Point[]{new Point(544, 85), new Point(735, 85), new Point(735, 371), new Point(544, 371)});
        //roi.setPoints(new Point[]{new Point(544, 85), new Point(735, 85), new Point(735, 371), new Point(544, 371)});
        roi.setPoints(new Point[]{new Point(460, 85), new Point(645, 85), new Point(645, 350), new Point(460, 350)});



        //GrabCutSegmentator seg = new GrabCutSegmentator(roi, "/home/mv/5dv115/testdata/catwalk_3.avi", 110);//70
        Segmentator seg = new CvFrameDifferentor();

        Mask mask = seg.segment(roi, "/home/mv/5dv115/testdata/catwalk_3.avi", 110);
        for(int i = 0; i < mask.getMask().length; i++) {
            System.out.println(Arrays.toString(mask.getMask()[i]));
        }
        Frame frame = new Frame();
        frame.setMask(mask.getMask());
       // Mat mat = TwodUtilities.frameToMat(frame);

       displayImage(Mat2BufferedImage(TwodUtilities.frameToMat(frame)));

        Mat mat = TwodUtilities.maskToMat(mask);
        //displayImage(Mat2BufferedImage(mat));

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(mat.clone(), contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
		System.out.println("contour size: " + contours.size());
        System.out.println(contours.get(0).size());
		RegionOfInterest roi2 = new RegionOfInterest(TwodUtilities.toPointArray(contours.get(0)));
        System.out.println(roi2.getStringRepresentation());
        //Mat mat3 = new Mat(mat.rows(), mat.cols(), CvType.CV_8UC1);
        int asd = 0;
        for(Point p : TwodUtilities.toPointArray(contours.get(0))) {
            mat.put(p.y, p.x, new byte[]{(byte)0, (byte)255, (byte)0});
            asd++;
            if(asd == 255) asd = 0;
        }

        System.out.println("Contour: " + Arrays.toString(TwodUtilities.toPointArray(contours.get(0))));


        //displayImage(Mat2BufferedImage(mat));
 /*
        Segmentator seg = new FrameDifferentor();
        System.out.println("done");

        RegionOfInterest roi = new RegionOfInterest();
        roi.setPoints(new Point[]{new Point(544,85), new Point(735,85), new Point(735,371), new Point(544,371)});
        Mask mask = seg.segment(roi, "/home/mv/5dv115/testdata/catwalk_3.avi", 70);

        //Frame frame = new Frame();
        //frame.setMask(mask.getMask());
        //displayImage(Mat2BufferedImage(TwodUtilities.frameToMat(frame)));
        Frame frame = new Frame();
        frame.setMask(mask.getMask());

        System.out.print("Calculating contours... ");
        Mat mat = TwodUtilities.frameToMat(frame);
        Mat mat2 = new Mat();
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.cvtColor(mat, mat2, Imgproc.COLOR_BGR2GRAY);
        Imgproc.findContours(mat2, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
        System.out.println(" done");

        System.out.println("Contour size: " + contours.size());

        //Mat mat3 = new Mat(mat.rows(), mat.cols(), CvType.CV_8UC1);
        RegionOfInterest roi2 = new RegionOfInterest(TwodUtilities.toPointArray(contours.get(0)));
        System.out.println(roi2.getStringRepresentation());
        int asd = 0;
        for(Point p : TwodUtilities.toPointArray(contours.get(0))) {
            mat.put(p.y, p.x, new byte[]{(byte)0, (byte)255, (byte)0});
            asd++;
            if(asd == 255) asd = 0;
        }
        displayImage(Mat2BufferedImage(mat));

        System.out.println("Contour: " + Arrays.toString(TwodUtilities.toPointArray(contours.get(0))));
*/
        //TwodUtilities.toPointArray(contours);

        System.out.println("Tracking... ");
        LucasKanadeTracker tracker = new LucasKanadeTracker();
        TrackingResult rois = tracker.track("/home/mv/5dv115/testdata/catwalk_3.avi", 110, mask);
        System.out.println("done");
        System.out.println(rois.getTrackingPoints());

        JsonParser parser = new JsonParser();
        System.out.println(parser.trackingResultToJson(rois, "56f64240-00f6-40bc-8971-ffdfa9b7adb4","1"));

       // displayImage(Mat2BufferedImage(mat));
        /*
        LucasKanadeTracker tracker = new LucasKanadeTracker();
        System.out.println("File exists: " + new File("/home/mv/5dv115/testdata/catwalk_3.avi").exists());*/
        //tracker.track("/home/mv/5dv115/testdata/catwalk_3.avi", 70, mask);
        /*
        System.out.println("File exists: " + new File("/home/mv5/5dv115/testdata/catwalk_3.avi").exists());
        tracker.track("/home/mv5/5dv115/testdata/catwalk_3.avi", 50, new Mask());*/
    }

    public static BufferedImage Mat2BufferedImage(Mat m) {
        // source:
        // http://answers.opencv.org/question/10344/opencv-java-load-image-to-gui/
        // Fastest code
        // The output can be assigned either to a BufferedImage or to an Image

        int type = BufferedImage.TYPE_BYTE_GRAY;
        if (m.channels() > 1) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        int bufferSize = m.channels() * m.cols() * m.rows();
        byte[] b = new byte[bufferSize];
        m.get(0, 0, b); // get all the pixels
        BufferedImage image = new BufferedImage(m.cols(), m.rows(), type);
        final byte[] targetPixels = ((DataBufferByte) image.getRaster()
                .getDataBuffer()).getData();
        System.arraycopy(b, 0, targetPixels, 0, b.length);
        return image;
    }

    public static void displayImage(Image img2) {
        // BufferedImage img=ImageIO.read(new File("/HelloOpenCV/lena.png"));
        ImageIcon icon = new ImageIcon(img2);
        JFrame frame = new JFrame();
        frame.setLayout(new FlowLayout());
        frame.setSize(img2.getWidth(null) + 50, img2.getHeight(null) + 50);
        JLabel lbl = new JLabel();
        lbl.setIcon(icon);
        frame.add(lbl);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
