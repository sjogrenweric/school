package twod;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import utils.Log;
import utils.TwodUtilities;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * RegionOfInterest.java
 *
 *
 * @author codegrain
 * @since 29-04-2015
 *
 */
public class RegionOfInterest {

    private Point[] points;
    Log log = new Log(RegionOfInterest.class);

    public RegionOfInterest(Point[] points) {
        this.points = points;
    }

    public RegionOfInterest() {

    }

    public  RegionOfInterest(String stringRepresentation) {
        String[] pointString = stringRepresentation.split("\\D*\\s+");
        Point[] points = new Point[(pointString.length / 2) - 1];
        log.info("points: " + Arrays.toString(pointString));
        log.info("points length: " + pointString.length);
        int count = 0;
        for(int i = 1; i < pointString.length-2; i = i+2) {
            points[count++] = new Point(Integer.parseInt(pointString[i].split("\\.")[0]), Integer.parseInt(pointString[i+1].split("\\.")[0]));
        }

        setPoints(points);
    }

    public Point[] getPoints() {
        return points;
    }

    public void setPoints(Point[] points) {
        this.points = points;
    }

    public String getStringRepresentation() {
        StringBuilder out = new StringBuilder();
        out.append("M ");

        for (int i = 0; i < points.length; i++) {
            if(i > 0) {
                out.append("L ");
            }
            out.append(points[i].getX() + " " + points[i].getY() + " ");
        }

        out.append("Z");

        return out.toString();
    }

    public String toString() {
        return getStringRepresentation();
    }

    public Mask toMask(int width, int height) {
        Mat mat = Mat.zeros(height, width, CvType.CV_8U);
        List<MatOfPoint> contours = new LinkedList<MatOfPoint>();
        MatOfPoint matOfPoint = new MatOfPoint();

        List<org.opencv.core.Point> cvPoints = new LinkedList<org.opencv.core.Point>();
        for (Point p : points) {
            cvPoints.add(new org.opencv.core.Point(p.x, p.y));
        }
        matOfPoint.fromList(cvPoints);
        contours.add(matOfPoint);
        Imgproc.drawContours(mat, contours, -1, new Scalar(1), -1);

       // System.out.println(mat);

        int[][] mask = new int[mat.rows()][mat.cols()];

        for(int row = 0; row < mat.rows(); row++) {
            for(int col = 0; col < mat.cols(); col++) {
                mask[row][col] = (int) mat.get(row, col)[0];
                //System.out.print(mask[row][col] + " ");
            }
           // System.out.println();
        }

        return new Mask(mask);
    }

}
