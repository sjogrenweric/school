package twod;

import java.awt.*;
import java.util.List;

/**
 * Container for data about a selection.
 */
public class SelectionData {

    List<Point[]> points;
    int frameNr;
    double time;
    String id;
    String resolution;
    String tool;
    String video;
    String framePath;

    /**
     * Creates a new SelectionData that contains the given list of arrays of point.
     * @param points
     */
    public SelectionData(List<Point[]> points) {
        this.points = points;
    }

    /**
     * Creates a new empty SelectionData.
     */
    public SelectionData() {}

    public String getVideoDir() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getFramePath() {
        return framePath;
    }

    public void setFramePath(String framePath) {
        this.framePath = framePath;
    }

    public int getFrameNr() {
        return frameNr;
    }

    public void setFrameNr(int frameNr) {
        this.frameNr = frameNr;
    }

    public String getTool() {
        return tool;
    }

    public void setTool(String tool) {
        this.tool = tool;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public List<Point[]> getPoints() {
        return points;
    }

    public void setPoints(List<Point[]> points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return "SelectionData{" +
                "points=" + points +
                ", id='" + id + '\'' +
                ", time='" + time + '\'' +
                ", frameNr='" + frameNr + '\'' +
                ", resolution='" + resolution + '\'' +
                ", tool='" + tool + '\'' +
                ", video='" + video + '\'' +
                ", framePath='" + framePath + '\'' +
                '}';
    }
}
