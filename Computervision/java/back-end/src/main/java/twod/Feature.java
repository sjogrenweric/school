package twod;

import java.awt.Point;


/**
 * The Feature class represents a feature point (for tracking).
 */
public class Feature{

	private Point point; //Coordinates of the feature
	private double angle; //orientation of the feature (-1 if not applicable)
	private int octave; //Octave (pyramid layer), from which the keypoint has been extracted
	private double size; //Diameter of the useful keypoint adjacent area.
	
	public Feature(Point point){
		this.point = point;
		this.angle = -1;
	}
	
	public Feature(Point point, double angle, int octave, double size){
		this.point = point;
		this.angle = angle;
		this.octave = octave;
		this.size = size;
	}
	
	public Point getPoint(){
		return this.point;
	}
	
	public double getAngle(){
		return this.angle;
	}
	
	public int getOctave(){
		return this.octave;
	}
	public double getSize(){
		return this.size;
	}
}
