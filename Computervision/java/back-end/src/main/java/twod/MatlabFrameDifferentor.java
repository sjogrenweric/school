package twod;

import com.mathworks.toolbox.javabuilder.*;
import matlab.MatlabSegment;
import utils.Log;

import java.util.Arrays;

/**
 * The MatlabFrameDifferentor segments the given area by using frame differenting. The class uses exported MATLAB
 * functions.
 */
public class MatlabFrameDifferentor implements Segmentator {

    private static Log log = new Log(MatlabFrameDifferentor.class);

    private Object[] result;
    private MatlabSegment seg;

    public MatlabFrameDifferentor() throws MWException {
        seg = new MatlabSegment();
    }

    public Mask segment(SelectionData cp) {
        return segment(new RegionOfInterest(), cp.getFramePath(), cp.getFrameNr());
    }

    public Mask segment(RegionOfInterest roi, String filepath, int frameNr) {
        Mask m = new Mask();
        try {
            MWLogicalArray selectionMask = new MWLogicalArray(roi.toMask(960, 540).getMask());
            result = seg.fdSegment(1,new Object[] {filepath, frameNr, selectionMask});
            MWLogicalArray res = (MWLogicalArray) result [0];

            int[][] mask = new int[res.getDimensions()[0]][res.getDimensions()[1]];

            int row = 0;
            int col = 0;
            for (int i = 1; i <res.numberOfElements (); i++) {
                row++;
                if (row == res.getDimensions()[0]) {
                    row = 0;
                    col++;
                }
                if (res.get(i).equals(true)) {
                    mask[row][col] = 1;
                } else{
                    mask[row][col] = 0;
                }
            }
            m.setMask(mask);
        } catch (Exception e) {
            System.err.println("Exception: " + e.toString());
        }
        finally
        {
            MWArray.disposeArray(result);
        }


        return m;
    }
}
