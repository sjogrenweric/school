package twod;

import java.awt.*;

/**
 * Frame.java
 *
 *
 * @author codegrain
 * @since 29-04-2015
 *
 */
public class Frame {

    /**
     * This class represents an image frame. The frame is represented with a 3-dimensional int array.
     */

    public static enum ColorModel {
        RGB, HSV, LAB
    }
    public static int RED = 0, GREEN = 1, BLUE = 2;
    public static int HUE = 0, SATURATION = 1, VALUE = 2;

    private int[][][] frame;

    public Frame() {

    }

    public Frame(int[][][] frame) {
        this.frame = frame;
    }

    public int[][][] getFrame() {
        return frame;
    }

    public void setFrame(int[][][] frame) {
        this.frame = frame;
    }
    
    public int rows(){
    	return this.frame.length;
    }
    
    public int columns(){
    	return this.frame[0].length;
    }

    /**
     * Applies a mask on the frame. Areas outside the mask is set to color value (0,0,0).
     * @param mask
     */
    public void setMask(int[][] mask) {
        this.frame = new int[mask.length][mask[0].length][3];
        for (int i = 0; i < frame.length; i++) {
            for (int j = 0; j <frame[i].length ; j++) {
                frame[i][j][0] = mask[i][j]*255;
                frame[i][j][1] = mask[i][j]*255;
                frame[i][j][2] = mask[i][j]*255;
            }
        }
    }
}
