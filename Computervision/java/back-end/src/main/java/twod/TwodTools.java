package twod;

/**
 * Tools for 2d.
 */
public class TwodTools {

    /**
     * Initiates the 2d back-end.
     */
    public static void init() {
        SegmentationCombiner.getInstance();
    }
}
