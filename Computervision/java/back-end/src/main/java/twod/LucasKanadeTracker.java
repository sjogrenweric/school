package twod;


import org.apache.commons.lang.ArrayUtils;
import org.bytedeco.javacpp.opencv_video;
import org.opencv.core.*;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.video.BackgroundSubtractor;
import org.opencv.video.BackgroundSubtractorKNN;
import org.opencv.video.BackgroundSubtractorMOG2;
import org.opencv.utils.Converters;
import org.opencv.video.Video;
import org.opencv.videoio.VideoCapture;
import utils.Log;
import utils.PointRemover;
import utils.TwodUtilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * LucasKanadeTracker.java
 * <p/>
 * An object tracker based on Lucas-Kanade method.
 *
 * @author codegrain
 * @since 07-05-2015
 */
public class LucasKanadeTracker implements Tracker {

    private static final double TOLERANCE = 5;


    private Log log = new Log(LucasKanadeTracker.class);

    /**
     * Tracks features in the current video
     *
     * @param featureSet a set of features to track
     * @param frameIndex the index of the current frame
     * @param frames     current video as an array of frames
     * @return a box that bounds the ROI
     */
    public TrackingResult track(Feature[] featureSet, int frameIndex, Frame[] frames) {
        return null;//use the next method for now
    }

    public TrackingResult track(String filePath, int currentFrameIndex, Mask mask) {//Frame[] frames

        VideoCapture capture = new VideoCapture(filePath);
        ArrayList<Mat> frameList = new ArrayList<Mat>();
        Mat fgMask = new Mat();
        opencv_video.BackgroundSubtractorMOG bgSub = new opencv_video.BackgroundSubtractorMOG(3);
        if (!capture.isOpened())
            capture.open(0);
        if (!capture.isOpened()) {
            System.out.println("Can't open file.");
            return null;
        }
        try {
            Mat frame = new Mat();
            while (capture.read(frame)) {
                frameList.add(frame);
                frame = new Mat();
            }
            capture.release();
        } catch (Exception e) {
            capture.release();
            e.printStackTrace();
            System.out.println("Can't read file.");
            return null;
        }

        System.out.println("Frame size:" + frameList.size());

		/*BufferedImage frame = null;
        try {
			//FrameGrab grab;
			//grab = new FrameGrab(Files.newByteChannel(FileSystems.getDefault().getPath("/home/mv/5dv115/testdata", "catwalk_3.avi")));
			//grab.seek(0);
			//grab = new FrameGrab(new File("/home/mv/5dv115/testdata/catwalk_3.avi"));
			for (int i = 0; i < 100; i++) {
				//frame = grab.getFrame();
				frame = FrameGrab.getFrame(new File("/home/mv/5dv115/testdata/catwalk_3.avi"), i);
				byte[] pixels = ((DataBufferByte) frame.getRaster().getDataBuffer()).getData();
				Mat mat = new Mat();
				mat.put(0, 0, pixels);
				frameList.add(mat);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JCodecException e) {
			e.printStackTrace();
		}*/

        //frameList.trim();
        Rect[] boundingRect = new Rect[frameList.size()];
        Mat currentROI = TwodUtilities.getROI(mask, frameList.get(currentFrameIndex));

        //Imgproc.cvtColor(currentROI, currentROI, Imgproc.COLOR_BGR2GRAY);

        //Point[] allPoints = new Point[];


		/*
		Good feature detectors to evaluate:
		FeatureDetector.DYNAMIC_MSER


		 */
        MatOfPoint2f currentFeatures = new MatOfPoint2f(EdgeCornerFeatureExtractor.goodFeatures(currentROI.clone()).toArray());
        //MatOfPoint2f currentFeatures = new MatOfPoint2f(EdgeCornerFeatureExtractor.getFeatures(currentROI.clone(), FeatureDetector.DYNAMIC_MSER).toArray());

		/*Point[] features1 = new MatOfPoint2f(EdgeCornerFeatureExtractor.goodFeatures(currentROI.clone()).toArray()).toArray();
		Point[] features2 = new MatOfPoint2f(EdgeCornerFeatureExtractor.getFeatures(currentROI.clone(), FeatureDetector.DYNAMIC_HARRIS).toArray()).toArray();
		Point[] features3 = new MatOfPoint2f(EdgeCornerFeatureExtractor.getFeatures(currentROI.clone(), FeatureDetector.FAST).toArray()).toArray();
		Point[] features4 = new MatOfPoint2f(EdgeCornerFeatureExtractor.getFeatures(currentROI.clone(), FeatureDetector.AKAZE).toArray()).toArray();
		Point[] features5 = new MatOfPoint2f(EdgeCornerFeatureExtractor.getFeatures(currentROI.clone(), FeatureDetector.GRID_MSER).toArray()).toArray();

		Object[] merge = ArrayUtils.addAll(features1, features2);
		merge = ArrayUtils.addAll(merge, features3);
		merge = ArrayUtils.addAll(merge, features4);
		merge = ArrayUtils.addAll(merge, features5);

		MatOfPoint2f currentFeatures = new MatOfPoint2f((Point[])merge);*/

        Point[] currentFeaturesArr = currentFeatures.toArray();
		/*boundingRect[currentFrameIndex] = Imgproc.boundingRect(new MatOfPoint(currentFeaturesArr));

		CascadeClassifier humanDetect = new CascadeClassifier("/home/mv/5dv115/haarcascades/haarcascade_mcs_upperbody.xml");
		double scaleFactor = 1.1;
		int minNeighbors = 3;
		int flags = opencv_objdetect.CV_HAAR_DO_CANNY_PRUNING;
		Size minSize = new Size(30, 30);
		Size maxSize = new Size(500, 500);
		boolean outputRejectLevels;*/

        //MatOfPoint2f currentFeatures = new MatOfPoint2f(EdgeCornerFeatureExtractor.getFeatures(currentROI.clone(), FeatureDetector.DYNAMIC_MSER).toArray());
        boundingRect[currentFrameIndex] = Imgproc.boundingRect(new MatOfPoint(currentFeaturesArr));

        Point[] allFeatures = currentFeaturesArr.clone();
        PointRemover pointRemover = new PointRemover(currentFeaturesArr.length, 10);

        TrackingResult trackingResult = new TrackingResult();
        Map<Point, Integer> stillPoints = new HashMap<Point, Integer>();

        for (int i = currentFrameIndex + 1; i < frameList.size(); i++) { //currentFrameIndex + 100; i++) {

			/*if(i == currentFrameIndex){
				boundingRect[i] = Imgproc.boundingRect(new MatOfPoint(currentFeaturesArr));
				continue;
			}*/
            //System.err.println("i: " + i);

            currentROI = frameList.get(i - 1);

            Mat nextROI = frameList.get(i);

            MatOfPoint2f nextFeatures = new MatOfPoint2f();
            MatOfByte status = new MatOfByte();
            MatOfFloat error = new MatOfFloat();
            int winSize = 13; //11, 13, 15
            int maxLevel = 8;//5,
            TermCriteria ofTermination = new TermCriteria();
            ofTermination.epsilon = 0.3;
            ofTermination.maxCount = 20;
            int oldData = 30;


            //Video.calcOpticalFlowPyrLK(currentROI, nextROI, currentFeatures, nextFeatures, status, error, new Size(winSize, winSize), maxLevel);
			/*List<Mat> pyramid1 = new ArrayList<Mat>(maxLevel);
			List<Mat> pyramid2 = new ArrayList<Mat>(maxLevel);
			Video.buildOpticalFlowPyramid(currentROI, pyramid1, new Size(winSize, winSize), maxLevel);
			Video.buildOpticalFlowPyramid(nextROI, pyramid2, new Size(winSize, winSize), maxLevel);

			System.out.println("TYPEEEEE : " + CvType.typeToString(pyramid1.get(0).type()) + " " + CvType.typeToString(currentROI.type()) + " " + CvType.typeToString(Converters.vector_Mat_to_Mat(pyramid1).depth()));
			System.out.println(Converters.vector_Mat_to_Mat(pyramid1).depth() + " != " + CvType.CV_8U);

			*/

            //Video.calc
            Video.calcOpticalFlowPyrLK(currentROI, nextROI, currentFeatures, nextFeatures, status, error, new Size(winSize, winSize), maxLevel, ofTermination, 0, 0.001);

            Point[] nextFeaturesArray = nextFeatures.toArray();
            Point[] currentFeaturesArray = currentFeatures.toArray();

            //for (int j = 0; j < nextFeaturesArray.length; j++) {

				/*
				if(badPoints[j]) {
					continue;
				}

				//movement = Math.abs((currentFeaturesArray[j].y -  nextFeaturesArray[j].y))/Math.abs((currentFeaturesArray[j].x - nextFeaturesArray[j].x));
				movement = Math.sqrt(Math.pow(currentFeaturesArray[j].y -  nextFeaturesArray[j].y, 2) + Math.pow(currentFeaturesArray[j].x -  nextFeaturesArray[j].x, 2));

				if (oldFeatures.size() >= oldData) {
					double bigMovement = Math.sqrt(Math.pow(currentFeaturesArray[j].y - oldFeatures.get(0)[j].y, 2) + Math.pow(currentFeaturesArray[j].x - oldFeatures.get(0)[j].x, 2));
					if(meanMegaMovement > 10 && bigMovement/meanMegaMovement < 0.25) {
						badPoints[j] = true;
						//System.out.println("REMOVEASDASDASDASDASDASD");
					}


				}
				//log.info("Mean: " + meanDist + " Movement: " + movement);

				int stillCount = 0;
				/*if(movement < 1 && meanDist > 2){
					//increase
					Integer count = stillPoints.remove(currentFeaturesArray[j]);
					count = count == null? 1 : count + 1;
					stillPoints.put(nextFeaturesArray[j], count);
					stillCount = count;
				} else {
					Integer count = stillPoints.remove(currentFeaturesArray[j]);
					if(count != null && count > 1) {
						stillPoints.put(nextFeaturesArray[j], count - 1);
					}
				}*/
				/*
				if (movement < (meanDist*TOLERANCE)) { // && stillCount < 15) {
					//goodFeatures.add(nextFeaturesArray[j]);
				} else {
					if(stillCount >= 5) {
						log.info("Removed because of still count: "  + stillCount);
					}
					badPoints[j] = true;
					//log.info("Removed: " + movement / meanDist + " (Mean: " + meanDist + ")");
				}*/
            //}
			/*nextFeaturesArray = new Point[goodFeatures.size()];
			for (int j = 0; j < goodFeatures.size(); j++) {
				nextFeaturesArray[j] = goodFeatures.get(j);
			}*/

            //log.info("Points left: " + nextFeaturesArray.length);

            //nextFeatures = new MatOfPoint2f(nextFeaturesArray);

            //Point[] nextFeaturesArr = nextFeatures.toArray();
			/*MatOfRect humanBoxes = new MatOfRect();
			humanDetect.detectMultiScale(nextROI, humanBoxes, scaleFactor, minNeighbors, flags, minSize, maxSize);
			Rect largestRect = TwodUtilities.findLargestRect(humanBoxes.toArray());
			double centroidX = largestRect.x + (largestRect.width/2);
			double centroidY = largestRect.y + (largestRect.height/2);*/

            Point[] nextFeaturesArr = nextFeatures.toArray();
			/*byte[] trackStatus = status.toArray();
			float[] trackError = error.toArray();
			for(int j = 0; j < nextFeaturesArr.length; j++){
				if (trackStatus[j] == 0 || trackError[j] > 0) {
					continue;

				}
			}*/

            Point[] goodPoints = pointRemover.removePoints(nextFeaturesArray);
            //trackingResult.addTrackingPoint((long) (i / 30.0 * 1000), new RegionOfInterest(TwodUtilities.pointToPoint(goodPoints)));
            pointRemover.addPoints(nextFeaturesArray);
            boundingRect[i] = Imgproc.boundingRect(new MatOfPoint(goodPoints));
			trackingResult.addTrackingPoint((long) (i / 30.0 * 1000), new RegionOfInterest(TwodUtilities.rectToPoints(boundingRect[i])));

			currentFeatures = nextFeatures;
            //System.out.println("Area-" + i + " = " + boundingRect[i].area() + " Points left: " + nextFeatures.toArray().length);
            //boundingRect[i] = Imgproc.boundingRect(new MatOfPoint(nextFeaturesArr));
            //System.out.println("Area-" + i + " = " + boundingRect[i].area());


            //trackingResult.addTrackingPoint((long)(i/30.0*1000), new RegionOfInterest(TwodUtilities.pointToPoint(nextFeaturesArray)));
            //boundingRect[i] = Imgproc.boundingRect(new MatOfPoint(nextFeaturesArray));
            //System.out.println("Area-" + i + " = " + boundingRect[i].area() + " Points left: " + nextFeatures.toArray().length);

            if (nextFeatures.toArray().length <= 1) {
                log.info("Object lost at frame " + i);
                break;
            }
            //currentROI = nextROI;
        }
/*
		for(int i = currentFrameIndex; i < frameList.size(); i++) {
			//changing outliers to the median value
			RegionOfInterest roi = new RegionOfInterest(TwodUtilities.rectToPoints(boundingRect[i]));
			//System.out.println(roi.getPoints());
			trackingResult.addTrackingPoint((long)(i/30.0*1000), roi);
		}*/
        return trackingResult;
    }

    private double meanMovement(Point[] prevFrame, Point[] currFrame) {
        double totalMovement = 0;
        for (int i = 0; i < prevFrame.length; i++) {
            totalMovement += Math.sqrt(Math.pow(currFrame[i].y - prevFrame[i].y, 2) + Math.pow(currFrame[i].x - prevFrame[i].x, 2));
        }

        return totalMovement / prevFrame.length;
    }
	/*private MatOfPoint extractFeatures(Mat roi){
		Imgproc.cvtColor(roi, roi, Imgproc.COLOR_BGR2GRAY);
		MatOfPoint features = new MatOfPoint();
		int maxCorners = 150;//23, 25, 50
		double qualityLevel = 0.01;
		double minDistance = 10;
		//Mat featMask = new Mat();
		int blockSize = 3;
		boolean useHarris = false;//
		double k = 0.04;
		Imgproc.goodFeaturesToTrack(roi, features, maxCorners, qualityLevel, minDistance, new Mat(), blockSize, useHarris, k);
		return features;
	}
	public MatOfPoint surfFeatures(Mat roi){
		//Mat roi = TwodUtilities.roiInMat(mask, frame);
		Imgproc.cvtColor(roi, roi, Imgproc.COLOR_BGR2GRAY);
		MatOfKeyPoint features = new MatOfKeyPoint();
		FeatureDetector siftDetector = FeatureDetector.create(FeatureDetector.DYNAMIC_MSER);
		siftDetector.detect(roi, features);
		KeyPoint[] kps = features.toArray();
		Point[] ps = new Point[kps.length];
		for(int i = 0; i < kps.length; i++){
			ps[i] = kps[i].pt;
		}
		return new MatOfPoint(ps);
	}*/
}
