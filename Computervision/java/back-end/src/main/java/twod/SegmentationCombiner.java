package twod;

import com.mathworks.toolbox.javabuilder.MWException;
import utils.Log;

import java.awt.*;

/**
 * The SegmentationCombiner combines the segmentation result of different segmentation methods. It uses
 * Grab-Cut and frame differenting.
 */
public class SegmentationCombiner implements Segmentator {


    private static Log log;
    private static SegmentationCombiner motherSegmentator = null;

    private enum SegmentationType {
        FRAME_DIFF, GRAB_CUT, NONE;
    }

    private GrabCutSegmentator grabCutSegmentator;
    private CvFrameDifferentor cvFrameDifferentor;
    private Mask latestMask;
    private SegmentationType latestSegmentationType;

    public static SegmentationCombiner getInstance() {
        if (motherSegmentator == null) {
            try {
                log = new Log(SegmentationCombiner.class);
                motherSegmentator = new SegmentationCombiner();
            } catch (MWException e) {
                throw new InstantiationError();
            }
        }

        return motherSegmentator;
    }

    private SegmentationCombiner() throws MWException {
        //frameDifferentor = new FrameDifferentor();
        latestSegmentationType = SegmentationType.NONE;
        cvFrameDifferentor = new CvFrameDifferentor();
    }

    /**
     * Segmentates the selected foreground in the specified frame number by combining different
     * segmentation methods. The method first tries the Grab-Cut algorithm. If that one fails, frame
     * differencing will be used. If neither of those algorithms succeeds of extracting the foreground,
     * no segmentation will be performed and the intital selection will be returned instead.
     *
     * @param roi region of interest in the current frame
     * @param filepath path to the video file
     * @param frameNr the frame number
     * @return the resulting segmentation
     */
    public Mask segment(RegionOfInterest roi, String filepath, int frameNr) {

        this.grabCutSegmentator = new GrabCutSegmentator(roi, filepath, frameNr);

        latestMask = this.grabCutSegmentator.segment(); //

        if (latestMask == null) {
            latestMask = cvFrameDifferentor.segment(roi, filepath, frameNr);;//resultingMasks.get(0);
            if (latestMask == null) {
                log.info("Segmentation failed. Using selection as ROI");
                latestSegmentationType = SegmentationType.NONE;
                latestMask = roi.toMask(960, 540);
            } else {
                log.info("Using frame differencing as segmentation method.");
                latestSegmentationType = SegmentationType.FRAME_DIFF;
            }
        } else {
            log.info("Using Grab-Cut as segmentation method.");
            latestSegmentationType = SegmentationType.GRAB_CUT;
        }

        return latestMask;
    }


    /**
     * Updates the segmentation result by providing feedback.
     * @param posFbPoints points indicating the positive feedback
     * @param negFbPoints points indicating the negative feedback
     * @return the resulting segmentation of the feedback
     */
    public Mask setFeedback(Point[] posFbPoints, Point[] negFbPoints) {
        Mask result;
        if (latestSegmentationType == SegmentationType.GRAB_CUT) {
            grabCutSegmentator.setFeedback(posFbPoints, negFbPoints);
            result = grabCutSegmentator.segment();
        } else {
            result = useSimpleFeedback(posFbPoints, negFbPoints);
        }

        return result;
    }

    /**
     * Implementation of a simple feedback type. The area inside the positive feedback points is added
     * to the segmentation, and the area inside the negative points is removed.
     * @param posFbPoints points indicating the positive feedback
     * @param negFbPoints points indicating the negative feedback
     * @return the resulting segmentation of the feedback
     */
    private Mask useSimpleFeedback(Point[] posFbPoints, Point[] negFbPoints) {
        int[][] positiveMask = new RegionOfInterest(posFbPoints).toMask(latestMask.getWidth(), latestMask.getHeight()).getMask();
        int[][] negativeMask = new RegionOfInterest(negFbPoints).toMask(latestMask.getWidth(), latestMask.getHeight()).getMask();
        int[][] latestMask = this.latestMask.getMask();

        for(int i = 0; i < this.latestMask.getHeight(); i++) {
            for(int j = 0; j < this.latestMask.getWidth(); j++) {
                if(positiveMask[i][j] == 1) {
                    latestMask[i][j] = 1;
                } else if(negativeMask[i][j] == 1) {
                    latestMask[i][j] = 0;
                }
            }
        }
        return this.latestMask;
    }
}
