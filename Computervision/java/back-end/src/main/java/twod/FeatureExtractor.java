package twod;

import twod.Feature;
import twod.Frame;
import twod.Mask;

/**
 * FeatureExtractor.java
 * 
 * An interface that specifies methods that all feature extractors should implement. 
 * 
 * @author codegrain
 * @since 24-04-2015
 *
 */
public interface FeatureExtractor {

	/**
	 * Extract features for the ROI in the current frame
	 * @param mask for the desired ROI
	 * @param frame current frame
	 * @return a set of features for the ROI
	 */
	Feature[] extractFeatures(Mask mask, Frame frame);
}