package twod;

import com.amazonaws.util.json.JSONException;

import java.util.*;

/**
 * Created by mv on 2015-05-07.
 */
public class TrackingCombiner {

    private static TrackingCombiner tracker = null;

    private LucasKanadeTracker lkt;

    private TrackingCombiner() {
        lkt = new LucasKanadeTracker();
    }

    public static TrackingCombiner getInstance() {
        if (tracker == null) {
            tracker = new TrackingCombiner();
        }

        return tracker;
    }


    public TrackingResult track(SelectionData selection) throws JSONException {

        RegionOfInterest roi =  new RegionOfInterest(selection.getPoints().get(0));
        //TODO hardcoded but always same size anyway
        return lkt.track(selection.getVideoDir(), selection.getFrameNr(), roi.toMask(960, 540));
    }
}
