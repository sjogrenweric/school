package twod;

import twod.Feature;
import twod.Frame;
import twod.RegionOfInterest;

/**
 * Tracker.java
 * 
 * An interface that specifies methods that all trackers should implement.
 * 
 * @author codegrain
 * @since 24-04-2015
 *
 */
public interface Tracker{

	/**
	 * Tracks features in the current video
	 * @param featureSet a set of features to track
	 * @param frameIndex the index of the current frame
	 * @param frames current video as an array of frames
	 * @return a box that bounds the ROI
	 */
	TrackingResult track(Feature[] featureSet, int frameIndex, Frame[] frames);
}