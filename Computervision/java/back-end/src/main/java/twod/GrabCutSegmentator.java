package twod;

import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import temp.TestLucas;
import utils.TwodUtilities;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * An segmentator based on Grab-Cut algorithm.
 *
 * @author codegrain
 * @since 20-05-2015
 */
public class GrabCutSegmentator implements Segmentator {

    private Mat currentFrame;
    private Rect roiRect;//inner rectangle
    private Rect outerRect;//outer rectangle -> subimage
    private Mat croppedMat;//image as big as the outer rectangle
    private Mat maskMat;//mask as big as the outer rectangle
    private Mat outputMask ;
    private Mat bgdModel;
    private Mat fgdModel;
    private boolean feedbackSet = false;

    /**
     * GrabCutSegmentator constructor
     *
     * @param roi region of interest in the current frame
     * @param filepath path to the video file
     * @param frameNr the frame number
     */
    public GrabCutSegmentator(RegionOfInterest roi, String filepath, int frameNr){

        VideoCapture capture = new VideoCapture(filepath);
        this.currentFrame = new Mat();
        if(!capture.isOpened()) {
            capture.open(0);
        }
        if(!capture.isOpened()){
            System.out.println("Can't open file.");
            return;
        }
        try{
            int i = 0;
            while(capture.read(this.currentFrame)) {
                if(i == frameNr){
                    capture.release();
                    break;
                }
                i++;
            }
            capture.release();
        }catch(Exception e){
            capture.release();
            e.printStackTrace();
            System.out.println("Can't read file.");
            return;
        }
        this.roiRect = TwodUtilities.pointsToRect(roi.getPoints());
        this.roiRect.x++;
        this.roiRect.y++;
        this.roiRect.width -= 2;
        this.roiRect.height -= 2;
        this.outputMask = new Mat();

        this.croppedMat = cropImage(roiRect); //subimage -> for speed
        this.maskMat = new Mat(this.croppedMat.size(), CvType.CV_8UC1);
        this.maskMat = modifyMask();
    }

    /**
     * Segments out a ROI from the current frame using Grab-Cut algorithm
     *
     * @return a mask for the ROI
     */
    public Mask segment(){
        int iterCount = 1;
        int mode = 0;
        if(!this.feedbackSet)
            mode = Imgproc.GC_INIT_WITH_RECT;//without feedback
        else {
            mode = Imgproc.GC_INIT_WITH_MASK;//with feedback
        }
        this.bgdModel = new Mat();//output
        this.fgdModel = new Mat();//output
        this.outputMask = this.maskMat;
        Imgproc.grabCut(this.croppedMat, outputMask, this.roiRect, bgdModel, fgdModel, iterCount, mode);
        Mat outputCopy = outputMask.clone();

        for(int i = 0; i < outputCopy.rows(); i++){
            for(int j = 0; j < outputCopy.cols(); j++){
                if(((int)outputCopy.get(i,j)[0] & 1) == 1)
                    outputCopy.put(i, j, 1);
                else
                    outputCopy.put(i, j, 0);
            }
        }

        Mat finalMask = Mat.zeros(this.currentFrame.size(), CvType.CV_8UC1);//as big as the original frame
        outputCopy.copyTo(finalMask.submat(this.outerRect));

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Mat maskCopy = finalMask.clone();
        Imgproc.findContours(maskCopy, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
        double maxSize = 0;
        int maxIndex = -1;

        if (contours.size() == 0) {
            return null;
        }

        for(int i = 0; i < contours.size(); i++){
            double tempArea = Imgproc.contourArea(contours.get(i));
            if(tempArea > maxSize){
                maxSize = tempArea;
                maxIndex = i;
            }
        }

        for(int i = 0; i < contours.size(); i++){
            if(i == maxIndex) {
                Imgproc.drawContours(finalMask, contours, i, new Scalar(255), -1);
            } else {
                Imgproc.drawContours(finalMask, contours, i, new Scalar(0), -1);
            }
        }

        return TwodUtilities.matToMask(finalMask);
    }

    /**
     * Segments out a ROI from the current frame using Grab-Cut algorithm
     * @param roi region of interest in the current frame
     * @param filepath path to the video file
     * @param frameNr the frame number
     * @return a mask for the ROI
     */
	public Mask segment(RegionOfInterest roi, String filepath, int frameNr) {

        return new Mask();
	}

    /**
     * Sets positive (foreground) and negative (background) feedback from user
     * so that it will be used by the  Grab-Cut algorithm
     * @param posFbPoints Point[] positive feedback points
     * @param negFbPoints Point[] negative feedback points
     */
    public void setFeedback(java.awt.Point[] posFbPoints, java.awt.Point[] negFbPoints){
        int[][] positiveMask = null;
        int[][] negativeMask = null;

        if(posFbPoints != null && posFbPoints.length != 0){
            for(int i = 0; i < posFbPoints.length; i++){
                Point pxl = new Point(posFbPoints[i].getX() - outerRect.x, posFbPoints[i].getY() - outerRect.y);
                int radius = 2; //2
                int thickness = -1; //default

                Imgproc.circle(this.maskMat, pxl, radius, new Scalar(Imgproc.GC_FGD), thickness);
            }
            this.feedbackSet = true;

            positiveMask = new RegionOfInterest(posFbPoints).toMask(960, 540).getMask();
        }

        if(negFbPoints != null && negFbPoints.length != 0){
            for(int i = 0; i < negFbPoints.length; i++){
                Point pxl = new Point(negFbPoints[i].getX() - outerRect.x, negFbPoints[i].getY() - outerRect.y);
                int radius = 2; //2
                int thickness = -1; //default
                Imgproc.circle(this.maskMat, pxl, radius, new Scalar(Imgproc.GC_BGD), thickness);
            }
            this.feedbackSet = true;

            negativeMask = new RegionOfInterest(negFbPoints).toMask(960, 540).getMask();
        }

        for(int i = 0; i < this.maskMat.height(); i++) {
            for(int j = 0; j < this.maskMat.width(); j++) {
                if(positiveMask != null && positiveMask[i][j] == 1) {
                    Imgproc.circle(this.maskMat, new Point(i - outerRect.x, j - outerRect.y), 1, new Scalar(Imgproc.GC_FGD), -1);
                } else if(negativeMask != null && negativeMask[i][j] == 1) {
                    Imgproc.circle(this.maskMat, new Point(i - outerRect.x, j - outerRect.y), 1, new Scalar(Imgproc.GC_BGD), -1);
                }
            }
        }
    }

    /**
     * Crops the image and returns a matrix containing the given region of interest increased by 100 pixels
     * in all directions.
     *
     * @param roiRect the region of interest
     * @return the cropped images as a mat
     */
    private Mat cropImage(Rect roiRect){
        this.outerRect = new Rect(); //outer rect with innerRect+100 in all directions
        this.outerRect.x = (roiRect.x - 100);
        this.outerRect.y = (roiRect.y - 100);
        this.outerRect.width = roiRect.width + 200;
        this.outerRect.height = roiRect.height + 200;
        Rect innerRect = new Rect(100, 100, roiRect.width, roiRect.height);//inner rect selected by user
        if(this.outerRect.x < 0){
            this.outerRect.x = 0;
            innerRect.x = roiRect.x; //<100
        }
        if(this.outerRect.y < 0){
            this.outerRect.y = 0;
            innerRect.y = roiRect.y; //<100
        }
        if((this.outerRect.x + this.outerRect.width) > this.currentFrame.cols())
            this.outerRect.width = this.currentFrame.cols() - this.outerRect.x;
        if((outerRect.y + outerRect.height) > this.currentFrame.rows())
            this.outerRect.height = this.currentFrame.rows() - this.outerRect.y;

        this.roiRect = innerRect;//modify roi (inner) rectangle
        return this.currentFrame.submat(this.outerRect);//returns a croppedImage
    }

    /**
     * Modifies the mask.
     *
     * @return the mat
     */
    private Mat modifyMask() {
        Mat modifiedMask = this.maskMat.setTo(new Scalar(Imgproc.GC_BGD));//set all mask pixels to GC_BGD
        this.roiRect.x = Math.max(0, this.roiRect.x);
        this.roiRect.y = Math.max(0, this.roiRect.y);

        this.roiRect.width = Math.min(this.roiRect.width, this.croppedMat.cols() - this.roiRect.x);
        this.roiRect.height = Math.min(this.roiRect.height, this.croppedMat.rows() - this.roiRect.y);

        Mat roiMat = new Mat(roiRect.size(), CvType.CV_8UC1);
        roiMat.setTo(new Scalar(Imgproc.GC_PR_FGD));//set mask pixels in ROI to GC_PR_FGD
        roiMat.copyTo(modifiedMask.submat(roiRect));

        return modifiedMask;
    }
}