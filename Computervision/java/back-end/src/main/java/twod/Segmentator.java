package twod;

import twod.Frame;
import twod.Mask;
import twod.RegionOfInterest;

/**
 * Segmentator.java
 * 
 * An interface that specifies methods that all segmenting techniques should implement.
 * 
 * @author codegrain
 * @since 24-04-2015
 *
 */
public interface Segmentator{

	/**
	 * Segments out a ROI from the current frame
	 * @param roi region of interest in the current frame
	 * @param filepath path to the video file
	 * @param frameNr the frame number
	 * @return a mask for the ROI
	 */
	Mask segment(RegionOfInterest roi, String filepath, int frameNr);
}