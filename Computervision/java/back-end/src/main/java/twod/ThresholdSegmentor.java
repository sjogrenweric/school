package twod;

import org.bytedeco.javacpp.opencv_core;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;
import twod.Mask;
import twod.RegionOfInterest;
import twod.Segmentator;
import utils.TwodUtilities;

import java.util.ArrayList;
import java.util.List;

/**
 * Segmentator that uses thresholding to perform the segmentation.
 */
public class ThresholdSegmentor implements Segmentator {

    /**
     * Segments out a ROI from the current frame.
     *
     * @param roi region of interest in the current frame
     * @param filepath path to the video file
     * @param frameNr the frame number
     * @return a mask for the ROI
     */
    public Mask segment(RegionOfInterest roi, String filepath, int frameNr) {
        VideoCapture capture = new VideoCapture(filepath);

        if(!capture.isOpened()){
            System.out.println("Error");
        }
        else {
            Mat frame = new Mat();

            while(true){
                if (capture.read(frame)){
                    break;
                }
            }
            for(int i = 0; i < frameNr; i++) {
                while(true){
                    if (capture.grab()){
                        break;
                    }
                }
            }
            while(true){
                if (capture.read(frame)){

                    Mat mask = TwodUtilities.maskToMat(roi.toMask(frame.width(), frame.height()));

                    Mat processed = new Mat(frame.rows(), frame.cols(), frame.type());
                    Imgproc.cvtColor(frame, processed, Imgproc.COLOR_RGB2GRAY);
                    Imgproc.threshold(processed, processed, 100, 1, Imgproc.THRESH_BINARY_INV+Imgproc.THRESH_OTSU);
                    Mat kernel = Mat.ones((int)(frame.rows() / 200), (int)(frame.cols() / 200), CvType.CV_8U);
                    Imgproc.morphologyEx(processed, processed, Imgproc.MORPH_OPEN, kernel);
                    Imgproc.dilate(processed, processed, kernel);
                    Imgproc.dilate(processed, processed, kernel);
                    Imgproc.dilate(processed, processed, kernel);

                    Core.MinMaxLocResult minmax = Core.minMaxLoc(processed);

                    Core.multiply(processed, mask, processed);

                    capture.release();

                    List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
                    Mat maskCopy = processed.clone();
                    Imgproc.findContours(maskCopy, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
                    double maxSize = 0;
                    int maxIndex = -1;
                    for(int i = 0; i < contours.size(); i++){
                        double tempArea = Imgproc.contourArea(contours.get(i));
                        if(tempArea > maxSize){
                            maxSize = tempArea;
                            maxIndex = i;
                        }
                    }

                    for(int i = 0; i < contours.size(); i++){
                        if(i == maxIndex) {
                            Imgproc.drawContours(processed, contours, i, new Scalar(255), -1);
                        } else {
                            Imgproc.drawContours(processed, contours, i, new Scalar(0), -1);
                        }
                    }

                    return TwodUtilities.matToMask(processed);
                }
            }
        }

        return null;
    }
}
