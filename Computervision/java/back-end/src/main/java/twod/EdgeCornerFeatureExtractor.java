package twod;

import org.opencv.core.*;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgproc.Imgproc;
	
import twod.Frame;
import twod.Mask;
import twod.Feature;
import utils.TwodUtilities;

/**
 * The EdgeCornerFeatureExtractor extracts features from a given frame (provided a mask). It supports retrieval
 * of ORB, FAST and SURF features.
 */
public class EdgeCornerFeatureExtractor implements FeatureExtractor{

	/**
	 * Extract features for the ROI in the current frame.
	 * It specifically extracts Shi-Tomasi corner features
	 * which are good for tracking.
	 * @param mask for the desired ROI
	 * @param frame the current frame
	 * @return a set of features for the ROI
	 */
	public Feature[] extractFeatures(Mask mask, Frame frame){
		Mat roi = TwodUtilities.roiInMat(mask, frame);
		Imgproc.cvtColor(roi, roi, Imgproc.COLOR_BGR2GRAY); //Convert to gray-scale
		MatOfPoint features = new MatOfPoint();
		int maxCorners = 50;//23, 25
		double qualityLevel = 0.01;
		double minDistance = 10;
		//Mat mask = new Mat();
		int blockSize = 3;
		boolean useHarris = false;//
		double k = 0.04;
		Imgproc.goodFeaturesToTrack(roi, features, maxCorners, qualityLevel, minDistance, new Mat(), blockSize, useHarris, k);
		return toPointArray(features);
	}

	/**
	 * Extract FAST features
	 * @param mask for the desired ROI
	 * @param frame the current frame
	 * @return a set of features for the ROI
	 */
	public Feature[] fastFeatures(Mask mask, Frame frame){
		Mat roi = TwodUtilities.roiInMat(mask, frame);
		MatOfKeyPoint features = new MatOfKeyPoint();
		FeatureDetector siftDetector = FeatureDetector.create(FeatureDetector.FAST);
		siftDetector.detect(roi, features);

		return toPointArray(features);
	}

	/**
	 * Extract ORB features
	 * @param mask for the desired ROI
	 * @param frame the current frame
	 * @return a set of features for the ROI
	 */
	public Feature[] orbFeatures(Mask mask, Frame frame){
		Mat roi = TwodUtilities.roiInMat(mask, frame);
		MatOfKeyPoint features = new MatOfKeyPoint();
		FeatureDetector siftDetector = FeatureDetector.create(FeatureDetector.ORB);
		siftDetector.detect(roi, features);
		return toPointArray(features);
	}

	/**
	 * Extract SURF features
	 * @param mask for the desired ROI
	 * @param frame the current frame
	 * @return a set of features for the ROI
	 */
	public Feature[] surfFeatures(Mask mask, Frame frame){
		Mat roi = TwodUtilities.roiInMat(mask, frame);
		MatOfKeyPoint features = new MatOfKeyPoint();
		FeatureDetector siftDetector = FeatureDetector.create(FeatureDetector.SURF);
		siftDetector.detect(roi, features);
		return toPointArray(features);
	}


	/**
	 * Converts the given points to features.
	 * @param points points to convert
	 * @return the resulting features
	 */
	private Feature[] toPointArray(MatOfPoint points){
		Point[] pArray = points.toArray();
		Feature[] features = new Feature[pArray.length];
		for(int i = 0; i < features.length; i++){
			int x = (int) pArray[i].x;
			int y = (int) pArray[i].y;
			features[i] = new Feature(new java.awt.Point(x, y));
		}
		return features;
	}

	/**
	 * Converts the given points to features.
	 * @param keyPoints points to convert
	 * @return the resulting features
	 */
	private Feature[] toPointArray(MatOfKeyPoint keyPoints){
		KeyPoint[] kpArray = keyPoints.toArray();
		//java.awt.Point[] features = new java.awt.Point[kpArray.length];
		Feature[] features = new Feature[kpArray.length];
		for(int i = 0; i < features.length; i++){
			int x = (int) kpArray[i].pt.x;
			int y = (int) kpArray[i].pt.y;
			features[i] = new Feature(new java.awt.Point(x, y), kpArray[i].angle, kpArray[i].octave, kpArray[i].size);
		}
		return features;
	}

	/**
	 * Finds good features in the provided Mat.
	 * @param roi the mat to look for good features in
	 * @return
	 */
	public static MatOfPoint goodFeatures(Mat roi){
		Imgproc.cvtColor(roi, roi, Imgproc.COLOR_BGR2GRAY);
		MatOfPoint features = new MatOfPoint();
		int maxCorners = 150;//23, 25, 50
		double qualityLevel = 0.01;
		double minDistance = 10;
		//Mat featMask = new Mat();
		int blockSize = 3;
		boolean useHarris = false;//
		double k = 0.04;
		Imgproc.goodFeaturesToTrack(roi, features, maxCorners, qualityLevel, minDistance, new Mat(), blockSize, useHarris, k);
		return features;
	}

	/**
	 * Finds the specified type of features in the provide mat.
	 * @param roi the mat to look for features in
	 * @param featureDetectorType the type of feature to detect
	 * @return
	 */
	public static MatOfPoint getFeatures(Mat roi, int featureDetectorType){
		Imgproc.cvtColor(roi, roi, Imgproc.COLOR_BGR2GRAY);
		MatOfKeyPoint features = new MatOfKeyPoint();
		FeatureDetector siftDetector = FeatureDetector.create(featureDetectorType);
		siftDetector.detect(roi, features);
		KeyPoint[] kps = features.toArray();
		Point[] ps = new Point[kps.length];
		for(int i = 0; i < kps.length; i++){
			ps[i] = kps[i].pt;
		}
		return new MatOfPoint(ps);
	}


}
