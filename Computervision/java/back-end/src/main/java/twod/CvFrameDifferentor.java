package twod;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.BackgroundSubtractor;
import org.opencv.video.BackgroundSubtractorMOG2;
import org.opencv.video.Video;
import org.opencv.videoio.VideoCapture;
import utils.TwodUtilities;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * The CvFrameDifferentor segments the given area by using frame differenting. The class uses exported MATLAB
 * functions.
 */
public class CvFrameDifferentor implements Segmentator {

    private BackgroundSubtractorMOG2 subtractor;

    public CvFrameDifferentor() {
        this.subtractor = Video.createBackgroundSubtractorMOG2();;
        subtractor.setDetectShadows(false);
        subtractor.setNMixtures(3);
    }

    /**
     * Segment using OpenCV frame differencing.
     * @param roi region of interest in the current frame
     * @param filepath path to the video file
     * @param frameNr the frame number
     * @return
     */
    @Override
    public Mask segment(RegionOfInterest roi, String filepath, int frameNr) {

        /* Read frames */
        VideoCapture capture = new VideoCapture(filepath);
        Mat prevFrame = null;
        Mat currentFrame = null;
        Mat fgMask = new Mat();
        if(!capture.isOpened())
            capture.open(0);
        if(!capture.isOpened()){
            System.out.println("Can't open file.");
            return null;
        }
        try{
            Mat frame = new Mat();
            int currentFrameNum = 0;
            while(capture.read(frame)) {
                if(currentFrameNum == frameNr) {
                    currentFrame = frame;
                    break;
                } else if(currentFrameNum == frameNr -1) {
                    prevFrame = frame;
                }
                currentFrameNum ++;
                frame = new Mat();
            }
            capture.release();
        }catch(Exception e){
            capture.release();
            e.printStackTrace();
            System.out.println("Can't read file.");
            return null;
        }

        Mat roiMask = TwodUtilities.maskToMat(roi.toMask(960, 540));

        /* Perform frame differenting. */
        subtractor.apply(prevFrame, fgMask);
        subtractor.apply(currentFrame, fgMask);

        /* Remove areas outside region of interest. */
        fgMask = fgMask.mul(roiMask);

        /* Find largest countor. */
        Imgproc.dilate(fgMask, fgMask, new Mat());
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(fgMask, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
        if (contours.size() == 0) {
            return null;
        }
        MatOfPoint largestContour = TwodUtilities.findLargestContour(contours);

        /* Return mask of largest contour. */
        Mat result = Mat.zeros(roiMask.rows(), roiMask.cols(), CvType.CV_8UC1);
        Imgproc.drawContours(result, Arrays.asList(new MatOfPoint[]{largestContour}), -1, new Scalar(1), -1);
        return TwodUtilities.matToMask(result);

    }
}
