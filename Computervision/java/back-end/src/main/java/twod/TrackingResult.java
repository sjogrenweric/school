package twod;

import java.util.LinkedList;
import java.util.List;

/**
 * Container for storing the result from a tracking.
 */
public class TrackingResult {

    List<TrackingPoint> trackingPoints;

    /**
     * Creates a new empty TrackingResult.
     */
    public TrackingResult() {
        this(new LinkedList<TrackingPoint>());
    }

    /**
     * Creates a new TrackingResult which contains the given list of tracking points.
     * @param trackingPoints
     */
    public TrackingResult(List<TrackingPoint> trackingPoints) {
        this.trackingPoints = trackingPoints;
    }

    /**
     * Adds the specified time and regions of interest as a tracking point.
     *
     * @param timeMs the in ms into the tracking where the region of interest is used
     * @param roi the region of interest
     */
    public void addTrackingPoint(long timeMs, RegionOfInterest roi) {
        trackingPoints.add(new TrackingPoint(timeMs, roi));
    }

    /**
     * Returns the tracking points.
     *
     * @return the tracking points.
     */
    public List<TrackingPoint> getTrackingPoints() {
        return trackingPoints;
    }

    /**
     * Class for representing a tracking point.
     */
    public class TrackingPoint {
        public final long timeMs;
        public final RegionOfInterest roi;

        /**
         * Creates a new TrackingPoint with the specified time and region od interest.
         * @param timeMs
         * @param roi
         */
        public TrackingPoint(long timeMs, RegionOfInterest roi) {
            this.timeMs = timeMs;
            this.roi = roi;
        }

        @Override
        public String toString() {
            return "TrackingPoint{" +
                    "timeMs=" + timeMs +
                    ", roi=" + roi +
                    '}';
        }
    }
}
