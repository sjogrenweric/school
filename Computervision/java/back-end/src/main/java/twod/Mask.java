package twod;

import org.apache.commons.lang.ArrayUtils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.imgproc.Imgproc;
import utils.TwodUtilities;

import javax.swing.plaf.synth.Region;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Mask.java
 *
 * @author codegrain
 * @since 29-04-2015
 *
 */
public class Mask {

    private int[][] mask;

    public Mask() {}

    public Mask(int[][] mask) {
        this.mask = mask;
    }

    public int[][] getMask() {
        return mask;
    }

    public void setMask(int[][] mask) {
        this.mask = mask;
    }

    public int getHeight() {
        return mask.length;
    }

    public int getWidth() {
        return mask[0].length;
    }
    /*
        public int[][] getBorders() {

        }
    */
    public RegionOfInterest getBorders() {
        Frame frame = new Frame();
        frame.setMask(mask);

        Mat mat = TwodUtilities.frameToMat(frame);
        Mat mat2 = new Mat();
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.cvtColor(mat, mat2, Imgproc.COLOR_BGR2GRAY);
        Imgproc.findContours(mat2, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
        System.err.println("Contour size: " + contours.size());

        /*Object[] merged = new Object[0];
        for (int i = 0; i < contours.size(); i++) {
            merged = ArrayUtils.addAll(merged, TwodUtilities.toPointArray(contours.get(i)));
        }

        Point[] points = new Point[merged.length];
        for (int i = 0; i < merged.length; i++) {
            points[i] = (Point)merged[i];
        }*/

        return new RegionOfInterest(TwodUtilities.toPointArray(contours.get(0)));
    }

    public static void main(String[] argv) {
        System.out.println("Hello world!");
    }
}
