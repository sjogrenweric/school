#! /bin/bash

avconv -i $1.mp4 -c:a copy -c:v mjpeg -qscale 2 -s 960x540 -q:v 1 -b 65536k -r 30 $1.avi

