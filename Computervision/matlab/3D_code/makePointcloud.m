%function [ disparityMap, point3D ] = makePointcloud( scene_name , stereoParams, image_no)
function [ point3D ] = makePointcloud( disparityMap , stereoParams, image)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
%     
% scene_name = 'erik';
% image_no = 10;
% 
% folder_left = fullfile(pwd(),scene_name,'left','scene', 'img');
% folder_right = fullfile(pwd(),scene_name,'right', 'scene', 'img');
% 
% images_left = dir(strcat(folder_left, '/*.jpg'));
% images_right = dir(strcat(folder_right,'/*.jpg'));
% 
% n_images = min(size(images_left,1),size(images_right,1))
% 
% images1 = cell(n_images,1);
% images2 = cell(n_images,1);
% for i=1: n_images
%     images1(i) = cellstr(fullfile(folder_left,images_left(i).name));
%     images2(i) = cellstr(fullfile(folder_right,images_right(i).name));
% end
% 
% I1 = imread(char(images1(image_no)));
% I2 = imread(char(images2(image_no)));
% 
% [J1, J2] = rectifyStereoImages(I1, I2, stereoParams);
%     %dispImg1 = wiener2(rgb2gray(J1), [5 5]);
%     %dispImg2 = wiener2(rgb2gray(J2), [5 5]);
% dispImg1 = rgb2gray(J1);
% dispImg2 = rgb2gray(J2);
% 
%     %disparityMap = disparity(dispImg1, dispImg2, 'ContrastThreshold', 0.99, 'DistanceThreshold', 50);
% disparityMap = disparity(dispImg1, dispImg2);
%     %disparityMap = disparity(dispImg1, dispImg2, 'Method', 'BlockMatching', 'BlockSize', 31, 'UniquenessThreshold', 1);
%     %disparityMap = disparity(rgb2gray(J1), rgb2gray(J2), 'UniquenessThreshold', 0);
%     %imshow(imclose(imerode(disparityMap, ones(17)),ones(5)))
% 
% figure;
% imshow(J1);
% figure;
% imshow(disparityMap, [0, 64], 'InitialMagnification', 50);
% colormap('jet');
% colorbar;
% title('Disparity Map');

%% Reconstruct the 3-D Scene
% Reconstruct the 3-D world coordinates of points corresponding to each
% pixel from the disparity map.

point3D = reconstructScene(disparityMap, stereoParams);


% Convert from millimeters to meters.
point3D = point3D / 1000;
%point3Dfiltered = pcdenoise(pointCloud(point3D, 'Color', J1));
% showPointCloud(point3Dfiltered,'VerticalAxis', 'X');
%% Visualize the 3-D Scene

% Plot points between 3 and 7 meters away from the camera.
z = point3D(:, :, 3);
maxZ = 10;
minZ = 0;
zdisp = z;
zdisp(z < minZ | z > maxZ) = NaN;
point3Ddisp = point3D;
point3Ddisp(:,:,3) = zdisp;
figure;
showPointCloud(point3Ddisp, image, 'VerticalAxis', 'Y',...
    'VerticalAxisDir', 'Down' );

xlabel('X'); 
ylabel('Y');
zlabel('Z');

%get depth for each pixel
%depthImg = point3d(:,:,3);

%scale image to the range (0,10)
%scaledDepth = uint8((double(depthImg) ./ 10) .* 255);

%sets zero values to max (255)
%filteredImg = (uint8(scaledImg == 0).*255) + scaledImg;

end

