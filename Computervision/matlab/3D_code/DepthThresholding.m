
% % Load the stereoParameters object.
% load('handshakeStereoParams.mat');

%% Create Video File Readers and the Video Player
% Create System Objects for reading and displaying the video
%videoFileLeft = 'handshake_left.avi';
%videoFileRight = 'handshake_right.avi';

videoFileLeft = 'erik/left/scene/vid/scene.mp4';
videoFileRight = 'erik/right/scene/vid/scene.mp4';

readerLeft = vision.VideoFileReader(videoFileLeft, 'VideoOutputDataType', 'uint8');
readerRight = vision.VideoFileReader(videoFileRight, 'VideoOutputDataType', 'uint8');
player = vision.VideoPlayer('Position', [20, 400, 850, 650]);

%% Read and Rectify Video Frames
% frameLeft = readerLeft.step();
% frameRight = readerRight.step();
% 
% [frameLeftRect, frameRightRect] = ...
%     rectifyStereoImages(frameLeft, frameRight, stereoParams);
% 
% % figure;
% % imshow(stereoAnaglyph(frameLeftRect, frameRightRect));
% % title('Rectified Video Frames');
% 
% %% Compute Disparity
% frameLeftGray  = rgb2gray(frameLeftRect);
% frameRightGray = rgb2gray(frameRightRect);
%     
% disparityMap = disparity(frameLeftGray, frameRightGray);
% % figure;
% % imshow(disparityMap, [0, 64]);
% % title('Disparity Map');
% % colormap jet
% % colorbar
% 
% %% Detect People in the Left Image
% 
% % Create the people detector object. Limit the minimum object size for
% % speed.
% peopleDetector = vision.PeopleDetector('MinSize', [166 83]);
% 
% % Detect people.
% bboxes = peopleDetector.step(frameLeftGray);

% figure;
% imshow(insertShape(frameLeftRect, 'rectangle', bboxes));
% title('Detected People');

% %% Create a Mask for the detected person
% [M N] = size(frameLeftGray);
% Mask = zeros(M,N);
% for i=1:M 
%     for j=1:N
%         if ( j>=bboxes(1) && j<=bboxes(1)+bboxes(3)) && ( i>=bboxes(2)...
%                 && i<=bboxes(2)+bboxes(4))
%             Mask(i,j) = 1;
%         end
%     end
% end
% 
% disparityMap = im2double(uint8(disparityMap));
% MaskedIm = disparityMap.*Mask;
% MaskedIm = histeq(MaskedIm);
% FinalIm = im2bw(MaskedIm,graythresh(MaskedIm));
% figure;
% imshow(FinalIm);

%% Process the Rest of the Video
% Apply the steps described above to detect people and measure their
% distances to the camera in every frame of the video.

offsetFrames = round(50*relOffset);

for i=1:offsetFrames
    readerLeft.step();
end

while ~isDone(readerLeft) && ~isDone(readerRight)
    % Read the frames.
    frameLeft = readerLeft.step();
    frameRight = readerRight.step();
    
    % Rectify the frames.
    [frameLeftRect, frameRightRect] = ...
        rectifyStereoImages(frameLeft, frameRight, stereoParams);
    
    % Convert to grayscale.
    frameLeftGray  = rgb2gray(frameLeftRect);
    frameRightGray = rgb2gray(frameRightRect);
    
    % Compute disparity. 
    disparityMap = disparity(frameLeftGray, frameRightGray);
    
    % Detect people.
    bboxes = peopleDetector.step(frameLeftGray);
    
    if ~isempty(bboxes)
        % Create a Mask for the detected person
        [M N] = size(frameLeftGray);
        Mask = zeros(M,N);
        if isempty(Mask)
            Mask = PrevMask;
        else
            for i=1:M 
                for j=1:N
                    if ( j>=bboxes(1) && j<=bboxes(1)+bboxes(3)) && ( i>=bboxes(2)...
                            && i<=bboxes(2)+bboxes(4))
                        Mask(i,j) = 1;
                    end
                end
            end
        end
        PrevMask = Mask;
        disparityMap = im2double(uint8(disparityMap));
        MaskedIm = disparityMap.*Mask;
        MaskedIm = histeq(MaskedIm);
        FinalIm = im2bw(MaskedIm,graythresh(MaskedIm));
        %figure;
        imshow(FinalIm);           
    end
    dispFrame = frameLeftRect;
    % Display the frame.
    step(player, dispFrame);
end

% Clean up.
reset(readerLeft);
reset(readerRight);
release(player);
