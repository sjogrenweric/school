function PlotVidTresh(Video)

%%This function gives the number of the frame when a peek of illumination
%%on the image is detected. Used for synchronizing 

videosync = VideoReader(Video); 

% Read one frame at a time until the end of the video is reached or a flash
% is detected.
 k = 1;
 while hasFrame(videosync)
    F = readFrame(videosync);
    frame = rgb2gray(F);
    frame = im2double(frame);
    T = graythresh(frame);
    Ts(k) = T;
    k = k+1; 
 end
 
figure;
plot(1:k-1,Ts);

end