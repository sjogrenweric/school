function frames = writeFrame(metaFrames)

for i = 1:numel(metaFrames)
    metaFrame = metaFrames(i);
    
    if ~isempty(metaFrame.method)         
        for j = 1:numel(metaFrame.frame)
            command = sprintf('%s%s%f%s%s%s%s%s', metaFrame.method, ' -y -ss ', metaFrame.frame(j).offsetTime, ' -i ', metaFrame.videoPath, ' -frames:v 1 ', metaFrame.frame(j).targetName, ' < /dev/null');
            [status, cmdout] = system(command);

            if status ~= 0
                fprintf('Received error status: %d\n\n%s\n', status, command);
                error(cmdout)
            end
        end
        
    else
        error('i don wanna run in slowmo');
    end
end
         
end