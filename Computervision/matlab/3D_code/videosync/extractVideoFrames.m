 function extractVideoFrames(camData, extractionDir, extractionPoints)
 %input: 
 % path to json (that you generate with findVideoOffsets)
 % path to where extracted images are  to be saved
 % set of time-points where frame will be extracted, ex: 
 % extractionPoints = 1:1/50:10 will extract 50 frames/sec from second 1.0 to 10.0
 
 
opt.SimplifyCell=1; %Converts 'cell'-type to 'struct'-type
%camData = loadjson(jsonFile,opt);
%camData = ofs;

%Check whether save location ends with '/'
[path, filename, ~] = fileparts(extractionDir);    
if ~isempty(filename)
    extractionDir = sprintf('%s/%s', path, filename)
end

extractionMethod = frameExtractionMethod();
if ~isempty(extractionMethod)
    fprintf('Using "%s" to extract frames\n', extractionMethod);
end

for camIndex = 1:numel(camData)
    
    currCam = camData(camIndex).camera;    
    sampleIndex = currCam.sampleOffset;
    
    fprintf('Using camera(id): %d, video-file: %s\n', currCam.id, currCam.audioInfo.Filename);
           
    time = linspace(0, currCam.audioInfo.Duration, currCam.audioInfo.TotalSamples);
    metaFrames(camIndex).videoPath = currCam.audioInfo.Filename;
    metaFrames(camIndex).method = extractionMethod;
    
    for extractionPointIndex = 1:numel(extractionPoints)
            
        %timeAbsDiff = abs(time - extractionPoints(extractionPointIndex));
        %[idx, idx] = min(timeAbsDiff); %index of closest time-value in interval
        %sampleIndex = currCam.sampleOffset + idx;   
        if camIndex == 1
            extractedFramePath = sprintf('%s/left/scene/frames/left%02d%s', extractionDir, extractionPointIndex, '.jpg');
        else
            extractedFramePath = sprintf('%s/right/scene/frames/right%02d%s', extractionDir, extractionPointIndex, '.jpg');
        end
        

        metaFrames(camIndex).frame(extractionPointIndex).targetName = extractedFramePath;
        metaFrames(camIndex).frame(extractionPointIndex).offsetTime = time(sampleIndex) + extractionPoints(extractionPointIndex);
        %metaFrames(camIndex).frame(extractionPointIndex).offsetTime = time(sampleIndex);
        metaFrames(camIndex).frame(extractionPointIndex).offsetFrame = round(time(sampleIndex) * currCam.videoInfo.FrameRate);
                     
    end
    
    writeFrame(metaFrames);

end

            