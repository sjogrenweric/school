function  exportData = findVideoOffsets(videos, storagePath, extraFrames)
% input: 
% paths to video for each camera ex. [vid-cam1, vid-cam2, vid-cam3]
% path to where images will be stored
% num frames before and after estimated point of hand-clap, used for
% user selection

% Left-click shows a larger view of the image that was clickedo n
% left-clicking the larger view closes the view.
% Right-click selects the image (select the first frame where the hand-clap occurs)

% Close the main figure window to exit the selection process and trigger
% json export + frame extraction.



scrsz = get(0,'ScreenSize');
rootFig = figure('Position', [0,scrsz(4)/2,scrsz(3)/2,scrsz(4)/2]);%,'KeyPressFcn',@(obj,evt) 0);

%Check whether save location ends with '/'
[path, locname, ~] = fileparts(storagePath);
if ~isempty(locname)
    path = sprintf('%s/%s', path, locname);
end

extractionMethod = frameExtractionMethod();
if ~isempty(extractionMethod)
    fprintf('Using "%s" to extract frames\n', extractionMethod);
end


numImgs =extraFrames * 2 + 1;
generatedFrames = cell(numImgs * numel(videos),1);
storedFilenames = cell(numImgs * numel(videos),1);
midIdx = floor((numImgs + 1)/2);
finalFrame(1:numel(videos)) = cell(numel(videos),1);
clapIdy(1:numel(videos)) = midIdx;

for videoIndex = 1:numel(videos)
    
    currentVideo = char(videos(videoIndex));
    fprintf('Using video-file: %s\n', currentVideo);
    
    audioInfo = audioinfo(currentVideo);
    videoObj = VideoReader(currentVideo);
    mediaInfo = mmfileinfo(currentVideo);
    [audioSamples, ~] = audioread(currentVideo);
    
    x = linspace(0, audioInfo.Duration, audioInfo.TotalSamples);
    
    %Calculate audio-sample average if multi-channel audio
    if audioInfo.NumChannels > 1
        samplesSum = audioSamples(:,1);
        for channel = 2:audioInfo.NumChannels
            samplesSum = samplesSum + audioSamples(:,channel);
        end
        audioSamples = (samplesSum ./ audioInfo.NumChannels);
    end
    
    %subplot(3,1,videoIndex);
    %plot(x, audioSamples);
    
    % Find index of highest peak-value(y) and so that y = audioSamples(index) in the set of abs(audio samples)
    [y, index] = max(abs(audioSamples));
    
    %When using dB as measurement
    %db = mag2db(audioSamples);
    %[y index] = max(db);
    %min(db(~isinf(db) | db>0))
    
    
    outputName = sprintf('vid-cam%d', videoIndex);   
    imageName = sprintf('%s/%s.jpg', path, outputName);
    
    % Data to export to json
    cameraData.id = videoIndex;
    cameraData.sampleOffset = index;
    cameraData.timeOffset = x(index);                               % perhaps exclude this
    cameraData.frameOffset = round(x(index) * videoObj.FrameRate);  % perhaps exclude this
    cameraData.videoInfo = get(videoObj);
    cameraData.videoInfo.Format = mediaInfo.Video.Format;
    cameraData.audioInfo = audioInfo;
    cameraData.imagePath = imageName;
    exportData(videoIndex).camera = cameraData;
    
    fprintf('Found peak audio at sample-index %d, at time: %fs (frame ~%d), valued at %f\n', index, x(index), cameraData.frameOffset, y);       
    
    if ~isempty(extractionMethod)
        for i = 1:numImgs
            
            extractedFramePath = sprintf('%s/%s_%d.jpg', path, outputName, i);
            command = sprintf('%s%s%f%s%s%s%s%s', extractionMethod, ' -y -ss ', x(index) + magicFunc(i), ' -i ', cameraData.audioInfo.Filename, ' -frames:v 1 ', extractedFramePath, ' < /dev/null');
            
            [status, cmdout] = system(command);
            
            if status ~= 0
                fprintf('Received error status: %d, using command:\n%s\n', status, command);
                error(cmdout)
            else
                generatedFrames{((videoIndex - 1) * numImgs) +i} = imread(extractedFramePath);
                storedFilenames{((videoIndex - 1) * numImgs) +i} = extractedFramePath;     
                if i == midIdx
                    finalFrame(videoIndex) = {extractedFramePath};
                end
            end
        end
        
    else
        error('h?rru nej');
    end
    
end

% Create view for all images
for i=1:numel(videos)
    for j = 1:numImgs
        subaxis(3,numImgs,(i-1)*numImgs + j, 'P',0, 'S',0, 'M',0);
        h = imshow(generatedFrames{(i-1)*numImgs + j}, 'InitialMag',100, 'Border','tight');
        title(num2str((i-1)*numImgs + j))
        set(h, 'ButtonDownFcn',{@mainFigCallback,i,j})
    end
end

% Wait for user to close main figure window
waitfor(rootFig);


% Remove stored images that won't be used
% Recalculate timeoffset, frameoffset and sampleoffset -> update exported
% data
% Rename the stored image
for k = 1:numel(videos)
    storedFilenames = storedFilenames(~strcmp(finalFrame(k),storedFilenames));
        
    exportData(k).camera.timeOffset = exportData(k).camera.timeOffset + magicFunc(clapIdy(k));
    exportData(k).camera.frameOffset = round(exportData(k).camera.timeOffset * videoObj.FrameRate);
    timeAbsDiff = abs(x - exportData(k).camera.timeOffset);
    [tidx, tidx] = min(timeAbsDiff);
    exportData(k).camera.sampleOffset = tidx;
        
    [SUCCESS,MESSAGE,MESSAGEID] = movefile(finalFrame{k}, exportData(k).camera.imagePath, 'f');
    if SUCCESS == 0
        error(MESSAGE);
    end
end

for f = 1:numel(storedFilenames)
delete(storedFilenames{f});
end


%Store json file
opt.FileName = sprintf('%s/offsets.json', path);
%savejson('', exportData, opt);


    function ret = magicFunc(value)
        if value < midIdx
            ret = value - 1 * midIdx;
        elseif value == midIdx
            ret = 0;
        else
            ret = value - midIdx;
        end
        
        ret = ret * 1/50;
    end

    %callback functions
    function mainFigCallback(o,e,idx, idy)
        % show selected image in a new figure
        if e.Button == 1
            figure(2),hh = imshow(generatedFrames{((idx-1)*numImgs + idy)});
            title(num2str(idy))
            set(hh, 'ButtonDownFcn',{@subFigCallback,idx, idy})
        elseif e.Button == 3
            finalFrame(idx) = storedFilenames(((idx-1)*numImgs + idy));
            clapIdy(idx) = idy;
            fprintf('Selected %d,%d\n', idx, idy);
            fig = get(gcf);
            if fig.Number ~= 1
                close(gcf);
            end
        end
    end

    function subFigCallback(o,e,idx,idy)
        if e.Button == 3
            finalFrame(idx) = storedFilenames(((idx-1)*numImgs + idy));
            clapIdy(idx) = idy;
            fprintf('Selected %d,%d\n', idx,idy);
        end
        close(gcf);        
    end
end


