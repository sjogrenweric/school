function method = frameExtractionMethod()

if isunix    
    [ffmpegStatus, ~] = system('which ffmpeg');
    [avconvStatus, ~] = system('which avconv');
    
    if ffmpegStatus == 0
        method ='ffmpeg';
    elseif avconvStatus == 0
        method = 'avconv';    
    end
end
    

end