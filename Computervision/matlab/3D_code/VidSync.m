function [kinit,kfinal] = VidSync(Video)

%%This function gives the number of the frame when a peek of illumination
%%on the image is detected. Used for synchronizing 

videosync = VideoReader(Video); 

% vidWidth = videosync.Width;
% vidHeight = videosync.Height;
% %Create a movie structure array
%  mov = struct( 'cdata',zeros(vidHeight,vidWidth,3,'uint8'),...
%      'colormap',[]);

% Read one frame at a time until the end of the video is reached or a flash
% is detected.
 kinit = 0;
 kfinal = 0;
 k = 1;
 Tprev = 1;
 Offset = 0.01; % Selected offset for flash detection
 
while hasFrame(videosync)
    F = readFrame(videosync);
    frame = rgb2gray(F);
    frame = im2double(frame);
    T = graythresh(frame);
    if T > Tprev + Offset
        if kinit == 0
            kinit = k;
        elseif kfinal == 0
            kfinal = k;
        else
            break;
        end
    end
    k = k+1;
    Tprev = T;
end

diffFrames = kfinal - kinit;

if (kinit == 0) && (kfinal == 0)
    disp('No flash detected.');
elseif ((kinit == 0)&&(kfinal ~= 0))||((kinit ~= 0)&&(kfinal == 0))
    disp('One flash detected.');
else
    disp('Both flash detected');
    disp('Gap between flashes->');
    disp(diffFrames);
end

