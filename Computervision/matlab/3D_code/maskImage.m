function [ result ] = maskImage( img, box )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

[M N] = size(img);
Mask = zeros(M,N);
for i=1:M 
    for j=1:N
        if ( j>=box(1) && j<=box(1)+box(3)) && ( i>=box(2)...
                && i<=box(2)+box(4))
            Mask(i,j) = 1;
        end
    end
end

img = imcomplement(img);
img2 = im2double(uint8(img));
% MaskedIm = disparityMap.*Mask;
result = img2.*Mask;

end

