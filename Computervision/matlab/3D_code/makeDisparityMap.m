function [ disparityMap, J1 ] = makeDisparityMap( scene_name, stereoParams, image_no )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

folder_left = fullfile(pwd(),scene_name,'left','scene', 'frames');
folder_right = fullfile(pwd(),scene_name,'right', 'scene', 'frames');

images_left = dir(strcat(folder_left, '/*.jpg'));
images_right = dir(strcat(folder_right,'/*.jpg'));

n_images = min(size(images_left,1),size(images_right,1))

images1 = cell(n_images,1);
images2 = cell(n_images,1);
for i=1: n_images
    images1(i) = cellstr(fullfile(folder_left,images_left(i).name));
    images2(i) = cellstr(fullfile(folder_right,images_right(i).name));
end

I1 = imread(char(images1(image_no)));
I2 = imread(char(images2(image_no)));

[J1, J2] = rectifyStereoImages(I1, I2, stereoParams);
%dispImg1 = wiener2(rgb2gray(J1), [5 5]);
%dispImg2 = wiener2(rgb2gray(J2), [5 5]);
dispImg1 = rgb2gray(J1);
dispImg2 = rgb2gray(J2);

%disparityMap = disparity(dispImg1, dispImg2, 'ContrastThreshold', 0.99, 'DistanceThreshold', 50);
disparityMap = disparity(dispImg1, dispImg2);
%disparityMap = disparity(dispImg1, dispImg2, 'Method', 'BlockMatching', 'BlockSize', 31, 'UniquenessThreshold', 1);
%disparityMap = disparity(rgb2gray(J1), rgb2gray(J2), 'UniquenessThreshold', 0);
%imshow(imclose(imerode(disparityMap, ones(17)),ones(5)))

figure;
imshow(J1);
figure;
imshow(disparityMap, [0, 64], 'InitialMagnification', 50);
%DISPMAP=imread(disparityMap);%, [0, 64], 'InitialMagnification', 50);
%imfuse(J1, DISPMAP,'blend','Scaling','joint');
colormap('jet');
colorbar;
title('Disparity Map');


end

