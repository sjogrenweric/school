#!/usr/bin/python

import fileinput
import os
import sys

i = 1
#for line in fileinput.input():
prefix = sys.argv[1]
pairs = []
print "Renaming:"
for f in sys.argv[2:]:
  #line = line.rstrip()
  path = os.path.dirname(f)
  newname = "{0}/{1}{2:02d}.jpg".format(path, prefix,i)
  print f, "->", newname
  #os.rename(line,newname)
  pairs.append((f,newname))
  i+=1

sys.stdout.write("Are you sure? (y/n): ")
c = sys.stdin.read(1)
if c == 'y' or c == 'Y':
  for p in pairs:
    os.rename(p[0],p[1])
else:
  print "Aborting"


