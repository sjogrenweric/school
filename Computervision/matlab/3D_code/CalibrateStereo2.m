function [ stereoParams ] = CalibrateStereo2(scene_path, cal_folder, errorThreshold)
%CALIBRATESTEREO Summary of this function goes here
%   Detailed explanation goes here
%cal_folder = 'close';
% cal_folder = 'medium';
% cal_folder = 'far';

folder_left = fullfile(scene_path,'left', 'calibration',cal_folder);
folder_right = fullfile(scene_path,'right', 'calibration', cal_folder);


%strcat(folder_right, calSubFolderName, '/', cal_folder)
images_left = dir(folder_left);%dir(strcat(folder_left, calSubFolderName, '/', cal_folder));
images_right = dir(folder_right); %dir(strcat(folder_right, calSubFolderName, '/', cal_folder));
n_images = min(size(images_left,1),size(images_right,1))

images1 = cell((n_images - 2),1);  % -2 to remove '.' and '..' folders
images2 = cell((n_images - 2),1);  % -2 to remove '.' and '..' folders
offset = 0;
for i=1: n_images %% offset by 2 to exclude '.' and '..
    if strncmpi(images_left(i).name, '.',1) ~= 1 &&  strncmpi(images_right(i).name,'.',1) ~= 1
        images1(i - offset) = cellstr(fullfile(folder_left,images_left(i).name));
        images2(i - offset) = cellstr(fullfile(folder_right,images_right(i).name));
    else 
        offset = offset + 1;
    end
end

calibrationComplete = false;
while ~calibrationComplete
    [imagePoints, boardSize, pairsUsed] = detectCheckerboardPoints(images1, images2);

    % Generate world coordinates of the checkerboard points.
    squareSize = 41; % millimeters
    worldPoints = generateCheckerboardPoints(boardSize, squareSize);

    % Compute the stereo camera parameters.
    stereoParams = estimateCameraParameters(imagePoints, worldPoints);

    cam1Params = stereoParams.CameraParameters1;
    imagesUsed = size(cam1Params.ReprojectionErrors(:,:,:),3);
    errorCount = size(cam1Params.ReprojectionErrors(:,:,1),1);
    errors = zeros(imagesUsed,errorCount);
    meanErrors = zeros(imagesUsed,1);
    for i=1:imagesUsed
        for j=1:errorCount
            errors(i,j) = norm(cam1Params.ReprojectionErrors(j,:,i));

        end
        meanErrors(i) = mean(errors(i,:));
    end

    [maxError, errorIndex] = max(meanErrors);
    totalMean = stereoParams.MeanReprojectionError;
    %errorThreshold = 1.0;
    if maxError > errorThreshold
        fprintf('Removing image at index %d, error is %.2f, avg mean is %.2f, rel. error is %.2f\n', errorIndex, maxError, totalMean, totalMean/maxError);
        index = 1;
        for i=1:imagesUsed
            if pairsUsed(i) == 0
                index = index + 1;
            end

            if i == errorIndex
                images1(index) = [];
                images2(index) = [];
                break;
            end
            index = index + 1;
        end
    else
        fprintf('Calibration complete!\n');
        calibrationComplete = true;
    end
    %showReprojectionErrors(stereoParams);
end



end

