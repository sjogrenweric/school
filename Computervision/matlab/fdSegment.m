function [ mask ] = fdSegment( file, frameNum, smask )

trainingFrames = 50;

reader = VideoReader(file); %vision.VideoFileReader(file);
detector = vision.ForegroundDetector('NumGaussians', 5, ...
            'NumTrainingFrames', trainingFrames, 'MinimumBackgroundRatio', 0.1);
        
if frameNum <= trainingFrames + 1
    for N=frameNum + (trainingFrames + 1):-1:frameNum
        frame = read(reader, N); %reader.step();
        mask = detector.step(frame);
        if N == frameNum
            mask = detectObjects(mask, smask);
        end
    end
else
    for N=frameNum - (trainingFrames + 1):frameNum
        frame = read(reader, N); %reader.step();
        mask = detector.step(frame);
        if N == frameNum
            mask = detectObjects(mask, smask);
        end
    end
end

end

function mask = detectObjects(mask, smask)
    % Apply morphological operations to remove noise and fill in holes.
    mask = imopen(mask, strel('rectangle', [3,3]));
    mask = imclose(mask, strel('rectangle', [15, 15]));
    mask = imfill(mask, 'holes'); 
    mask = imopen(mask, strel('disk', 5));
    
    mask = logical(mask .* smask);

     % Find biggest blob
    [Labels] = bwlabel(mask);
    RegionStats = regionprops(mask, 'Area');
    [~, Largest_Label] = max([RegionStats.Area]);
    if ~isempty(Largest_Label)
        Labels(Labels ~= Largest_Label) = 0;
        Labels(Labels ~= 0) = 1;
        mask = logical(Labels);
    end
    
end