Image = imread('file.png');
Gray = rgb2gray(Image);

Filter = fspecial('gaussian', [10 10], 3);
Gauss=imfilter(Image,Filter,'same');

Hue = rgb2hsv(Image);
Hue = Hue(:,:,3);
threshVal = graythresh(Hue); % 0.4471; %
Tresh=im2bw(im2double(Hue), threshVal);
Tresh2=bwareaopen(Tresh, 200);
Tresh2=imcomplement(Tresh2);
[Labels] = bwlabel(Tresh2);
RegionStats = regionprops(Tresh2, 'Area');
[~, Largest_Label] = max([RegionStats.Area]);
LargestObject=Labels;
LargestObject(LargestObject ~= Largest_Label) = 0;



BWoutline = bwperim(LargestObject);
Segout = Image;
Segout(BWoutline) = 255;
imshow(Segout), title('outlined original image');

Image2=imread('model_up.png');
xpos=210;
ypos=73;
Image2(ypos:(ypos+155),xpos:(xpos+173))=Segout(1:156,1:174);
imshow(Image2);