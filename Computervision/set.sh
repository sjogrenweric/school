#!/bin/bash

git config --global push.default matching
git config --global user.email "$1@cs.umu.se"
git config --global user.name "$1"
