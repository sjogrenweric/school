#! /bin/bash
if [ $# -lt 2 ]; then
	echo "Invalid number of arguments. Usage: ./gitcommit.sh <\"message\"> <username>"
	exit;
fi

git commit -a -m "$1"  --author="\""$2" <"$2"@cs.umu.se>\""

