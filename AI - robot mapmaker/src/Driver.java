import java.awt.Color;
import java.util.ArrayList;

public class Driver implements Runnable {
	
	private RobotCommunication robotcomm; // communication drivers
	Position[] positions = {};
	private boolean isRunning;
	LocalizationResponse lr = new LocalizationResponse();
	DifferentialDriveRequest dr = new DifferentialDriveRequest();
	Position robotPosition;
	Position carrotPosition = new Position(100, 100);
	boolean safeMode = false;
	int currentPosition = 0;
	
	private ReactiveModule reactive;
	

	
	
	static double angleTolerance = 0.05;
	double distanceTolerance = 0.2;
	double speed = 1;
	double distanceToNextCarrot = 0;
	static long forwardTime = 0;
	static long turnTime = 0;
	
	
	public Driver(RobotCommunication comm, boolean isRunning){
		this.isRunning = isRunning;
		this.robotcomm = comm;
		reactive = new ReactiveModule(comm);
		try {
			dr.setAngularSpeed(0);
			dr.setLinearSpeed(speed);
			performRequest();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void init(ArrayList<Position> positions){
	//	System.out.print("init started...");
		this.isRunning = true;
		int i = 0;
		//System.err.println("GOAL POSITION = " + positions.get(positions.size()-1).toString());
		this.positions = new Position[positions.size()];
		for(Position p : positions){
			
				this.positions[i] = Utils.internalPositionToGPosition((int)p.getX(),(int)p.getY(),Cartographer.MAPSIZEX,Cartographer.MAPSIZEY);
			i++;
		}
	//	System.out.print(" init finished");
		
	}
	
	public boolean isRunning() {
		return isRunning;
	}
	

	@Override
	public void run() {
		long time = System.currentTimeMillis();
				
		System.out.println("driver running...");
		try {			
		//	getRobotPosition();
		//	carrotPosition = chooseCarrot();
			
	//		turnToPositon(carrotPosition);
		

			while (!( Utils.distanceToPosition(carrotPosition, positions[positions.length - 1])<0.1) &&!carrotPosition.equals(positions[positions.length - 1])) {
//				if(System.currentTimeMillis() - time >=10000){
//					stopRobot();
//					System.err.println("too long breaking");
//					break;
//				} 
			//	System.out.println("NEAREST: " + nearestObstacle());
				//carrotPosition = chooseCarrot(true);		
				/*if(nearestObstacle() <=0.4 ){//&& !carrotPosition.equals(positions[0])){
					
						//driveBackwards(0.2);
						stopRobot();
						System.err.println("too close, breaking");
						break;
						
						//turnToCarrot();
					//	turnToPositon(carrotPosition);
						//driveToCarrot();
					
				} else*/ 
				getRobotPosition();
				if(reactive.nearestObstacle() <=0.8){
				
					System.err.println("too close, saferun");
					speed = 0.4;
					carrotPosition = chooseCarrot(true);
					if (Utils.distanceToPosition(robotPosition, carrotPosition) < distanceTolerance){
						//stopRobot();
						continue;
					}else{
					turnToCarrot();
					//turnToPositon(carrotPosition);
					driveToCarrot();
					}
					
				}
				else{
					speed = 0.7;
					
					//turnToCarrot();
					//System.out.format("%f is distance between, %f is distance tolerance \n"
						//	,Utils.distanceToPosition(robotPosition, carrotPosition)
						//	,distanceTolerance);
					carrotPosition = chooseCarrot(false);
					if (Utils.distanceToPosition(robotPosition, carrotPosition) < distanceTolerance){
						System.out.println("continuing");
						continue;		
					}else{
						turnToCarrot();
					//turnToPositon(carrotPosition);
					driveToCarrot();
					}
				}
			}
			stopRobot();
			isRunning = false;
		} catch(Exception e){
			
		}
	}

	private void driveBackwards(double timeInSeconds) throws Exception {
		dr.setAngularSpeed(0);
		dr.setLinearSpeed(-speed);	
		performRequest();
		Thread.sleep((long)timeInSeconds*1000);
	}


	/**
	 * Set the robot to drive with full speed in a straight line until the robot
	 * is within a pre-defined distance to the carrot.
	 * 
	 * @throws Exception
	 */
	private void driveToCarrot() throws Exception {
	//	long time = System.currentTimeMillis();
		do {
			getRobotPosition();
			dr.setAngularSpeed(0);
			dr.setLinearSpeed(speed);
			performRequest();
		} while (robotPosition.getDistanceTo(carrotPosition) < distanceTolerance);
	//	forwardTime += System.currentTimeMillis() - time;
	}

	/**
	 * Allow the robot to move forwards while at the same time turning towards
	 * the current carrot along the path. Turn until the angle between the robot
	 * and the carrot is smaller than a pre-set angle tolerance. If the program
	 * executes in safe mode then the linear speed of the robot will depend upon
	 * the distance of the nearest obstacle.
	 * 
	 * @throws Exception
	 */
	private void turnToCarrot() throws Exception {
		double turnAngle;
		long time = System.currentTimeMillis();
		do {

			getRobotPosition();
			carrotPosition = chooseCarrot(false);
		//	System.out.println("driving towards position .." + carrotPosition.toString());
		//	System.out.println("current position .." + robotPosition.toString());
		//	System.out.println("goal position .." + positions[positions.length-1]);
			if (distanceToNextCarrot != robotPosition
					.getDistanceTo(carrotPosition)) {
//				System.out
//						.printf("Distance to next carrot: %.2f m, distance "
//								+ "to nearest obstacle: %.2f m \n",
//								distanceToNextCarrot, nearestObstacle());
							}
			distanceToNextCarrot = robotPosition.getDistanceTo(carrotPosition);

			turnAngle = calculateTurnAngle();
			double nearestObstacle = reactive.nearestObstacle();
			dr.setAngularSpeed(turnAngle * Math.PI);

			if (carrotPosition.equals(positions[positions.length - 1])) {
				dr.setLinearSpeed(0);
			} else {
				if (!safeMode) {
					dr.setLinearSpeed(speed);
				} else {

					dr.setLinearSpeed(nearestObstacle < speed / 2 ? speed / 2
							: nearestObstacle);
				}
			}
			performRequest();

		} while (Math.abs(turnAngle) >= angleTolerance);
		turnTime += System.currentTimeMillis() - time;
	}

	/**
	 * Request the current position of the robot in the global coordinate
	 * system.
	 * 
	 * @throws Exception
	 */
	private void getRobotPosition() throws Exception {
		robotcomm.getResponse(lr);
		robotPosition = new Position(getPosition(lr));
	}
	double[] getPosition(LocalizationResponse lr) {
		return lr.getPosition();
	}

	/**
	 * Turn towards given point while not moving forwards.
	 * 
	 * @param carrot
	 *            , the point to turn towards.
	 * @throws Exception
	 */
	private void turnToPositon(Position carrot) throws Exception {
		double turnAngle = 0;
		do {
			getRobotPosition();
			turnAngle = calculateTurnAngle();
			dr.setAngularSpeed(turnAngle * Math.PI);
			dr.setLinearSpeed(0);
			//System.err.println("turning with turnAngle " + turnAngle );
			performRequest();
		} while (Math.abs(turnAngle) >= angleTolerance);
	}

	

	/**
	 * Convert the position of the carrot and robot to local coordinates and
	 * then calculate the rotational angle between them.
	 * 
	 * @return angle to the carrot
	 */
	private double calculateTurnAngle() {
		double angle = getBearingAngle(lr);
		Position carrotLocal = new Position(Utils.localCoordinates(
				carrotPosition.getX(), carrotPosition.getY(), angle));
		Position robotLocal = new Position(Utils.localCoordinates(
				robotPosition.getX(), robotPosition.getY(), angle));

		return robotLocal.getBearingTo(carrotLocal);
	}

	/**
	 * Choose a suitable carrot based on the distance to obstacles measured by
	 * the laser scanner. Let the distance to the carrot to be shorter than the
	 * distance to the nearest obstacle.
	 * 
	 * @return
	 * @throws Exception
	 */
	private Position chooseCarrot(boolean safe) throws Exception {
		int prevIndex = currentPosition;
		Position prevCarrot = carrotPosition;

		getRobotPosition();
		double obstacleDistance = reactive.nearestObstacle();
		Position carrot ;
		
		currentPosition = reactive.getDynamicCarrot(safe, obstacleDistance, positions, prevIndex, robotPosition);
		//getDynamicCarrot(safe, obstacleDistance);
		
		carrot = positions[currentPosition];
		
	
	/*	if (!carrot.equals(prevCarrot))
			System.out.println("Choosing carrot: jumped "
					+ (currentPosition - prevIndex) + " positions.");*/
		return carrot;

	}


	

	/**
	 * Set linear and angular speed to zero to stop robot movement.
	 * 
	 * @throws Exception
	 */
	private void stopRobot() throws Exception {
		dr.setAngularSpeed(0);
		dr.setLinearSpeed(0);
		performRequest();
	}

	
	private void performRequest() throws Exception {
		int responseCode = 0, tries = 0;
		do {
			responseCode = robotcomm.putRequest(dr);
			if (responseCode != 204) {
				System.err.println("Request failed: " + dr.getPath());
				tries++;
			}
		} while (responseCode != 204 && tries < 3);
		if (tries == 3) {
			System.err
					.println("Unable to perform request, system shutting down.");
			System.exit(1);
		}

	}
	
	double getBearingAngle(LocalizationResponse lr) {
		double e[] = lr.getOrientation();

		double angle = 2 * Math.atan2(e[3], e[0]);
		angle = angle * 180 / Math.PI;
		if (angle < 0)
			angle = 360 + angle;
		return angle;
	}



}
