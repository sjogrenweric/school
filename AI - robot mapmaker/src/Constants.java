
public class Constants {
	public static final int WHITE = 0;
	public static final int GREY = 7;
	public static final int BLACK = 15;
	public static final int EXPAND = 11;
	public static final int PROXIMITYPOS = 10;
}
