/**
 * Created by eric on 2015-12-13.
 */
public class Robot3 {

    private RobotCommunication robotcomm; // communication drivers
    static Position[] positions = {};
    LocalizationResponse lr = new LocalizationResponse();
    DifferentialDriveRequest dr = new DifferentialDriveRequest();
    Position robotPosition;
    Position carrotPosition = new Position(100, 100);
    int currentPosition = 0;
    static boolean safeMode = false;
    static double angleTolerance = 0.05;
    double distanceTolerance = 0.05;
    double speed = 1;
    double distanceToNextCarrot = 0;
    static long forwardTime = 0;
    static long turnTime = 0;

    /**
     * Create a robot connected to host "host" at port "port"
     *
     * @param host
     *            normally http://127.0.0.1
     * @param port
     *            normally 50000
     */
    public Robot3(String host, int port) {
        robotcomm = new RobotCommunication(host, port);
    }

    /**
     * Check if a string is a numeric value.
     *
     * @param str
     *            String to be evaluated
     * @return true if the string is a numeric value otherwise false
     */
    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /**
     * Start the robot and measure the time it takes to complete the course.
     *
     * @param args
     */
    public static void main(String[] args) {
        Robot3 robot = processArgsAndCreateRobot(args);

        try {
            long time = System.currentTimeMillis();
            robot.run();
            time = (System.currentTimeMillis() - time);

            System.out.println("Robot completed path on " + time / 1000 + ":"
                    + time / 10 % 100 + " seconds.");
            System.out.println("Robot was in turn-mode for " + turnTime / 1000
                    + ":" + turnTime / 10 % 100 + " seconds.");
            System.out.println("Robot was in forward-driving-mode for "
                    + forwardTime / 1000 + ":" + forwardTime / 10 % 100
                    + " seconds.");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Create path positions and robot based on default values or argument
     * values.
     *
     * @param args
     * @return
     */
    private static Robot3 processArgsAndCreateRobot(String[] args) {
        String host = null, path = null;
        int port = 0;

        for (int i = 0; i < args.length; i++) {
            System.out.println("Found argument: " + args[i]);
            if (args[i].contains("http")) {
                host = args[i];
            } else if (isNumeric(args[i].toString())) {
                port = Integer.parseInt(args[i]);
            } else if (args[i].contains("safe")) {
                safeMode = true;
            } else if (args[i].contains("json")) {
                path = args[i];
            }
        }
      //  positions = Utils.readPathFile(path == null ? "Path-around-table-and-back.json": path);

        return new Robot3((host == null ? "http://127.0.0.1" : host),
                (port == 0 ? 50000 : port));
    }

    /**
     * Main loop to drive the robot along the path.
     *
     * @throws Exception
     */
    private void run() throws Exception {
        if (safeMode) {
            getRobotPosition();
            carrotPosition = chooseCarrot();
            turnToPositon(carrotPosition);
        }

        while (!carrotPosition.equals(positions[positions.length - 1])) {
            turnToCarrot();
            if (robotPosition.getDistanceTo(carrotPosition) < distanceTolerance)
                continue;
            driveToCarrot();
        }

        stopRobot();
    }

    /**
     * Set the robot to drive with full speed in a straight line until the robot
     * is within a pre-defined distance to the carrot.
     *
     * @throws Exception
     */
    private void driveToCarrot() throws Exception {
        long time = System.currentTimeMillis();
        do {
            getRobotPosition();
            dr.setAngularSpeed(0);
            dr.setLinearSpeed(speed);
            performRequest();
        } while (robotPosition.getDistanceTo(carrotPosition) < distanceTolerance);
        forwardTime += System.currentTimeMillis() - time;
    }

    /**
     * Execute current differential drive request to the MDRS service. If the
     * response code is anything else than 204, it will try to re-do the request
     * three times before giving up. After three attempts the service is assumed
     * to be unreachable and the program exits.
     *
     * @throws Exception
     */
    private void performRequest() throws Exception {
        int responseCode = 0, tries = 0;
        do {
            responseCode = robotcomm.putRequest(dr);
            if (responseCode != 204) {
                System.err.println("Request failed: " + dr.getPath());
                tries++;
            }
        } while (responseCode != 204 && tries < 3);
        if (tries == 3) {
            System.err
                    .println("Unable to perform request, system shutting down.");
            System.exit(1);
        }

    }

    /**
     * Allow the robot to move forwards while at the same time turning towards
     * the current carrot along the path. Turn until the angle between the robot
     * and the carrot is smaller than a pre-set angle tolerance. If the program
     * executes in safe mode then the linear speed of the robot will depend upon
     * the distance of the nearest obstacle.
     *
     * @throws Exception
     */
    private void turnToCarrot() throws Exception {
        double turnAngle;
        long time = System.currentTimeMillis();
        do {

            getRobotPosition();
            carrotPosition = chooseCarrot();
            if (distanceToNextCarrot != robotPosition
                    .getDistanceTo(carrotPosition)) {
                System.out
                        .printf("Distance to next carrot: %.2f m, distance "
                                        + "to nearest obstacle: %.2f m \n",
                                distanceToNextCarrot, nearestObstacle());
            }
            distanceToNextCarrot = robotPosition.getDistanceTo(carrotPosition);

            turnAngle = calculateTurnAngle();
            double nearestObstacle = nearestObstacle();
            dr.setAngularSpeed(turnAngle * Math.PI);

            if (carrotPosition.equals(positions[positions.length - 1])) {
                dr.setLinearSpeed(0);
            } else {
                if (!safeMode) {
                    dr.setLinearSpeed(speed);
                } else {

                    dr.setLinearSpeed(nearestObstacle < speed / 2 ? speed / 2
                            : nearestObstacle);
                }
            }
            performRequest();

        } while (Math.abs(turnAngle) >= angleTolerance);
        turnTime += System.currentTimeMillis() - time;
    }

    /**
     * Request the current position of the robot in the global coordinate
     * system.
     *
     * @throws Exception
     */
    private void getRobotPosition() throws Exception {
        robotcomm.getResponse(lr);
        robotPosition = new Position(getPosition(lr));
    }

    /**
     * Turn towards given point while not moving forwards.
     *
     * @param carrot
     *            , the point to turn towards.
     * @throws Exception
     */
    private void turnToPositon(Position carrot) throws Exception {
        double turnAngle = 0;
        do {
            getRobotPosition();
            turnAngle = calculateTurnAngle();
            dr.setAngularSpeed(turnAngle * Math.PI);
            dr.setLinearSpeed(0);
            performRequest();
        } while (Math.abs(turnAngle) >= angleTolerance);
    }

    /**
     * Find the shortest distance to an obstacle in a forward-facing cone
     *  of 271
     * - 108 = 163 degrees.
     *
     * @return Shortest distance to obstacle
     * @throws Exception
     */
    private double nearestObstacle() throws Exception {
        double min = 20;
        double dist;

        LaserEchoesResponse ls = new LaserEchoesResponse();
        robotcomm.getResponse(ls);
        double[] echoes = ls.getEchoes();

        int frontCone = 54;
        for (int i = frontCone; i < echoes.length - frontCone; i++) {
            dist = echoes[i];
            if (dist < min) {
                min = dist;
            }
        }

        return min;
    }

    /**
     * Convert the position of the carrot and robot to local coordinates and
     * then calculate the rotational angle between them.
     *
     * @return angle to the carrot
     */
    private double calculateTurnAngle() {
        double angle = getBearingAngle(lr);
        Position carrotLocal = new Position(Utils.localCoordinates(
                carrotPosition.getX(), carrotPosition.getY(), angle));
        Position robotLocal = new Position(Utils.localCoordinates(
                robotPosition.getX(), robotPosition.getY(), angle));

        return robotLocal.getBearingTo(carrotLocal);
    }

    /**
     * Choose a suitable carrot based on the distance to obstacles measured by
     * the laser scanner. Let the distance to the carrot to be shorter than the
     * distance to the nearest obstacle.
     *
     * @return
     * @throws Exception
     */
    private Position chooseCarrot() throws Exception {
        int prevIndex = currentPosition;
        Position prevCarrot = carrotPosition;

        getRobotPosition();
        double obstacleDistance = nearestObstacle();

        Position carrot = positions[currentPosition];
        while(currentPosition != positions.length - 1 && robotPosition.getDistanceTo(positions[currentPosition + 1]) < obstacleDistance) {
            currentPosition++;
            carrot = positions[currentPosition];
        }


        if (!carrot.equals(prevCarrot))
            System.out.println("Choosing carrot: jumped "
                    + (currentPosition - prevIndex) + " positions.");
        return carrot;

    }

    /**
     * Set linear and angular speed to zero to stop robot movement.
     *
     * @throws Exception
     */
    private void stopRobot() throws Exception {
        dr.setAngularSpeed(0);
        dr.setLinearSpeed(0);
        performRequest();
    }

    /**
     * Extract the robot bearing from the response
     *
     * @param lr
     * @return angle in degrees
     */
    double getBearingAngle(LocalizationResponse lr) {
        double e[] = lr.getOrientation();

        double angle = 2 * Math.atan2(e[3], e[0]);
        angle = angle * 180 / Math.PI;
        if (angle < 0)
            angle = 360 + angle;
        return angle;
    }

    /**
     * Extract the position
     *
     * @param lr
     * @return coordinates
     */
    double[] getPosition(LocalizationResponse lr) {
        return lr.getPosition();
    }
}
