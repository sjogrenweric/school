
public class Position
{
   private double x, y;
  
   private int proximity = 20;

   public Position(double pos[])
   {
      this.x = pos[0];
      this.y = pos[1];
   }

   public Position(double x, double y)
   {
      this.x = x;
      this.y = y;
   }

   public double getX() { return x; }
   public double getY() { return y; }
   
   public int getIntX(){ return (int)x; }
   public int getIntY(){ return (int)y; }

   public double getDistanceTo(Position p)
   {
      return Math.sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y));
   }

   // bearing relative 'north'
   public double getBearingTo(Position p)
   {
      return Math.atan2(p.y - y, p.x - x);
   }
   
  
   
   /**
    * Checks if a position is the vicinity of a position
    * @param pos
    * @return
    */
   public boolean inProximity(Position pos){
	   double distance = Utils.distanceToPosition(this, pos);//Math.sqrt(Math.pow((Math.abs(this.x - pos.getX())),2) + Math.pow((Math.abs(this.y - pos.getY())),2));
	   if(distance < proximity)
		   return true;
	   return false;
   }
   
   public int compareTo(Position p){
	   if((p.getIntX() == (int)this.x && p.getIntY() == (int)this.y)){
		   return 0;
	   }
	   else{
		   return -1;
	   }
	   
	   
   }
   @Override
   public boolean equals(Object obj) {
	   Position p = (Position)obj;
	   if(p.getX() == this.getX()) {
		   if(p.getY() == this.getY()) {
			   return true;
		   }
	   }
	return false;
	   
   }
   
   
   @Override
   public String toString(){
	   return "("+x+","+y+")";
   }
}
