import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;

public class Cartographer {	

	private static final boolean xbool = true;
	private static final boolean ybool = false;
	private boolean once = true;
	
	private int x1,x2,y1,y2;
	public static int MAPSIZEX;
	public static int MAPSIZEY;
	private int[][] internalMap;
	private  ArrayList<Position> explorePositions;
	
	private Position robotPosition;
	
	private RobotCommunication robotcomm;
	private ShowMap map;

	public Cartographer(RobotCommunication robotcomm) {
		this.robotcomm = robotcomm;
	}
	
	public void init(String[] args) {
		explorePositions = new ArrayList<>();
		x1 = Integer.parseInt(args[1]);
		y1 = Integer.parseInt(args[2]);
		x2 = Integer.parseInt(args[3]);
		y2 = Integer.parseInt(args[4]);
	//	System.out.println(x1);
	//	System.out.println(y1);
		
		MAPSIZEX = 500;
		MAPSIZEY =  (y2+Math.abs(y1)) * (MAPSIZEX-1)/2/Math.abs(x1);
		Utils.calcfunction(x1, x2, y1, y2, MAPSIZEX, MAPSIZEY);	
		internalMap = new int[MAPSIZEX][MAPSIZEY];
		paintMapGrey();
		
		
		//startPosition = getRobotPosition();
		//startPosition = new Position(Utils.convertToMapCoordinates(startPosition.getX(),MAPSIZEX,xbool)
		//		,Utils.convertToMapCoordinates(startPosition.getY(),MAPSIZEY,ybool));
		
	}

	
	
    /**
	 * A simple example of how to use the ShowMap class that creates a map from
	 * your grid, updates it and saves it to file
	 */
	private void createMap(int[][] internalMap) {
		int nRows = MAPSIZEX;
		int nCols = MAPSIZEY;
		/* use the same no. of rows and cols in map and grid */
		if(once){
			makemap();
			once = false;
			
		}
		
		/* Creating a grid with 0.5 */
		int[][] grid = new int[nRows][nCols];
		for (int i = 0; i < nRows; i++) {
			for (int j = 0; j < nCols; j++) {
				grid[i][j] = Constants.GREY;
			}
		}
		
		//cleanupWalls();
		//test
		try {
			robotPosition = Robot.getRobotPosition();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int robotCol = Utils.convertToMapCoordinates(robotPosition.getX(),MAPSIZEX, true);
		int robotRow = Utils.convertToMapCoordinates(robotPosition.getY(),MAPSIZEY, false);
		/* create some obstacles (black/grey)*/
		// Upper left side:
		for(int j = 0 ; j < MAPSIZEX ;j++){
			for(int k= 0 ; k < MAPSIZEY ;k++){
				//if(internalMap[j][k] != EXPAND){
				//	grid[j][k] = WHITE;
				//}else{
					grid[j][k] = internalMap[j][k];
				//}
			
				}	
				
		}
		
		for(Position pos : explorePositions){
			
			//grid[(int)pos.getX()][(int)pos.getY()] = 10;
		}
		
		

		int maxVal = 15;
		// Update the grid
		map.updateMap(grid, maxVal, robotRow, robotCol);
	}
	
	public void findExplorablePoints() {
		explorePositions = new ArrayList<Position>();
		
		for (int i = 0; i < MAPSIZEX ; i++) {
			for (int j = 0; j < MAPSIZEY; j++) {
				if(internalMap[i][j] == Constants.WHITE){
					if(existsUnexplored(i,j,Constants.GREY) && !existsUnexplored(i,j,Constants.BLACK)
							&& i > 0 && i<MAPSIZEX && j > 0 && j < MAPSIZEY){
						explorePositions.add(new Position(i,j));
					}
				}
			}
		}		
	}
	
	/**
	 *TODO fixa detta snyggare
	 *m�ste g�ra om s� att den kollar p� kanter ist�llet typ
	 *kan bli tom
	 * @return
	 */
	public ArrayList<Position> findExplorationsWayPoints(){
		
		ArrayList<Position> exploreWaypoint = new ArrayList<Position>();
		
		ArrayList<ArrayList<Position>> positions = new ArrayList<ArrayList<Position>>();
		ArrayList<Position> proximityPos;
		
		HashMap<Position, Boolean> addedPositions = new HashMap<Position, Boolean>();
		
		
		int x=0,y=0;
	
		//System.err.println(explorePositions);
		for(Position pos : explorePositions){
			//System.out.println("hej");
			proximityPos = new ArrayList<Position>();	
		
			if(!addedPositions.containsKey(pos)){
				proximityPos.add(pos);
				addedPositions.put(pos, true);
				
				for(Position proxpos : explorePositions){
					if(!addedPositions.containsKey(proxpos)){
						if(pos.inProximity(proxpos)){
							proximityPos.add(proxpos);
							addedPositions.put(proxpos, true);
						}
					}
				
				}
				positions.add(proximityPos);
			}
			
		}
	//	System.err.println(positions);
		for(ArrayList<Position> proximityPositions : positions){
			if(proximityPositions.size() > Constants.PROXIMITYPOS){
			// calc average
				for(Position pos : proximityPositions){
					x+=pos.getX();
					y+=pos.getY();
				}
				x/=proximityPositions.size();
				y/=proximityPositions.size();
				if(Utils.checkOutOfBoundsInternal(x, y, MAPSIZEX-20,MAPSIZEY-20, 20)){
					if(internalMap[x][y] == Constants.WHITE){
						exploreWaypoint.add(new Position(x,y));
					} else{
						Position p = findClosestWhiteArea(new Position(x,y),5);
						exploreWaypoint.add(p);
					}
				}
				//map.drawSquare(y, x);
			}
		}
		
		//System.err.println(exploreWaypoint);
		return exploreWaypoint;
	}
	
	private Position findClosestWhiteArea(Position pos,int expand) {
	
		ArrayList<Position> neighs;
		neighs = Utils.getAllNeighbours(pos, expand);
		//System.err.println(neighs);
		for(Position posi : neighs){
		//	System.err.println("neighs");
			if(internalMap[posi.getIntX()][posi.getIntY()]== Constants.WHITE){
			//	System.err.println("white");
				return posi;
			}
		}
		return findClosestWhiteArea(pos, expand+5);
	}
	
	public void cleanupWalls(){
		int whites;
		for (int i = 0; i < MAPSIZEX ; i++) {
			for (int j = 0; j < MAPSIZEY; j++) {
				if(internalMap[i][j] == Constants.BLACK){
					whites=0;
					for (int k = i-1; k <= i+1 ; k++) {
						for (int l = j-1; l <= j+1; l++) {
							if(Utils.checkOutOfBoundsInternal(k, l, MAPSIZEX,MAPSIZEY, 0)){
								
								if(internalMap[k][l] == Constants.WHITE){
									whites++;
								}
							}
						}
					}
					if(whites >=7){
						internalMap[i][j] = Constants.WHITE;
					}
				}
			}
		}
	}
	
    private void makemap(){
    	int nRows = MAPSIZEX;
		int nCols = MAPSIZEY;
		boolean showGUI = true; // set this to false if you run in putty
		
		map = new ShowMap(nRows, nCols, showGUI);
    }


	public void readAndCreateMap(LocalizationResponse lr,
			LaserEchoesResponse ler, double[] angles) throws Exception {
	//	System.out.println("creating map");
		double addwhite = 0.1;
		 robotPosition = Robot.getRobotPosition();
		// System.out.println("getRobotPosition");

		// Ask the robot for laser echoes
		robotcomm.getResponse(ler);
		// System.out.println("ler");
		double[] echoes = ler.getEchoes();
		for(int j = 45 ; j < 45+180; j+=1 ){

		Double x = Utils.coordinateOfObject(Robot.getBearingAngle(), angles[j], robotPosition, echoes[j]).getX();
		Double y = Utils.coordinateOfObject(Robot.getBearingAngle(), angles[j], robotPosition, echoes[j]).getY();
	
		//System.err.println("checking : "+ x + " : " + y + " x1 : "+ x1 + " x2: "+ x2 + " y1: "+y1 + " y2:" + y2);
			if(echoes[j] < 40 && Utils.checkOutOfBoundsGlobal(x, y, x1,x2, y1,y2) 
					&& Utils.checkOutOfBoundsInternal(Utils.convertToMapCoordinates(x, MAPSIZEX, xbool), Utils.convertToMapCoordinates(y, MAPSIZEY, ybool), MAPSIZEX, MAPSIZEY, 0)
					//&& internalMap[Utils.convertToMapCoordinates(x)][Utils.convertToMapCoordinates(y)] != WHITE
					///&& internalMap[Utils.convertToMapCoordinates(x)][Utils.convertToMapCoordinates(y)] != EXPAND
					){
				//System.err.println("checking : "+ x + " : " + y + " x1 : "+ x1 + " x2: "+ x2 + " y1: "+y1 + " y2:" + y2);
				internalMap[Utils.convertToMapCoordinates(x, MAPSIZEX, xbool)][Utils.convertToMapCoordinates(y, MAPSIZEY, ybool)] = Constants.BLACK;
					
				
			}
			
			if( Utils.checkOutOfBoundsGlobal(x, y, x1,x2, y1,y2) ){

				drawWhiteToObject(lr, angles, addwhite, echoes, j);	
			}
			
			if( !Utils.checkOutOfBoundsGlobal(x, y, x1,x2, y1,y2)  ){

				drawWhiteToEdge(lr, angles, addwhite, j);
					
			}
		
		}
	//	System.out.println("find explorable points");
		findExplorablePoints();
	//	System.out.println("create map");
		createMap(internalMap);
	}
	
	private void drawWhiteToEdge(LocalizationResponse lr, double[] angles, double addwhite, int j) {
		double distance;
		int mcordwx,mcordwy;
		distance = 0;
		double wx = Utils.coordinateOfObject(Robot.getBearingAngle(), angles[j], robotPosition, distance).getX();
		double wy = Utils.coordinateOfObject(Robot.getBearingAngle(), angles[j], robotPosition, distance).getY();
		
		mcordwx = Utils.convertToMapCoordinates(wx,MAPSIZEX, xbool);
		mcordwy = Utils.convertToMapCoordinates(wy,MAPSIZEY, ybool);
		
		while((wx <= x2 && wx >= x1) && (wy <= y2 && wy >= y1) 
				&& Utils.checkOutOfBoundsInternal(mcordwx, mcordwy, MAPSIZEX,MAPSIZEY, 0)){
				/*&& (Utils.convertToMapCoordinates(wx) > 0 && Utils.convertToMapCoordinates(wx) < MAPSIZE)  &&
				(Utils.convertToMapCoordinates(wy) > 0 && Utils.convertToMapCoordinates(wy) < MAPSIZE)) {*/
			
			distance +=addwhite;
			if(internalMap[mcordwx][mcordwy] != Constants.BLACK && internalMap[mcordwx][mcordwy] != Constants.WHITE && internalMap[mcordwx][mcordwy] != Constants.EXPAND){
					internalMap[mcordwx][mcordwy] = Constants.WHITE;
				
			}
			wx = Utils.coordinateOfObject(Robot.getBearingAngle(), angles[j], robotPosition, distance).getX();
			wy = Utils.coordinateOfObject(Robot.getBearingAngle(), angles[j], robotPosition, distance).getY();
			
			mcordwx = Utils.convertToMapCoordinates(wx,MAPSIZEX, xbool);
			mcordwy = Utils.convertToMapCoordinates(wy,MAPSIZEY, ybool);
				
		
		}
	}
	

	
	private void drawWhiteToObject(LocalizationResponse lr, double[] angles, double addwhite, double[] echoes, int j) {
		double distance;
		distance = echoes[j]-addwhite;
		double wx,wy;
		int mcordwx,mcordwy;
			
		while(distance > 0){
			wx = Utils.coordinateOfObject(Robot.getBearingAngle(), angles[j], robotPosition, distance).getX();
			wy = Utils.coordinateOfObject(Robot.getBearingAngle(), angles[j], robotPosition, distance).getY();
			mcordwx = Utils.convertToMapCoordinates(wx,MAPSIZEX, true);
			mcordwy = Utils.convertToMapCoordinates(wy,MAPSIZEY, false);
			distance -=addwhite;
			if(Utils.checkOutOfBoundsInternal(mcordwx, mcordwy, MAPSIZEX,MAPSIZEY, 0)) {
				if(internalMap[mcordwx][mcordwy] != Constants.BLACK  && internalMap[mcordwx][mcordwy] != Constants.WHITE &&  internalMap[mcordwx][mcordwy] != Constants.EXPAND ){
				
						internalMap[mcordwx][mcordwy] = Constants.WHITE;
					
				}
			}
			
		}
	}

	public void drawPath(ArrayList<Position> path) {
		Position temp;
		for(Position p : path){
			map.drawSquare2((int)p.getY(), (int)p.getX(), Color.ORANGE);
			temp = Utils.internalPositionToGPosition((int)p.getX(), (int)p.getY(), MAPSIZEX, MAPSIZEY);
			p = new Position(Utils.convertToMapCoordinates(temp.getX(), MAPSIZEX,true), Utils.convertToMapCoordinates(temp.getY(), MAPSIZEY,false));
			map.drawSquare2((int)p.getY(), (int)p.getX(), Color.ORANGE);
		}
		
	}
	
	private boolean existsUnexplored(int x, int y, int color){

		for (int i = x-1; i <= x+1 ; i++) {
			for (int j = y-1; j <= y+1; j++) {
				if((i > 0 && i < MAPSIZEX ) && (j > 0 && j < MAPSIZEY )  ){
					if(internalMap[i][j] == color){
						return true;
					}	
				}	
			}
		}
		return false;
	
	}

	public int[][] getInternalMap() {
		return internalMap;
	}
	
	public void cleanUpAndSave(){
		cleanupWalls();
		createMap(internalMap);
	}
	
	private void paintMapGrey() {
		for(int j = 0 ; j < MAPSIZEX ;j++){
			for(int k= 0 ; k < MAPSIZEY ;k++){		
				 internalMap[j][k] = Constants.GREY;	
				}				
		}
	}
}
