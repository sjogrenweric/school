import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

public class PathFinder {
	private static final int PROXIMITYDISTANCETOGOAL = 10;
	private int[][] waveFrontMap;
	private int[][] internalMap;
	private boolean once = true;
	private ArrayList<Position> path;
	
	private Position robot;
	
	private Position p;
	private Position temp;
	private ArrayList<Position> neighs;
	
	public PathFinder(int[][] internalMap) {
		//System.arraycopy( internalMap, 0, this.internalMap, 0, internalMap.length );
		this.internalMap = new int[internalMap.length][];
		for(int i=0; i<internalMap.length; i++) {
			this.internalMap[i] = Arrays.copyOf(internalMap[i], internalMap[i].length);
		}
		expandWalls();
		waveFrontMap = new int[Cartographer.MAPSIZEX][Cartographer.MAPSIZEY];
		once = true;
		path = new ArrayList<Position>();
	}

	

	


	public ArrayList<Position> getPathToWayPoint(Position goal, Position robot){
		robot = makeSureTheRobotIsOnWhite(robot);
		//System.out.println("generating heatMap");
		generateHeatMap(goal,robot);
		//System.out.println("done generating heatMap");
		generatePath(new Position (robot.getX(),robot.getY()));
	//	System.err.println("PATH:" + path);
	//	printHeatMapAsGif();
	//	printHeatMapToConsole();
		return path;
	}
	private Position makeSureTheRobotIsOnWhite(Position robotPosition) {
		Position position = robotPosition;
		if(internalMap[robotPosition.getIntX()][robotPosition.getIntY()]==Constants.EXPAND ||
				internalMap[robotPosition.getIntX()][robotPosition.getIntY()]==Constants.BLACK){	
			System.out.println("old position = " + robotPosition.toString());
			position = findClosestWhiteArea(robotPosition,5);
			System.out.println("new position = " + position.toString());
		}
		return position;
		
		
	}
	
	/**
	 * 
	 * @param goal local
	 * @param robot local
	 */
	private void generateHeatMap(Position goal, Position robot){
		
		int i = goal.getIntX();
		int j =	goal.getIntY();
		ArrayList<Position> neighs;
		Queue<Position> posqueue = new LinkedList<Position>();
		int low=1;
		int proximity = 100;
		Position currentPosition = goal;
		if(once){
			//specialfall f�r f�rsta noden
			waveFrontMap[goal.getIntX()][goal.getIntY()]=1;
			neighs = getNeighbours(goal, Constants.WHITE);
			for(Position position : neighs){
				waveFrontMap[position.getIntX()][position.getIntY()] = 2;
				posqueue.add(position);
			}
		
		}	
		
		
		int timesToRun = 10000;
		int k = 0;
		//System.out.println("posqueue = " + posqueue.toString());
		while(!posqueue.isEmpty()/*Utils.distanceToPosition(currentPosition, robot) >= proximity*/){
			//System.out.println("not empty");
			currentPosition = posqueue.poll();
			if(currentPosition == null)
				break;
			neighs = getNeighbours(currentPosition, Constants.WHITE);
			low = getLowestNonZeroNeighbourValue(neighs)+2;
			//waveFrontMap[currentPosition.getIntX()][currentPosition.getIntY()] = low;
			for(Position position : neighs){
				if(waveFrontMap[position.getIntX()][position.getIntY()] <= 0) {
					waveFrontMap[position.getIntX()][position.getIntY()] = low;
					Utils.checkIfExistsElseAdd(posqueue, position);
				}
			}
			//System.out.println("____________");
//			if(k > timesToRun) {
//				printHeatMapToConsole();
//				try {
//					Thread.sleep(100000);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		k++;
			
		}
		
		//printHeatMapToConsole();
			
		
			
		
		//printHeat();
	}
	
	private void setValueOfPositions(ArrayList<Position> neighs, int i) {
		// TODO Auto-generated method stub
		
	}

	private Position findClosestWhiteArea(Position pos,int expand) {
		ArrayList<Position> neighs;
		ArrayList<Position> closeWhiteNeighs = new ArrayList<Position>();
		neighs = Utils.getAllNeighbours(pos, expand);
		if(neighs.size()%2==1){
			neighs.remove(0);
		}
		int j = neighs.size()/2;
		closeWhiteNeighs.add(neighs.get(j));
		for(int i = 1 ; i <neighs.size()/2; i++){
			closeWhiteNeighs.add(neighs.get(j+i));
			closeWhiteNeighs.add(neighs.get(j-i));
		}
		//System.err.println(closeWhiteNeighs.size());
		for(Position posi : closeWhiteNeighs){
			if(internalMap[posi.getIntX()][posi.getIntY()] == Constants.WHITE){
				return posi;
			}
		}
		return findClosestWhiteArea(pos, expand+5);
		
		
		
	}

	private void generatePath(Position pos){
		
		if(pos == null){
			return;
		}
		temp = new Position(pos.getX(),pos.getY());
		if(waveFrontMap[temp.getIntX()][temp.getIntY()]<=PROXIMITYDISTANCETOGOAL){
			return;
		}
		Utils.checkIfExistsElseAdd(path, temp);
		neighs = getNeighbours(temp,Constants.WHITE);
//		for(Position nei : neighs){
//			if(path.contains(nei)){
//				neighs.remove(nei);
//			}
//			
//		}
		p = getLowestNonZeroNeighbour(neighs);	
		generatePath(p); 
		return;
	}	



	
	private ArrayList<Position> createPath() {
		
		return null;
	}
	
    private ArrayList<Position> getNeighbours(Position pos, int color) {
		ArrayList<Position> neighbours = new ArrayList<Position>();
		
		int expandAmount = 1;
		
		for (int i = (int) (pos.getIntX()-expandAmount); i <= pos.getIntX()+expandAmount ; i++) {
			for (int j = (int) (pos.getIntY()-expandAmount); j <= pos.getIntY()+expandAmount; j++) {
				
				if(pos.getIntX() != i || pos.getIntY() != j){ //inte vi
					if(Utils.checkOutOfBoundsInternal(i, j, Cartographer.MAPSIZEX,Cartographer.MAPSIZEY, 0)){
						if((internalMap[i][j] == color)){
								if(!pos.equals(new Position(i,j))){
									neighbours.add(new Position(i,j));
								} 
							}
					}
					
				} 
			}
		}
		return neighbours;
	}
    
    private int getLowestNonZeroNeighbourValue(ArrayList<Position> neighbours) {
    	int lowestNeighbour = 9999;
    	int temp;
    	for(Position p : neighbours) {
    		//System.out.println(" p+ " +p + " value " +waveFrontMap[p.getIntX()][p.getIntY()] + "   neigh "+ neighbours );
    		temp = waveFrontMap[p.getIntX()][p.getIntY()];
    	
    		if(temp < lowestNeighbour && temp >0) {
    			lowestNeighbour = temp;
    		}
    		
    	}
    	return lowestNeighbour;
    }
    
    private Position getLowestNonZeroNeighbour(ArrayList<Position> neighbours) {
    	int temp;
    	Position[] pos = new Position[5];
    	int index=0;
    	
    	int lowestNeighbour = getLowestNonZeroNeighbourValue(neighbours);
    	
    	//System.out.println( "   neigh "+ neighbours );
    	for(Position p : neighbours) {
    		//System.out.println(" p+ " +p + " value " +waveFrontMap[p.getIntX()][p.getIntY()] + "   neigh "+ neighbours );
    		temp = waveFrontMap[p.getIntX()][p.getIntY()];
    	
    		if(temp == lowestNeighbour) {
    			lowestNeighbour = temp;
    			pos[index]= p;
    			index++;
    		}
    		
    	}
    	//return pos[0];//lowestNeighbour;
    	return index >=2 ? pos[1] : pos[0];
    }
    

	
	
	private void expandWalls(){
		//TODO place somewhere else?
		int expandAmount = 10;
		for (int i = 0; i < Cartographer.MAPSIZEX ; i++) {
			for (int j = 0; j < Cartographer.MAPSIZEY; j++) {
				
				if(internalMap[i][j] == Constants.BLACK  ){
					for (int k = i-expandAmount; k <= i+expandAmount ; k++) {
						for (int l = j-expandAmount; l <= j+expandAmount; l++) {
							if(Utils.checkOutOfBoundsInternal(k, l, Cartographer.MAPSIZEX,Cartographer.MAPSIZEY, 0)){
								if(internalMap[k][l] == Constants.WHITE){
									
										internalMap[k][l] = Constants.EXPAND;
									
								}
							}
						}
					}
				}
			}
		}	
	}
	
	private void printHeatMapAsGif() {
		ShowMap map = new ShowMap(500,500,true);
		for(int i = 0 ; i < 500 ; i++){
			for(int j = 0 ; j < 500 ; j++){
				if(waveFrontMap[i][j]>0 && waveFrontMap[i][j]<255){
				map.setRGB2(j, i, new Color(waveFrontMap[i][j],waveFrontMap[i][j],waveFrontMap[i][j]));
				//map.drawSquare2(j, i, new Color(waveFrontMap[i][j],waveFrontMap[i][j],waveFrontMap[i][j]));
				}
			}
		}
	}
	
	private void printHeatMapToConsole() {
		int nrOfOnesInMap = 0;
		for(int i=0; i<Cartographer.MAPSIZEX; i++) {
			for(int j=0; j<Cartographer.MAPSIZEY; j++) {
				if(waveFrontMap[i][j] == 1) {
					nrOfOnesInMap++;
				}
				System.out.format("%3d", waveFrontMap[i][j]);
			}
			System.out.println("");
		}
		System.out.println("total number of ones in this map = " + nrOfOnesInMap);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//	private int[][] internalMap;
//	
//	ShowMap map ;
//	
//	private int[][] waveFrontMap;

//	
//	
//	
//	public Path getPathToPosition(int carX, int carY, int xPos, int yPos) {
//		System.out.println("generating heatmap");
//		generateHeatMap(xPos, yPos, Utils.convertToMapCoordinates((double)carX),  Utils.convertToMapCoordinates((double)carY));
//		System.out.println("done");
//		//System.out.println("f�rdig med heatmappen");
//		Path path = new Path();
//		System.out.println("generating path");
//		path = generatePath(Utils.convertToMapCoordinates((double)carX), Utils.convertToMapCoordinates((double)carY));
//		System.out.println("done");
//		return path;
//	}
//
//
//
//	private Path generatePath(int carX, int carY) {
//	
//		Position prevNeig = new Position(0,0);
//		Position lowestNeighbour ;
//		int car = waveFrontMap[carX][carY];
//		Path path = new Path();
////		if(car == 0) {
////		/*	System.out.println("carX = " + carX);
////			System.out.println("carY = " + carY);
////			System.out.println("hej");*/
////			//throw new NoPathAvailableException();
////			
////		} else {
//			int x = carX;
//			int y = carY;
//			System.out.println("creating path");
//			int i = 0;
//			while(i<1000 && !path.containsGoal()) {
//				i++;
//				lowestNeighbour = findLowestNeighbour(x, y);
//				if(prevNeig.equals(lowestNeighbour)){
//					return path;
//				}	
//				path.addPositionToPath(lowestNeighbour);
//				path.checkGoal(waveFrontMap[(int)lowestNeighbour.getX()][(int)lowestNeighbour.getY()]);
//
//				x = (int)lowestNeighbour.getX();
//				y = (int)lowestNeighbour.getY();
//			
//			}
//			System.out.println("path contains goal = " + path.containsGoal());
//			System.out.println("done creating path");
////		}
//	
//		return path;
//	}
//
//
//
//	private Position findLowestNeighbour(int x, int y) {
//		ArrayList<Position> lowpos = new ArrayList<Position>();
//	
//		//TODO om mer �n 2 samma ta mitten annars minsta
//		int move =4;
//		ArrayList<Position> neighs ;
//		int expandAmount = 1;
//		int lowestNeighbourHeat = waveFrontMap[x][y];
//		Position prevPos;
//		for (int i = (int) (x-expandAmount); i <= x+expandAmount ; i++) {
//			for (int j = (int) (y-expandAmount); j <= y+expandAmount; j++) {
//				if((x != i || y != j) && Utils.checkOutOfBounds(i, j, Robot.MAPSIZE, 0)){
//					if(waveFrontMap[i][j] == lowestNeighbourHeat && waveFrontMap[i][j] != 0) {
//						Position way = new Position(i, j);
////						if(waveFrontMap[i][j]>25) {
////							neighs =  Utils.getAllNeighbours(way, 5);	
////							way = padding(move, way,neighs);
////						}
//						lowpos.add(way);
//					
//						
//						lowestNeighbourHeat = waveFrontMap[i][j];
//					} else if((waveFrontMap[i][j] < lowestNeighbourHeat) && waveFrontMap[i][j] != 0) {
//						Position way = new Position(i, j);
//						lowpos = new ArrayList<Position>();
//						lowpos.add(way);
//					}
//				}
//			}
//		}
//		if(lowpos.size() != 0) {
//			return (lowpos.size()>= 2 ? lowpos.get(1) : lowpos.get(0) );
//		} else {
//			System.err.println("lowpos size was 0 for position " + x + "," + y);
//			System.err.println("with heatmap value = " + waveFrontMap[x][y]);
//			System.err.println("neighbours heatmap values are = " + waveFrontMap[x-1][y]
//					 			+" "+ waveFrontMap[x-1][y+1]
//					 			+" "+ waveFrontMap[x-1][y-1]
//					 			+" "+ waveFrontMap[x][y-1]
//					 			+" "+ waveFrontMap[x][y+1]
//								+" "+ waveFrontMap[x+1][y+1]
//								+" "+ waveFrontMap[x+1][y]
//							 	+" "+ waveFrontMap[x+1][y-1] 
//										);
//			System.err.println("heatmap = ");
//			printHeatMap();
//			return null;
//		}
//			//return lowpos.get(0);
//	}
//
//
//
//	private Position padding(int move, Position way, ArrayList<Position> neighs) {
//		
//
//		for(Position p : neighs){
//			if(internalMap[p.getIntX()][p.getIntY()] == Robot.EXPAND){
//				way = new Position((p.getIntX() < way.getIntX() ? way.getIntX()+move : way.getIntX()-move), 
//						(p.getIntY() < way.getIntY() ? way.getIntY()+move : way.getIntY()-move));
//				return way;
//			}
//		}
//		return way;
//	}
//
//
//
//	private void generateHeatMap(int xPos, int yPos, int carX, int carY) {
//	
//		Position pos = new Position(xPos, yPos);
//		Queue<Position> queue = new LinkedList<Position>();
//		ArrayList<Position> neighbours;
//		int heat = 2;
//		waveFrontMap[xPos][yPos]= 1;
//		queue.add(pos);
//		while(!queue.isEmpty()){
//			pos = queue.poll();	
//			
//			int x = (int)pos.getX();
//			int y = (int)pos.getY();
//			int min;
//			neighbours = getNeighbours(pos, Robot.WHITE);
//			for(Position neighbour : neighbours) {
//				min = Integer.MAX_VALUE/2;
//				ArrayList<Position> neighbours2 = Utils.getAllNeighbours(neighbour, 1);
//				
//				if(neighbours2.size()< 5){
//					System.out.println(neighbours2);
//				}
//				for(Position posi : neighbours2 ){
//					
//					//System.out.println(waveFrontMap[(int)posi.getX()][(int)posi.getY()] + " x " +(int)posi.getX() + " y " + (int)posi.getY());
//					
//					int val = waveFrontMap[(int)posi.getX()][(int)posi.getY()];	
//					if(val != 0 && val<min){
//						//TODO kan bli max value
//						min = val;
//						
//					}
//					if((int)posi.getX() ==  carX && (int)posi.getY() == carY ){
//					//if(Utils.distanceToPosition(posi, new Position(carX, carY)) < 20) {
//						waveFrontMap[(int)posi.getX()][(int)posi.getY()] = (min < 0 ? 0 : min) +2;
//						waveFrontMap[(int)neighbour.getX()][(int)neighbour.getY()] = (min < 0 ? 0 : min) +1;
//						//printHeatMap();
//						return;
//						
//					}
//				}
//				//System.out.println("------------------");
//				if(min == Integer.MAX_VALUE){
//					System.err.println("error");
//					printHeatMap();
//					System.exit(-1);
//				}
//				
//				waveFrontMap[(int)neighbour.getX()][(int)neighbour.getY()] = (min < 0 ? 0 : min) +1;
//				
//				
//				queue.add(neighbour);
//						
//			}
//			heat++;
//			
//		}
//		//System.out.println("heatmap:");
//		//printHeatMap();
//	}
//	
//	

//
//
//
//	private ArrayList<Position> getNeighbours(Position pos, int color) {
//		ArrayList<Position> neighbours = new ArrayList<Position>();
//		int expandAmount = 1;
//		for (int i = (int) (pos.getX()-expandAmount); i <= pos.getX()+expandAmount ; i++) {
//			for (int j = (int) (pos.getY()-expandAmount); j <= pos.getY()+expandAmount; j++) {
//				if(pos.getX() != i || pos.getY() != j){
//					if(Utils.checkOutOfBounds(i, j, Robot.MAPSIZE, 0)){
//						if((internalMap[i][j] == color)){
//							if(waveFrontMap[i][j] == 0) {
//								neighbours.add(new Position(i,j));
//							}
//						}
//					}
//				}
//			}
//		}
//		return neighbours;
//	}
//
//	
//
//
//
//	private void genHeat(int x, int y) {
//		
//	}
//	
//	
//	private void addPadding() {
//		
//	}

	
	
}
