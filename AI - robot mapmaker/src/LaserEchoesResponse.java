import java.util.ArrayList;
import java.util.Map;

/**
 * Returns a list of laser echoes. 
 * @author Thomas Johansson
 * 
 * Updated by Ola Ringdahl 2014-09-10
 * 
 */
public class LaserEchoesResponse implements Response
{
   private Map<String, Object> data;

   public void setData(Map<String, Object> data)
   {
      this.data = data;
   }

   public double[] getEchoes()
   {
      ArrayList echoes = (ArrayList)data.get("Echoes");      
      
      Object[] list = echoes.toArray();
      double[] result = new double[list.length];
      for (int i= 0 ; i < result.length; i++)
    	  if(list[i] instanceof Double)
    		  result[i] = (Double)list[i];    // unboxing
    	  else if(list[i] instanceof Integer) {
    		  //System.out.println("Integer.....");
    		  result[i] = ((Integer)list[i]).doubleValue();    // unboxing
    	  }
      
      
      return result;
   }

   public String getPath()
   {
      return "/lokarria/laser/echoes";
   }

   public long getTimestamp()
   {
      return (Long)data.get("TimeStamp");
   }

}
