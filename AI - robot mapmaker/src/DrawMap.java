//import java.util.ArrayList;
//
//public class DrawMap implements Runnable{
//
//	LocalizationResponse lr ;
//	LaserEchoesResponse ler ;
//	LaserPropertiesResponse lpr;
//	DifferentialDriveRequest dr;
//	RobotCommunication robotcomm;
//	private Position robotPosition;
//	public static final  int MAPSIZE = 500;
//	private int internalMap[][];
//	double[] angles;
//	
//	private ShowMap map;
//	
//	private double prevBearing;
//	private boolean prevBearingonce = true;
//	private boolean once = true;
//	
//	private  ArrayList<Position> explorePositions;
//	
//	public DrawMap(RobotCommunication comm, ArrayList<Position>  explorePositions,int internalMap[][]){
//		lr = new LocalizationResponse();
//		ler = new LaserEchoesResponse();
//		lpr = new LaserPropertiesResponse();
//		dr = new DifferentialDriveRequest();
//		this.explorePositions= explorePositions;
//		this.internalMap = internalMap;
//		dr.setAngularSpeed(Math.PI * 0.2);
//		dr.setLinearSpeed(0.3);
//	
//		this.robotcomm = comm;
//		
//		for(int j = 0 ; j < MAPSIZE ;j++){
//			for(int k= 0 ; k < MAPSIZE ;k++){
//				
//				 internalMap[j][k] = Robot.GREY;
//			
//				}	
//				
//		}
//	}
//	
//	@Override
//	public void run() {
//		int rc;
//		try {
//			rc = robotcomm.putRequest(dr);
//		
//			System.out.println("Response code " + rc);
//			robotcomm.getResponse(lpr);
//			angles = getLaserAngles(lpr);
//			
//			} catch (Exception e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//			while(true){
//				try {
//					robotcomm.getResponse(lr);
//				
//				
//				try {
//					Thread.sleep(10);
//				} catch (InterruptedException ex) {
//				}
//		
//				if(prevBearingonce){
//					prevBearing = getBearingAngle(lr);
//					prevBearingonce = false;
//					readAndCreateMap(lr, ler, angles);
//					
//				}else{
//					if(Math.abs(getBearingAngle(lr) - prevBearing) < 0.01 ){
//						readAndCreateMap(lr, ler, angles);
//					} else{
//						Thread.sleep(100);
//					}
//						prevBearing = getBearingAngle(lr);
//					
//					
//				}
//				expandWalls();
//				
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//				System.out.println("end of run");
//		}
//	}
//	
//	double getBearingAngle(LocalizationResponse lr) {
//
//        return lr.getHeadingAngle();
//    }
//
//	double[] getLaserAngles(LaserPropertiesResponse lpr) {
//		int beamCount = (int) ((lpr.getEndAngle() - lpr.getStartAngle()) / lpr
//				.getAngleIncrement()) + 1;
//		double[] angles = new double[beamCount];
//		double a = lpr.getStartAngle();
//		for (int i = 0; i < beamCount; i++) {
//			angles[i] = a;
//			// We get roundoff errors if we use AngleIncrement. Use 1 degree in
//			// radians instead
//			a += 1 * Math.PI / 180;// lpr.getAngleIncrement();
//		}
//		return angles;
//	}
//	
//	private void readAndCreateMap(LocalizationResponse lr,
//			LaserEchoesResponse ler, double[] angles) throws Exception {
//		double addwhite = 0.1;
//		 getRobotPosition();
//		 
//
//		// Ask the robot for laser echoes
//		robotcomm.getResponse(ler);
//		double[] echoes = ler.getEchoes();
//		for(int j = 45 ; j < 45+180; j+=1 ){
//
//		Double x = Utils.coordinateOfObject(getBearingAngle(lr), angles[j], robotPosition, echoes[j]).getX();
//		Double y = Utils.coordinateOfObject(getBearingAngle(lr), angles[j], robotPosition, echoes[j]).getY();
//		
//			if(echoes[j] < 40 && Utils.checkOutOfBounds(x, y, Robot.mapEndPos, Robot.mapStartPos) ){
//				internalMap[Utils.convertToMapCoordinates(x)][Utils.convertToMapCoordinates(y)] = Robot.BLACK;
//					
//				
//			}if( Utils.checkOutOfBounds(x, y, Robot.mapEndPos, Robot.mapStartPos)){
//				drawToObject(lr, angles, addwhite, echoes, j);	
//			}
//			
//			if( !Utils.checkOutOfBounds(x, y, Robot.mapEndPos, Robot.mapStartPos) ){
//				drawToEdge(lr, angles, addwhite, j);
//					
//			}
//		
//		}
//		findExplorablePoints();		
//		createMap(internalMap);
//	}
//	
//	 /**
//		 * A simple example of how to use the ShowMap class that creates a map from
//		 * your grid, updates it and saves it to file
//		 */
//		private void createMap(int[][] internalMap) {
//			int nRows = MAPSIZE;
//			int nCols = MAPSIZE;
//			/* use the same no. of rows and cols in map and grid */
//			if(once){
//				makemap();
//				once = false;
//			}
//			
//			/* Creating a grid with 0.5 */
//			int[][] grid = new int[nRows][nCols];
//			for (int i = 0; i < nRows; i++) {
//				for (int j = 0; j < nCols; j++) {
//					grid[i][j] = 7;
//				}
//			}
//			
//			int robotRow = Utils.convertToMapCoordinates(robotPosition.getY());
//			int robotCol = Utils.convertToMapCoordinates(robotPosition.getX());
//			/* create some obstacles (black/grey)*/
//			// Upper left side:
//			for(int j = 0 ; j < MAPSIZE ;j++){
//				for(int k= 0 ; k < MAPSIZE ;k++){
//					
//					 grid[j][k] = internalMap[j][k];
//				
//					}	
//					
//			}
//			
//			for(Position pos : explorePositions){
//				
//				//grid[(int)pos.getX()][(int)pos.getY()] = 10;
//			}
//			
//			
//
//			int maxVal = 15;
//			// Update the grid
//			map.updateMap(grid, maxVal, robotRow, robotCol);
//		}
//	    private void makemap(){
//	    	int nRows = MAPSIZE;
//			int nCols = MAPSIZE;
//			boolean showGUI = true; // set this to false if you run in putty
//			map = new ShowMap(nRows, nCols, showGUI);
//	    }
//	private void findExplorablePoints() {
//		explorePositions = new ArrayList<Position>();
//		
//		for (int i = 0; i < MAPSIZE ; i++) {
//			for (int j = 0; j < MAPSIZE; j++) {
//				if(internalMap[i][j] == Robot.WHITE){
//					if(existsUnexplored(i,j,Robot.GREY) && !existsUnexplored(i,j,Robot.BLACK)
//							&& i != 0 && i!=MAPSIZE && j != 0 && j != MAPSIZE){
//						explorePositions.add(new Position(i,j));
//					}
//				}
//			}
//		}		
//	}
//
//	private void drawToEdge(LocalizationResponse lr, double[] angles, double addWhite, int j) {
//		double distance;
//		int mcordwx,mcordwy;
//		distance = 0;
//		double wx = Utils.coordinateOfObject(getBearingAngle(lr), angles[j], robotPosition, distance).getX();
//		double wy = Utils.coordinateOfObject(getBearingAngle(lr), angles[j], robotPosition, distance).getY();
//		
//		mcordwx = Utils.convertToMapCoordinates(wx);
//		mcordwy = Utils.convertToMapCoordinates(wy);
//		
//		while((wx < Robot.mapEndPos && wx > Robot.mapStartPos) && (wy < Robot.mapEndPos && wy > Robot.mapStartPos) 
//				&& Utils.checkOutOfBounds(mcordwx, mcordwy, MAPSIZE, 0)){
//				/*&& (Utils.convertToMapCoordinates(wx) > 0 && Utils.convertToMapCoordinates(wx) < MAPSIZE)  &&
//				(Utils.convertToMapCoordinates(wy) > 0 && Utils.convertToMapCoordinates(wy) < MAPSIZE)) {*/
//			
//			distance +=addWhite;
//			if(internalMap[mcordwx][mcordwy] != Robot.BLACK && internalMap[mcordwx][mcordwy] != Robot.WHITE && internalMap[mcordwx][mcordwy] != Robot.EXPAND){
//					internalMap[mcordwx][mcordwy] = Robot.WHITE;
//				
//			}
//			wx = Utils.coordinateOfObject(getBearingAngle(lr), angles[j], robotPosition, distance).getX();
//			wy = Utils.coordinateOfObject(getBearingAngle(lr), angles[j], robotPosition, distance).getY();
//			
//			mcordwx = Utils.convertToMapCoordinates(wx);
//			mcordwy = Utils.convertToMapCoordinates(wy);
//				
//		
//		}
//	}
//
//	private void drawToObject(LocalizationResponse lr, double[] angles, double addwhite, double[] echoes, int j) {
//		double distance;
//		distance = echoes[j]-addwhite;
//		double wx,wy;
//		int mcordwx,mcordwy;
//			
//		while(distance > 0){
//			wx = Utils.coordinateOfObject(getBearingAngle(lr), angles[j], robotPosition, distance).getX();
//			wy = Utils.coordinateOfObject(getBearingAngle(lr), angles[j], robotPosition, distance).getY();
//			mcordwx = Utils.convertToMapCoordinates(wx);
//			mcordwy = Utils.convertToMapCoordinates(wy);
//			distance -=addwhite;
//			if(internalMap[mcordwx][mcordwy] != Robot.BLACK && internalMap[mcordwx][mcordwy] != Robot.WHITE && internalMap[mcordwx][mcordwy] != Robot.EXPAND){
//			
//					internalMap[mcordwx][mcordwy] = Robot.WHITE;
//				
//			}
//				
//			
//		}
//	}
//	
//	private void expandWalls(){
//		//TODO place somewhere else?
//		int expandamount = 4;
//		for (int i = 0; i < MAPSIZE ; i++) {
//			for (int j = 0; j < MAPSIZE; j++) {
//				
//				if(internalMap[i][j] == Robot.BLACK){
//					for (int k = i-expandamount; k <= i+expandamount ; k++) {
//						for (int l = j-expandamount; l <= j+expandamount; l++) {
//							if(Utils.checkOutOfBounds(k, l, MAPSIZE, 0)){
//								if(internalMap[k][l] == Robot.WHITE){
//									
//										internalMap[k][l] = Robot.EXPAND;
//									
//								}
//							}
//						}
//					}
//				}
//			}
//		}	
//	}
//	 private Position getRobotPosition() throws Exception {
//	        robotcomm.getResponse(lr);
//	        double[] coordinates = lr.getPosition();
//	        robotPosition = new Position(coordinates[0], coordinates[1]);
//	        return robotPosition;
//	    }
//	 
//	 private boolean existsUnexplored(int x, int y, int color){
//
//			for (int i = x-1; i <= x+1 ; i++) {
//				for (int j = y-1; j <= y+1; j++) {
//					if((i > 0 && i < MAPSIZE ) && (j > 0 && j < MAPSIZE )  ){
//						if(internalMap[i][j] == color){
//							return true;
//						}	
//					}	
//				}
//			}
//			return false;
//		
//		}
//
//}
