import java.util.ArrayList;

public class Path {
	public ArrayList<Position> path;
	private int index;
	private Position endPos;
	private boolean containsGoal = false;
	public Path() {
		path = new ArrayList<Position>();
		index = 0;
	}
	
	public Position getNextPosition() {
		index++;
		if(index < path.size()) {
			return path.get(index);			
		}
		return null;
	}
	
	public ArrayList<Position> getPath() {
		return path;
	}
	
	public ArrayList<Position> getMapPathCoordinates(){
		ArrayList<Position> mapPath = new ArrayList<Position>();
		for(Position pos : path){
			Utils.checkIfExistsElseAdd(mapPath, pos);
		}
		
		return mapPath;
	}
	
	public void addPositionToPath(Position position) {
		Utils.checkIfExistsElseAdd(path, position);
		//path.add(position);
	}
	
	public boolean hasEnded() {
		if(index == path.size()) {
			return true;
		}
		return false;
	}
	

	public boolean containsGoal() {
		return containsGoal;
	}

	public void checkGoal(int g) {
		if(g < 50) {
			containsGoal = true;
		}
		
	}
	
	public int size(){
		return path.size();
	}

}
