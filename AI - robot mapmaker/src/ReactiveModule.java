
public class ReactiveModule {
	
	private RobotCommunication robotcom;
	
	public ReactiveModule(RobotCommunication robotcom){
		this.robotcom = robotcom;
	}
	
	/**
	 * Find the shortest distance to an obstacle 
	 * @return Shortest distance to obstacle
	 * @throws Exception
	 */
	public double nearestObstacle()  {
		double min = 20;
		double dist;
		
		LaserEchoesResponse ls = new LaserEchoesResponse();
		try {
			robotcom.getResponse(ls);
		} catch (Exception e) {
			System.err.println("Could not talk to robot");
			e.printStackTrace();
		}
		double[] echoes = ls.getEchoes();

		//int frontCone = 54;
		for (int i = 0; i < echoes.length ; i++) {
			dist = echoes[i];
			if (dist < min) {
				min = dist;
			}
		}

		return min;
	}
	
	/**
	 * Returns a carrot position depending on the distance to objects
	 * @param safe
	 * @param obstacleDistance
	 * @param positions
	 * @param currentPosition
	 * @param robotPosition
	 * @return
	 */
	public int getDynamicCarrot(boolean safe, double obstacleDistance,Position[] positions, int currentPosition, Position robotPosition) {
		
		if(safe){
			while(currentPosition != positions.length - 1  && robotPosition.getDistanceTo(positions[currentPosition + 1]) < nearestObstacle()/4){//&& robotPosition.getDistanceTo(positions[currentPosition + 1]) < obstacleDistance) {	
				currentPosition++;
			}
			
		} else{
			while(currentPosition != positions.length - 1  && robotPosition.getDistanceTo(positions[currentPosition + 1]) < nearestObstacle()/2){//&& robotPosition.getDistanceTo(positions[currentPosition + 1]) < obstacleDistance) {	
				currentPosition++;
			}
		}
		return currentPosition;
	}
}
