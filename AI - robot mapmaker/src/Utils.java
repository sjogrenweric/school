import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;



public class Utils {
	
	private int x1,x2,y1,y2;
	private static double  kx,ky,mx,my; 

    /**
     * Convert global coordinates into local coordinates, given x, y and bearing.
     * @param x coordinate
     * @param y coordinate
     * @param bearings
     * @return
     */
    public static double[] localCoordinates(double x, double y, double bearings){
        double bearing = Math.toRadians(bearings);
        double localx = x * Math.cos(bearing) + y * Math.sin(bearing);
        double localy = (-1) * x * Math.sin(bearing) + y * Math.cos(bearing);
        //System.out.println("localy = " + localy);
        //System.out.println("localx = " + localx);
        double[] res = {localx,localy};
        return res;

    }
    
    /**
     * Finds the coordinate of an object found by the laser
     * @param bearing
     * @param angles
     * @param carPos
     * @param distanceToObject
     * @return
     */
    public static Position coordinateOfObject(double bearing, double angles, Position carPos, double distanceToObject) {
    	double angleToObject = angles;
    	
    	double xPrime = (distanceToObject-0.15) * Math.cos(angleToObject);
    	double yPrime = (distanceToObject-0.15) * Math.sin(angleToObject);  	
    	
    	double x = carPos.getX() + xPrime * Math.cos(bearing) - yPrime * Math.sin(bearing);
    	double y = carPos.getY() +xPrime*Math.sin(bearing) + yPrime*Math.cos(bearing);

    	return new Position(x,y);
    }
    
    /**
     * calculates the functions that will be used for the conversions between global and local coordinates
     * @param x1
     * @param x2
     * @param y1
     * @param y2
     * @param mapSizeX
     * @param mapSizeY
     */
    public static void calcfunction(double x1, double x2 , double y1, double y2,double mapSizeX, double mapSizeY){
 
    	kx = mapSizeX/(x2-x1);
    	mx = -kx*x1;
    	System.out.println("mapSizeX = " + mapSizeX);
    	System.out.println("x1 x2" + x1 + x2);
    	System.out.println("kx = " + kx);
    	System.out.println("mx = " + mx);
    	ky = mapSizeY/(y2-y1);
    	my = -ky *y1;
    	
    }
    
    
    /**
     * converts global coordinates to local coordinates
     * @param glob
     * @param size
     * @param x
     * @return
     */
    public static int convertToMapCoordinates(Double glob,int size, boolean x) {
		 Double returnval;
		 
		 if(x){
			 returnval = (kx*glob)+mx; 
		 }else{
			 returnval = (ky*glob)+my; 
		 }
		return returnval.intValue()-1;
	}
    
    /**
     * Converts a global position to a local one
     * @param pos
     * @param sizex
     * @param sizey
     * @return
     */
    public static Position convertToMapCoordinates(Position pos,int sizex,int sizey){
    	return new Position(convertToMapCoordinates(pos.getX(),sizex,true),convertToMapCoordinates(pos.getY(),sizey,false));
    }
    
   
    /**
     * Checks out of bounds in the local map
     * @param x
     * @param y
     * @param maxX
     * @param maxY
     * @param min
     * @return
     */
    public static boolean checkOutOfBoundsInternal(double x, double y, int maxX,int maxY, int min){
    	
    	if((x >= maxX || x < min) || (y >= maxY || y < min)){
    		return false;
    	}	
    	
    	return true;	
    }
    
    /**
     * checks out of bounds in the global map
     * @param x
     * @param y
     * @param startX
     * @param endX
     * @param startY
     * @param endY
     * @return
     */
    public static boolean checkOutOfBoundsGlobal(double x, double y, double startX,double endX, double startY,double endY ){
    	if((x >= endX || x < startX) || (y >= endY || y < startY)){
    		return false;
    	}
    	return true;
    	
    }
    
    /**
     * Converts a local point to a global coordinate
     * @param ox
     * @param oy
     * @param sizeX
     * @param sizeY
     * @return
     */
    public static Position internalPositionToGPosition(int ox, int oy, int sizeX, int sizeY) {
    	double gx = (ox-mx)/kx;
    	double gy = (oy-my)/ky;
    	return new Position(gx,gy);
    }
    
    /**
     * Returns a new random waypoint from a list of waypoints
     * @param waypoints
     * @param robotPos
     * @return
     */
    public static Position findClosestWayPoint(ArrayList<Position> waypoints, Position robotPos){
    	
    	double dist;
    	double min = Integer.MAX_VALUE;
    	
    	Position minWaypoint = null;
    	
    	Random r = new Random();
    	
    	System.out.println("waypoints size = " + waypoints.size());
    	
    	int rand = r.nextInt(waypoints.size());
    	
    	return waypoints.get(rand);
    }
    
    /**
     * Returns the distance between 2 positions
     * @param p1
     * @param p2
     * @return
     */
    public static double distanceToPosition(Position p1, Position p2){
    	 return Math.sqrt(Math.pow((Math.abs(p1.getX() - p2.getX())),2) + Math.pow((Math.abs(p1.getY() - p2.getY())),2));
    }

    /**
     * Returns all the neighbours of a position
     * @param pos
     * @param expandAmount
     * @return
     */
    public static ArrayList<Position> getAllNeighbours(Position pos, int expandAmount) {
		ArrayList<Position> neighbours = new ArrayList<Position>();
		for (int i = (int) (pos.getX()-expandAmount); i <= pos.getX()+expandAmount ; i++) {
			for (int j = (int) (pos.getY()-expandAmount); j <= pos.getY()+expandAmount; j++) {
				
				if(pos.getX() != i || pos.getY() != j){
					if(Utils.checkOutOfBoundsInternal(i, j, Cartographer.MAPSIZEX,Cartographer.MAPSIZEY, 0)){
						neighbours.add(new Position(i,j));
					}
				}
				
			}
		}
		return neighbours;
	}
    
    /**
     * Adds a position to a list of positions if it does not exists in the list
     * @param positions
     * @param pos
     */
    public static void checkIfExistsElseAdd(ArrayList<Position> positions, Position pos) {
		boolean exists=false;
		for(Position posi : positions){
			if(posi.equals(pos)){
				exists=true;
			}
		}
		if(!exists){
			//Collections.sort(movablePositions);
			positions.add(pos);
		}
	}
    
    /**
     * Adds a position to a queue if it does not exist in the queue
     * @param positions
     * @param pos
     */
    public static void checkIfExistsElseAdd(Queue<Position> positions, Position pos) {
		boolean exists=false;
		for(Position posi : positions){
			if(posi.equals(pos)){
				exists=true;
			}
		}
		if(!exists){
			//Collections.sort(movablePositions);
			positions.add(pos);
		}
	}
    
    
    
}