
public class MapMaker {
	
	static Robot robot;
	//private static String url = "http://94.245.51.231";
	private static int port = 50000;

	public static void main(String[] args) {
		if(args.length==5){
			robot = new Robot(args[0], port);
			try {
				robot.init(args);
				robot.run();
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		} else{
			System.out.println("Not enough arguments, usage : <url> <x1> <y1> <x2> <y2>");
		}
	}

}
