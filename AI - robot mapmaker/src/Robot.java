import java.awt.Color;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.ser.std.SerializableSerializer;

public class Robot {	
	private static final boolean xbool = true;
	private static final boolean ybool = false;
	private boolean prevBearingonce = true;
	
	//public static int MAPSIZEX;
	//public static int MAPSIZEY;
	public static  int mapStartPos; //first argument should be less than mapEndPos 
	public static int mapEndPos; //second argument should be more mapStartPos

	public static boolean running = false;
	


	
	private double prevBearing;
	
	private static RobotCommunication robotcomm; // communication drivers
	private PathFinder  pathfinder;
	private Cartographer cartographer;
	private Driver driver;
	
	public static LocalizationResponse lr ;
	public static LaserEchoesResponse ler;
	public static LaserPropertiesResponse lpr;
	//private static DifferentialDriveRequest ddr = new DifferentialDriveRequest();
	
	private static Position robotPosition;
	private Position startPosition;
	
	ArrayList<Position> pl = null;

	
	public Robot(String host, int port) {
		robotcomm = new RobotCommunication(host, port);
		
			
		//	pathfinder = new PathFinder(internalMap);
		
		
	}

	/**
	 * This simple main program creates a robot, sets up some speed and turning
	 * rate and then displays angle and position for 16 seconds.
	 * 
	 * @param args
	 *            not used
	 * @throws Exception
	 *             not caught
	 */
	public void init(String[] args) throws Exception {	
		//do cartographer shit
		//init cartographer
		lr = new LocalizationResponse();
		cartographer = new Cartographer(robotcomm);
		cartographer.init(args);
		pathfinder = new PathFinder(cartographer.getInternalMap());

		System.out.println("Creating Robot");
	}
	
	public void run(){
		ArrayList<Position> pathToStart;
		try {
			go();
		
			cartographer.cleanUpAndSave();
		} catch (Exception e) {
			
				cartographer.cleanUpAndSave();
				goHome();
			}
		
	}


	
	public void go() throws Exception  {
		
	
		System.out.println("Creating response");

		ler = new LaserEchoesResponse();
		lpr = new LaserPropertiesResponse();


		System.out.println("Creating request");
		DifferentialDriveRequest dr = new DifferentialDriveRequest();
		// set up the request to move in a circle
		dr.setAngularSpeed(Math.PI * 0.2);
		dr.setLinearSpeed(0.3);

		System.out.println("Start to move robot");
		int rc = robotcomm.putRequest(dr);
		System.out.println("Response code " + rc);
		robotcomm.getResponse(lpr);
		double[] angles = getLaserAngles(lpr);
		driver = new Driver(robotcomm,running);
		
		
		
	
	/*	Thread t = new Thread(new DrawMap(robotcomm,explorePositions,internalMap));
		System.out.println("hej2");
		t.start();
		System.out.println("thread initialized");*/
		int delay = 0 ;
		while(true) {
				
				robotcomm.getResponse(lr);
				
				try {
					Thread.sleep(10);
				} catch (InterruptedException ex) {
				}

				if(prevBearingonce){
					prevBearing = getBearingAngle();
					prevBearingonce = false;			
					cartographer.readAndCreateMap(lr, ler, angles);
					
				}else{
					if(Math.abs(getBearingAngle() - prevBearing) < 0.01 ){
						cartographer.readAndCreateMap(lr, ler, angles);
					} else{
					//	System.out.println("thread sleeping");
						Thread.sleep(10);
					}
						prevBearing = getBearingAngle();
					
					
				}
			//	cartographer.cleanupWalls();			
				cartographer.findExplorablePoints();
				
				if(pl != null) {
					//cartographer.drawPath(pl);

				}
				 getRobotPosition();
			//TODO function check if path can be found instead
				 if(delay > 10){
					 if(!driver.isRunning() ){
						 System.out.println("finding a new path..");
						
						 ArrayList<Position> path = findWayPointAndPath();
						 System.out.println("path found");
						
						 pl = path;
						 if(path.size()<10){
							 throw new NoPathAvailableException();
						 }
						 System.out.println("starting new driver..");
						 driver = new Driver(robotcomm,running);
						 driver.init(path);
						 Thread t = new Thread(driver);
						 t.start();
					 }
				 }else{
					 delay++;
				 }
				 
			
		}
			
		

			
	}
	
	private void goHome() {
		ArrayList<Position> pathToStart;
	//	System.out.println("SHOULD GO HOME NOW");
		try {
			robotcomm.getResponse(lr);
			robotPosition = getRobotPosition();
			robotPosition = new Position(Utils.convertToMapCoordinates(robotPosition.getX(),Cartographer.MAPSIZEX,xbool),
					Utils.convertToMapCoordinates(robotPosition.getY(), Cartographer.MAPSIZEY,ybool));
			System.out.println("robot position " + robotPosition);
			pathToStart = pathfinder.getPathToWayPoint(startPosition,robotPosition);
			 new Driver(robotcomm,running).init(pathToStart);
			 Thread t = new Thread(driver);
			 System.out.println("starting new driver that goes home..");
			 System.out.println("startposition " + startPosition.toString());
			 System.out.println("path to start = " + pathToStart.toString());
			 t.start();
			 while(t.isAlive()) {
				 Thread.sleep(100);
			 } 
			 System.out.println("Program finished...");
			 System.exit(1);
		} catch (Exception e2) {
			//System.out.println("Could not find path back to start position");
			System.out.println("Shutting down program");
			System.exit(-1);
		}
	}

	

	private ArrayList<Position> findWayPointAndPath() throws Exception {
		pathfinder = new PathFinder(cartographer.getInternalMap());
		robotPosition = getRobotPosition();
		robotPosition = new Position(Utils.convertToMapCoordinates(robotPosition.getX(), Cartographer.MAPSIZEX,xbool)
				,Utils.convertToMapCoordinates(robotPosition.getY(), Cartographer.MAPSIZEY,ybool));
		//System.out.println("before the if");

		
		
		
		
		ArrayList<Position> path;
		ArrayList<Position> waypoints = cartographer.findExplorationsWayPoints();
		System.out.println("after cartographer exploration way points");
		Position closestWayPoint = Utils.findClosestWayPoint(waypoints, robotPosition);
		//System.err.println(closestWayPoint);
		
		closestWayPoint = findClosestWaypoint(pathfinder, waypoints, closestWayPoint);
		
		path = pathfinder.getPathToWayPoint(closestWayPoint, robotPosition);
		//System.out.println("after");
		
	
		
		return path;
	}

	private Position findClosestWaypoint(PathFinder pf, ArrayList<Position> waypoints, Position closestWayPoint)
			throws InterruptedException {

			while(pf.getPathToWayPoint(closestWayPoint,robotPosition) == null || pf.getPathToWayPoint(closestWayPoint,robotPosition).size() == 0){
				//System.err.println("closest waypoint = " + closestWayPoint.toString());
				waypoints.remove(closestWayPoint);
				closestWayPoint = Utils.findClosestWayPoint(waypoints, robotPosition);
			}
			
			
	
		return closestWayPoint;
	}



	public static double getBearingAngle() {		
		return lr.getHeadingAngle();
	}
	
	/**
	 * Get corresponding angles to each laser beam
	 * 
	 * @param lpr
	 * @return laser angles in radians
	 */
	double[] getLaserAngles(LaserPropertiesResponse lpr) {
		int beamCount = (int) ((lpr.getEndAngle() - lpr.getStartAngle()) / lpr
				.getAngleIncrement()) + 1;
		double[] angles = new double[beamCount];
		double a = lpr.getStartAngle();
		for (int i = 0; i < beamCount; i++) {
			angles[i] = a;
			// We get roundoff errors if we use AngleIncrement. Use 1 degree in
			// radians instead
			a += 1 * Math.PI / 180;// lpr.getAngleIncrement();
		}
		return angles;
	}
	
	
	
	
	/**
     * Request the current position of the robot in the global coordinate
     * system.
     *
     * @throws Exception
     */
    public static Position getRobotPosition() throws Exception {
        robotcomm.getResponse(lr);
        double[] coordinates = lr.getPosition();
        robotPosition = new Position(coordinates[0], coordinates[1]);
        return robotPosition;
    }
	

    



	


	/*
	private void expandWalls(){
		//TODO place somewhere else?
		int expandAmount = 12;
		for (int i = 0; i < MAPSIZE ; i++) {
			for (int j = 0; j < MAPSIZE; j++) {
				
				if(internalMap[i][j] == BLACK){
					for (int k = i-expandAmount; k <= i+expandAmount ; k++) {
						for (int l = j-expandAmount; l <= j+expandAmount; l++) {
							if(Utils.checkOutOfBounds(k, l, MAPSIZE, 0)){
								if(internalMap[k][l] == WHITE){
									
										internalMap[k][l] = EXPAND;
									
								}
							}
						}
					}
				}
			}
		}	
	}
	*/

	


	
	
	
	

}
